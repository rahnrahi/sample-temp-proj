export const lineBreak = string => string.replace(/\n/gi, '\n')

export const delay = time => new Promise(res => setTimeout(res, time))

export const isValidDate = date =>
  Boolean(
    Object.prototype.toString.call(date) === '[object Date]' &&
      !isNaN(date.getTime())
  )

export const isFunction = fn => typeof fn === 'function'

export const hexToRgba = (hex, opacity) => {
  const result = hex
    .replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => '#' + r + r + g + g + b + b
    )
    .substring(1)
    .match(/.{2}/g)
    .map(x => parseInt(x, 16))

  return { red: result[0], green: result[1], blue: result[2], opacity }
}

export const initials = name => {
  if (name) {
    //splits words to array
    const nameArray = name.split(' ')

    let initials = ''

    //if it's a single word, return 1st and 2nd character
    if (nameArray.length === 1) {
      return nameArray[0].charAt(0) + '' + nameArray[0].charAt(1)
    } else {
      initials = nameArray[0].charAt(0)
    }
    //else it's more than one, concat the initials in a loop
    //we've gotten the first word, get the initial of the last word

    //first word
    for (let i = nameArray.length - 1; i < nameArray.length; i++) {
      initials += nameArray[i].charAt(0)
    }
    //return capitalized initials
    return initials.toUpperCase()
  }
}

export const getFlyoutPlacements = () => {
  return [
    'topLeft',
    'topCenter',
    'topRight',
    'rightTop',
    'rightCenter',
    'rightBottom',
    'bottomLeft',
    'bottomCenter',
    'bottomRight',
    'leftTop',
    'leftCenter',
    'leftBottom',
  ]
}
