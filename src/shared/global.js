import { createGlobalStyle, css } from 'styled-components'
export const globalFonts = css`
  @font-face {
    font-family: Gilroy-Regular;
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Regular.woff')
      format('woff');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Regular.woff2')
      format('woff2');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Regular.otf')
      format('opentype');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: Gilroy-Medium;
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Medium.woff')
      format('woff');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Medium.woff2')
      format('woff2');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Medium.otf')
      format('opentype');
    font-style: normal;
  }

  @font-face {
    font-family: Gilroy-SemiBold;
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-SemiBold.woff')
      format('woff');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-SemiBold.woff2')
      format('woff2');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-SemiBold.otf')
      format('opentype');
    font-style: normal;
  }

  @font-face {
    font-family: Gilroy-Bold;
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Bold.woff')
      format('woff');
    src: url('https://d1zluv99gyzypt.cloudfront.net/assets/fonts/Gilroy/Gilroy-Bold.woff2')
      format('woff2');
    font-weight: bold;
    font-style: normal;
  }

  @font-face {
    font-family: IBM plex sans;
    src: url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@600&display=swap')
      format('opentype');
    font-weight: bold;
    font-style: normal;
  }
  * {
    font-family: Gilroy-Medium;
  }
`
export const GlobalStyle = createGlobalStyle`
 ${globalFonts}`
