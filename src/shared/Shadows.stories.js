import React from 'react'
import { Shadows as Shadow } from '../atoms/shadow/Shadows'
import {
  SHADOW_DESIGN,
  SHADOW_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../hooks/constants'

export default {
  title: 'Foundation/Shadows',
  component: Shadow,
}

const Template = args => <Shadow {...args} />

export const Shadows = Template.bind({})

Shadows.args = {}
Shadows.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHADOW_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHADOW_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
