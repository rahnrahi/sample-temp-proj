export const LTR = 'ltr'
export const RTL = 'rtl'
export const RTL_LANGUAGES = [
  'ar',
  'arc',
  'dv',
  'fa',
  'ha',
  'he',
  'khw',
  'ks',
  'ku',
  'ps',
  'ur',
  'yi',
]

export const DESIGN_TAB_MOCKUP = 'Design'

export const DESIGN_TAB_PRESENTATION = 'Presentation'

export const PRIMARY_BTN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10244%3A42011'

export const PRIMARY_BTN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10244%3A42011&scaling=min-zoom&page-id=4457%3A18298'

export const SECONDARY_BTN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10244%3A42673'

export const SECONDARY_BTN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10244%3A42673&scaling=min-zoom&page-id=4457%3A18298'

export const QUATERNARY_BTN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11393%3A48001'

export const QUATERNARY_BTN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11393%3A48001&scaling=min-zoom&page-id=4457%3A18298'

export const TERTIARY_BTN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10250%3A45028'

export const TERTIARY_BTN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10250%3A45028&scaling=min-zoom&page-id=4457%3A18298'

export const ICON_BTN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A69394'

export const ICON_BTN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A69394&scaling=min-zoom&page-id=4457%3A18298'

export const CHECKBOX_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A39199'

export const CHECKBOX_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A39199&scaling=min-zoom&page-id=8867%3A39125'

export const RADIO_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10393%3A47288'

export const RADIO_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10393%3A47288&scaling=min-zoom&page-id=8867%3A39125'

export const SWITCH_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10393%3A47455'

export const SWITCH_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10393%3A47455&scaling=min-zoom&page-id=8867%3A39125'

export const INPUT_FIELD_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=7911%3A35909'

export const INPUT_FIELD_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=7911%3A35909&scaling=min-zoom&page-id=7015%3A31447'

export const INPUT_SMALL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10453%3A47337'

export const INPUT_SMALL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10453%3A47337&scaling=min-zoom&page-id=7015%3A31447'

export const INPUT_SAVE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11406%3A48817'

export const INPUT_SAVE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11406%3A48817&scaling=min-zoom&page-id=7015%3A31447'

export const INPUT_TEXTAREA_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10455%3A46283'

export const INPUT_TEXTAREA_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10455%3A46283&scaling=min-zoom&page-id=7015%3A31447'

export const INPUT_CHIP_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70098'

export const INPUT_CHIP_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70098&scaling=min-zoom&page-id=4229%3A12693'
export const CALENDAR_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A39426'

export const CALENDAR_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A39426&scaling=min-zoom&page-id=3975%3A11591'

export const TIMEPICKER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A40095'

export const TIMEPICKER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A40095&scaling=min-zoom&page-id=3975%3A11591'

export const HTMLEDITOR_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10382%3A45187'

export const HTMLEDITOR_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10382%3A45187&scaling=min-zoom&page-id=10382%3A45187'

export const POPOVER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10397%3A46286'

export const POPOVER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10397%3A46286&scaling=min-zoom&page-id=10397%3A46226'

export const TABS_HORIZONTAL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A38193'

export const TABS_HORIZONTAL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A38193&scaling=min-zoom&page-id=4265%3A13539'

export const TABS_VERTICAL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A77398'

export const TABS_VERTICAL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A77398&scaling=min-zoom&page-id=4265%3A13539'

export const TABS_STEPPER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4265%3A13539'

export const TABS_STEPPER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4265%3A13539&scaling=min-zoom&page-id=4265%3A13539'

export const NAVIGATION_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A76995'

export const NAVIGATION_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A76995&scaling=min-zoom&page-id=4265%3A13539'

export const BREADCRUMB_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10164%3A41830'

export const BREADCRUMB_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10164%3A41830&scaling=min-zoom&page-id=10164%3A41827'

export const PAGINATION_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10476%3A49825'

export const PAGINATION_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10476%3A49825&scaling=min-zoom&page-id=10466%3A56262'

export const TOASTR_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10446%3A51841'

export const TOASTR_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10446%3A51841&scaling=min-zoom&page-id=4401%3A0'
export const CARD_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4187%3A0'

export const CARD_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4187%3A0&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const EXPANDABLE_CARD_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41065'

export const EXPANDABLE_CARD_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41065&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const EXPANDABLE_CARD_IMAGE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41065'

export const EXPANDABLE_CARD_IMAGE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41065&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const CARD_ACTION_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46679'

export const CARD_ACTION_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46679&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const CARD_STANDARD_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A47378'

export const CARD_STANDARD_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A47378&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const CARD_HIERARCHY_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46101'

export const CARD_HIERARCHY_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46101&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const CARD_FOLDER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A47191'

export const CARD_FOLDER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A47191&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const CARD_ACCORDIAN_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10341%3A43067'

export const CARD_ACCORDIAN_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10341%3A43067&scaling=min-zoom&page-id=4187%3A0&starting-point-node-id=8848%3A41065'

export const SEARCH_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4146%3A13760'

export const SEARCH_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=4146%3A13760&scaling=min-zoom&page-id=4146%3A13725'

export const LOADING_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10440%3A45775'

export const LOADING_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10440%3A45775&scaling=min-zoom&page-id=10440%3A45374&starting-point-node-id=10440%3A45775'

export const SHIMMER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10440%3A45775'

export const SHIMMER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10440%3A45775&scaling=min-zoom&page-id=10440%3A45374&starting-point-node-id=10440%3A45775'

export const PROGRESS_TRACKER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A88526'

export const PROGRESS_TRACKER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10636%3A88526&scaling=min-zoom&page-id=8855%3A37791'

export const PROGRESS_STEPPER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A41392'

export const PROGRESS_STEPPER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A41392&scaling=min-zoom&page-id=8855%3A37791'

export const INFO_MESSAGE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46033'

export const INFO_MESSAGE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10465%3A46033&scaling=min-zoom&page-id=10465%3A46030&starting-point-node-id=10465%3A46033'
export const DROPDOWN_SINGLE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10460%3A48583'

export const DROPDOWN_SINGLE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8867%3A39921&scaling=min-zoom&page-id=8850%3A70650'

export const DROPDOWN_FLYOUT_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70654'

export const DROPDOWN_FLYOUT_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70654&scaling=min-zoom&page-id=8850%3A70650'

export const DROPDOWN_WITHINPUT_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10453%3A47837'

export const DROPDOWN_WITHINPUT_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10453%3A47837&scaling=min-zoom&page-id=8850%3A70650'

export const DAM_MODEL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10454%3A46148'

export const DAM_MODEL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10454%3A46148&scaling=min-zoom&page-id=10454%3A46144'

export const FORM_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10616%3A51712'

export const FORM_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10616%3A51712&scaling=min-zoom&page-id=7015%3A31447'

export const MODAL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41791'

export const MODAL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8848%3A41791&scaling=min-zoom&page-id=7575%3A37036'

export const TABLE_EXPANDABLE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8855%3A38056'

export const TABLE_EXPANDABLE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8855%3A38056&scaling=min-zoom&page-id=3260%3A14'

export const TABLE_DND_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11429%3A48473'

export const TABLE_DND_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=13208%3A52125&scaling=min-zoom&page-id=3260%3A14'

export const TABLE_SIMPLE_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A49461'

export const TABLE_SIMPLE_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A49461&scaling=min-zoom&page-id=3260%3A14'

export const TABLE_CHECKBOX_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10824%3A47958'

export const TABLE_CHECKBOX_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10824%3A47958&scaling=min-zoom&page-id=3260%3A14'

export const TABLE_SCROLL_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A45750'

export const TABLE_SCROLL_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A45750&scaling=min-zoom&page-id=3260%3A14'

export const TABLE_EMPTY_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A46169'

export const TABLE_EMPTY_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9934%3A46169&scaling=min-zoom&page-id=3260%3A14'

export const HEADER_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A73127'

export const HEADER_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A73127&scaling=min-zoom&page-id=8850%3A72675'
export const TYPOGRAPHY_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9937%3A52215'

export const TYPOGRAPHY_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9937%3A52215&scaling=min-zoom&page-id=9937%3A52128'

export const ICONOGRAPHY_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A68030'

export const ICONOGRAPHY_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A68030&scaling=min-zoom&page-id=8850%3A68029'

export const FABRIC_LOGO_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10509%3A46733'

export const FABRIC_LOGO_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10509%3A46733&scaling=min-zoom&page-id=8850%3A68029'

export const COLOR_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9937%3A52129'

export const COLOR_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9937%3A52129&scaling=min-zoom&page-id=9937%3A52129'

export const SHADOW_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9964%3A41901'

export const SHADOW_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9964%3A41901&scaling=min-zoom&page-id=9964%3A41900'
export const CHIPS_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70606'

export const CHIPS_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=8850%3A70606&scaling=min-zoom&page-id=4229%3A12693'

export const LINKS_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10250%3A45919'

export const LINKS_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10250%3A45919&scaling=min-zoom&page-id=10250%3A45832'

export const PILLS_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10384%3A45362'

export const PILLS_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=10384%3A45362&scaling=min-zoom&page-id=10384%3A45361'

export const TOOLTIP_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9015%3A38106'

export const TOOLTIP_PRESENTATION =
  'https://www.figma.com/proto/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=9015%3A38106&scaling=min-zoom&page-id=8850%3A65151'

export const SELECTABLE_CARD_DESIGN =
  'https://www.figma.com/file/Smajb6IVTAZW6Ztxpm3HXL/Design-system-Fabric?node-id=11347%3A47963'
