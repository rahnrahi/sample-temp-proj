import { useEffect } from 'react'

const useClickOutside = (ref, callback, isOpen) => {
  const refArray = Array.isArray(ref) ? ref : [ref]
  const handleClick = e => {
    if (
      refArray.every(
        refItem => refItem.current && !refItem.current.contains(e.target)
      )
    ) {
      callback()
    }
  }
  useEffect(() => {
    if (isOpen) {
      document.addEventListener('click', handleClick)
      return () => {
        document.removeEventListener('click', handleClick)
      }
    }
  }, [isOpen])
}
export default useClickOutside
