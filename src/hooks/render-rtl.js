import { renderDirection } from './utils'

const useLocaleDirection = localeCode => {
  let language = ''
  if (localeCode?.length) {
    const langCode = localeCode?.split('-')
    language = langCode[0]
  }
  return renderDirection(language)
}

export default useLocaleDirection
