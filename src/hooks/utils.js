import {
  RTL_LANGUAGES,
  RTL,
  LTR,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from './constants'

export const getLocalStorage = keyName => {
  return localStorage.getItem(keyName)
}

export const returnDirection = isRTL => {
  return isRTL ? RTL : LTR
}

export const isRTLLanguage = lang => {
  return RTL_LANGUAGES.indexOf(lang) > -1
}

export const renderDirection = language => {
  const isRTL = isRTLLanguage(language)
  return returnDirection(isRTL)
}

export const getDesignSpecifications = (
  designLink,
  presentationLink,
  type = 'figma',
  allowFullscreen = true
) => {
  return {
    design: [
      {
        name: DESIGN_TAB_MOCKUP,
        type,
        url: designLink,
        allowFullscreen,
      },
      {
        name: DESIGN_TAB_PRESENTATION,
        type,
        url: presentationLink,
        allowFullscreen,
      },
    ],
  }
}
