import * as utils from './utils'

test('Returns the design object with the design and presentation links passed in the params', () => {
  const response = {
    design: [
      {
        name: 'Design',
        type: 'figma',
        url: 'designLink',
        allowFullscreen: true,
      },
      {
        name: 'Presentation',
        type: 'figma',
        url: 'presentationLink',
        allowFullscreen: true,
      },
    ],
  }

  expect(
    utils.getDesignSpecifications('designLink', 'presentationLink')
  ).toStrictEqual(response)
})

test('Returns the design object when type and allowFullScreen is passed', () => {
  const response = {
    design: [
      {
        name: 'Design',
        type: 'iframe',
        url: 'designLink',
        allowFullscreen: false,
      },
      {
        name: 'Presentation',
        type: 'iframe',
        url: 'presentationLink',
        allowFullscreen: false,
      },
    ],
  }

  expect(
    utils.getDesignSpecifications(
      'designLink',
      'presentationLink',
      'iframe',
      false
    )
  ).toStrictEqual(response)
})

test('Checks for value stored in localStorage', () => {
  window.localStorage.setItem('account', 'testID')
  expect(utils.getLocalStorage('account')).toBe('testID')
})

test('Checks for the text direction to be set', () => {
  expect(utils.returnDirection(true)).toBe('rtl')
  expect(utils.returnDirection(false)).toBe('ltr')
})
