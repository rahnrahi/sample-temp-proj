import React from 'react'
import { Icon } from './Icon'
import { iconDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/Toggle + Alert Icons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'Checkbox',
          'CheckboxSelected',
          'Radio',
          'RadioSelected',
          'Notification',
        ],
      },
    },
  },
}

const Template = args => <Icon {...args} />
export const Content = Template.bind({})
Content.args = {
  iconName: 'Checkbox',
}
Content.parameters = { design: iconDesign }

export const FootPrint16px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Checkbox' size={16} />
    <Icon iconName='CheckboxSelected' size={16} />
    <Icon iconName='Radio' size={16} />
    <Icon iconName='RadioSelected' size={16} />
  </div>
)
FootPrint16px.storyName = 'FOOTPRINT /16px'
FootPrint16px.parameters = { design: iconDesign }

export const FootPrint40px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Notification' size={40} pathFill='#121213' />
  </div>
)
FootPrint40px.storyName = 'FOOTPRINT /40px'
FootPrint40px.parameters = { design: iconDesign }
