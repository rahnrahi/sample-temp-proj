import React from 'react'
import { Icon } from './Icon'
import { iconDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/Navigation Icons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'CircleDownArrow',
          'LeftArrow',
          'RightArrow',
          'DownArrow',
          'UpArrow',
          'Close',
          'Link',
        ],
      },
    },
  },
}

const Template = args => <Icon {...args} />
export const Navigation = Template.bind({})
Navigation.args = {
  iconName: 'CircleDownArrow',
}
Navigation.parameters = { design: iconDesign }
export const FootPrint18px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='CircleDownArrow' size={18} />
  </div>
)
FootPrint18px.storyName = 'FOOTPRINT /18px'
FootPrint18px.parameters = { design: iconDesign }

export const FootPrint16px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='LeftArrow' size={16} />
    <Icon iconName='RightArrow' size={16} />
    <Icon iconName='DownArrow' size={16} fill='#121213' />
    <Icon iconName='UpArrow' size={16} />
    <Icon iconName='Close' size={16} fill='#121213' viewBox='0 0 16 16' />
    <Icon iconName='Link' size={16} />
  </div>
)
FootPrint16px.storyName = 'FOOTPRINT /16px'
FootPrint16px.parameters = { design: iconDesign }
