import {
  ICONOGRAPHY_DESIGN,
  ICONOGRAPHY_PRESENTATION,
  FABRIC_LOGO_DESIGN,
  FABRIC_LOGO_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export const iconDesign = [
  {
    name: DESIGN_TAB_MOCKUP,
    type: 'figma',
    url: ICONOGRAPHY_DESIGN,
    allowFullscreen: true,
  },
  {
    name: DESIGN_TAB_PRESENTATION,
    type: 'figma',
    url: ICONOGRAPHY_PRESENTATION,
    allowFullscreen: true,
  },
]

export const logoDesign = [
  {
    name: DESIGN_TAB_MOCKUP,
    type: 'figma',
    url: FABRIC_LOGO_DESIGN,
    allowFullscreen: true,
  },
  {
    name: DESIGN_TAB_PRESENTATION,
    type: 'figma',
    url: FABRIC_LOGO_PRESENTATION,
    allowFullscreen: true,
  },
]
