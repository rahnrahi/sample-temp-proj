import React from 'react'
import { Icon } from './Icon'
import theme from '../../shared/theme'
import { iconDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/Action Icons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'Search',
          'Delete',
          'Refresh',
          'Checkmark',
          'Info',
          'Play',
          'AssignedToMe',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Eye',
          'Edit',
          'Lock',
          'ActionCopy',
          'DragAndDrop',
          'ActionEye',
        ],
      },
    },
  },
}

const Template = args => <Icon {...args} />
export const Action = Template.bind({})
Action.args = {
  iconName: 'Search',
}
Action.parameters = { design: iconDesign }

export const FootPrint24px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Search' size={24} />
    <Icon iconName='Delete' size={24} />
    <Icon iconName='Refresh' size={24} />
    <Icon iconName='Checkmark' size={24} fill='black' />
    <Icon iconName='Info' size={24} />
    <Icon iconName='Edit' size={24} />
    <Icon iconName='Lock' size={24} />
    <Icon
      iconName='ActionEye'
      size={24}
      fill={theme.palette.brand.primary.charcoal}
    />
    <Icon
      iconName='ActionCopy'
      size={24}
      fill={theme.palette.brand.primary.charcoal}
    />
  </div>
)
FootPrint24px.storyName = 'FOOTPRINT /24px'
FootPrint24px.parameters = { design: iconDesign }
export const FootPrint18px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Play' size={18} />
    <Icon iconName='AssignedToMe' size={50} viewBox='0 0 50 50' />
    <Icon iconName='Eye' size={18} />
  </div>
)
FootPrint18px.storyName = 'FOOTPRINT /18px'
FootPrint18px.parameters = { design: iconDesign }

export const FootPrint16px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Search' size={16} />
    <Icon iconName='Dots' size={16} />
    <Icon iconName='Import' size={16} fill='#121213' />
    <Icon iconName='Export' size={16} fill='#121213' />
    <Icon iconName='Lock' size={16} />
    <Icon
      iconName='DragAndDrop'
      size={16}
      fill={theme.palette.ui.neutral.grey2}
    />
  </div>
)
FootPrint16px.storyName = 'FOOTPRINT /16px'
FootPrint16px.parameters = { design: iconDesign }

export const FootPrint12px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Add' size={12} fill='#121213' />
  </div>
)
FootPrint12px.storyName = 'FOOTPRINT /12px'
FootPrint12px.parameters = { design: iconDesign }
