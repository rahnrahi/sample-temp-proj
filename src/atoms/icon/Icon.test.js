import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Icon } from './Icon'
import 'jest-styled-components'

afterEach(cleanup)
describe('<Icon/>', () => {
  it('renders Icon component correctly', () => {
    const { container } = render(<Icon iconName='FabricLogo' />)
    expect(container).toMatchSnapshot()
  })
})
