import React from 'react'
import { Icon } from './Icon'
import IconDoc from '../../../docs/Icon.mdx'
import { iconDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/utilIcons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'SkuLookUp',
          'ProductPerformance',
          'UserBehaviors',
          'RunningPromotions',
          'AssignedToMe',
          'Help',
          'Copy',
          'DigitalAsset',
        ],
      },
    },
  },
  parameters: {
    docs: {
      page: IconDoc,
    },
  },
}

const Template = args => <Icon {...args} />
export const Util = Template.bind({})
Util.args = {
  iconName: 'SkuLookUp',
}
Util.parameters = { design: iconDesign }

export const UtilIcons = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='SkuLookUp' />
    <Icon iconName='ProductPerformance' />
    <Icon iconName='UserBehaviors' />
    <Icon iconName='RunningPromotions' />
    <Icon iconName='AssignedToMe' />
    <Icon iconName='Help' />
    <Icon iconName='Copy' />
  </div>
)
UtilIcons.storyName = 'Util Icons /50px'
UtilIcons.parameters = { design: iconDesign }

export const UtilIcons40px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='SkuLookUp' size={40} />
    <Icon iconName='ProductPerformance' size={40} />
    <Icon iconName='UserBehaviors' size={40} />
    <Icon iconName='RunningPromotions' size={40} />
    <Icon iconName='AssignedToMe' size={40} />
    <Icon iconName='Help' size={40} />
    <Icon iconName='Copy' size={40} />
  </div>
)
UtilIcons40px.storyName = 'Util Icons /40px'
UtilIcons40px.parameters = { design: iconDesign }
