import React from 'react'
import { Icon } from './Icon'
import IconDoc from '../../../docs/Icon.mdx'
import { iconDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/Content Icons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'DownCaret',
          'UpCaret',
          'RightCaret',
          'AllInclusive',
          'Clock',
          'Flash',
          'Comment',
        ],
      },
    },
  },
  parameters: {
    docs: {
      page: IconDoc,
    },
  },
}

const Template = args => <Icon {...args} />
export const Content = Template.bind({})
Content.args = {
  iconName: 'Edit',
}
Content.parameters = { design: iconDesign }

export const FootPrint24px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
    <Icon iconName='Clock' />
    <Icon iconName='Flash' />
    <Icon iconName='Comment' />
  </div>
)
FootPrint24px.storyName = 'FOOTPRINT /24px'
FootPrint24px.parameters = { design: iconDesign }

export const FootPrint16px = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Edit' size={16} />
    <Icon iconName='GridView' size={16} />
    <Icon iconName='ListView' size={16} fill='#121213' />
    <Icon iconName='Settings' size={16} />
    <Icon iconName='DownCaret' size={16} fill='#121213' />
    <Icon iconName='UpCaret' size={16} />
    <Icon iconName='RightCaret' size={16} />
    <Icon iconName='AllInclusive' size={16} />
  </div>
)
FootPrint16px.storyName = 'FOOTPRINT /16px'
FootPrint16px.parameters = { design: iconDesign }
