import React from 'react'
import { Icon } from './Icon'
import { logoDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: [
          'FabricLogo',
          'Overview',
          'PIM',
          'XPM',
          'Offers',
          'OMS',
          'LoyaltyManagement',
          'Subscriptions',
          'Dropship',
          'POS',
          'SkuLookUp',
          'ProductPerformance',
          'UserBehaviors',
          'RunningPromotions',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'LeftArrow',
          'RightArrow',
          'DownArrow',
          'UpArrow',
          'UpCaret',
          'DownCaret',
          'RightCaret',
          'DownArrow',
          'UpArrow',
          'Close',
          'Link',
          'Search',
          'Delete',
          'Refresh',
          'Checkmark',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Pay',
          'Analytics',
          'Customer',
          'Calculator',
          'Calendar',
          'Lock',
        ],
      },
    },
  },
}

const Template = args => <Icon {...args} />

export const FabricLogo = Template.bind({})
FabricLogo.args = {
  iconName: 'FabricLogo',
}
FabricLogo.storyName = 'Fabric Logo'
FabricLogo.parameters = { design: logoDesign }

export const ProductIcons = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Icon iconName='Overview' />
    <Icon iconName='PIM' />
    <Icon iconName='XPM' />
    <Icon iconName='Offers' />
    <Icon iconName='OMS' />
    <Icon iconName='LoyaltyManagement' />
    <Icon iconName='Subscriptions' />
    <Icon iconName='Dropship' />
    <Icon iconName='POS' />
    <Icon iconName='Pay' />
    <Icon iconName='Analytics' />
    <Icon iconName='Customer' />
  </div>
)
ProductIcons.parameters = { design: logoDesign }
