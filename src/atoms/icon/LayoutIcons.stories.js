import React from 'react'
import { Icon } from './Icon'
import { logoDesign } from './icon-utils'

export default {
  title: 'Foundation/Iconography/Layout Icons',
  component: Icon,
  argTypes: {
    iconName: {
      control: {
        type: 'select',
        options: ['GridView', 'ListView'],
      },
    },
    fill: {
      control: {
        type: 'select',
        options: ['#121213', '#0D62FF'],
      },
    },
  },
}

const Template = args => <Icon {...args} />
export const LayoutIcons = Template.bind({})
LayoutIcons.args = {
  iconName: 'GridView',
  fill: '#121213',
  size: 14,
}
LayoutIcons.parameters = { design: logoDesign }
