import React from 'react'
import { ListTitle } from './styles'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const Template = () => {
  return <ListTitle>Title</ListTitle>
}

export const BasicTitle = Template.bind({})
export default {
  title: 'Modules/Cards/List/ListTitle',
  component: ListTitle,
}
BasicTitle.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
