import React from 'react'
import { render } from '@testing-library/react'
import { ListTitle } from './'

describe('<ListTitle/>', () => {
  it('should render correctly', () => {
    const { container } = render(<ListTitle>List item content</ListTitle>)
    expect(container).toMatchSnapshot()
  })
})
