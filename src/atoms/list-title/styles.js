import styled from 'styled-components'
import { theme } from '../../shared'

export const ListTitle = styled.div`
  ${theme.typography.body}
  padding: 0 1rem;
  height: 24px;
  display: flex;
  align-items: center;
`
