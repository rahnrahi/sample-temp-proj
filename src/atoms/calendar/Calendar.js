import React, { useState, useEffect, useMemo, useRef, useCallback } from 'react'
import PropTypes from 'prop-types'
import { getYear, getMonth } from 'date-fns'
import en from 'date-fns/locale/en-GB'
import ReactDatePicker, {
  registerLocale,
  setDefaultLocale,
} from 'react-datepicker'
import useClickOutside from '../../hooks/click-outside'
import { isValidDate, isFunction } from '../../shared/utils'
import { days, months } from './Constant'
import { LeftArrow, RightArrow, WhiteDownArrow } from '../../assets/images'
import {
  StyledDatePicker,
  StyledDateHeader,
  StyledHeaderCenter,
  StyledYearSection,
  StyledDropdown,
  StyledOption,
  StyledArrow,
} from './Calendar.styles'
import { theme } from '../../shared'

registerLocale('en-GB', en)
setDefaultLocale('en-GB', en)

export const Calendar = ({
  initialValue,
  fixedHeight,
  onDateChange,
  yearsArray,
  numberOfYears,
  popperPlacement,
  customInput,
  zIndex,
  ...props
}) => {
  const [startDate, setStartDate] = useState(initialValue)
  const [show, setShow] = useState(false)
  const datePickerRef = useRef(null)

  useEffect(() => {
    setStartDate(initialValue)
  }, [initialValue])

  const years = useMemo(() => {
    if (!yearsArray.length) {
      const yearLength = numberOfYears > 10 ? numberOfYears : 10
      return Array.from(
        { length: yearLength },
        (x, i) => getYear(new Date()) + i
      )
    }
    return yearsArray.slice(0, numberOfYears)
  }, [numberOfYears, yearsArray])

  useClickOutside(
    datePickerRef,
    () => {
      setShow(false)
    },
    show
  )

  const onChange = useCallback(
    (date, e) => {
      setStartDate(date)
      isValidDate(date) && onDateChange(date, e)
      e.stopPropagation()
    },
    [onDateChange]
  )

  const handleDropdown = () => setShow(show => !show)

  const handleDatePickerClick = () => show && setShow(false)

  return (
    <StyledDatePicker
      ref={datePickerRef}
      onClick={handleDatePickerClick}
      zIndex={zIndex}
    >
      <ReactDatePicker
        {...props}
        customInput={
          isFunction(customInput)
            ? customInput({ value: startDate })
            : customInput
        }
        popperModifiers={{
          preventOverflow: {
            enabled: false,
            escapeWithReference: false,
            boundariesElement: 'scrollParent',
          },
          hide: {
            enabled: false,
          },
          flip: {
            behavior: ['bottom'],
          },
        }}
        popperPlacement={popperPlacement}
        formatWeekDay={x => days[x]}
        locale='en-GB'
        renderCustomHeader={({
          date,
          changeYear,
          decreaseMonth,
          increaseMonth,
        }) => (
          <StyledDateHeader>
            <StyledArrow data-testid='decrement-year' onClick={decreaseMonth}>
              <LeftArrow className='left-arrow' />
            </StyledArrow>
            <StyledHeaderCenter>
              <label data-testid='year-title'>{`${
                months[getMonth(date)]
              } ${getYear(date)}`}</label>
              <StyledYearSection onClick={handleDropdown}>
                <WhiteDownArrow className='white-arrow' />
                {show && (
                  <StyledDropdown>
                    {years.map(option => (
                      <StyledOption
                        key={option}
                        onClick={() => changeYear(option)}
                        active={getYear(date) === option}
                      >
                        {option}
                      </StyledOption>
                    ))}
                  </StyledDropdown>
                )}
              </StyledYearSection>
            </StyledHeaderCenter>
            <StyledArrow data-testid='increment-year' onClick={increaseMonth}>
              <RightArrow className='right-arrow' />
            </StyledArrow>
          </StyledDateHeader>
        )}
        selected={isValidDate(startDate) && startDate}
        onChange={onChange}
        dayClassName={date => date && 'day-padding'}
        fixedHeight={fixedHeight}
      />
    </StyledDatePicker>
  )
}

Calendar.defaultProps = {
  initialValue: new Date(),
  fixedHeight: true,
  onDateChange: () => {},
  numberOfYears: 10,
  yearsArray: [],
  popperPlacement: 'bottom-end',
  zIndex: theme.zIndex.calendar,
}

Calendar.propTypes = {
  onDateChange: PropTypes.func,
  fixedHeight: PropTypes.bool,
  numberOfYears: PropTypes.number,
  yearsArray: PropTypes.array,
  popperPlacement: PropTypes.oneOf([
    'top-start',
    'top-end',
    'right-start',
    'right-end',
    'bottom-start',
    'bottom-end',
    'left-start',
    'left-end',
  ]),
  /** eg. new Date() */
  minDate: PropTypes.instanceOf(Date),
  /** eg. new Date() */
  maxDate: PropTypes.instanceOf(Date),
  /** eg. new Date() */
  initialValue: PropTypes.instanceOf(Date),
  /** ({value})=> return input element */
  customInput: PropTypes.func,
  /** disable calendar */
  disabled: PropTypes.bool,
  /** set Z index of caldenar */
  zIndex: PropTypes.number,
}
