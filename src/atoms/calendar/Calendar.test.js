import React from 'react'
import { format } from 'date-fns'
import { render, fireEvent, cleanup } from '@testing-library/react'
import { Calendar } from './Calendar'
import { Input } from '../input'

afterEach(cleanup)

jest.mock('popper.js', () => {
  const PopperJS = jest.requireActual('popper.js')
  return class {
    static placements = PopperJS.placements
    constructor() {
      return {
        destroy: () => {},
        scheduleUpdate: () => {},
      }
    }
  }
})

describe('<Calendar/>', () => {
  it('should render Calendar input correctly', () => {
    const { container } = render(<Calendar popperPlacement='top-start' />)
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: '10-10-2020' } })
    expect(container).toMatchSnapshot()
  })

  it('should render Calendar with initial value correctly', () => {
    const { container } = render(
      <Calendar initialValue={new Date('2020-10-10 00:00')} />
    )
    const input = container.querySelector('input')
    expect(input.value).toBe('10/10/2020')
  })

  it('should render Calendar correctly', () => {
    const { container } = render(<Calendar />)
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: '10-10-2020' } })
    fireEvent.click(input)
    expect(container).toMatchSnapshot()
  })

  it('should render Calendar with next month correctly', () => {
    const { container, getByTestId } = render(<Calendar />)
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: '10-10-2020' } })
    fireEvent.click(input)
    fireEvent.click(getByTestId('increment-year'))
    expect(getByTestId('year-title')).toHaveTextContent('Nov 2020')
    expect(container).toMatchSnapshot()
  })

  it('should render Calendar with previous month correctly', () => {
    const { container, getByTestId } = render(<Calendar />)
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: '10-10-2020' } })
    fireEvent.click(input)
    fireEvent.click(getByTestId('decrement-year'))
    expect(getByTestId('year-title')).toHaveTextContent('Sept 2020')
    expect(container).toMatchSnapshot()
  })

  it('should render Calendar with custom input element correctly', () => {
    const { container, getByTestId } = render(
      <Calendar
        initialValue={new Date('2020-10-10 00:00')}
        dateFormat='dd/MM/yyyy'
        customInput={({ value }) => (
          <Input
            isFloatedLabel
            label='Date'
            inputProps={{
              value: value ? format(value, 'dd/MM/yyyy') : '',
              ['data-testid']: 'custom-input',
            }}
            maskOptions={{
              alias: 'datetime',
              placeholder: 'dd/mm/yyyy',
              inputFormat: 'dd/mm/yyyy',
            }}
          />
        )}
      />
    )
    const input = getByTestId('custom-input')
    expect(input).toHaveValue('10/10/2020')
    expect(container).toMatchSnapshot()
  })
})
