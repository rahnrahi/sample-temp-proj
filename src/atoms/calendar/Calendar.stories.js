import React from 'react'
import { Calendar } from './Calendar'
import { Input } from '../input'
import { addDays, format } from 'date-fns'
import styled from 'styled-components'
import CalendarsDocs from '../../../docs/CalendarAndTimePicker.mdx'
import { CALENDAR_DESIGN, CALENDAR_PRESENTATION } from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const design = getDesignSpecifications(CALENDAR_DESIGN, CALENDAR_PRESENTATION)[
  'design'
]

export default {
  title: 'Modules/Calendar & Time Picker/Calendar',
  component: Calendar,
  argTypes: {
    initialValue: {
      control: {
        type: 'date',
      },
    },
    minDate: {
      control: {
        type: 'date',
      },
    },
    maxDate: {
      control: {
        type: 'date',
      },
    },
  },
  parameters: {
    docs: {
      page: CalendarsDocs,
    },
  },
}

const StyledContainer = styled.div`
  position: relative;
  width: 100%;
  height: 600px;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Template = args => {
  return (
    <StyledContainer>
      <Calendar
        {...args}
        dateFormat='dd/MM/yyyy'
        // eslint-disable-next-line
        customInput={({ value }) => {
          return (
            // eslint-disable-next-line
            <Input
              isFloatedLabel
              label='Date'
              inputProps={{
                defaultValue: value ? format(value, 'dd/MM/yyyy') : '',
                ['data-testid']: 'custom-input',
                disabled: args.disabled,
              }}
              maskOptions={{
                alias: 'datetime',
                placeholder: 'dd/mm/yyyy',
                inputFormat: 'dd/mm/yyyy',
              }}
            />
          )
        }}
      />
    </StyledContainer>
  )
}

export const calendar = Template.bind({})

calendar.args = {
  yearsArray: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027],
  popperPlacement: 'bottom-end',
  initialValue: '',
  onDateChange: date => console.log('date', date),
}
calendar.parameters = { design }

export const pastDatesBlocked = Template.bind({})

pastDatesBlocked.args = {
  yearsArray: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027],
  popperPlacement: 'bottom-end',
  minDate: new Date(),
}
pastDatesBlocked.parameters = { design }

export const futureDatesBlocked = Template.bind({})

futureDatesBlocked.args = {
  yearsArray: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027],
  popperPlacement: 'bottom-end',
  maxDate: addDays(new Date(), 6),
}
futureDatesBlocked.parameters = { design }
