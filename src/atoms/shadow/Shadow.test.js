import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { Shadow } from './Shadow'

afterEach(cleanup)

describe('<Shadow/>', () => {
  it('should render Shadow component type:light level:level1 correctly', () => {
    const { container } = render(<Shadow type='light' level='level1' />)
    expect(container).toMatchSnapshot()
  })
})
