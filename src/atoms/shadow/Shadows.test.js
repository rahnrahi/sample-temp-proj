import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { Shadows } from './Shadows'

afterEach(cleanup)

describe('<Shadow/>', () => {
  it('should render Shadows component type:light level:level1 correctly', () => {
    const data = [
      {
        type: 'light',
        level: ['level1', 'level2'],
      },
    ]
    const { container } = render(<Shadows data={data} />)
    expect(container).toMatchSnapshot()
  })
})
