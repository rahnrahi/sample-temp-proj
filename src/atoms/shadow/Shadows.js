import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { theme } from '../../shared'
import { Shadow } from './Shadow'

const Box = styled.div`
  position: relative;
  font-family: ${theme.typography.h4.fontFamily};
  width: 134px;
  height: 134px;
  border-radius: 4px;
  background: #737f8f;
  border: 1px solid gray;
  &:before {
    content: '${({ level }) => level}';
    position: absolute;
    top: -20%;
    left: 0%;
  }
  &:after {
    position: absolute;
    content: '${({ path }) => path}';
    bottom: -25%;
  }
`

const Outercontainer = styled.div`
  display: flex;
  flex-direction: column;
  font-family: 'Gilroy-Medium';
`

const Innercontainer = styled.div`
  display: flex;
  position: relative;
  &:before {
    content: '${({ type }) => type}';
    position: absolute;
    top: 30px;
    left: 86px;
    font-size: 16px;
    color: #737f8f;
  }
  > div {
    margin: 86px;
    border-radius: 4px;
  }
`

export const Shadows = ({ data }) => {
  return (
    <Outercontainer>
      {data.map(({ type, level }, i) => {
        return (
          <Innercontainer
            key={i}
            type={`0${i + 1} Global components / ${
              type.charAt(0).toUpperCase() + type.slice(1)
            } Themetype`}
          >
            {level.map((l, j) => (
              <Shadow key={j} type={type} level={l}>
                <Box level={l} path={`theme.shadows.${type}.${l}`} />
              </Shadow>
            ))}
          </Innercontainer>
        )
      })}
    </Outercontainer>
  )
}

Shadows.defaultProps = {
  data: [
    {
      type: 'light',
      level: ['level0', 'level1', 'level2', 'level3'],
    },
    {
      type: 'dark',
      level: ['level0', 'level1'],
    },
  ],
}

Shadows.propTypes = {
  data: PropTypes.array,
}
