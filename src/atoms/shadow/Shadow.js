import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { theme } from '../../shared'

const Container = styled.div`
  ${({ boxShadow }) => boxShadow};
`

export const Shadow = ({
  type = 'light',
  level = 'level0',
  children,
  ...props
}) => {
  let boxShadow = theme.shadows[`${type}`][`${level}`]
  return (
    <Container boxShadow={boxShadow} {...props}>
      {children}
    </Container>
  )
}

Shadow.propTypes = {
  type: PropTypes.oneOf(['light', 'dark']),
  level: PropTypes.oneOf(['level0', 'level1', 'level2', 'level3']),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
