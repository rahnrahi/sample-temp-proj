import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

import { StyledLink } from './Link.style'

export const Link = React.forwardRef(
  ({ text, href, type, className, ...props }, ref) => {
    return (
      <StyledLink
        data-testid='link'
        {...props}
        href={href}
        ref={ref}
        className={cx({ [type]: type }, className)}
      >
        {text}
      </StyledLink>
    )
  }
)
Link.defaultProps = {
  type: 'primary',
}
Link.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.oneOf(['primary', 'secondary', 'tertiary']),
  className: PropTypes.string,
}
