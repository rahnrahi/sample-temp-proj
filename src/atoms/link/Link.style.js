import styled, { css } from 'styled-components'
import theme from '../../shared/theme'

export const StyledLink = styled.a`
  ${theme.typography.link.css};
  text-align: ${props => (props.isLeft ? 'left' : 'center')};
  outline: none;
  text-decoration: none;
  cursor: pointer;
  border: 1px solid transparent;
  padding: 0px 2px;

  ${({ disabled }) =>
    disabled &&
    css`
      color: ${theme.palette.ui.neutral.grey7};
      pointer-events: none;
    `}

  &.primary {
    color: ${theme.palette.ui.states.active};
    :hover {
      color: ${theme.palette.ui.states.hover};
    }
    :focus {
      border: 1px solid ${theme.palette.ui.cta.yellow};
      border-radius: 6px;
      color: ${theme.palette.ui.states.pressed};
    }
    :active {
      border: 1px solid transparent;
      color: ${theme.palette.ui.states.pressed};
    }
  }

  &.secondary {
    color: ${theme.palette.brand.primary.charcoal};
    :hover {
      color: ${theme.palette.brand.primary.gray};
    }
    :focus {
      border: 1px solid ${theme.palette.ui.cta.yellow};
      color: ${theme.palette.ui.neutral.grey1};
      border-radius: 6px;
    }
    :active {
      border: 1px solid transparent;
      color: ${theme.palette.ui.neutral.grey1};
    }
  }

  &.tertiary {
    ${theme.typography.h6.css};
    color: ${theme.palette.brand.primary.gray};
    :hover {
      color: ${theme.palette.ui.neutral.grey1};
    }
    :focus {
      border: 1px solid ${theme.palette.ui.cta.yellow};
      color: ${theme.palette.ui.neutral.grey1};
      border-radius: 6px;
    }
    :active {
      border: 1px solid transparent;
      color: ${theme.palette.brand.primary.charcoal};
    }
  }
`
