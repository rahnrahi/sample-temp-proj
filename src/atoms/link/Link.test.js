import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Link } from './Link'
import 'jest-styled-components'

afterEach(cleanup)

describe('<Link/>', () => {
  it('should render primary link', () => {
    const { container, getByTestId } = render(
      <Link text='I am a Link' href='/' type='primary' />
    )
    const link = getByTestId('link')
    expect(link).toHaveTextContent('I am a Link')
    expect(link.getAttribute('href')).toBe('/')
    expect(container).toMatchSnapshot()
  })

  it('should render secondary link', () => {
    const { container } = render(
      <Link text='I am a Link' href='/' type='secondary' />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render tertiary link', () => {
    const { container } = render(
      <Link text='I am a Link' href='/' type='tertiary' />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render primary link with disabled:true', () => {
    const { container } = render(
      <Link text='I am a Link' href='/' type='tertiary' disabled />
    )
    expect(container).toMatchSnapshot()
  })
})
