import React from 'react'

import { Link } from './Link'
import LinksDocs from '../../../docs/Links.mdx'
import {
  LINKS_DESIGN,
  LINKS_PRESENTATION,
  DESIGN_TAB_MOCKUP,
  DESIGN_TAB_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Links',
  component: Link,
  argTypes: {
    href: { control: 'text' },
    text: { control: 'text' },
    type: { control: 'select' },
  },
  parameters: {
    docs: {
      page: LinksDocs,
    },
  },
}

const Template = args => <Link {...args} />

export const Primary = Template.bind({})
Primary.args = {
  text: 'I am just a Link',
  href: '/',
  type: 'primary',
}
Primary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: LINKS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: LINKS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Secondary = Template.bind({})
Secondary.args = {
  text: 'I am just a Link',
  href: '/',
  type: 'secondary',
}
Secondary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: LINKS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: LINKS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Tertiary = Template.bind({})
Tertiary.args = {
  text: 'I am just a Link',
  href: '/',
  type: 'tertiary',
}
Tertiary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: LINKS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: LINKS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
