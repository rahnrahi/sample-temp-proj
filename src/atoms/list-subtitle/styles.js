import styled from 'styled-components'
import { theme } from '../../shared'

export const ListSubtitle = styled.div`
  ${theme.typography.h6}
  padding: 0.5rem 1rem 1rem;
  color: ${theme.palette.brand.primary.gray};
`
