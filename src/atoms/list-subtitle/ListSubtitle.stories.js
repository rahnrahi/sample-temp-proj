import React from 'react'
import { ListSubtitle } from './styles'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const Template = () => {
  return <ListSubtitle>Title</ListSubtitle>
}

export const BasicTitle = Template.bind({})
export default {
  title: 'Modules/Cards/List/ListSubtitle',
  component: ListSubtitle,
}
BasicTitle.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
