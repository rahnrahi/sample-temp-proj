import React from 'react'
import { render } from '@testing-library/react'
import { ListSubtitle } from './'

describe('<ListTitle/>', () => {
  it('should render correctly', () => {
    const { container } = render(<ListSubtitle>List item content</ListSubtitle>)
    expect(container).toMatchSnapshot()
  })
})
