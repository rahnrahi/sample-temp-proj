import React, { useState, useEffect, useRef } from 'react'
import { v4 as uuidv4 } from 'uuid'
import PropTypes from 'prop-types'

import { StyledCheckboxWrapper, StyledText, StyledBox } from './Checkbox.styles'
import { Checkmark, Partial } from '../../assets/images'
import { Tooltip } from '../tooltip/Tooltip'

export const Checkbox = ({
  label,
  name,
  value,
  onChange,
  disabled,
  className,
  checked,
  isPartial,
  otherProps,
  tooltipProps,
  ...restProps
}) => {
  const [selected, setSelected] = useState(false)
  const [showTooltip, setShowTooltip] = useState(false)
  const checkboxRef = useRef(uuidv4())

  useEffect(() => {
    setSelected(checked)
  }, [checked])

  const handleCheck = e => {
    setSelected(e.target.checked)
    onChange(e)
  }

  return (
    <StyledCheckboxWrapper
      disabled={disabled}
      className={className}
      tabIndex={0}
    >
      <StyledBox selected={selected}>
        {isPartial ? <Partial opacity='0' /> : <Checkmark opacity='0' />}
      </StyledBox>
      <input
        className='checkbox-input'
        type='checkbox'
        name={name || checkboxRef.current}
        onChange={handleCheck}
        value={value}
        checked={selected}
        {...restProps}
        {...otherProps}
      />
      <StyledText
        onMouseOver={e => {
          if (e.target.offsetWidth < e.target.scrollWidth) {
            setShowTooltip(true)
          }
        }}
        onMouseOut={() => setShowTooltip(false)}
      >
        {label}
      </StyledText>
      {showTooltip && (
        <Tooltip
          text={label}
          showTooltip={true}
          size='small'
          {...tooltipProps}
        />
      )}
    </StyledCheckboxWrapper>
  )
}

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
  className: '',
  onChange: () => {},
  label: '',
  isPartial: false,
}

Checkbox.propTypes = {
  /** Label for Checkbox*/
  label: PropTypes.string.isRequired,
  /** Value will be returned onChange event */
  value: PropTypes.string.isRequired,
  /** OnChange callback with entire event  */
  onChange: PropTypes.func.isRequired,
  /** A name for checkbox */
  name: PropTypes.string,
  /**Boolean value for checked state */
  checked: PropTypes.bool,
  /** Boolean value for Disabled state */
  disabled: PropTypes.bool,
  /** Boolean value for Partial state, to get partial state, we should pass checked=true and isPartial=true  */
  isPartial: PropTypes.bool,
  /** CSS class name */
  className: PropTypes.string,
  /** otherProps, is used to pass additional props which can accept input with checkbox, like onKeyDown, 	onMouseEnter, data-testid */
  otherProps: PropTypes.object,
  /** tooltipProps, is used to pass props into the tooltip component */
  tooltipProps: PropTypes.object,
}
