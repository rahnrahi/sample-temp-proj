import React from 'react'

import { Checkbox as CustomCheckbox } from './Checkbox'
import SelectionControlsDocs from '../../../docs/SelectionControls.mdx'
import {
  CHECKBOX_DESIGN,
  CHECKBOX_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Components/Selection Controls/Checkbox',
  component: CustomCheckbox,
  parameters: {
    docs: {
      page: SelectionControlsDocs,
    },
  },
}

const Template = args => <CustomCheckbox {...args} />

export const Checkbox = Template.bind({})
Checkbox.args = {
  label: 'Label',
  value: 'John Doe',
  checked: true,
}
Checkbox.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHECKBOX_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHECKBOX_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const CheckboxDefault = Template.bind({})
CheckboxDefault.args = {
  label: 'Label',
  value: 'John Doe',
}
CheckboxDefault.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHECKBOX_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHECKBOX_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

CheckboxDefault.storyName = 'Default'
