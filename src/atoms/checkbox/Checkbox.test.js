import React from 'react'
import { render, fireEvent, cleanup } from '@testing-library/react'
import { Checkbox } from './Checkbox'
import 'jest-styled-components'

afterEach(cleanup)

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 1),
  }
})

const originalOffsetWidth = Object.getOwnPropertyDescriptor(
  HTMLElement.prototype,
  'offsetWidth'
)

beforeAll(() => {
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  })
  Object.defineProperty(HTMLElement.prototype, 'scrollWidth', {
    configurable: true,
    value: 600,
  })
})

afterAll(() => {
  Object.defineProperty(
    HTMLElement.prototype,
    'offsetWidth',
    originalOffsetWidth
  )
  delete HTMLElement.prototype.scrollWidth
})

describe('<Checkbox/>', () => {
  it('should render checkbox with default props', () => {
    const { container } = render(<Checkbox label='Name' value='John Doe' />)
    expect(container).toMatchSnapshot()
  })

  it('should render checkbox with external prop values', () => {
    const { container, getByTestId } = render(
      <Checkbox
        label='Name'
        name='name'
        value='John Doe'
        onChange={() => {}}
        data-testid='checkbox'
      />
    )
    const checkbox = getByTestId('checkbox')
    fireEvent.click(checkbox)
    expect(checkbox).toBeChecked()
    expect(container).toMatchSnapshot()

    fireEvent.click(checkbox)
    expect(checkbox).not.toBeChecked()
    expect(container).toMatchSnapshot()
  })

  it('should render disabled checkbox', () => {
    const { container } = render(
      <Checkbox
        label='Name'
        name='name'
        value='John Doe'
        onChange={() => {}}
        disabled={true}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render partially selected checkbox', () => {
    const { container } = render(
      <Checkbox
        label='Name'
        name='name'
        value='John Doe'
        onChange={() => {}}
        disabled={false}
        checked={true}
        isPartial={true}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render tooltip when text overflows and label is mouseover-ed', async () => {
    const { getByText, findAllByText } = render(
      <Checkbox
        label='ReallyLongTextThatOverflows'
        name='reallyLongTextThatOverflows'
        value='Text Overflow'
        onChange={() => {}}
        disabled={false}
        checked={true}
        isPartial={true}
        data-testid='checkbox'
      />
    )
    const label = getByText('ReallyLongTextThatOverflows')
    let count = await findAllByText('ReallyLongTextThatOverflows')
    expect(count.length).toBe(1)

    fireEvent.mouseOver(label)
    count = await findAllByText('ReallyLongTextThatOverflows')
    expect(count.length).toBe(2)
    expect(count[1]).toBeVisible()

    fireEvent.mouseOut(label)
    count = await findAllByText('ReallyLongTextThatOverflows')
    expect(count.length).toBe(1)
  })
})
