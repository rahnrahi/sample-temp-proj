import React, { useState, useEffect, useMemo } from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash/noop'
import { isFunction } from '../../shared/utils'
import {
  StyledTabContainer,
  StyledTabHeading,
  StyledTabNavigation,
  StyledTabItem,
} from './Tab.styles'
import { TabType } from './Tab.enum'

export const Tab = ({
  variant,
  heading,
  children,
  navStyle,
  navClassName,
  tabChangeHandler,
  customActiveIndex,
  customSetActiveIndex,
  showSeparationLine,
  ...props
}) => {
  const [tabs, setTabs] = useState([])
  const [activeIndex, setActiveIndex] = useState(customActiveIndex || 0)
  const elements = useMemo(
    () => React.Children.toArray(children).filter(el => isFunction(el.type)),
    [children]
  )

  const Content = React.cloneElement(elements[activeIndex], {
    title: '',
    onHover: noop,
    onFocus: noop,
  })

  const handleTabChange = index => {
    customSetActiveIndex && customSetActiveIndex(index)
    setActiveIndex(index)
    isFunction(tabChangeHandler) && tabChangeHandler(index)
  }

  useEffect(() => {
    const newIndex =
      customActiveIndex !== undefined && customActiveIndex < elements.length
        ? customActiveIndex
        : activeIndex
    setActiveIndex(newIndex)
    customSetActiveIndex && customSetActiveIndex(newIndex)
  }, [activeIndex, customActiveIndex, customSetActiveIndex, elements])

  useEffect(() => {
    const tabs = elements.map((ele, i) => {
      if (ele.props.active && typeof ele.props.active === 'boolean') {
        setActiveIndex(i)
        customSetActiveIndex && customSetActiveIndex(i)
      }
      const { title, disabled, onHover, onFocus, tabIndex } = ele.props
      return {
        id: i,
        title,
        disabled,
        onHover,
        onFocus,
        tabIndex,
      }
    })
    setTabs(tabs)
    //Due to the possibility of this component being controlled, the Eslint below rule had to be disabled in order to allow it
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [elements])

  return (
    <StyledTabContainer variant={variant} {...props}>
      {heading && variant === TabType.HORIZONTAL && (
        <StyledTabHeading className='tab-heading'>{heading}</StyledTabHeading>
      )}
      <StyledTabNavigation
        variant={variant}
        showSeparationLine={showSeparationLine}
      >
        {tabs.length &&
          tabs.map((tab, i) => {
            return (
              <StyledTabItem
                key={`tab-item-${i}`}
                tabIndex={tab.tabIndex}
                onClick={() => !tab.disabled && handleTabChange(i)}
                onMouseOver={e => isFunction(tab.onHover) && tab.onHover(e)}
                onFocus={e => isFunction(tab.onFocus) && tab.onFocus(e)}
                active={activeIndex === i ? true : false}
                variant={variant}
                disabled={tab.disabled}
                style={navStyle}
                className={navClassName}
              >
                {variant === TabType.VERTICAL ? (
                  <span>{tab.title}</span>
                ) : (
                  tab.title
                )}
              </StyledTabItem>
            )
          })}
      </StyledTabNavigation>
      {Content}
    </StyledTabContainer>
  )
}

export const TabItem = ({ children, ...props }) => {
  return <div {...props}>{children}</div>
}

TabItem.propTypes = {
  /** The title of the tab */
  title: PropTypes.string,
  /** True if the tab is disabled, false otherwise */
  disabled: PropTypes.bool,
  /** The tab content */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

Tab.propTypes = {
  variant: PropTypes.oneOf(['horizontal', 'vertical']),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]),
  customActiveIndex: PropTypes.number,
  customSetActiveIndex: PropTypes.func,

  heading: PropTypes.string,
  /** set tabitem styles, eg. navStyle = {{ color:blue}} */
  navStyle: PropTypes.object,
  /** set tabitem styles, eg. navClassName='tabitem-space' */
  navClassName: PropTypes.string,
  tabChangeHandler: PropTypes.func,
  /** Separation Line under the Tab Items for Horizontal Tabs */
  showSeparationLine: PropTypes.bool,
}

Tab.defaultProps = {
  variant: 'horizontal',
  showSeparationLine: false,
}

export default Tab
