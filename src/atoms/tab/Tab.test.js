import React from 'react'
import { screen, render, fireEvent, cleanup } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { Tab, TabItem } from './Tab'

afterEach(cleanup)

describe('<Tab/>', () => {
  it('renders horizontal tab correctly', () => {
    const { container } = render(
      <Tab heading='ALL Items' variant='horizontal'>
        <TabItem title='Performance'>performance</TabItem>
        <TabItem title='Recent Activity'>Recent Activity</TabItem>
      </Tab>
    )
    expect(container).toMatchSnapshot()
  })

  it('renders vertical tab correctly', () => {
    const { container } = render(
      <Tab variant='vertical'>
        <TabItem title='Performance'>performance</TabItem>
        <TabItem title='Recent Activity'>Recent Activity</TabItem>
      </Tab>
    )
    expect(container).toMatchSnapshot()
  })

  it('renders horizontal tab with TabItem disabled', () => {
    const { container } = render(
      <Tab heading='All Items' variant='horizontal'>
        <TabItem title='Performance' disabled>
          performance
        </TabItem>
        <TabItem title='Recent Activity'>Recent Activity</TabItem>
      </Tab>
    )
    expect(container).toMatchSnapshot()
  })

  it('render selected tab content correctly', () => {
    const { getByText } = render(
      <Tab heading='All Items' variant='horizontal'>
        <TabItem title='Performance'>Content for performance</TabItem>
        <TabItem title='Recent Activity'>Content for Recent activity</TabItem>
      </Tab>
    )
    fireEvent.click(getByText('Performance'))
    expect(getByText('Content for performance')).toBeInTheDocument()
  })

  it('render tab with active tabitem', () => {
    const { getByText } = render(
      <Tab heading='All Items' variant='horizontal'>
        <TabItem title='Performance'>Content for performance</TabItem>
        <TabItem title='Recent Activity' active>
          Content for Recent activity
        </TabItem>
      </Tab>
    )
    expect(getByText('Content for Recent activity')).toBeInTheDocument()
  })

  it('render tab and act as a form', () => {
    let index = 0
    const setIndex = jest.fn(newIndex => {
      index = newIndex
    })

    const { getByText } = render(
      <Tab
        customActiveIndex={index}
        customSetActiveIndex={setIndex}
        heading='All Items'
        variant='horizontal'
      >
        <TabItem title='1. Details'>
          <>
            <input label='Name' className='input-template4' />
            <button onClick={() => setIndex(index + 1)}>Next</button>
          </>
        </TabItem>
        <TabItem title='2. Catalog'>
          <>
            <input label='Brand' className='input-template4' />
            <button>Save</button>
          </>
        </TabItem>
      </Tab>
    )
    fireEvent.click(getByText('Next'))
    expect(setIndex).toBeCalledWith(1)
  })

  it('should render horizontal tab with onHover and onFocus props', () => {
    const onHover = jest.fn()
    const onFocus = jest.fn()
    render(
      <Tab heading='All Items' variant='horizontal'>
        <TabItem
          tabIndex='0'
          title='Performance'
          onHover={onHover}
          onFocus={onFocus}
        >
          Content for performance
        </TabItem>
        <TabItem
          tabIndex='0'
          title='Recent Activity'
          onHover={onHover}
          onFocus={onFocus}
        >
          Content for Recent activity
        </TabItem>
      </Tab>
    )
    const performaceTab = screen.getByText('Performance')
    userEvent.hover(performaceTab)
    expect(onHover).toHaveBeenCalledTimes(1)
    userEvent.tab()
    expect(performaceTab).toHaveFocus()
    expect(onFocus).toHaveBeenCalledTimes(1)
  })
})
