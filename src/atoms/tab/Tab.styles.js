import styled, { css } from 'styled-components'
import { theme } from '../../shared'
import { TabType } from './Tab.enum'

export const StyledTabContainer = styled.div`
  display: flex;
  flex-direction: ${({ variant }) =>
    variant === TabType.VERTICAL ? 'row' : 'column'};
`

export const StyledTabHeading = styled.div`
  margin-bottom: 24px;
  ${theme.typography.subtitle2};
  color: ${theme.palette.brand.primary.charcoal};
`

export const StyledTabNavigation = styled.div`
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  display: flex;
  ${({ showSeparationLine, variant }) =>
    showSeparationLine &&
    variant === TabType.HORIZONTAL &&
    css`
      border-bottom: 1px solid ${theme.palette.ui.neutral.grey4};
    `};
  ${({ variant }) =>
    variant === TabType.HORIZONTAL
      ? css`
          flex-direction: row;
        `
      : css`
          flex-direction: column;
          align-self: flex-start;
        `}
`

const VerticalTabItemCss = css`
  ${theme.typography.body};
  display: flex;
  align-items: center;
  width: 125px;
  margin-bottom: 30px;
  span {
    border-bottom: 2px solid transparent;
  }
  :hover:not([disabled]) {
    color: ${theme.palette.ui.neutral.grey1};
  }
  ${({ active }) =>
    active &&
    css`
      color: ${theme.palette.brand.primary.charcoal};
      span {
        border-bottom: 2px solid ${theme.palette.ui.states.active};
      }
    `}
`
const HorizontalTabItemCss = css`
  ${theme.typography.h5};
  margin-right: 48px;
  :last-child {
    margin-right: 0px;
  }
  padding: 0px 4px 16px 4px;
  display: flex;
  align-self: flex-end;
  box-shadow: 0px -1px 0px transparent inset, 0px 1px 0px transparent;

  :hover:not([disabled]) {
    box-shadow: 0px -1px 0px ${theme.palette.brand.primary.charcoal} inset,
      0px 1px 0px ${theme.palette.brand.primary.charcoal};
  }

  ${({ active }) =>
    active &&
    css`
      color: ${theme.palette.brand.primary.charcoal};
      box-shadow: 0px -1px 0px ${theme.palette.ui.states.active} inset,
        0px 1px 0px ${theme.palette.ui.states.active};
    `}
`

export const StyledTabItem = styled.div`
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  :focus-visible {
    outline: 2px solid ${theme.palette.ui.cta.yellow};
    ${({ disabled }) =>
      !disabled &&
      css`
        box-shadow: 0px -1px 0px ${theme.palette.ui.states.active} inset,
          0px 1px 0px ${theme.palette.ui.states.active};
      `};
  }

  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};

  color: ${({ disabled }) =>
    disabled
      ? theme.palette.ui.neutral.grey7
      : theme.palette.brand.primary.gray};

  ${({ variant }) =>
    variant === TabType.HORIZONTAL ? HorizontalTabItemCss : VerticalTabItemCss}
  white-space: pre-wrap;
  text-align: left;
`
