import React, { useState } from 'react'
import styled from 'styled-components'
import { Tab, TabItem } from './Tab'
import { Button, Input } from '../index'
import TabsDocs from '../../../docs/Tabs.mdx'
import { Jumbotron } from '../../molecules/jumbotron'
import { TabType } from './Tab.enum'
import {
  TABS_HORIZONTAL_DESIGN,
  TABS_VERTICAL_DESIGN,
  TABS_STEPPER_DESIGN,
  TABS_VERTICAL_PRESENTATION,
  TABS_HORIZONTAL_PRESENTATION,
  TABS_STEPPER_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Tabs',
  component: Tab,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ['horizontal', 'vertical'],
      },
    },
    heading: { control: 'text' },
    tabs: {
      control: 'object',
    },
  },
  parameters: {
    docs: {
      page: TabsDocs,
      source: {
        type: 'code',
      },
    },
  },
}

const TabChildren = [
  {
    label: 'Performance',
    disabled: false,
  },
  {
    label: 'Disabled Tab',
    disabled: true,
  },
  {
    label: 'Recent Activity',
    disabled: false,
  },
  {
    label: 'Product Performance',
    disabled: false,
  },
]

const Template = args => {
  const ContentStyle = {
    width: '100%',
    marginLeft: args.variant === TabType.HORIZONTAL ? '0px' : '20px',
    marginTop: args.variant === TabType.VERTICAL ? '0px' : '20px',
  }

  return (
    <Tab {...args}>
      {Array.from(args.tabs).map(({ label, disabled }, idx) => (
        <TabItem
          key={`tab_${idx}`}
          tabIndex='0'
          title={label}
          onHover={() => console.log('onHover triggered')}
          onFocus={() => console.log('onFocus triggered')}
          disabled={disabled}
          style={ContentStyle}
        >
          <Jumbotron
            title={label}
            width='100%'
            buttonProps={{
              size: 'large',
              text: 'Continue',
            }}
            onClick={() => {}}
            primaryText={label}
            secondaryText={`Some Descriptions of ${label}`}
          />
        </TabItem>
      ))}
    </Tab>
  )
}

const StyledContainer = styled.div``

const StyledContent = styled.div`
  font-family: 'Gilroy-Medium';
  margin: 0 auto;
  padding: 60px;

  .input-template4 {
    max-width: 300px;
    margin-bottom: 20px;
  }
`

export const HorizontalTab = Template.bind({})
HorizontalTab.args = {
  variant: 'horizontal',
  showSeparationLine: true,
  tabs: TabChildren,
}
HorizontalTab.parameters = getDesignSpecifications(
  TABS_HORIZONTAL_DESIGN,
  TABS_HORIZONTAL_PRESENTATION
)

export const VerticalTab = Template.bind({})
VerticalTab.args = {
  variant: 'vertical',
  tabs: TabChildren,
}
VerticalTab.parameters = getDesignSpecifications(
  TABS_VERTICAL_DESIGN,
  TABS_VERTICAL_PRESENTATION
)

const StyledSpacer = styled.div`
  width: 100%;
  height: 50px;
`
const ButtonContainer = styled.div`
  display: flex;
  button:last-child {
    margin-left: 10px;
  }
`

const StepperTemplate = args => {
  const [index, setIndex] = useState(0)
  const [inputTab1, setInputTab1] = useState('')
  const [inputTab2, setInputTab2] = useState('')

  return (
    <StyledContainer>
      <ButtonContainer>
        <Button
          onClick={() =>
            index === 0
              ? setIndex(1)
              : window.alert(`Name: ${inputTab1}, Brand: ${inputTab2}`)
          }
          size='small'
          text={index === 0 ? 'Next' : 'Save'}
        />
      </ButtonContainer>
      <StyledSpacer />
      <Tab
        {...args}
        customActiveIndex={index}
        customSetActiveIndex={setIndex}
        navClassName='tabitem-space'
      >
        <TabItem title='1. Details'>
          <StyledContent>
            <Input
              label='Name'
              className='input-template4'
              inputProps={{
                value: inputTab1,
                onChange: event => setInputTab1(event.target.value),
              }}
            />
          </StyledContent>
        </TabItem>

        <TabItem title='2. Catalog'>
          <StyledContent>
            <Input
              label='Brand'
              className='input-template4'
              inputProps={{
                value: inputTab2,
                onChange: event => setInputTab2(event.target.value),
              }}
            />
          </StyledContent>
        </TabItem>
      </Tab>
    </StyledContainer>
  )
}

export const TabWithProgressStepper = StepperTemplate.bind({})

TabWithProgressStepper.args = {
  variant: 'horizontal',
  showSeparationLine: true,
}

TabWithProgressStepper.parameters = {
  docs: {
    source: {
      type: 'code',
      format: true,
    },
  },
  design: getDesignSpecifications(
    TABS_STEPPER_DESIGN,
    TABS_STEPPER_PRESENTATION
  )['design'],
}
