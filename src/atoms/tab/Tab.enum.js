export const TabType = Object.freeze({
  HORIZONTAL: 'horizontal',
  VERTICAL: 'vertical',
})
