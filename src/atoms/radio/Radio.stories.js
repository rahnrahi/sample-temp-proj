import React from 'react'
import { Radio } from './Radio'
import SelectionControlsDocs from '../../../docs/SelectionControls.mdx'
import {
  RADIO_DESIGN,
  RADIO_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Components/Selection Controls/Radio Button',
  component: Radio,
  argTypes: {
    label: 'Label',
    checked: { control: 'boolean' },
    iconName: {
      control: {
        type: 'select',
        options: [
          'FabricLogo',
          'Overview',
          'PIM',
          'XPM',
          'Offers',
          'OMS',
          'LoyaltyManagement',
          'Subscriptions',
          'Dropship',
          'POS',
          'SkuLookUp',
          'ProductPerformance',
          'UserBehaviors',
          'RunningPromotions',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'LeftArrow',
          'RightArrow',
          'UpArrow',
          'UpCaret',
          'DownCaret',
          'RightCaret',
          'Close',
          'Link',
          'Search',
          'Delete',
          'Refresh',
          'Checkmark',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Pay',
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'Radio',
          'RadioSelected',
          'CheckboxSelected',
          'Checkbox',
          'Notification',
          'CircleClose',
          'Customize',
          'Eye',
          'FabricLogoName',
          'Checkmark',
          'Cross',
          'Analytics',
          'Customer',
          'CSR',
          'Calculator',
          'Calendar',
          'Lock',
          'AllInclusive',
          'Flash',
          'Clock',
        ],
      },
    },
    withHover: { control: 'boolean' },
    withFocus: { control: 'boolean' },
    disabled: { control: 'boolean' },
    tabIndex: { table: { disable: true } },
    children: { table: { disable: true } },
    name: { table: { disable: true } },
    value: { table: { disable: true } },
    onChange: { table: { disable: true } },
    className: { table: { disable: true } },
    id: { table: { disable: true } },
  },
  parameters: {
    docs: {
      page: SelectionControlsDocs,
    },
  },
}

const Template = args => <Radio {...args} />

export const Default = Template.bind({})

Default.args = {
  label: 'Label',
  value: 'default',
  checked: true,
  withHover: true,
  withFocus: true,
  disabled: false,
}
Default.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: RADIO_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: RADIO_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
