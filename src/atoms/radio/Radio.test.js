import React from 'react'
import { Radio } from './Radio'
import { render, cleanup, fireEvent, act } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'uuid'
import 'jest-styled-components'

afterEach(cleanup)
jest.mock('uuid', () => {
  return { v4: jest.fn(() => 1) }
})

const props = {
  label: 'Radio Label',
  value: 'default',
  checked: false,
  name: 'radio-button',
  tabIndex: 0,
  id: 'radio',
  onChange: jest.fn(),
}

describe('<Radio/>', () => {
  it('should render a standard radio button without an icon', async () => {
    const { container, findByText, queryByTestId } = render(
      <Radio {...props} />
    )

    expect(container).toMatchSnapshot()
    expect(queryByTestId('radio-icon')).not.toBeInTheDocument()

    const radioButtonAndLabelSpacingStyle = 'margin-left:6px;'
    const lineHeightStyle = 'line-height: 16px;'

    expect(await findByText('Radio Label')).toHaveStyle(
      radioButtonAndLabelSpacingStyle + lineHeightStyle
    )
  })

  it('should render a checked radio button when checked=true', async () => {
    const { getByRole, findByText, rerender } = render(<Radio {...props} />)

    const radioButton = getByRole('radio', { name: 'Radio Label' })
    expect(radioButton).toBeInTheDocument()
    expect(radioButton).not.toBeChecked()

    fireEvent.click(await findByText('Radio Label'))

    expect(props.onChange).toHaveBeenCalledTimes(1)

    act(() => {
      rerender(<Radio {...props} checked={true} />)
    })

    expect(radioButton).toBeChecked()
  })

  it('should render a radio button with an icon', async () => {
    const { findByText, queryByTestId } = render(
      <Radio {...props} iconName='DownArrow' />
    )

    const icon = queryByTestId('radio-icon')
    const radioButtonAndIconSpacingStyle = 'margin-left:16px;'
    const iconAndLabelSpacingStyle = 'margin-right:4px;'
    const lineHeightStyle = 'line-height: 16px;'

    expect(icon).toBeInTheDocument()
    expect(icon).toHaveStyle(
      radioButtonAndIconSpacingStyle + iconAndLabelSpacingStyle
    )

    expect(await findByText('Radio Label')).toHaveStyle(lineHeightStyle)
  })

  it('should not render a radio button with an icon if the icon name specified is invalid', async () => {
    const { findByText, queryByTestId } = render(
      <Radio {...props} iconName='doesNotExist' />
    )

    expect(queryByTestId('radio-icon')).not.toBeInTheDocument()
    expect(await findByText('Radio Label')).toBeInTheDocument()
  })
})
