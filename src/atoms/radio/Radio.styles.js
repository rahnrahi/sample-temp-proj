import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledRadio = styled.div`
  user-select: none;
  position: relative;
  display: flex;
  align-items: center;
  ${theme.typography.h6.css};
  ${({ disabled }) => disabled && `pointer-events: none;`};

  .radio-label {
    ${({ hasIcon }) => `padding: ${hasIcon ? '4px' : '8px'} 16px;`}
    display: inline-flex;
    align-items: center;
    outline: none;
    border-radius: 2px;
    cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};

    ${({ withHover }) =>
      `
      &:hover {
        background-color: ${
          withHover ? theme.palette.ui.neutral.grey5 : 'transparent'
        };
      }
    `}

    .label {
      line-height: 16px;
    }

    .circle {
      border: 1px solid ${theme.palette.ui.neutral.grey1};
      border-radius: 100%;
      width: 16px;
      height: 16px;
      position: relative;
      box-sizing: border-box;
      background-color: #ffffff;

      + .radio-icon {
        margin-left: 16px;
        margin-right: 4px;
      }

      + .label {
        margin-left: 6px;
      }

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        border-radius: 100%;
        background-color: ${theme.palette.ui.cta.blue};
        opacity: 0;
        width: 4px;
        height: 4px;
        transition: width ease-out 0.2s, height ease-out 0.2s;
      }
    }
  }

  .radio-input {
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
    height: 0;
    width: 0;

    &:disabled {
      cursor: default;

      ~ .radio-label {
        opacity: 0.4;
        cursor: not-allowed;

        &:hover {
          background: transparent;
        }
      }
    }

    &:active:not(:checked) {
      & ~ .radio-label {
        background: transparent;

        .circle {
          background-color: ${theme.palette.ui.neutral.grey1};
        }
      }
    }

    &:checked {
      & ~ .radio-label .circle {
        border: 1px solid ${theme.palette.ui.cta.blue};

        &:before {
          opacity: 1;
          width: 10px;
          height: 10px;
        }
      }

      &:active {
        & ~ .radio-label {
          background: transparent;

          .circle {
            border-color: ${theme.palette.ui.neutral.grey1};

            &:before {
              background-color: ${theme.palette.ui.neutral.grey1};
            }
          }
        }
      }
    }

    ${({ withFocus }) => `
      &:focus ~ .radio-label {
        box-shadow: 0 0 0 1px ${
          withFocus ? theme.palette.ui.cta.yellow : 'transparent'
        };
      }
    `}
  }
`
