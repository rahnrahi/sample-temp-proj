import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { v4 as uuidv4 } from 'uuid'
import { StyledRadio } from './Radio.styles'
import cx from 'classnames'
import { Icon } from '../icon/Icon'

const ICON_SIZE = 24

export const Radio = ({
  label,
  value,
  checked,
  onChange,
  name,
  tabIndex = 0,
  disabled,
  className,
  children,
  withHover = true,
  withFocus = true,
  iconName,
  ...other
}) => {
  const uid = useMemo(() => uuidv4(), [])

  return (
    <StyledRadio
      {...other}
      className={cx({
        [className]: className,
      })}
      withHover={withHover}
      withFocus={withFocus}
      disabled={disabled}
      hasIcon={iconName != null}
    >
      <input
        type='radio'
        name={name || uid}
        className='radio-input'
        checked={checked || false}
        value={value}
        onChange={event => onChange(event)}
        id={uid}
        tabIndex={tabIndex}
        disabled={disabled}
      />
      <label htmlFor={uid} className='radio-label'>
        <span className='circle'></span>
        {iconName && (
          <Icon
            data-testid='radio-icon'
            className='radio-icon'
            size={ICON_SIZE}
            iconName={iconName}
          />
        )}
        <span className='label'>{label}</span>
        <span>{children}</span>
      </label>
    </StyledRadio>
  )
}

Radio.propTypes = {
  /** label for radio input */
  label: PropTypes.string.isRequired,
  /** value for radio input */
  value: PropTypes.string.isRequired,
  /**A unique ID for radio input*/
  id: PropTypes.string,
  className: PropTypes.string,
  /** selected state for radio input */
  checked: PropTypes.bool,
  /** a name for radio input, used for forms*/
  name: PropTypes.string,
  /** a callback function*/
  onChange: PropTypes.func,
  /** to disable radio input*/
  disabled: PropTypes.bool,
  /** TabIndex is required for give accessibility to radio input*/
  tabIndex: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /** Enable/Disable hover state. default is `true`*/
  withHover: PropTypes.bool,
  /** Enable/Disable focused state. default is `true`*/
  withFocus: PropTypes.bool,
  /**
   * The name of the icon from the icon library to use.
   * Drives whether or not an icon appears for the radio button.
   */
  iconName: PropTypes.string,
}

Radio.defaultProps = {
  checked: false,
}
