import React from 'react'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import { BreadcrumbNavLink } from './BreadcrumbNavLink'

afterEach(cleanup)
beforeEach(() => {
  jest.clearAllMocks()
})

describe('<BreadcrumbNavLink />', () => {
  const onClick = jest.fn(() => {})
  let props = {
    label: 'Label1',
    onClick,
  }
  it('check for loading', () => {
    render(<BreadcrumbNavLink {...props} />)
    expect(screen.getByTestId('breadcrumb-navlink')).toBeInTheDocument()
  })

  it('checks for click on breadcrumb label', () => {
    render(<BreadcrumbNavLink {...props} />)
    let breadCrumbLabelDiv = screen.getByTestId('breadcrumb-navlink')
    fireEvent(
      breadCrumbLabelDiv,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      })
    )
    expect(props.onClick.mock.calls.length).toBe(1)
  })

  it('checks for click on breadcrumb label', () => {
    props = {
      label: 'Label1',
    }
    render(<BreadcrumbNavLink {...props} />)
    let breadCrumbLabelDiv = screen.getByTestId('breadcrumb-navlink')
    fireEvent(
      breadCrumbLabelDiv,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      })
    )
    expect(props.onClick).toBeUndefined()
  })
})
