import React from 'react'
import PropTypes from 'prop-types'
import { StyledDiv } from './BreadcrumbNavLink.style'

export const BreadcrumbNavLink = ({ label, ...props }) => {
  return (
    <StyledDiv title={label} {...props} data-testid='breadcrumb-navlink'>
      {label}
    </StyledDiv>
  )
}

BreadcrumbNavLink.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
}
