import styled, { css } from 'styled-components'
const clickable = css`
  &:hover {
    text-decoration: underline;
  }
  cursor: pointer;
`
export const StyledDiv = styled.div`
  ${props => props.onClick && clickable}

  font-size: 12px;
  letter-spacing: 0.9px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 150px;
  min-width: 50px;
  padding: 2px;
`
