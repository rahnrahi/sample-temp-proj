import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledMessage = styled.div`
  ${theme.typography.caption.css};
  color: ${theme.palette.ui.cta.red};
`
