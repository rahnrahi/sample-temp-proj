import React from 'react'

import { ErrorMessage } from './Error'

export default {
  title: 'Components/Misc Control/errormessage',
  component: ErrorMessage,
}

const Template = args => <ErrorMessage {...args} />

export const Errormessage = Template.bind({})
Errormessage.args = {
  text: 'Select SKUs for inclusion list using Browse SKUs CTA',
}
