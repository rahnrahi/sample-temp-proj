import React from 'react'
import PropTypes from 'prop-types'

import { StyledMessage } from './Error.style'

export const ErrorMessage = ({ text, children, ...props }) => (
  <StyledMessage {...props}>{children ? children : text}</StyledMessage>
)

ErrorMessage.propTypes = {
  text: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
