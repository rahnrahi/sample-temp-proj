import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { ErrorMessage } from './Error'
import 'jest-styled-components'

afterEach(cleanup)

describe('<ErrorMessage/>', () => {
  const errorMessage = 'Email is a required field'
  it('should render error message correctly', () => {
    const { container } = render(<ErrorMessage text={errorMessage} />)
    expect(container).toHaveTextContent(errorMessage)
    expect(container).toMatchSnapshot()
  })
  it('should render error message with children', () => {
    const { container } = render(<ErrorMessage>{errorMessage}</ErrorMessage>)
    expect(container).toHaveTextContent(errorMessage)
    expect(container).toMatchSnapshot()
  })
})
