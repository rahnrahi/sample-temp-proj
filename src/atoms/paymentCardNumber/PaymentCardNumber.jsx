import React from 'react'
import PropTypes from 'prop-types'

import {
  Root,
  ImageContainer,
  CardImage,
  CardNumber,
} from './PaymentCardNumber.style'

export const PaymentCardNumber = ({ cardImage, cardNumber, ...rest }) => {
  const testId = rest['data-testid'] ?? rest.dataTestIda

  return (
    <Root {...rest}>
      <ImageContainer>
        <CardImage data-testid={`${testId}-image`} src={cardImage} />
      </ImageContainer>
      <CardNumber data-testid={`${testId}-number`}>{cardNumber}</CardNumber>
    </Root>
  )
}

PaymentCardNumber.propTypes = {
  cardImage: PropTypes.string,
  cardNumber: PropTypes.string,
}
