import styled from 'styled-components'
import theme from '../../shared/theme'

export const Root = styled.div`
  margin-bottom: 1rem;

  display: flex;
  align-items: center;
`

export const ImageContainer = styled.div`
  width: 1rem;

  border: 0.5px solid ${theme.palette.ui.neutral.grey5};
  background-color: ${theme.palette.brand.primary.white};
`

export const CardImage = styled.img`
  width: 100%;
  height: auto;
`

export const CardNumber = styled.div`
  margin-left: 1rem;

  ${theme.typography.h6}
  color: ${theme.palette.brand.primary.charcoal};
`
