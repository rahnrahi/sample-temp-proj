import React from 'react'
import { default as Collapse } from './index'
import PropTypes from 'prop-types'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const ComponentHeader = ({ isExpanded }) => (
  <div>Component Header {isExpanded ? '(expanded)' : '(collapsed)'}</div>
)
ComponentHeader.propTypes = {
  isExpanded: PropTypes.bool,
}

const Template = args => {
  return (
    <Collapse maxOpened={args.maxOpened}>
      <Collapse.Panel header={'Text as header'}>Content 1</Collapse.Panel>
      <Collapse.Panel header={<b>Element header</b>}>Content 2</Collapse.Panel>
      <Collapse.Panel header={ComponentHeader}>Content 3</Collapse.Panel>
    </Collapse>
  )
}

export const HeaderCustomization = Template.bind({})
HeaderCustomization.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)

export const AllowMultipleExpandedAtSameTime = Template.bind({})
AllowMultipleExpandedAtSameTime.args = {
  maxOpened: 2,
}
AllowMultipleExpandedAtSameTime.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
export default {
  title: 'Modules/Cards/List/Collapse',
  component: Collapse,
}
