import React from 'react'
import Collapse from './index'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'

afterEach(() => {
  cleanup()
})
const TestHeader = () => {
  return <div data-testid={'header-component'} />
}

describe('Collapse', () => {
  describe('Given text node as header', () => {
    it('Should render it correctly', () => {
      setup({}, 'Header text')
      const header = screen.getByTestId('test-header')
      expect(header).toHaveTextContent('Header text')
    })
  })
  describe('Given number node as header', () => {
    it('Should render it correctly', () => {
      setup({}, 123)
      const header = screen.getByTestId('test-header')
      expect(header).toHaveTextContent('123')
    })
  })
  describe('Given react element as header', () => {
    it('Should render it correctly', () => {
      setup({}, <div data-testid={'element-header'}>Header inside</div>)
      const header = screen.getByTestId('test-header')
      const headerInside = screen.getByTestId('element-header')
      expect(headerInside).toBeInTheDocument()
      expect(header).toHaveTextContent('Header inside')
    })
  })
  describe('Given component as header', () => {
    it('Should render it correctly', () => {
      setup({}, TestHeader)
      const header = screen.getByTestId('header-component')
      expect(header).toBeInTheDocument()
    })
  })
  describe('Given defaultExpandedSections property', () => {
    it('should render given sections expanded', () => {
      setup({ defaultExpandedSections: ['1'] }, 'Header')
      expect(screen.getByText('Collapse content')).toBeInTheDocument()
    })
    it('should render gracefully handle malformed value sections expanded', () => {
      expect(() =>
        setup({ defaultExpandedSections: null }, 'Header')
      ).not.toThrow()
    })
  })
  describe('Given maxOpened property', () => {
    it('should allow only 1 section to be expanded at same time, when maxOpened=1', () => {
      render(
        <Collapse>
          <Collapse.Panel
            id={'1'}
            header={'Header 01'}
            data-testid={'header-1'}
          >
            <div data-testid={'content-1'}>Collapse content 01</div>
          </Collapse.Panel>
          <Collapse.Panel
            id={'2'}
            header={'Header 02'}
            data-testid={'header-2'}
          >
            <div data-testid={'content-2'}>Collapse content 02</div>
          </Collapse.Panel>
        </Collapse>
      )
      const header1 = screen.getByText('Header 01')
      const header2 = screen.getByText('Header 02')
      fireEvent.click(header1)
      expect(screen.getByTestId('content-1')).toBeInTheDocument()
      fireEvent.click(header2)
      expect(screen.queryByTestId('content-1')).not.toBeInTheDocument()
      expect(screen.getByTestId('content-2')).toBeInTheDocument()
    })
    it('should allow only 2 sections to be expanded at same time, when maxOpened=2', () => {
      render(
        <Collapse maxOpened={2}>
          <Collapse.Panel
            id={'1'}
            header={'Header 01'}
            data-testid={'header-1'}
          >
            <div data-testid={'content-1'}>Collapse content 01</div>
          </Collapse.Panel>
          <Collapse.Panel
            id={'2'}
            header={'Header 02'}
            data-testid={'header-2'}
          >
            <div data-testid={'content-2'}>Collapse content 02</div>
          </Collapse.Panel>
          <Collapse.Panel
            id={'3'}
            header={'Header 03'}
            data-testid={'header-3'}
          >
            <div data-testid={'content-3'}>Collapse content 03</div>
          </Collapse.Panel>
        </Collapse>
      )
      const header1 = screen.getByText('Header 01')
      const header2 = screen.getByText('Header 02')
      const header3 = screen.getByText('Header 03')
      fireEvent.click(header1)
      expect(screen.getByTestId('content-1')).toBeInTheDocument()
      expect(screen.queryByTestId('content-2')).not.toBeInTheDocument()
      expect(screen.queryByTestId('content-3')).not.toBeInTheDocument()

      fireEvent.click(header2)
      expect(screen.queryByTestId('content-1')).toBeInTheDocument()
      expect(screen.queryByTestId('content-2')).toBeInTheDocument()
      expect(screen.queryByTestId('content-3')).not.toBeInTheDocument()

      fireEvent.click(header3)
      expect(screen.queryByTestId('content-1')).not.toBeInTheDocument()
      expect(screen.queryByTestId('content-2')).toBeInTheDocument()
      expect(screen.queryByTestId('content-3')).toBeInTheDocument()
    })
  })
  describe('Given onCollapse/onExpand properties', () => {
    it('should call onCollapse & onExpand callback when section expands and collapses', () => {
      const onCollapse = jest.fn()
      const onExpand = jest.fn()
      setup({ onCollapse, onExpand }, <div data-testid={'header'}>Header</div>)
      const header = screen.getByTestId('header')
      fireEvent.click(header)
      expect(onExpand).toHaveBeenCalledTimes(1)
      expect(onCollapse).toHaveBeenCalledTimes(0)
      expect(onExpand).toHaveBeenCalledWith('1', ['1'])
      fireEvent.click(header)
      expect(onExpand).toHaveBeenCalledTimes(1)
      expect(onCollapse).toHaveBeenCalledTimes(1)
      expect(onCollapse).toHaveBeenCalledWith('1', [])
    })
  })
  describe('Given defaultExpandedSections property', () => {
    it('should expand sections accordingly on initial render', () => {
      setup(
        { defaultExpandedSections: ['1'] },
        <div data-testid={'header'}>Header</div>
      )
      expect(screen.getByText('Collapse content')).toBeInTheDocument()
    })
  })
})

const setup = (props = {}, header = null) => {
  return render(
    <Collapse {...props}>
      <Collapse.Panel id={'1'} header={header} data-testid={'test-header'}>
        Collapse content
      </Collapse.Panel>
    </Collapse>
  )
}
