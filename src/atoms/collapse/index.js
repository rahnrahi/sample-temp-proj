import Panel from './Panel'
import PropTypes from 'prop-types'
import React, { createContext, useCallback, useMemo, useState } from 'react'

export const CollapseContext = createContext(null)

const Collapse = ({
  children,
  maxOpened = 1,
  defaultExpandedSections = [],
  onCollapse,
  onExpand,
}) => {
  const [expandedSections, setExpandedSections] = useState(
    Array.isArray(defaultExpandedSections)
      ? defaultExpandedSections.slice(0, Math.max(maxOpened, 0))
      : []
  )

  const handleSectionExpand = useCallback(
    (prevSections, sectionKey) => {
      const allSections = [...prevSections, sectionKey]
      const collapsedSections = allSections.slice(0, Math.min(-maxOpened, 0))
      const updatedValue = allSections.slice(Math.min(-maxOpened, 0))
      if (typeof onExpand === 'function') {
        onExpand(sectionKey, updatedValue)
      }
      if (typeof onCollapse === 'function') {
        for (const section of collapsedSections) {
          onCollapse(section, updatedValue)
        }
      }
      return updatedValue
    },
    [onCollapse, onExpand, maxOpened]
  )
  const handleSectionCollapse = useCallback(
    (prevSections, sectionKey) => {
      const updatedValue = prevSections
        .filter(currentKey => currentKey !== sectionKey)
        .slice(Math.min(-maxOpened, 0))
      if (typeof onCollapse === 'function') {
        onCollapse(sectionKey, updatedValue)
      }
      return updatedValue
    },
    [maxOpened, onCollapse]
  )

  const expandSection = useCallback(
    sectionKey => {
      setExpandedSections(prevSections => {
        return handleSectionExpand(prevSections, sectionKey)
      })
    },
    [handleSectionExpand]
  )

  const collapseSection = useCallback(
    sectionKey => {
      setExpandedSections(prevSections => {
        return handleSectionCollapse(prevSections, sectionKey)
      })
    },
    [handleSectionCollapse]
  )

  const toggleSection = useCallback(
    sectionKey => {
      setExpandedSections(prevSections => {
        const currentValue = prevSections.find(
          currentKey => currentKey === sectionKey
        )
        if (currentValue !== undefined && currentValue !== null) {
          return handleSectionCollapse(prevSections, sectionKey)
        }
        return handleSectionExpand(prevSections, sectionKey)
      })
    },
    [handleSectionCollapse, handleSectionExpand]
  )

  const contextValue = useMemo(() => {
    return {
      expandSection,
      expandedSections,
      collapseSection,
      toggleSection,
    }
  }, [collapseSection, expandSection, expandedSections, toggleSection])

  return (
    <CollapseContext.Provider value={contextValue}>
      {React.Children.map(children, (child, idx) => {
        const key = child.props.id || idx.toString()
        if (React.isValidElement(child)) {
          return React.cloneElement(child, { id: key, key })
        }
        return child
      })}
    </CollapseContext.Provider>
  )
}

Collapse.propTypes = {
  maxOpened: PropTypes.number,
  onCollapse: PropTypes.func,
  onExpand: PropTypes.func,
  defaultExpandedSections: PropTypes.array,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

Collapse.Panel = Panel
export default Collapse
