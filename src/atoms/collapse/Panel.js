import PropTypes from 'prop-types'
import { CollapseContext } from './index'
import React, { useCallback, useContext, useMemo } from 'react'
import ReactIs from 'react-is'

const Panel = ({ id, children, header, className, ...otherProps }) => {
  const ctx = useContext(CollapseContext)
  const toggleSection = useCallback(() => ctx?.toggleSection(id), [ctx, id])
  const isExpanded = useMemo(() => {
    return ctx?.expandedSections.includes(id)
  }, [ctx?.expandedSections, id])
  const headerElement = useMemo(() => {
    if (React.isValidElement(header)) {
      return React.cloneElement(header, { isExpanded })
    }
    if (typeof header === 'string') {
      return header
    }
    if (ReactIs.isValidElementType(header)) {
      return React.createElement(header, { isExpanded })
    }
    return header
  }, [header, isExpanded])
  return (
    <section className={className} {...otherProps}>
      <header onClick={toggleSection}>{headerElement}</header>
      {isExpanded && children}
    </section>
  )
}

Panel.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  header: PropTypes.oneOfType([PropTypes.element, PropTypes.elementType]),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
export default Panel
