import styled, { css } from 'styled-components'
import { theme } from '../../shared'

const withFloatCss = css`
  top: -8px;
  color: ${theme.palette.brand.primary.gray};
  ${({ disabled }) => disabled && `color: ${theme.palette.brand.primary.gray}`};
`

const withoutFloatCss = css`
  top: 14px;
  color: ${theme.palette.brand.primary.charcoal};
  ${({ disabled }) => disabled && `color: ${theme.palette.ui.neutral.grey7}`};
`

export const StyledTimepicker = styled.div`
  padding-top: 10px;
  width: ${({ width }) => width && width};
  .react-datepicker {
    border: none;
  }
  .react-datepicker__triangle {
    display: none;
  }
  .react-datepicker__header {
    display: none;
  }
  .react-datepicker__time-container {
    width: 200px;
    background: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.neutral.grey2};
    border-radius: 4px;
    box-shadow: 0px 6px 16px rgba(115, 127, 143, 0.16);
    overflow: hidden;
  }
  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box {
    width: 200px;
    overflow: hidden;
    text-align: left;
  }
  .react-datepicker__time-list {
    height: 176px !important;
    ::-webkit-scrollbar {
      width: 4px;
    }

    ::-webkit-scrollbar-track {
      background: transparent;
    }

    ::-webkit-scrollbar-thumb {
      background: ${theme.palette.brand.primary.gray};
      border-radius: 10px;
    }
    &:focus {
      outline: none !important;
    }
  }
  .react-datepicker__time-list-item {
    ${theme.typography.h6.css};
    height: 44px !important;
    line-height: 44px;
    padding: 0px 24px !important;
    &:hover {
      background-color: ${theme.palette.ui.neutral.grey5} !important;
    }
    &:focus {
      outline: none;
    }
  }
  .react-datepicker__time-list-item--selected {
    background-color: ${theme.palette.brand.primary.white} !important;
    color: ${theme.palette.brand.primary.charcoal} !important;
    font-weight: normal !important;
    &:hover {
      background-color: ${theme.palette.ui.neutral.grey5} !important;
    }
  }
  .react-datepicker-popper[data-placement^='bottom'] {
    margin-top: 4px;
  }

  .react-datepicker__input-container {
    position: relative;
    ${({ disabled }) => disabled && `user-select:none`};
    :before {
      position: absolute;
      content: ${({ label }) => `'${label}'`};
      transition: all 0.1s ease;
      ${({ isFloat }) => (isFloat ? withFloatCss : withoutFloatCss)};
      ${theme.typography.h6.css};
      line-height: 17px;
      z-index: 11;
    }

    input[type='text'] {
      width: ${({ width }) => width && width};
      outline: none !important;
      border: none !important;
      border-bottom: ${({ disabled }) =>
        `1px solid ${
          disabled
            ? theme.palette.ui.neutral.grey7
            : theme.palette.brand.primary.charcoal
        } !important;`};
      padding: 16px 0;
      box-sizing: border-box;
      ${({ disabled }) =>
        disabled &&
        `
				pointer-events: none;
				opacity: 0.5;
				color: inherit;
			`}
    }
  }
`
