import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import enUs from 'date-fns/locale/en-US'
import ReactDatePicker, {
  registerLocale,
  setDefaultLocale,
} from 'react-datepicker'
import { isFunction } from '../../shared/utils'

import { StyledTimepicker } from './Timepicker.style'

registerLocale('en-US', enUs)
setDefaultLocale('en-US', enUs)

export const Timepicker = ({
  label,
  onChange,
  timeIntervals,
  date,
  width,
  disabled,
  ...props
}) => {
  const [startDate, setStartDate] = useState(date)
  const [focus, setFocus] = useState(false)

  useEffect(() => {
    setStartDate(date)
  }, [date])

  const handleDateChange = date => {
    setStartDate(date)
    isFunction(onChange) && onChange(date)
  }

  const handleFocus = useCallback(() => setFocus(true), [])
  const handleBlur = useCallback(() => setFocus(false), [])

  useEffect(() => {
    setTimeList()
  }, [focus])

  return (
    <StyledTimepicker
      label={label}
      width={width}
      isFloat={focus || startDate !== null}
      disabled={disabled}
    >
      <ReactDatePicker
        {...props}
        customTimeInput={
          <CustomInput value={startDate} onChange={setStartDate} />
        }
        selected={startDate}
        onChange={handleDateChange}
        onInputClick={() => setTimeList()}
        onChangeRaw={e => !props.canUserTypeIntoInput && e.preventDefault()}
        onFocus={handleFocus}
        onBlur={handleBlur}
        locale='en-US'
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={timeIntervals}
        timeCaption='Time'
        dateFormat='hh:mm aa'
        timeFormat='hh:mm aa'
        popperModifiers={{
          preventOverflow: {
            enabled: false,
            escapeWithReference: false,
            boundariesElement: 'scrollParent',
          },
          hide: {
            enabled: false,
          },
          flip: {
            behavior: ['bottom'],
          },
        }}
        popperPlacement='bottom-start'
      />
    </StyledTimepicker>
  )
}

Timepicker.defaultProps = {
  label: '',
  timeIntervals: 15,
  width: '118px',
  disabled: false,
  canUserTypeIntoInput: false,
}

Timepicker.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  /** set date externally */
  date: PropTypes.instanceOf(Date),
  timeIntervals: PropTypes.number,
  width: PropTypes.string,
  disabled: PropTypes.bool,
  canUserTypeIntoInput: PropTypes.bool,
}

const CustomInput = ({ value, onChange }) => {
  return <input value={value} onChange={e => onChange(e.target.value)} />
}

CustomInput.propTypes = {
  value: PropTypes.instanceOf(Date),
  onChange: PropTypes.func,
}

const appendText = (item, str) => (item.innerHTML += str)

const setTimeList = () => {
  const listItems = document.querySelectorAll(
    '.react-datepicker__time-list-item'
  )
  listItems.length !== 0 &&
    listItems.forEach((item, i) => {
      const meridiem = i < listItems.length / 2 ? 'AM' : 'PM'
      !item.innerHTML.includes(meridiem) && appendText(item, meridiem)
    })
}
