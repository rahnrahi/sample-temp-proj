import React from 'react'
import { render, fireEvent, cleanup } from '@testing-library/react'
import { Timepicker } from './Timepicker'

afterEach(cleanup)

const fn = jest.fn()

describe('<Timepicker/>', () => {
  it('should render Timepicker input correctly', () => {
    const { container } = render(
      <Timepicker
        label='Start-time'
        date={new Date('2017-01-01 00:00')}
        onChange={fn}
      />
    )
    const input = container.querySelector('input')
    expect(input).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('should render Timepicker with timelist correctly', () => {
    const { container } = render(
      <Timepicker
        label='Start-time'
        date={new Date('2017-01-01 00:00')}
        intervals={15}
        onChange={fn}
      />
    )
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: new Date('2017-01-02 00:00') } })
    fireEvent.click(input)
    expect(container).toMatchSnapshot()
  })

  it('should render disabled Timepicker without value', () => {
    const { container } = render(
      <Timepicker
        label='Start-time'
        date={null}
        intervals={15}
        onChange={fn}
        disabled
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render disabled Timepicker with value', () => {
    const { container } = render(
      <Timepicker
        label='Start-time'
        date={new Date('2017-01-01 00:00')}
        intervals={15}
        onChange={fn}
        disabled
      />
    )
    expect(container).toMatchSnapshot()
  })
})
