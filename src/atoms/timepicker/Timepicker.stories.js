import React from 'react'
import styled from 'styled-components'
import { Timepicker as TimePicker } from './Timepicker'
import TimePickersDocs from '../../../docs/CalendarAndTimePicker.mdx'
import {
  TIMEPICKER_DESIGN,
  TIMEPICKER_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const design = getDesignSpecifications(
  TIMEPICKER_DESIGN,
  TIMEPICKER_PRESENTATION
)['design']

export default {
  title: 'Modules/Calendar & Time Picker/Timepicker',
  component: TimePicker,
  argTypes: {},
  parameters: {
    docs: {
      page: TimePickersDocs,
    },
  },
}

const StyledContainer = styled.div`
  margin-top: '50px';
`

const Template = args => (
  <StyledContainer>
    <TimePicker {...args} />
  </StyledContainer>
)

export const Timepicker = Template.bind({})

Timepicker.args = {
  label: 'Start-time',
  width: '188px',
  date: null,
  timeIntervals: 15,
  onChange: dateTimeStr => console.log(dateTimeStr),
}
Timepicker.parameters = { design }

export const DisabledTimepickerWithTime = Template.bind({})

DisabledTimepickerWithTime.args = {
  label: 'Start-time',
  width: '188px',
  date: new Date(),
  timeIntervals: 15,
  disabled: true,
}
DisabledTimepickerWithTime.parameters = { design }

export const DisabledTimepickerWithoutTime = Template.bind({})

DisabledTimepickerWithoutTime.args = {
  label: 'Start-time',
  width: '188px',
  date: null,
  timeIntervals: 15,
  disabled: true,
}
DisabledTimepickerWithoutTime.parameters = { design }
