export const InputSize = Object.freeze({
  LARGE: 'Large',
  SMALL: 'Small',
})
