import styled from 'styled-components'
import { theme } from '../../shared'
import { InputSize } from './InputWithSave.enums'

export const InputWithSaveComponent = styled.div`
  display: flex;
  font-family: ${theme.typography.h4.fontFamily};
  &,
  * {
    box-sizing: border-box;
  }
`

export const StyledInputContainer = styled.div`
  display: flex;
  min-width: 16px;
  width: ${({ width }) => width};
  flex-direction: column;
  .error {
    margin-top: 4px;
    ${theme.typography.caption};
    font-weight: 400;
    color: ${theme.palette.ui.cta.red};
    word-wrap: break-word;
  }
`

export const StyledIdleText = styled.div`
  width: 100%;
  padding-top: 12px;
  padding-bottom: 4px;
  cursor: text;
  color: ${({ size }) =>
    size === InputSize.LARGE
      ? theme.palette.brand.primary.charcoal
      : theme.palette.brand.primary.gray};
  ${({ size }) =>
    size === InputSize.LARGE ? theme.typography.h4 : theme.typography.h6}
  &:hover {
    color: ${({ size }) =>
      size === InputSize.SMALL && theme.palette.brand.primary.charcoal};
    border-bottom: 1px ${theme.palette.brand.primary.charcoal} solid;
  }
  ${({ isEdit }) =>
    isEdit && {
      display: 'none',
    }}
`

export const StyledInput = styled.input`
  width: 100%;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  padding-left: 0px;
  padding-top: 12px;
  padding-bottom: 4px;
  padding-right: 0px;
  word-break: normal;
  color: ${theme.palette.brand.primary.charcoal};
  background-color: ${theme.palette.brand.primary.white};
  outline: none;
  ${({ size }) =>
    size === InputSize.LARGE ? theme.typography.h4 : theme.typography.h6}

  ${({ isEdit }) =>
    !isEdit && {
      display: 'none',
    }}

  border: none;
  border-bottom: 1px
    ${({ error }) =>
      error ? theme.palette.ui.error.one : theme.palette.brand.primary.gray}
    solid;

  &:hover {
    border-bottom: 1px
      ${({ error }) =>
        error
          ? theme.palette.ui.error.one
          : theme.palette.brand.primary.charcoal}
      solid;
  }
  &:focus {
    border-bottom: 1px ${theme.palette.ui.cta.blue} solid;
  }
`

export const StyledButtonGroup = styled.div`
  margin-left: 16px;
  display: flex;
  width: 72px;
  justify-content: space-between;
  .button {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    border: none;
    width: 32px;
    height: 32px;
    cursor: pointer;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
  }
  .save-button {
    background-color: ${theme.palette.ui.cta.blue};
    svg {
      stroke: ${theme.palette.brand.primary.white};
    }
    &:disabled,
    &[disabled] {
      opacity: 0.8;
      cursor: default;
    }
  }
  .cancel-button {
    border: 1px solid black;
    background-color: ${theme.palette.brand.primary.white};
    svg {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }
`
