import React, { useState } from 'react'
import { InputWithSave } from './InputWithSave'
import {
  INPUT_SAVE_DESIGN,
  INPUT_SAVE_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

import InputWithSaveDoc from '../../../docs/InputWithSave.mdx'

export default {
  title: 'Components/Input Fields/Single Line/With Save',
  component: InputWithSave,
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
      page: InputWithSaveDoc,
    },
  },
}

const design = getDesignSpecifications(
  INPUT_SAVE_DESIGN,
  INPUT_SAVE_PRESENTATION
)['design']

const Template = args => {
  const [value, setValue] = useState('')
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  // Example validator
  const validate = function (val) {
    return val !== undefined && (val.length > 5 || val.length == 0)
  }

  // Example onSave handler
  const onSaveHandler = function (currentValue) {
    if (!validate(currentValue)) {
      setError(true)
      setErrorMessage('Unable to save, length should not be less than 6')
    } else {
      setError(false)
      if (currentValue.length > 0) {
        setValue(currentValue)
      }
    }
  }

  // Example onChange handler
  const onChange = e => {
    if (e.target && e.target.value && !validate(e.target.value)) {
      setError(true)
      setErrorMessage('Length is less than 6')
    } else {
      setError(false)
    }
  }

  return (
    <InputWithSave
      value={value}
      error={error}
      errorMessage={errorMessage}
      onSave={onSaveHandler}
      onChange={onChange}
      size={args.size}
      {...args}
    />
  )
}

export const Large = Template.bind({})

Large.args = {
  size: 'Large',
  inputId: 'input-with-save-large',
}
Large.parameters = { design }

export const Small = Template.bind({})

Small.args = {
  size: 'Small',
  inputId: 'input-with-save-small',
}
Small.parameters = { design }
