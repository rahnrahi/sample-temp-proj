import React from 'react'
import { InputWithSave } from '.'
import { InputSize } from './InputWithSave.enums'
import { fireEvent, render, screen } from '@testing-library/react'
import { Simulate } from 'react-dom/test-utils'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(() => {
  jest.clearAllMocks()
})
const commonProps = {
  onSave: jest.fn(),
  onBlur: jest.fn(),
  onFocus: jest.fn(),
  onChange: jest.fn(),
}

const LargeInputProps = {
  size: InputSize.LARGE,
  inputId: 'input-with-save-large',
  tabIndex: 0,
  ...commonProps,
}

const SmallInputProps = {
  size: InputSize.SMALL,
  inputId: 'input-with-save-small',
  tabIndex: 0,
  ...commonProps,
}

describe('<InputWithSave/> - Large', () => {
  it('renders Input With Save component - Large', () => {
    const { container } = render(<InputWithSave {...LargeInputProps} />)
    expect(container).toMatchSnapshot()
  })

  it('should render the input component when clicked on the section', () => {
    render(<InputWithSave {...LargeInputProps} />)

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )

    const saveChangesButton = screen.getByTestId(
      `${LargeInputProps.inputId}-save-changes-${LargeInputProps.tabIndex}`
    )
    const cancelChangesButton = screen.getByTestId(
      `${LargeInputProps.inputId}-cancel-changes-${LargeInputProps.tabIndex}`
    )

    expect(inputComponent).toBeInTheDocument()
    expect(saveChangesButton).toBeInTheDocument()
    expect(cancelChangesButton).toBeInTheDocument()
  })

  it('clicking on save button should fire the onSave function', () => {
    render(<InputWithSave {...LargeInputProps} />)
    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )
    expect(inputComponent).toBeInTheDocument()

    const saveChangesButton = screen.getByTestId(
      `${LargeInputProps.inputId}-save-changes-${LargeInputProps.tabIndex}`
    )
    expect(saveChangesButton).toBeInTheDocument()
    Simulate.change(inputComponent, { target: { value: 'Something' } })
    fireEvent.click(saveChangesButton)
    expect(LargeInputProps.onSave.mock.calls.length).toEqual(1)
  })

  it('should write in the input field', () => {
    render(<InputWithSave {...LargeInputProps} />)
    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()

    expect(idleText).toBeInTheDocument()
    fireEvent.click(idleText)
    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )
    Simulate.change(inputComponent, { target: { value: 'Something' } })
    expect(inputComponent.value).toEqual('Something')
  })

  it('should call the onChange function when value is changed inside the input field', () => {
    render(<InputWithSave {...LargeInputProps} />)

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )
    expect(inputComponent).toBeInTheDocument()

    Simulate.change(inputComponent, { target: { value: 'S' } })
    expect(LargeInputProps.onChange.mock.calls.length).toBe(1)
  })

  it('should render the error with alert attribute', () => {
    render(
      <InputWithSave
        error={true}
        errorMessage={'something'}
        {...LargeInputProps}
      />
    )
    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    const errorComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-error-${LargeInputProps.tabIndex}`
    )
    expect(errorComponent).toBeInTheDocument()
    expect(errorComponent).toHaveAttribute('role', 'alert')
  })

  it('should not render the error message', () => {
    render(<InputWithSave {...LargeInputProps} />)

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )

    expect(idleText).toBeInTheDocument()

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    expect(() =>
      screen.getByTestId(
        `${LargeInputProps.inputId}-error-${LargeInputProps.tabIndex}`
      )
    ).toThrowError()
  })

  it('should pass the currentValue to onSave function on clicking the save button', () => {
    render(<InputWithSave {...LargeInputProps} />)

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )

    fireEvent.keyPress(idleText, { key: 'Enter', code: 'Enter', charCode: 13 })

    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )
    expect(inputComponent).toBeInTheDocument()

    const saveChangesButton = screen.getByTestId(
      `${LargeInputProps.inputId}-save-changes-${LargeInputProps.tabIndex}`
    )
    expect(saveChangesButton).toBeInTheDocument()
    Simulate.change(inputComponent, { target: { value: 'Something' } })
    fireEvent.click(saveChangesButton)
    expect(LargeInputProps.onSave.mock.calls[0][0]).toEqual('Something')
  })

  it('clicking on cancel button should show the idle text again', () => {
    render(<InputWithSave {...LargeInputProps} />)

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )
    expect(idleText).toBeInTheDocument()
    fireEvent.click(idleText)

    const inputComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-text-input-${LargeInputProps.tabIndex}`
    )
    expect(inputComponent).toBeInTheDocument()

    const cancelChangesButton = screen.getByTestId(
      `${LargeInputProps.inputId}-cancel-changes-${LargeInputProps.tabIndex}`
    )
    expect(cancelChangesButton).toBeInTheDocument()
    fireEvent.click(cancelChangesButton)
    expect(idleText).toBeInTheDocument()
  })

  it('should show an error message when the error prop is set to true, and error message is passed', () => {
    render(
      <InputWithSave
        error={true}
        errorMessage='Something went wrong'
        {...LargeInputProps}
      />
    )

    const idleText = screen.getByTestId(
      `${LargeInputProps.inputId}-idle-text-${LargeInputProps.tabIndex}`
    )

    expect(idleText).toBeInTheDocument()
    fireEvent.click(idleText)

    const errorComponent = screen.getByTestId(
      `${LargeInputProps.inputId}-error-${LargeInputProps.tabIndex}`
    )
    expect(errorComponent).toBeInTheDocument()
  })
})

describe('<InputWithSave/> - Small', () => {
  it('renders Input With Save component - Small', () => {
    const { container } = render(<InputWithSave {...SmallInputProps} />)
    expect(container).toMatchSnapshot()
  })
})
