import React, { useState, forwardRef, useRef, useEffect } from 'react'
import { PropTypes } from 'prop-types'
import {
  StyledInput,
  StyledInputContainer,
  InputWithSaveComponent,
  StyledButtonGroup,
  StyledIdleText,
} from './InputWithSave.styles'
import { Icon } from '../icon/Icon'
import useLocaleDirection from '../../hooks/render-rtl'

const isFunction = x => typeof x === 'function'

const InputWithSave = forwardRef(
  (
    {
      tabIndex,
      placeholder,
      error,
      errorMessage,
      onSave,
      onBlur,
      onFocus,
      onChange,
      className,
      locale,
      value,
      size,
      width,
      inputId,
      ...containerPropRest
    },
    ref
  ) => {
    const [isEdit, setIsEdit] = useState(false)
    const [currentValue, setCurrentValue] = useState(value)
    const inputRef = useRef(ref || null)
    const dir = useLocaleDirection(locale)

    useEffect(() => {
      isEdit && inputRef.current.focus()
    }, [isEdit])

    useEffect(() => {
      setCurrentValue(value)
    }, [value])

    const activateEdit = () => {
      setIsEdit(true)
    }

    const deactivateEdit = () => {
      setIsEdit(false)
    }

    const handleInputChange = e => {
      setCurrentValue(e.target.value)
      isFunction(onChange) && onChange(e)
    }

    const onSaveHandler = () => {
      isFunction(onSave) && onSave(currentValue)
      !error && deactivateEdit()
    }

    const ErrorComponent = () => (
      <p
        className='error'
        role='alert'
        id={`${inputId}-error-${tabIndex}`}
        data-testid={`${inputId}-error-${tabIndex}`}
      >
        {errorMessage}
      </p>
    )

    return (
      <InputWithSaveComponent
        size={size}
        className={className}
        id={`${inputId}-component-${tabIndex}`}
        data-testid={`${inputId}-component-${tabIndex}`}
      >
        <StyledInputContainer
          width={width}
          id={`${inputId}-container-${tabIndex}`}
          data-testid={`${inputId}-container-${tabIndex}`}
          size={size}
          {...containerPropRest}
        >
          <StyledIdleText
            id={`${inputId}-idle-text-${tabIndex}`}
            data-testid={`${inputId}-idle-text-${tabIndex}`}
            tabIndex={tabIndex}
            size={size}
            onClick={activateEdit}
            onKeyPress={e => e.key === 'Enter' && activateEdit()}
            className='idle-section'
            ref={ref}
            isEdit={isEdit}
            aria-label={`${value || placeholder}, press enter to edit`}
          >
            {value || placeholder}
          </StyledIdleText>
          <StyledInput
            autoFocus
            id={`${inputId}-text-input-${tabIndex}`}
            data-testid={`${inputId}-text-input-${tabIndex}`}
            type='text'
            dir={dir}
            ref={inputRef}
            size={size}
            placeholder={placeholder}
            tabIndex={tabIndex}
            value={currentValue}
            error={error}
            onChange={handleInputChange}
            onBlur={e => isFunction(onBlur) && onBlur(e)}
            onFocus={e => isFunction(onFocus) && onFocus(e)}
            isEdit={isEdit}
          />
          {isEdit && error && <ErrorComponent />}
        </StyledInputContainer>
        {isEdit && (
          <StyledButtonGroup>
            <button
              type='button'
              aria-label='Press enter to Save Changes'
              id={`${inputId}-save-changes-${tabIndex}`}
              data-testid={`${inputId}-save-changes-${tabIndex}`}
              onClick={onSaveHandler}
              tabIndex={tabIndex}
              className='button save-button'
              aria-invalid={error}
              aria-errormessage={`${inputId}-error-${tabIndex}`}
            >
              <Icon size={16} iconName='Checkmark' />
            </button>
            <button
              className='button cancel-button'
              type='button'
              aria-label='Press enter to Cancel Changes'
              tabIndex={tabIndex}
              id={`${inputId}-cancel-changes-${tabIndex}`}
              data-testid={`${inputId}-cancel-changes-${tabIndex}`}
              onClick={deactivateEdit}
            >
              <Icon size={16} iconName='Close' />
            </button>
          </StyledButtonGroup>
        )}
      </InputWithSaveComponent>
    )
  }
)

InputWithSave.propTypes = {
  /** The placeholder text for the input field */
  placeholder: PropTypes.string,
  /** TabIndex for the Input Field */
  tabIndex: PropTypes.number,
  /** Toggle Error, only shows the error in edit mode */
  error: PropTypes.bool,
  /** The error message to show, only shown when the error value is set to true */
  errorMessage: PropTypes.string,
  /** Value of the Input Field */
  value: PropTypes.string,
  /** Fixed width of the input field */
  width: PropTypes.string,
  /** Unique id for the component */
  inputId: PropTypes.string,
  /** Size of the input field */
  size: PropTypes.oneOf(['Large', 'Small']),
  className: PropTypes.string,
  /** onChange(event): On changing the value of the input, the event is passed as an argument to this function */
  onChange: PropTypes.func,
  /** onSave(currentValue): Gets triggered when the save button is clicked, the current value in the input field is passed as an argument */
  onSave: PropTypes.func,
  /** onBlur(event): Behavior on focusing out of the input field, the event is passed as an argument */
  onBlur: PropTypes.func,
  /** onFocus(event) Behavior on focusing in to the input field, the event is passed as an argument */
  onFocus: PropTypes.func,
  /** Locale code */
  locale: PropTypes.string,
}

InputWithSave.defaultProps = {
  placeholder: 'Placeholder Text',
  tabIndex: 0,
  size: 'Large',
  width: '484px',
  inputId: 'input-with-save',
  error: false,
  locale: 'en',
}

export { InputWithSave }
