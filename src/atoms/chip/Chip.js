import React from 'react'
import PropTypes from 'prop-types'
import { StyledChip, StyledCloseButton, ErrorPill } from './Chip.styles'
import CloseBtn from '../../assets/images/cross'

export const Chip = ({
  children,
  onRemove,
  showRemoveIcon,
  disabled,
  isSelectable,
  isSelected,
  onClick,
  pillLabel,
  ...props
}) => {
  const handleOnChipClick = e => {
    e.preventDefault()
    if (isSelectable) {
      onClick({ label: children, isSelected: !isSelected })
    }
  }

  const handleOnRemoveClick = e => {
    e.stopPropagation()
    onRemove(e, props)
  }

  return (
    <StyledChip
      data-testid='chip'
      disabled={disabled}
      onClick={handleOnChipClick}
      isSelected={isSelectable && isSelected}
      {...props}
    >
      {children}
      {pillLabel && <ErrorPill data-testid='pill' text={pillLabel} />}
      {showRemoveIcon && (
        <StyledCloseButton disabled={disabled}>
          <CloseBtn
            data-testid='chip_remove_icon'
            onClick={handleOnRemoveClick}
          />
        </StyledCloseButton>
      )}
    </StyledChip>
  )
}

Chip.defaultProps = {
  disabled: false,
  onRemove: () => null,
  showRemoveIcon: true,
  isSelectable: false,
  isSelected: false,
  isError: false,
  pillLabel: '',
}

Chip.propTypes = {
  /** Chip can be element, string, number boolean  */
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.element,
    PropTypes.node,
  ]).isRequired,
  /** a callback function on Click on Remove icon which will contain both event and id 'e => onClose(e, id)'*/
  onRemove: PropTypes.func.isRequired,
  /** a boolean value to disable */
  disabled: PropTypes.bool,
  /** A value to enable remove Icon */
  showRemoveIcon: PropTypes.bool,
  isSelectable: PropTypes.bool,
  isSelected: PropTypes.bool,
  onClick: PropTypes.func,
  /** A boolean value to show error state*/
  isError: PropTypes.bool,
  /** A string value to show pill inside chip*/
  pillLabel: PropTypes.string,
}
