import styled, { css } from 'styled-components'
import { theme } from '../../shared'
import { Pill } from '../pill/Pill'

const errorStyle = css`
  border-color: ${theme.palette.ui.cta.red};
  background: rgba(213, 0, 0, 0.2);
`

export const StyledChip = styled.button`
  ${theme.typography.h6.css};
  max-height: 33px;
  background: ${`${theme.palette.brand.primary.gray}10`};
  color: ${theme.palette.brand.primary.charcoal};
  padding: 8px 16px;
  border-radius: 20px;
  white-space: nowrap;
  border: 1px solid transparent;
  outline: none;
  cursor: pointer;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  margin-right: 8px;
  ${({ isError }) => isError && `${errorStyle}`};
  :hover {
    background: ${`${theme.palette.brand.primary.gray}30`};
  }
  :focus {
    border: 1px solid ${theme.palette.ui.cta.yellow};
  }
  :active {
    border: 1px solid transparent;
    background: ${theme.palette.ui.neutral.grey1};
    color: white;
    svg {
      path {
        fill: white;
      }
    }
  }
  ${({ isSelected }) =>
    isSelected &&
    css`
      background: ${theme.palette.ui.neutral.grey1};
      color: ${theme.palette.brand.primary.white};

      :hover {
        background: ${theme.palette.ui.neutral.grey1}cc;
      }
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      pointer-events: none;
      color: #6e6e6e;
      background-color: ${theme.palette.ui.neutral.grey5};
    `}
`
export const StyledCloseButton = styled.span`
  margin-left: 10px;

  svg {
    ${({ disabled }) => disabled && `fill: #6E6E6E;`};
  }
`
export const ErrorPill = styled(Pill)`
  max-height: 24px;
  margin: 4px 0 4px 10px;
  text-transform: none;
  color: ${theme.palette.brand.primary.charcoal};
  background-color: ${theme.palette.brand.primary.white};
`
