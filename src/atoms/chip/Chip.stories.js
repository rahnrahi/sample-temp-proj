import React from 'react'

import { Chip } from './Chip'
import ChipDocs from '../../../docs/Chip.mdx'
import {
  CHIPS_DESIGN,
  CHIPS_PRESENTATION,
  DESIGN_TAB_MOCKUP,
  DESIGN_TAB_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Chip',
  component: Chip,
  argTypes: {
    children: { control: 'text' },
    disabled: { control: 'boolean' },
    isSelected: { control: 'boolean' },
    isSelectable: { control: 'boolean' },
  },
  parameters: {
    docs: {
      page: ChipDocs,
    },
  },
}

const Template = args => <Chip {...args} />

export const ChipComponent = Template.bind({})
ChipComponent.args = {
  children: 'Chip Label',
  id: 123,
  onRemove: (e, props) => console.log(e, props),
}
ChipComponent.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHIPS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHIPS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ChipWithPill = Template.bind({})
ChipWithPill.args = {
  children: 'Chip Label',
  id: 123,
  onRemove: (e, props) => console.log(e, props),
  pillLabel: 'Pill Label',
}
ChipWithPill.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHIPS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHIPS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ChipWithError = Template.bind({})
ChipWithError.args = {
  children: 'Chip Label',
  id: 123,
  onRemove: (e, props) => console.log(e, props),
  isError: true,
}
ChipWithError.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHIPS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHIPS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ChipWithPillAndError = Template.bind({})
ChipWithPillAndError.args = {
  children: 'Chip Label',
  id: 123,
  onRemove: (e, props) => console.log(e, props),
  isError: true,
  pillLabel: 'Pill Label',
}
ChipWithPillAndError.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: CHIPS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: CHIPS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
