import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Chip } from './Chip'
import 'jest-styled-components'
import { theme } from '../../shared'

afterEach(() => {
  cleanup()
  jest.resetAllMocks()
})

const onRemove = jest.fn()
const onClick = jest.fn()

describe('<Chip/>', () => {
  it('renders Chip component correctly', () => {
    const { container } = render(<Chip onRemove={onRemove}>Chip Label</Chip>)
    expect(container).toMatchSnapshot()
  })

  it('close action must call fireEvent.click', () => {
    const { getByTestId } = render(
      <Chip id='chip' label='Chip Label' onRemove={onRemove} />
    )
    const chip = getByTestId('chip_remove_icon')
    fireEvent.click(chip.firstElementChild, {})
    expect(onRemove).toHaveBeenCalledTimes(1)
  })

  it('should be selected if isSelectable is true', () => {
    const mockLabel = 'My Chip'
    const { getByTestId } = render(
      <Chip
        onRemove={onRemove}
        onClick={onClick}
        isSelected={false}
        isSelectable
      >
        {mockLabel}
      </Chip>
    )

    const chip = getByTestId('chip')
    fireEvent.click(chip)

    expect(onClick).toHaveBeenCalledTimes(1)
    expect(onClick).toHaveBeenCalledWith({
      label: mockLabel,
      isSelected: true,
    })
  })

  it('should have dark background if isSelected is true', () => {
    const { getByTestId } = render(
      <Chip onRemove={onRemove} onClick={onClick} isSelected isSelectable>
        Label
      </Chip>
    )

    const chip = getByTestId('chip')
    expect(chip).toHaveStyle(`background:${theme.palette.ui.neutral.grey1}`)
  })

  it('should not call onClick if isSelectable is false and background should not change', () => {
    const { getByTestId } = render(
      <Chip
        onRemove={onRemove}
        onClick={onClick}
        isSelectable={false}
        isSelected
      >
        Chip Label
      </Chip>
    )

    const chip = getByTestId('chip')
    expect(chip).toHaveStyle(`background:${theme.palette.brand.primary.gray}10`)

    fireEvent.click(chip)
    expect(onClick).toHaveBeenCalledTimes(0)
  })

  it('should render chip with error state without pill label correctly', () => {
    const { container, queryByTestId } = render(
      <Chip onRemove={onRemove} onClick={onClick} isError>
        Chip Label
      </Chip>
    )
    expect(queryByTestId('pill')).not.toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('should render chip with error state with pill label correctly', () => {
    const { container, getByTestId } = render(
      <Chip
        onRemove={onRemove}
        onClick={onClick}
        isError
        pillLabel='Pill Label'
      >
        Chip Label
      </Chip>
    )
    expect(getByTestId('pill')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
