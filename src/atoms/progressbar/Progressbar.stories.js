import React from 'react'
import { Progressbar } from './Progressbar'
import ProgressTrackersDocs from '../../../docs/ProgressTrackers.mdx'
import {
  PROGRESS_TRACKER_DESIGN,
  PROGRESS_TRACKER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Progress Trackers/Progress Indicator',
  component: Progressbar,
  argTypes: {
    progress: { control: 'number' },
  },
  parameters: {
    docs: {
      page: ProgressTrackersDocs,
    },
  },
}

const Template = args => <Progressbar {...args} />

export const DefaultProgressbar = Template.bind({})
DefaultProgressbar.args = {
  progress: 10,
}
DefaultProgressbar.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PROGRESS_TRACKER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PROGRESS_TRACKER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ProgressbarWithCustomFiller = Template.bind({})
ProgressbarWithCustomFiller.args = {
  progress: 45,
  fillerProps: {
    style: {
      backgroundColor: '#57BFF9',
    },
  },
}
ProgressbarWithCustomFiller.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PROGRESS_TRACKER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PROGRESS_TRACKER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
