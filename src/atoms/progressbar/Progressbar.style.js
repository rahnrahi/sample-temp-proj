import styled, { css } from 'styled-components'
import theme from '../../shared/theme'

const commonTextStyle = css`
  ${theme.typography.h6.css};
  color: #64707d;
`

export const StyledContainer = styled.div`
  width: 123px;
  height: 32px;
  overflow: hidden;
`

export const StyledProgressbar = styled.div`
  height: 8px;
  border-radius: 10px;
  background: ${theme.palette.ui.neutral.grey4};
`

export const StyledFiller = styled.div`
  width: ${({ progress }) => (progress ? `${progress}%` : '0%')};
  height: 8px;
  border-radius: inherit;
  background: ${theme.palette.ui.cta.green};
  transition: width 0.5s ease-in-out;
`
export const StyledProgressLabel = styled.div`
  margin-top: 8px;
  ${commonTextStyle};
  text-align: left;
`
export const StyledCompleted = styled.div`
  display: flex;
  align-items: center;
  width: 125px;
  height: 16px;
  ${commonTextStyle};
`
export const StyledStatus = styled.span`
  margin-right: 8px;
  width: 8px;
  height: 8px;
  border-radius: 8px;
  background: ${theme.palette.ui.cta.green};
`
