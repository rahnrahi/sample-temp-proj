import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  StyledContainer,
  StyledProgressbar,
  StyledFiller,
  StyledProgressLabel,
  StyledCompleted,
  StyledStatus,
} from './Progressbar.style'

export const Progressbar = ({
  progress,
  containerProps,
  fillerProps,
  labelProps,
}) => {
  const [completed, setCompleted] = useState(false)
  const _progress = progress > 0 ? Math.min(progress, 100) : 0

  useEffect(() => {
    let delay
    if (_progress === 100) {
      delay = setTimeout(() => {
        setCompleted(true)
      }, 5000)
    } else {
      setCompleted(false)
    }
    return () => {
      clearTimeout(delay)
    }
  }, [_progress])

  return completed ? (
    <StyledCompleted {...containerProps}>
      <StyledStatus data-testid='completed-status' {...fillerProps} /> Completed
    </StyledCompleted>
  ) : (
    <StyledContainer>
      <StyledProgressbar {...containerProps}>
        <StyledFiller {...fillerProps} progress={_progress} />
        <StyledProgressLabel {...labelProps}>
          {_progress}% completed
        </StyledProgressLabel>
      </StyledProgressbar>
    </StyledContainer>
  )
}

Progressbar.defaultProps = {
  progress: 0,
}
Progressbar.propTypes = {
  progress: PropTypes.number.isRequired,
  containerProps: PropTypes.object,
  fillerProps: PropTypes.object,
  labelProps: PropTypes.object,
}
