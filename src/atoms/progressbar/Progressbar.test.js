import React from 'react'
import { render, screen, cleanup } from '@testing-library/react'
import { Progressbar } from './Progressbar'
import 'jest-styled-components'

jest.useFakeTimers()

afterEach(cleanup)

const containerProps = {
  style: {
    width: '200px',
  },
}

const fillerProps = {
  style: {
    backgroundColor: 'blue',
  },
}

const labelProps = {
  style: {
    fontSize: '16px',
  },
}

describe('<Progressbar/>', () => {
  describe.each([
    [undefined, '0%'],
    [45, '45%'],
    [100, '100%'],
  ])('should render progressbar', (value, result) => {
    it(`with ${result} when progress is ${value}`, () => {
      const { container } = render(<Progressbar progress={value} />)
      expect(screen.getByText(`${result} completed`)).toBeInTheDocument()
      expect(container).toMatchSnapshot()
    })
  })
  it('should render progressbar with completed status after 5 seconds', async () => {
    const { container } = render(<Progressbar progress={100} />)
    expect(screen.getByText('100% completed')).toBeInTheDocument()
    jest.runAllTimers()
    expect(screen.getByTestId('completed-status')).toBeInTheDocument()
    expect(screen.getByText('Completed')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
  it('should render custom progressbar', async () => {
    const { container } = render(
      <Progressbar
        progress={100}
        containerProps={containerProps}
        fillerProps={fillerProps}
        labelProps={labelProps}
      />
    )
    expect(screen.getByText('100% completed')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
