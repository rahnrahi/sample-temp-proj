import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { v4 as uuidv4 } from 'uuid'
import { StyledTooltip, Tooltip_types, Title, Text } from './Tooltip.styles'

const styles = {
  position: 'relative',
  cursor: 'default',
}

const useTooltip = (showTooltip, isExternalEvent) => {
  const [show, setShow] = useState(() =>
    isExternalEvent ? showTooltip : false
  )
  const uid = useRef(uuidv4())

  useEffect(() => {
    setShow(showTooltip)
  }, [showTooltip])

  useEffect(() => {
    const parent = document.getElementById(uid.current).parentElement
    if (parent) {
      parent.style.position = styles.position
      parent.style.cursor = styles.cursor
    }
    if (parent && !isExternalEvent) {
      const handleMouseEvent = e => {
        return e.type === 'mouseenter' ? setShow(true) : setShow(false)
      }
      parent.addEventListener('mouseenter', handleMouseEvent)
      parent.addEventListener('mouseleave', handleMouseEvent)

      return () => {
        parent.removeEventListener('mouseenter', handleMouseEvent)
        parent.removeEventListener('mouseleave', handleMouseEvent)
      }
    } else {
      parent.style.position = styles.position
      parent.style.cursor = styles.cursor
    }
  }, [])
  return { uid, show }
}

export const Tooltip = ({
  size,
  position,
  offsetX,
  offsetY,
  title,
  text,
  isExternalEvent,
  showTooltip,
  children,
  className,
  ...props
}) => {
  const [coordinates, setCoordinates] = useState({ x: 0, y: 0 })
  const { uid, show } = useTooltip(showTooltip, isExternalEvent, size)

  const StyledTooltipType = Tooltip_types[position]
    ? Tooltip_types[position]
    : Tooltip_types['bottom']

  useEffect(() => {
    const parent = document.getElementById(uid.current).parentElement
    const handleMousemove = e => {
      setCoordinates({
        x: e.clientX - parent.getBoundingClientRect().x + offsetX,
        y: e.clientY - parent.getBoundingClientRect().y + offsetY,
      })
    }
    if (parent && size === 'small') {
      parent.addEventListener('mousemove', handleMousemove)
      return () => parent.removeEventListener('mousemove', handleMousemove)
    }
  }, [uid, show, offsetX, offsetY])

  return size === 'small' ? (
    <StyledTooltip
      {...props}
      className={className}
      id={uid.current}
      show={show}
      {...coordinates}
    >
      {children ? children : <Text>{text}</Text>}
    </StyledTooltip>
  ) : (
    <StyledTooltipType
      {...props}
      className={className}
      id={uid.current}
      show={show}
    >
      {children ? (
        children
      ) : (
        <>
          <Title>{title}</Title>
          <Text>{text}</Text>
        </>
      )}
    </StyledTooltipType>
  )
}

Tooltip.defaultProps = {
  position: 'bottom',
  size: 'large',
  offsetX: -5,
  offsetY: 17,
  showTooltip: false,
  isExternalEvent: false,
}

Tooltip.propTypes = {
  /** tooltip title */
  title: PropTypes.string,
  /** tooltip text */
  text: PropTypes.string,
  position: PropTypes.oneOf(['left', 'right', 'top', 'bottom']),
  /**  size:large(static) and size:small(movable) */
  size: PropTypes.oneOf(['small', 'large']),
  /** offsetX only for movable tooltip */
  offsetX: PropTypes.number,
  /** offsetY only for movable tooltip */
  offsetY: PropTypes.number,
  /** pass showTooltip(true/false) based on external event(only for large size)*/
  showTooltip: PropTypes.bool,
  /** pass isExternalEvent=true in case of external event(only for large size)*/
  isExternalEvent: PropTypes.bool,
  /**
   * **DEPRECATED**: Use the `Popover` component for custom content.
   *
   * Custom content to show in the tooltip container.
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
}
