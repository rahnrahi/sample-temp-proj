import React from 'react'
import styled from 'styled-components'
import { Tooltip } from './Tooltip'
import TooltipsDocs from '../../../docs/Tooltips.mdx'
import {
  TOOLTIP_DESIGN,
  TOOLTIP_PRESENTATION,
  DESIGN_TAB_MOCKUP,
  DESIGN_TAB_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Tooltips',
  component: Tooltip,
  argTypes: {},
  parameters: {
    docs: {
      page: TooltipsDocs,
    },
  },
}

const StyledContainer = styled.div`
  margin: 10em auto;
  background: gold;
  width: 100px;
  text-align: center;
  .tooltip {
    box-sizing: border-box;
    padding: 10px;
    width: 300px;
  }
`

const Template = args => (
  <StyledContainer>
    Hover over me
    <Tooltip {...args} className='custom-tooltip' />
  </StyledContainer>
)

export const Fixed = Template.bind({})
Fixed.args = {
  size: 'large',
  position: 'bottom',
  title: 'Tooltip',
  text: 'Text for tooltip goes here and it is responsive. ',
  showTooltip: false,
  isExternalEvent: false,
}
Fixed.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TOOLTIP_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TOOLTIP_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Movable = Template.bind({})
Movable.args = {
  size: 'small',
  text: 'Hover over',
}
Movable.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TOOLTIP_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TOOLTIP_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
