import styled, { keyframes } from 'styled-components'
import { theme } from '../../shared'

export const StyledSimpleInputComponent = styled.div`
  .error {
    margin-top: 4px;
    margin-left: 1px;
    ${theme.typography.caption};
    font-family: ${theme.typography.link.fontFamily};
    color: ${({ disabled }) =>
      disabled ? theme.palette.ui.neutral.grey7 : theme.palette.ui.cta.red};
  }
  flex: 1;
  box-sizing: border-box;
  font-family: ${theme.typography.link.fontFamily};
  ${({ width }) => width && `width: ${width};`}
`

export const StyledSimpleInputContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  position: relative;

  * {
    box-sizing: border-box;
  }

  .input {
    &__parent {
      width: 100%;
    }
  }

  &.password {
    input {
      padding-right: 50px !important;
    }
    .show-pass {
      svg {
        path {
          fill: ${theme.palette.brand.primary.charcoal};
        }
      }
    }
  }

  &.floated-label {
    :not(.disabled) {
      :hover {
        .input {
          &__wrapper {
            border-bottom: 1px solid
              ${({ error }) =>
                error
                  ? theme.palette.ui.cta.red
                  : theme.palette.brand.primary.charcoal};
          }
        }
      }
    }

    .input {
      &__parent {
        padding-top: 10px;
      }
      &__label {
        position: absolute;
        top: 26px;
        color: ${({ error }) =>
          error ? theme.palette.ui.cta.red : theme.palette.brand.primary.gray};
      }
      &__wrapper {
        display: flex;
        align-items: center;
        border-bottom: ${({ showBorderWithFloatedLabel, disabled }) =>
          showBorderWithFloatedLabel &&
          `1px solid ${
            disabled
              ? theme.palette.ui.neutral.grey8
              : theme.palette.brand.primary.gray
          };`};
        .icon {
          position: static;
          transform: none;
          font-size: 13px;
          width: max-content;
          color: ${({ disabled }) =>
            `${
              disabled
                ? theme.palette.ui.neutral.grey7
                : theme.palette.brand.primary.charcoal
            };`};

          &.left {
            padding-right: 5px;
          }
          &.right {
            padding-left: 5px;
          }
        }

        input {
          padding: 16px 0px 16px;
          border: none;

          ${({ isFocused }) =>
            !isFocused &&
            `
            &::placeholder {
              color: transparent;
            }
          `}
        }
      }
    }

    &.container_focus,
    &.filled,
    &.has_prefix {
      .input__label {
        top: 2px;
        color: ${({ error }) =>
          error ? theme.palette.ui.cta.red : theme.palette.brand.primary.gray};
      }
    }

    &.container_focus {
      :not(.disabled) {
        .input__wrapper {
          border-bottom: 1px solid
            ${({ error }) =>
              error ? theme.palette.ui.cta.red : theme.palette.ui.cta.blue};
        }
      }
    }

    &.filled.disabled {
      .input__parent {
        span,
        input {
          opacity: 1;
          font-family: 'Gilroy-Regular';
          color: ${theme.palette.ui.neutral.grey7};
        }
      }
    }
  }

  &.boxed {
    .input {
      &__label {
        padding-bottom: 5px;
      }
    }
  }

  &.container_error {
    :not(.disabled) {
      .input {
        &__wrapper {
          border-color: ${theme.palette.ui.cta.red};
        }
      }
    }
  }

  &.disabled {
    .input__label {
      color: ${theme.palette.ui.neutral.grey7};
    }
  }

  .input_parent {
    flex-wrap: wrap;
    display: flex;
    width: 100%;
    height: 100%;
    position: relative;
    flex: 0 1 auto;
    padding: 0 0.75rem;
    padding-top: 15px;
  }

  .input {
    &__label {
      width: 100%;
      font-size: 13px;
      font-weight: normal;
      display: flex;
      flex-flow: row nowrap;
      z-index: 1;
      box-sizing: border-box;
      position: absolute;
      top: 0px;
      transition: 0.2s;
      letter-spacing: 0.02em;
      white-space: nowrap;

      &--hidden {
        position: absolute;
        text-indent: -9999px;
      }

      .label-text {
        float: left;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      .locale-label {
        float: right;
        margin-left: auto;
      }
    }

    &__wrapper {
      position: relative;

      .icon {
        position: absolute;
        width: 20px;
        height: auto;
        top: 50%;
        transform: translateY(-50%);
        left: 2px;

        svg {
          vertical-align: middle;
        }
      }
    }

    &__show {
      cursor: pointer;
      font-size: 0.675rem;
      text-decoration: underline;
      line-height: 1;
      font-weight: 300;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      right: 20px;
      width: auto;
    }
  }

  &.icon-left {
    input {
      padding-left: 35px;
    }
  }

  &.sm {
    &.boxed {
      &.icon-left {
        .icon {
          .left {
            left: 10px;
          }
        }
      }
    }

    &.icon-left {
      .icon {
        width: 14px;
        left: 10px;
      }
    }
    input {
      padding-top: 11px;
      padding-bottom: 11px;
    }

    &.icon-left {
      input {
        padding-left: 40px;
      }
    }
  }

  &.md {
    input {
      padding-top: 14px;
      padding-bottom: 12px;
    }
  }
`
const onAutoFillStart = keyframes`
 from {/\*\*/} to {/\*\*/}}
`

const boxedStyles = `
  border: 1px solid ${theme.palette.brand.primary.charcoal};
  font-size: 13px;
  padding: 15px 14px;
  border-radius: 4px;
  box-sizing: border-box;
`

export const StyledSimpleInput = styled.input`
  border: none;
  border-bottom: 1px ${theme.palette.brand.primary.charcoal} solid;
  background-clip: padding-box;
  display: block;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  padding-top: 16px;
  padding-bottom: 16px;
  word-break: normal;
  line-height: initial;
  transition: all 0.2s ease-out;
  outline: none;
  letter-spacing: 0.02em;
  font-size: 13px;
  background-color: ${theme.palette.brand.primary.white} !important;

  &:disabled {
    pointer-events: none;
    opacity: 0.5;
    color: inherit;
  }

  &.custom {
    padding: 0px;
    margin: 0px;

    &:focus {
      border: none;
      padding: none;
    }
  }

  &:-webkit-autofill {
    animation-name: ${onAutoFillStart};
    transition: background-color 50000s ease-in-out 0s;
  }

  ${props => {
    if (props.boxed) return boxedStyles
  }}
`

/****Field Input Styles****/
export const StyledFieldInputComponent = styled.div`
  flex: 1;
  box-sizing: border-box;
  font-family: ${theme.typography.link.fontFamily};
  width: ${({ width }) => width ?? 'auto'};
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 40px;
  padding: 9px 0px;
  background: ${theme.palette.brand.primary.white};
  box-shadow: ${props =>
    props.showLines
      ? `inset 0px 1px 0px ${theme.palette.ui.neutral.grey4}, inset 0px -1px 0px ${theme.palette.ui.neutral.grey4}`
      : 'none'};

  .error {
    margin-left: 15px;
    ${theme.typography.caption};
    font-family: ${theme.typography.link.fontFamily};
    color: ${theme.palette.ui.cta.red};
  }

  .input {
    &__label {
      letter-spacing: 0.26px;
      font-size: 13px;
      z-index: 1;
      font-family: ${theme.typography.h6};
      color: ${({ disabled }) =>
        disabled
          ? theme.palette.ui.neutral.grey7
          : theme.palette.brand.primary.gray};
      margin-right: 16px;
      flex: 1 1 auto;
      max-width: 20%;
      &--hidden {
        position: absolute;
        text-indent: -9999px;
      }
    }
  }
  &.password {
    input {
      padding-right: 50px !important;
    }
    .show-pass {
      svg {
        path {
          fill: ${theme.palette.brand.primary.charcoal};
        }
      }
    }
  }
`

export const StyledFieldInputContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  position: relative;
  &.floated-label {
    .input {
      &__wrapper {
        display: flex;
        align-items: center;
        border-bottom: ${({ showBorderWithFloatedLabel, disabled, error }) => {
          let borderStyles = '1px solid'
          if (showBorderWithFloatedLabel) {
            if (disabled) borderStyles += theme.palette.ui.neutral.grey8
            else if (error) borderStyles += theme.palette.ui.cta.red
            else borderStyles += theme.palette.brand.primary.charcoal
          }
          return borderStyles
        }};
        .icon {
          position: static;
          transform: none;
          font-size: 13px;
          width: max-content;
          color: ${({ disabled }) => {
            if (disabled) return theme.palette.ui.neutral.grey7
            else return theme.palette.brand.primary.charcoal
          }};
          &.left {
            padding-right: 5px;
          }
          &.right {
            padding-left: 5px;
          }
        }

        input {
          border: none;
          ${({ isFocused }) => {
            if (!isFocused) {
              return '&::placeholder { color: transparent; }'
            }
          }};
        }
      }
    }
  }

  .input {
    &__wrapper {
      position: relative;
      padding-bottom: 3px;
      width: 100%;
      .icon {
        position: absolute;
        width: 20px;
        height: auto;
        top: 50%;
        transform: translateY(-50%);
        left: 2px;
        svg {
          vertical-align: middle;
        }
      }
    }

    &__show {
      cursor: pointer;
      font-size: 0.675rem;
      text-decoration: underline;
      line-height: 1;
      font-weight: 300;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      right: 20px;
      width: auto;
    }
  }

  &.icon-left {
    input {
      padding-left: 35px;
    }
  }

  &.sm {
    &.boxed {
      &.icon-left {
        .icon {
          .left {
            left: 10px;
          }
        }
      }
    }

    &.icon-left {
      .icon {
        width: 14px;
        left: 10px;
      }
    }
    input {
      padding-top: 11px;
      padding-bottom: 11px;
    }

    &.icon-left {
      input {
        padding-left: 40px;
      }
    }
  }

  &.md {
    input {
      padding-top: 14px;
      padding-bottom: 12px;
    }
  }
`

export const StyledFieldInput = styled.input`
  border: none;
  background-clip: padding-box;
  display: block;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  word-break: normal;
  line-height: initial;
  transition: all 0.2s ease-out;
  outline: none;
  font-size: 13px;
  background-color: ${theme.palette.brand.primary.white};
  min-height: 20px;
  &:disabled {
    pointer-events: none;
    opacity: 0.5;
    color: inherit;
  }

  &.custom {
    padding: 0px;
    margin: 0px;

    &:focus {
      border: none;
      padding: none;
    }
  }

  &:-webkit-autofill {
    animation-name: ${onAutoFillStart};
    transition: background-color 50000s ease-in-out 0s;
  }

  ${props => {
    if (props.boxed) return boxedStyles
  }}
`
