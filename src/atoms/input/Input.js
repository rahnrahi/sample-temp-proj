import React, {
  useState,
  useEffect,
  useCallback,
  useRef,
  forwardRef,
} from 'react'
import PropTypes from 'prop-types'
import { InputType, Type } from './input.enums'
import { Icons } from '../icon/Icon'
import Eye from '../../assets/images/eye'
import cx from 'classnames'
import {
  StyledFieldInputComponent,
  StyledFieldInputContainer,
  StyledSimpleInput,
  StyledSimpleInputComponent,
  StyledSimpleInputContainer,
  StyledFieldInput,
} from './Input.styles'
import useLocaleDirection from '../../hooks/render-rtl'

const isFunction = x => typeof x === 'function'

const StyledInputComponent = ({ inputType, children, ...rest }) => {
  return inputType == InputType.SIMPLE ? (
    <StyledSimpleInputComponent inputType={inputType} {...rest}>
      {children}
    </StyledSimpleInputComponent>
  ) : (
    <StyledFieldInputComponent inputType={inputType} {...rest}>
      {children}
    </StyledFieldInputComponent>
  )
}

const StyledInputContainer = ({ inputType, children, ...rest }) => {
  return inputType == InputType.SIMPLE ? (
    <StyledSimpleInputContainer inputType={inputType} {...rest}>
      {children}
    </StyledSimpleInputContainer>
  ) : (
    <StyledFieldInputContainer inputType={inputType} {...rest}>
      {children}
    </StyledFieldInputContainer>
  )
}

const StyledInput = forwardRef(({ inputType, children, ...rest }, ref) => {
  return inputType == InputType.SIMPLE ? (
    <StyledSimpleInput inputType={inputType} ref={ref} {...rest}>
      {children}
    </StyledSimpleInput>
  ) : (
    <StyledFieldInput inputType={inputType} ref={ref} {...rest}>
      {children}
    </StyledFieldInput>
  )
})

const PasswordToggleIcon = (type, value, showPass, inputId, setPass) => {
  return (
    type === Type.PASSWORD &&
    value.length > 0 && (
      <span
        className={cx({
          input__show: true,
          [`show-pass`]: showPass,
        })}
        onClick={() => setPass(!showPass)}
        data-testid={`icon-${inputId}`}
      >
        <Eye />
      </span>
    )
  )
}

const InputParent = ({
  label,
  canShowLocaleLabel,
  locale,
  placeholder,
  inputType,
  children,
}) =>
  inputType === InputType.SIMPLE ? (
    <label className='input__parent'>
      {label ? (
        <div className='input__label'>
          <span className='label-text'>{label}</span>
          {canShowLocaleLabel && <span className='locale-label'>{locale}</span>}
        </div>
      ) : (
        // Used for ADA compliance
        <span className='input__label--hidden'>{placeholder}</span>
      )}
      {children}
    </label>
  ) : (
    <>{children}</>
  )

const InputComponent = forwardRef(
  (
    {
      inputType,
      containerClassName,
      width,
      inputProps,
      error,
      isFocused,
      kind,
      value,
      isAutofilled,
      icon,
      isFloatedLabel,
      prefix,
      showBorderWithFloatedLabel,
      containerPropsRest,
      label,
      locale,
      localeCode,
      handleBlur,
      handleKeyPress,
      handleFocus,
      handleChange,
      showLines,
      inputId,
      showPass,
      suffix,
      setPass,
      errorMessage,
      errorProps,
    },
    ref
  ) => {
    const {
      className: inputClassName,
      placeholder,
      boxed,
      type,
      disabled,
      ...inputPropsRest
    } = inputProps || {}

    const Icon = Icons[icon]
    const dir = useLocaleDirection(localeCode)
    const canShowLocaleLabel = locale && !boxed

    const InputWrapper = () => (
      <div className='input__wrapper'>
        {prefix && <span className='icon left'>{prefix}</span>}
        <StyledInput
          inputType={inputType}
          dir={dir}
          ref={ref}
          placeholder={placeholder}
          onBlur={handleBlur}
          onKeyPress={handleKeyPress}
          onFocus={handleFocus}
          onChange={handleChange}
          className={inputClassName}
          disabled={disabled}
          value={value}
          boxed={boxed}
          id={inputId}
          lang={locale}
          data-testid={inputId}
          type={type === Type.PASSWORD && showPass ? Type.TEXT : type}
          {...inputPropsRest}
        />
        {icon && (
          <span className='icon right'>
            <Icon />
          </span>
        )}
        {suffix && !canShowLocaleLabel && (
          <span className='icon right'>{suffix}</span>
        )}
        {PasswordToggleIcon(type, value, showPass, inputId, setPass)}
      </div>
    )

    return (
      <StyledInputComponent
        inputType={inputType}
        className={containerClassName}
        width={width}
        disabled={disabled}
        showLines={showLines}
      >
        {inputType === InputType.FIELD && (
          <label htmlFor={inputId} className='input__label'>
            {label ?? placeholder}
          </label>
        )}
        <StyledInputContainer
          inputType={inputType}
          className={cx({
            container_error: error,
            container_focus: isFocused,
            [kind]: kind,
            boxed: boxed,
            filled: value.length > 0 || isAutofilled,
            has_prefix: prefix && prefix.length > 0,
            ['icon-left']: icon,
            ['floated-label']: isFloatedLabel || prefix,
            disabled: disabled,
            password: type === Type.PASSWORD,
          })}
          isFocused={isFocused}
          disabled={inputProps.disabled}
          showBorderWithFloatedLabel={showBorderWithFloatedLabel}
          error={error}
          {...containerPropsRest}
        >
          <InputParent
            label={label}
            canShowLocaleLabel={canShowLocaleLabel}
            locale={locale}
            placeholder={placeholder}
            inputType={inputType}
          >
            {InputWrapper()}
          </InputParent>
        </StyledInputContainer>
        {error && errorMessage && (
          <div className='error' {...errorProps}>
            {errorMessage}
          </div>
        )}
      </StyledInputComponent>
    )
  }
)

const Input = forwardRef(
  (
    {
      inputProps,
      errorProps,
      label,
      locale,
      localeCode,
      className: containerClassName,
      icon,
      isFloatedLabel = true,
      error,
      kind,
      errorMessage,
      width,
      prefix,
      suffix,
      maskOptions,
      showBorderWithFloatedLabel,
      inputType,
      inputId,
      showLines,
      ...containerPropsRest
    },
    ref
  ) => {
    const [value, setValue] = useState(inputProps.value || '')
    const isControlled = typeof inputProps.value === 'string'
    const [isFocused, setFocus] = useState(false)
    const [showPass, setPass] = useState(false)

    let inputRef = useRef(null)
    if (ref) {
      inputRef = ref
    }
    const [isAutofilled, setAutofill] = useState(false)

    const setInputValue = useCallback((value = '') => {
      setValue(value)
    }, [])

    useEffect(() => {
      if (isControlled) {
        return setInputValue(inputProps.value)
      }

      setInputValue(inputProps.defaultValue)
    }, [inputProps.defaultValue, inputProps.value, isControlled, setInputValue])

    useEffect(() => {
      if (inputProps.value) {
        setInputValue(inputProps.value)
        setFocus(true)
      }
    }, [inputProps.value, setInputValue])

    useEffect(() => {
      if (
        maskOptions &&
        Object.keys(maskOptions).length > 0 &&
        inputRef &&
        inputRef.current
      ) {
        import('inputmask').then(module => {
          module.default(maskOptions).mask(inputRef.current)
        })
      }
    }, [inputRef, maskOptions])

    useEffect(() => {
      inputRef &&
        inputRef.current &&
        inputRef.current.addEventListener(
          'animationstart',
          () => setAutofill(true),
          false
        )
    }, [inputRef])

    const handleBlur = event => {
      !prefix && setFocus(false)
      if (!value.length && isAutofilled) setAutofill(false)

      if (isFunction(inputProps.onBlur)) {
        inputProps.onBlur(event)
      }
    }

    const handleFocus = event => {
      setFocus(true)

      if (isFunction(inputProps.onFocus)) {
        inputProps.onFocus(event)
      }
    }

    const handleChange = event => {
      if (!isControlled) {
        setInputValue(event.target.value)
      }

      if (isFunction(inputProps.onChange)) {
        inputProps.onChange(event)
      }
    }

    const handleKeyPress = event => {
      if (inputProps.type === Type.NUMBER) {
        const keys = ['e', 'E']
        if (keys.includes(event.key)) {
          event.preventDefault()
        }
      }
      if (isFunction(inputProps.onKeyPress)) {
        inputProps.onKeyPress(event)
      }
    }
    const childrenProps = {
      inputProps,
      error,
      isFocused,
      kind,
      value,
      isAutofilled,
      icon,
      isFloatedLabel,
      prefix,
      showBorderWithFloatedLabel,
      containerPropsRest,
      label,
      locale,
      localeCode,
      handleBlur,
      handleKeyPress,
      handleFocus,
      handleChange,
      inputId,
      showPass,
      suffix,
      setPass,
      errorMessage,
      errorProps,
      containerClassName,
      width,
      showLines,
    }
    return (
      <InputComponent ref={inputRef} inputType={inputType} {...childrenProps} />
    )
  }
)

Input.propTypes = {
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.string,
  boxed: PropTypes.bool,
  className: PropTypes.string,
  inputId: PropTypes.string,
  /** Shows lines on the top and bottom of the input field */
  showLines: PropTypes.bool,
  /** Settings for disabled, placeholder, value, etc. */
  inputProps: PropTypes.shape({
    defaultValue: PropTypes.string,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
    type: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
  }),
  errorProps: PropTypes.object,
  kind: PropTypes.string,
  isFloatedLabel: PropTypes.bool,
  inputType: PropTypes.oneOf(['Simple', 'Field']),
  /** width should be given with units like 100px */
  width: PropTypes.string,
  /** Prefix to show in input field */
  prefix: PropTypes.string,
  /** Suffix to show in input field */
  suffix: PropTypes.string,
  /** Mask Props object, accept mask as regex/patterns */
  maskOptions: PropTypes.shape({
    mask: PropTypes.string,
    placeholder: PropTypes.string,
    regex: PropTypes.string,
    alias: PropTypes.string,
    inputFormat: PropTypes.string,
  }),
  showBorderWithFloatedLabel: PropTypes.bool,
  /** Locale label to show on a 'Simple' input field that is not boxed.
   *
   * Suffix will be hidden if the locale label is visible. */
  locale: PropTypes.string,
  /** Locale Code Value to Support the Directional Input */
  localeCode: PropTypes.string,
}

Input.defaultProps = {
  inputProps: {
    type: 'text',
  },
  inputType: 'Simple',
  showBorderWithFloatedLabel: true,
  showLines: true,
}

InputComponent.propTypes = {
  ...Input.propTypes,
  /** className for adding stylings */
  containerClassName: PropTypes.string,
  /** Function for handling the behavior on losing focus */
  handleBlur: PropTypes.func,
  /** Function for handling the behavior on Value Change */
  handleChange: PropTypes.func,
  /** Function for handling the behavior on Focus */
  handleFocus: PropTypes.func,
  /** Function for handling the behavior on Key Press */
  handleKeyPress: PropTypes.func,
  /** Whether the input is auto filled or not */
  isAutofilled: PropTypes.bool,
  /** Whether the input is in focus or not */
  isFocused: PropTypes.bool,
  /** Whether to show password or not */
  showPass: PropTypes.bool,
  /** Handler for changing the value of showPass */
  setPass: PropTypes.func,
  /** The value in the Input */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  containerPropsRest: PropTypes.object,
}

InputParent.propTypes = {
  label: PropTypes.string,
  canShowLocaleLabel: PropTypes.bool,
  locale: PropTypes.string,
  placeholder: PropTypes.string,
  children: PropTypes.node,
  inputType: PropTypes.oneOf(['Simple', 'Field']),
}

const StyledPropType = {
  /** Type of the Input Field */
  inputType: PropTypes.oneOf(['Simple', 'Field']),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

StyledInputComponent.propTypes = StyledPropType
StyledInputContainer.propTypes = StyledPropType
StyledInput.propTypes = StyledPropType
export { Input }
