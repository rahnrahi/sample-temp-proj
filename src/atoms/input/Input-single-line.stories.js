import React from 'react'
import { Input } from './'
import InputFieldsDocs from '../../../docs/InputFields.mdx'
import {
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION,
  INPUT_SMALL_DESIGN,
  INPUT_SMALL_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Components/Input Fields/Single Line',
  component: Input,
  parameters: {
    docs: {
      page: InputFieldsDocs,
    },
  },
  argTypes: {
    icon: {
      control: {
        type: 'select',
        options: ['Search', 'Close', 'Info', 'Edit', 'Link'],
      },
    },
  },
}

const Template = args => <Input {...args} />

export const Medium = Template.bind({})
Medium.args = {
  className: 'primary',
  label: 'Field Label',
  width: '350px',
}
Medium.parameters = getDesignSpecifications(
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION
)

export const Small = Template.bind({})
Small.args = {
  className: 'primary',
  label: 'Field Label',
  inputProps: {
    disabled: false,
  },
  width: '350px',
  inputType: 'Field',
}
Small.parameters = getDesignSpecifications(
  INPUT_SMALL_DESIGN,
  INPUT_SMALL_PRESENTATION
)
