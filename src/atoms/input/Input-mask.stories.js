import React from 'react'
import { Input } from '.'
import InputFieldsDocs from '../../../docs/InputFields.mdx'
import {
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Components/Input Fields/Masked inputs',
  component: Input,
  parameters: {
    docs: {
      page: InputFieldsDocs,
    },
  },
}

const design = getDesignSpecifications(
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION
)['design']

const Template = args => <Input {...args} />

export const Date = Template.bind({})

const args = {
  width: '350px',
}

Date.args = {
  ...args,
  className: 'date-mask',
  label: 'Date',
  maskOptions: {
    alias: 'datetime',
    placeholder: 'dd/mm/yyyy',
    inputFormat: 'dd/mm/yyyy',
  },
  inputProps: {
    type: 'text',
  },
}
Date.parameters = { design }

export const Time = Template.bind({})
Time.args = {
  ...args,
  className: 'time-mask',
  label: 'Time',
  maskOptions: {
    alias: 'datetime',
    placeholder: 'HH:MM',
    inputFormat: 'HH:MM',
  },
  inputProps: {
    type: 'text',
  },
}
Time.parameters = { design }

export const Phone = Template.bind({})
Phone.args = {
  ...args,
  className: 'phone-mask',
  label: 'Phone (Indian format)',
  maskOptions: {
    mask: '(999) 999-9999',
    inputFormat: '(999) 999-9999',
  },
  inputProps: {
    type: 'text',
  },
}
Phone.parameters = { design }

export const Email = Template.bind({})
Email.args = {
  ...args,
  className: 'email-mask',
  label: 'Email',
  maskOptions: {
    alias: 'email',
  },
  inputProps: {
    type: 'text',
  },
}
Email.parameters = { design }

export const Regex = Template.bind({})
Regex.args = {
  ...args,
  className: 'regex-mask',
  label: 'Custom Regex (only numbers)',
  maskOptions: {
    regex: '[0-9]*',
  },
  inputProps: {
    type: 'text',
  },
}
Regex.parameters = { design }
