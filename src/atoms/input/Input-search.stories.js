import React from 'react'
import { Input } from './'
import SearchDocs from '../../../docs/Search.mdx'
import {
  SEARCH_DESIGN,
  SEARCH_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Search',
  component: Input,
  parameters: {
    docs: { page: SearchDocs },
  },
}

const Template = args => <Input {...args} />

export const SearchSecondary = Template.bind({})
SearchSecondary.args = {
  className: 'search-global',
  label: '',
  icon: 'Search',
  isFloatedLabel: false,
  kind: 'sm',
  inputProps: {
    placeholder: 'Search',
    boxed: true,
  },
  width: '350px',
}
SearchSecondary.storyName = 'Secondary'
SearchSecondary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SEARCH_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SEARCH_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const SearchTertiary = Template.bind({})
SearchTertiary.args = {
  className: 'search-local',
  icon: 'Search',
  label: '',
  kind: 'md',
  isFloatedLabel: false,
  inputProps: {
    boxed: false,
  },
  width: '350px',
}
SearchTertiary.storyName = 'Tertiary'
SearchTertiary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SEARCH_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SEARCH_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
