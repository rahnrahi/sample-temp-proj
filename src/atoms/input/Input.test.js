import React from 'react'
import { Input } from './'
import { InputType, Type } from './input.enums'
import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import { Simulate } from 'react-dom/test-utils'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(() => {
  jest.clearAllMocks()
})

let onBlur = jest.fn()
let onFocus = jest.fn()
let onChange = jest.fn()
let onKeyPress = jest.fn()

const primaryProps = {
  className: 'primary',
  label: 'Field Label',
  inputId: 'primary_input_Id',
  isFloatedLabel: true,
  width: '300px',
  inputProps: {
    onBlur,
    onFocus,
    onChange,
    onKeyPress,
    isControlled: true,
    value: 'fabric',
  },
  inputType: InputType.SIMPLE,
}

const searchProps = {
  className: 'search-global',
  inputId: 'search_input_Id',
  label: '',
  icon: 'Search',
  isFloatedLabel: false,
  kind: 'sm',
  inputProps: {
    placeholder: 'Search',
    boxed: true,
  },
  inputType: InputType.SIMPLE,
}

const searchLocalProps = {
  className: 'search-local',
  inputId: 'search_local_input_Id',
  icon: 'Search',
  label: '',
  kind: 'md',
  isFloatedLabel: false,
  inputProps: {
    boxed: false,
  },
  inputType: InputType.SIMPLE,
}

const passwordProps = {
  className: 'password',
  inputId: 'pass_local_input_Id',
  label: 'Password',
  isFloatedLabel: true,
  inputProps: {
    onBlur,
    onFocus,
    onChange,
    onKeyPress,
    type: Type.PASSWORD,
    value: 'test',
  },
  inputType: InputType.SIMPLE,
}

const prefixProps = {
  className: 'primary',
  inputId: 'prefix_local_input_Id',
  label: 'Field Label',
  isFloatedLabel: true,
  width: '300px',
  inputProps: {
    placeholder: 'Text',
    isControlled: true,
    value: 'fabric',
  },
  prefix: '$',
  inputType: InputType.SIMPLE,
}

const suffixProps = {
  className: 'primary',
  inputId: 'suffix_local_input_Id',
  label: 'Field Label',
  isFloatedLabel: true,
  width: '300px',
  inputProps: {
    placeholder: 'Text',
    isControlled: true,
    value: 'fabric',
  },
  suffix: '%',
  inputType: InputType.SIMPLE,
}

const numberProps = {
  inputId: 'number_local_input_Id',
  label: 'Field Label',
  isFloatedLabel: true,
  kind: 'sm',
  inputProps: {
    onBlur,
    onFocus,
    onChange,
    onKeyPress,
    type: Type.NUMBER,
  },
  inputType: InputType.SIMPLE,
}

const locale = 'fr'

describe('<Input/> Simple', () => {
  it('renders Input component - Primary', () => {
    const { container } = render(<Input {...primaryProps} />)
    const input = screen.getByTestId('primary_input_Id')
    fireEvent.focus(input)
    Simulate.change(input, { target: { value: 'fabric' } })
    fireEvent.blur(input)
    fireEvent.keyPress(input, { key: 'Enter', keyCode: 13, charCode: 13 })

    expect(primaryProps.inputProps.onFocus.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onBlur.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onKeyPress.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onChange.mock.calls.length).toBe(1)

    expect(input.value).toBe('fabric')
    expect(container).toMatchSnapshot()
  })
  it('renders Input component - Search', () => {
    const { container } = render(<Input {...searchProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input component - Search Local', () => {
    const { container } = render(<Input {...searchLocalProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input component - Password', () => {
    const { container } = render(<Input {...passwordProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input component - Password show/hide text', async () => {
    const { container } = render(<Input {...passwordProps} />)
    expect(container).toBeInTheDocument()

    const input = screen.getByTestId(passwordProps.inputId)
    fireEvent.keyPress(input, { key: '9', keyCode: 61, charCode: 61 })

    const icon = screen.getByTestId(`icon-${passwordProps.inputId}`)
    expect(icon).toBeInTheDocument()
    fireEvent.click(icon)

    await waitFor(() => {
      expect(
        container.querySelector(`icon-${passwordProps.inputId}`)
      ).not.toBeInTheDocument()
    })

    expect(passwordProps.inputProps.onKeyPress.mock.calls.length).toBe(1)
  })
  it('renders Input component - Prefix', () => {
    const { container } = render(<Input {...prefixProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input component - Suffix', () => {
    const { container } = render(<Input {...suffixProps} />)
    expect(container).toMatchSnapshot()
  })
  it('should render a locale label and set the lang attribute on the input field', async () => {
    const props = {
      ...primaryProps,
      inputProps: { ...primaryProps.inputProps },
      locale,
    }
    const { findByText, getByRole } = render(<Input {...props} />)

    expect(await findByText(locale)).toBeInTheDocument()
    expect(getByRole('textbox')).toHaveAttribute('lang', locale)
  })
  it('should render a locale label and hide the suffix if both are specified', async () => {
    const props = {
      ...suffixProps,
      inputProps: { ...suffixProps.inputProps },
      locale,
    }
    const { findByText, queryByText } = render(<Input {...props} />)

    expect(await findByText(locale)).toBeInTheDocument()
    expect(queryByText(suffixProps.suffix)).not.toBeInTheDocument()
  })
  it('should not render a locale label if the input is "boxed"', () => {
    const props = {
      ...primaryProps,
      inputProps: { ...primaryProps.inputProps, boxed: true },
      locale,
    }
    const { queryByText } = render(<Input {...props} />)

    expect(queryByText(locale)).not.toBeInTheDocument()
  })
})

describe('<Input/> Field', () => {
  it('renders label/placeholder', () => {
    primaryProps['inputType'] = InputType.FIELD
    primaryProps['inputId'] = 'primary_input_field_Id'
    const { container } = render(<Input {...primaryProps} />)
    const inputLabel = container.querySelector('.input__label')
    expect(inputLabel).toBeInTheDocument()
    expect(inputLabel).toHaveTextContent('Field Label')
  })
  it('renders Input Field component - Primary', () => {
    primaryProps['inputType'] = InputType.FIELD
    primaryProps['inputId'] = 'primary_input_field_Id'
    const { container } = render(<Input {...primaryProps} />)
    const input = screen.getByTestId('primary_input_field_Id')
    fireEvent.focus(input)
    Simulate.change(input, { target: { value: 'fabric' } })
    fireEvent.blur(input)
    fireEvent.keyPress(input, { key: 'Enter', keyCode: 13, charCode: 13 })

    expect(primaryProps.inputProps.onFocus.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onBlur.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onKeyPress.mock.calls.length).toBe(1)
    expect(primaryProps.inputProps.onChange.mock.calls.length).toBe(1)

    expect(input.value).toBe('fabric')
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component by id', () => {
    primaryProps['inputType'] = InputType.FIELD
    primaryProps['inputId'] = 'primary_input_field_Id'
    render(<Input {...primaryProps} />)
    const input = screen.getAllByTestId('primary_input_field_Id')
    expect(input).toBeTruthy()
  })
  it('renders Input Field component - Blur', async () => {
    const { container } = render(<Input {...primaryProps} />)
    expect(container).toBeInTheDocument()

    const input = screen.getByTestId(primaryProps.inputId)
    expect(input).toBeInTheDocument()

    fireEvent.change(input, { target: { value: 'fabric' } })
    fireEvent.blur(input)

    expect(primaryProps.inputProps.onBlur.mock.calls.length).toBe(1)
  })
  it('renders Input Field component - Disabled', () => {
    primaryProps['inputType'] = InputType.FIELD
    primaryProps['inputId'] = 'primary_input_field_Id'
    const { container } = render(
      <Input
        inputProps={{ disabled: true, ...primaryProps.inputProps }}
        {...primaryProps}
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Search', () => {
    searchProps['inputType'] = InputType.FIELD
    searchProps['inputId'] = 'search_input_field_Id'
    const { container } = render(<Input {...searchProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Search Local', () => {
    searchLocalProps['inputType'] = InputType.FIELD
    searchLocalProps['inputId'] = 'search_local_input_field_Id'
    const { container } = render(<Input {...searchLocalProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Password', () => {
    const { container } = render(<Input {...passwordProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Password show/hide text', async () => {
    passwordProps['inputType'] = InputType.FIELD
    passwordProps['inputId'] = 'pass_local_input_field_Id'
    const { container } = render(<Input {...passwordProps} />)
    expect(container).toBeInTheDocument()

    const input = screen.getByTestId(passwordProps.inputId)
    fireEvent.keyPress(input, { key: 'Enter', keyCode: 13, charCode: 13 })

    const icon = screen.getByTestId(`icon-${passwordProps.inputId}`)
    fireEvent.click(icon)

    await waitFor(() => {
      expect(
        container.querySelector(`icon-${passwordProps.inputId}`)
      ).not.toBeInTheDocument()
    })

    expect(passwordProps.inputProps.onKeyPress.mock.calls.length).toBe(1)
  })
  it('renders Input Field component - Prefix', () => {
    prefixProps['inputType'] = InputType.FIELD
    prefixProps['inputId'] = 'prefix_local_input_field_Id'
    const { container } = render(<Input {...prefixProps} />)
    expect(container).toMatchSnapshot()
  })

  it('renders Input Field component - Suffix', () => {
    suffixProps['inputType'] = InputType.FIELD
    suffixProps['inputId'] = 'suffix_local_input_field_Id'
    const { container } = render(<Input {...prefixProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Number', () => {
    suffixProps['inputType'] = InputType.FIELD
    suffixProps['inputId'] = 'number_local_input_field_Id'
    const { container } = render(<Input {...numberProps} />)
    expect(container).toMatchSnapshot()
  })
  it('renders Input Field component - Number', () => {
    numberProps['inputType'] = InputType.FIELD
    numberProps['inputId'] = 'number_local_input_field_Id'
    const { container } = render(<Input {...numberProps} />)
    expect(container).toBeInTheDocument()

    const input = screen.getByTestId(numberProps.inputId)
    fireEvent.keyPress(input, { key: '9', keyCode: 61, charCode: 61 })

    expect(numberProps.inputProps.onKeyPress.mock.calls.length).toBe(1)
  })
  it('renders Input Field component - Number not allowed character', () => {
    primaryProps['inputType'] = InputType.FIELD
    primaryProps['inputId'] = 'primary_input_field_Id'
    const { container } = render(<Input {...numberProps} />)
    expect(container).toBeInTheDocument()

    const input = screen.getByTestId(numberProps.inputId)
    fireEvent.keyPress(input, { key: 'E', keyCode: 4, charCode: 4 })

    expect(numberProps.inputProps.onKeyPress.mock.calls.length).toBe(0)
  })
  it('should not render a locale label', () => {
    const props = { ...primaryProps, inputType: InputType.FIELD, locale }
    const { queryByText } = render(<Input {...props} />)

    expect(queryByText(locale)).not.toBeInTheDocument()
  })
})
