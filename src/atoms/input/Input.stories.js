import React from 'react'
import { Input } from './'
import InputFieldsDocs from '../../../docs/InputFields.mdx'
import {
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Components/Input Fields',
  component: Input,
  parameters: {
    docs: {
      page: InputFieldsDocs,
    },
  },
  argTypes: {
    icon: {
      control: {
        type: 'select',
        options: ['Edit', 'Close', 'Link'],
      },
    },
  },
}

const Template = args => <Input {...args} />

export const Password = Template.bind({})
Password.args = {
  width: '350px',
  className: 'password',
  label: 'Password',
  inputProps: { ...{ type: 'password' } },
}
Password.parameters = getDesignSpecifications(
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION
)
