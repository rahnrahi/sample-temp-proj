import React from 'react'
import { Input } from './'
import InputFieldsDocs from '../../../docs/InputFields.mdx'
import {
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Number',
  component: Input,
  parameters: {
    docs: {
      page: InputFieldsDocs,
    },
  },
}

const Template = args => <Input {...args} />

export const Number = Template.bind({})
Number.args = {
  width: '350px',
  className: 'number',
  label: 'Number field',
  inputProps: {
    type: 'number',
    min: 0,
  },
}
Number.parameters = getDesignSpecifications(
  INPUT_FIELD_DESIGN,
  INPUT_FIELD_PRESENTATION
)
