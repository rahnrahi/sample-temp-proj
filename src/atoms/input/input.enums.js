export const InputType = Object.freeze({
  SIMPLE: 'Simple',
  FIELD: 'Field',
})

export const Type = Object.freeze({
  TEXT: 'text',
  PASSWORD: 'password',
  NUMBER: 'number',
})
