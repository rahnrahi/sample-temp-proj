import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Pill } from './Pill'
import theme from '../../shared/theme'

afterEach(cleanup)

const pillLabel = 'Pill Label'

describe('<Pill/>', () => {
  it('should render a Pill component with variant:success correctly', () => {
    const { container } = render(<Pill variant='success' text={pillLabel} />)
    expect(container).toMatchSnapshot()
  })

  it('should render a Pill component with variant:warning correctly', () => {
    const { container } = render(<Pill variant='warning' text={pillLabel} />)
    expect(container).toMatchSnapshot()
  })

  it('should render a Pill component with variant:alert correctly', () => {
    const { container } = render(<Pill variant='alert' text={pillLabel} />)
    expect(container).toMatchSnapshot()
  })

  it('should render an outline for the pill', async () => {
    const outlineColor = theme.palette.brand.secondary.purple

    const { findByText } = render(
      <Pill text={pillLabel} outlineColor={outlineColor} />
    )

    expect(await findByText(pillLabel)).toHaveStyle(
      `border: 1px solid ${outlineColor}`
    )
  })

  it('should render a Pill component without text-transform', () => {
    const { container } = render(
      <Pill text={pillLabel} ignoreTextTransform={true} />
    )
    const style = window.getComputedStyle(container)
    expect(style.textTransform).not.toBe('uppercase')
  })

  it('should render an ellipse inside of pill', () => {
    const outlineColor = theme.palette.brand.secondary.purple
    const { container } = render(
      <Pill
        text={pillLabel}
        ellipse={{
          enabled: true,
          outline: {
            enabled: true,
            thickness: 2,
            color: outlineColor,
          },
        }}
      />
    )
    expect(container).toMatchSnapshot()
  })
})
