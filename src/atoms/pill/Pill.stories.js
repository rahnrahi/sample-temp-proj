import React from 'react'
import styled from 'styled-components'
import { Pill as Pills } from './Pill'
import PillsDocs from '../../../docs/Pills.mdx'
import {
  PILLS_DESIGN,
  PILLS_PRESENTATION,
  DESIGN_TAB_MOCKUP,
  DESIGN_TAB_PRESENTATION,
} from '../../hooks/constants'

const StyledWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  .wrap-with-margin {
    margin: 5px 5px;
  }
`
const pillsValues = new Array(40)
  .fill()
  .map((_, i) => ({ value: `value ${i + 1}` }))
export default {
  title: 'Components/Pills',
  component: Pills,
  argTypes: {
    text: { control: 'text' },
    color: { control: 'color' },
    variant: {
      control: {
        type: 'select',
        options: ['success', 'warning', 'alert'],
      },
    },
  },
  parameters: {
    docs: {
      page: PillsDocs,
    },
  },
}

const Template = args => <Pills {...args} />

export const Pill = Template.bind({})
Pill.args = {
  text: 'Label',
  variant: 'success',
}
Pill.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PILLS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PILLS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const PillsWithWrap = () => (
  <StyledWrapper>
    {pillsValues.map(({ value }) => (
      <Pills text={value} className='wrap-with-margin' key={value} />
    ))}
  </StyledWrapper>
)
PillsWithWrap.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PILLS_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PILLS_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
