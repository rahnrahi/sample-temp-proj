import React from 'react'
import PropTypes from 'prop-types'
import { StyledPill, StyledEllipse, pillTypes } from './Pill.styles'

export const Pill = ({
  variant,
  text,
  color,
  outlineColor,
  textColor,
  ellipse,
  ignoreTextTransform,
  ...props
}) => {
  const pillType = pillTypes[variant]
    ? pillTypes[variant]
    : pillTypes['success']

  return (
    <StyledPill
      {...props}
      {...pillType}
      color={color}
      ignoreTextTransform={ignoreTextTransform}
      outlineColor={outlineColor}
      textColor={textColor ?? pillTypes[variant].textColor}
    >
      {ellipse?.enabled && (
        <StyledEllipse
          enableEllipseOutline={ellipse?.outline?.enabled}
          ellipseOutlineThickness={ellipse?.outline?.thickness}
          ellipseOutlineColor={ellipse?.outline?.color}
          ellipseFillColor={ellipse?.backgroundColor}
        />
      )}
      {text}
    </StyledPill>
  )
}

Pill.defaultProps = {
  ignoreTextTransform: false,
  variant: 'success',
  color: '',
}

Pill.propTypes = {
  /** Text for Pill*/
  text: PropTypes.string.isRequired,
  /** Variant to show the state of the Pill */
  variant: PropTypes.oneOf(['success', 'warning', 'alert']),
  /** Custom color, note: this will disable usage of variant prop*/
  color: PropTypes.string,
  /** Custom outline color. This will add a 1px solid border in the color provided to the pill */
  outlineColor: PropTypes.string,
  /** Custom text color. If provided, will overwrite default textColor associated with pill variant */
  textColor: PropTypes.string,
  /** Object for enabling and customizing ellipse inside of Pill */
  ellipse: PropTypes.shape({
    /** Enable/disable Optional Ellipse */
    enabled: PropTypes.bool,
    /** Optional custom ellipse outline color. Defaults to black */
    outline: PropTypes.shape({
      /** Enable/disable Optional Ellipse */
      enabled: PropTypes.bool,
      /** Custom ellipse outline thickness. Defaults to 2px*/
      thickness: PropTypes.number,
      /** Custom ellipse outline color. Defaults to grey1 */
      color: PropTypes.string,
    }),
    /** Custom ellipse fill color. Defaults to transparent */
    backgroundColor: PropTypes.string,
  }),
  /** Ignore the uppercase text transform */
  ignoreTextTransform: PropTypes.bool,
}
