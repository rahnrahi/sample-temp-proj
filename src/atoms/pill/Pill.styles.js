import styled from 'styled-components'
import theme from '../../shared/theme'

export const StyledPill = styled.span`
  font-family: ${theme.typography.link.fontFamily};
  font-size: ${theme.typography.caption.fontSize};
  line-height: 0.87875rem;
  letter-spacing: 0.075em;
  background: ${({ background, color }) =>
    color ? color : background && `${background}`};
  color: ${({ textColor }) => textColor && `${textColor}`};
  padding: 6px 8px 6px 8px;
  border-radius: 16px;
  box-sizing: border-box;
  ${({ outlineColor }) => outlineColor && `border: 1px solid ${outlineColor};`}
  display: inline-flex;
  align-items: center;
  ${({ ignoreTextTransform }) =>
    !ignoreTextTransform && 'text-transform: uppercase;'};
`

export const StyledEllipse = styled.div`
  display: inline-block;
  border-radius: 50%;
  ${({ enableEllipseOutline, ellipseOutlineThickness, ellipseOutlineColor }) =>
    enableEllipseOutline &&
    `border: ${ellipseOutlineThickness}px ${ellipseOutlineColor} solid;`}
  height: 8px;
  width: 8px;
  margin-right: 4px;
  background-color: ${({ ellipseFillColor }) => ellipseFillColor};
`

export const pillTypes = {
  success: {
    background: theme.palette.ui.cta.green,
    textColor: theme.palette.brand.primary.white,
  },
  warning: {
    background: theme.palette.ui.cta.yellow,
    textColor: theme.palette.brand.primary.charcoal,
  },
  alert: {
    background: theme.palette.ui.cta.red,
    textColor: theme.palette.brand.primary.white,
  },
}
