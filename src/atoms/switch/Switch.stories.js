import React from 'react'
import { Switch as ToggleSwitch } from './Switch'
import SelectionControlsDocs from '../../../docs/SelectionControls.mdx'
import {
  SWITCH_DESIGN,
  SWITCH_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Components/Selection Controls/Switch',
  component: ToggleSwitch,
  argTypes: {},
  parameters: {
    docs: {
      page: SelectionControlsDocs,
    },
  },
}

const template = args => <ToggleSwitch {...args} />

export const Switch = template.bind({})

Switch.args = {
  label: 'Label',
  initialState: false,
  disabled: false,
}
Switch.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SWITCH_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SWITCH_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
