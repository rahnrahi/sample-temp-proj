import React, { useState, useCallback } from 'react'
import {
  StyledSwitchWrapper,
  StyledToggleSwitch,
  StyledCheckbox,
  Slider,
  Styledtext,
} from './Switch.styles'
import PropTypes from 'prop-types'

export const Switch = ({
  initialState,
  label,
  ontoggle,
  disabled,
  ...props
}) => {
  const [toggle, setToggle] = useState(initialState)

  const handleToggle = useCallback(
    e => {
      setToggle(e.target.checked)
      ontoggle(e.target.checked)
    },
    [ontoggle]
  )

  return (
    <StyledSwitchWrapper disabled={disabled}>
      <StyledToggleSwitch>
        <StyledCheckbox
          {...props}
          disabled={disabled}
          data-testid='toggle'
          type='checkbox'
          checked={toggle}
          value={toggle}
          onChange={e => handleToggle(e)}
        />
        <Slider toggle={toggle} data-testid='slider'></Slider>
      </StyledToggleSwitch>
      {label && <Styledtext data-testid='label'>{label}</Styledtext>}
    </StyledSwitchWrapper>
  )
}

Switch.defaultProps = {
  label: '',
  ontoggle: () => {},
  initialState: false,
}

Switch.propTypes = {
  /** Label Text*/
  label: PropTypes.string,
  /** Function that is trigger when switch is toggled*/
  ontoggle: PropTypes.func,
  /** Initial On/Off state of the switch*/
  initialState: PropTypes.bool,
  /** Whether the Input is disabled*/
  disabled: PropTypes.bool,
}
