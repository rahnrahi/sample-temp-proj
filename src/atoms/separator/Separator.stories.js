import React from 'react'
import { Separator } from '.'

export default {
  title: 'Components/Misc Control/Separator',
  component: Separator,
}

const Template = args => <Separator {...args} />

export const Primary = Template.bind({})
Primary.args = {}
