import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import { Separator } from './'

afterEach(cleanup)

describe('<Separator/>', () => {
  it('renders correctly', () => {
    const { container } = render(<Separator />)
    expect(container).toMatchSnapshot()
  })
  it('should render separator with correct text', () => {
    render(<Separator text='Hello' />)
    const elem = screen.getByRole('heading', { level: 6 })
    const elem2 = screen.getByText(/hello/i)
    expect(elem).toBeInTheDocument()
    expect(elem2).toBeInTheDocument()
  })
})
