import styled from 'styled-components'
import { theme } from '../../shared/index'

export const SeparatorElement = styled.h6`
  height: 24px;
  margin: 0;
  background-color: ${theme.palette.ui.neutral.grey4};
  border-radius: 25px;
  min-width: 31px;
  padding: 0 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${theme.palette.brand.primary.charcoal};
  border-bottom: 1px solid ${theme.palette.ui.neutral.grey4};
  position: relative;
  top: 11px;
`
export const SeparatorContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  max-height: 24px;
`
export const BorderContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  border-bottom: 1px solid ${theme.palette.ui.neutral.grey4};
`
