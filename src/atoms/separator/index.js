import React from 'react'
import PropTypes from 'prop-types'
import {
  BorderContainer,
  SeparatorContainer,
  SeparatorElement,
} from './Separator.styles'

export const Separator = ({ text }) => {
  return (
    <SeparatorContainer>
      <BorderContainer>
        <SeparatorElement>{text}</SeparatorElement>
      </BorderContainer>
    </SeparatorContainer>
  )
}
Separator.defaultProps = {
  text: 'OR',
}
Separator.propTypes = {
  text: PropTypes.string,
}
