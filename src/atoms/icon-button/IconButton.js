import React from 'react'
import PropTypes from 'prop-types'
import { StyledIconButton } from './IconButton.styles'
import { Icons } from '../icon/Icon'
import cx from 'classnames'
export const IconButton = ({ icon, type, iconSize, ...rest }) => {
  const Icon = Icons[icon]
  return (
    <StyledIconButton
      className={cx({
        [type]: type,
      })}
      {...rest}
    >
      {Icon && (
        <Icon
          className='icon left-icon'
          size={iconSize}
          data-testid={`icon-${icon}`}
        />
      )}
    </StyledIconButton>
  )
}

IconButton.defaultProps = {
  disabled: false,
  isRounded: false,
  buttonSize: '40px',
  type: 'primary',
  iconSize: 16,
}

IconButton.propTypes = {
  /** Button Text */
  text: PropTypes.string,
  /** Function Callback */
  onClick: PropTypes.func.isRequired,
  /** By Default isisRoundIcon=true, default is false */
  isRounded: PropTypes.bool,
  /** Icon 'Add, Edit, Close, DownArrow, Info', default null */
  icon: PropTypes.string,
  /** Icon position 'left' and 'right', default undefined */
  iconPosition: PropTypes.string,
  /** Icon Size, 10, 12, 16 , 18 and 24 */
  iconSize: PropTypes.number,
  /** Button size */
  buttonSize: PropTypes.string,
  /** Button type. default primary */
  type: PropTypes.string,
  /** Function Callback */
  onMouseOver: PropTypes.func,
  /** True if the button is disabled; false otherwise */
  disabled: PropTypes.bool,
}
