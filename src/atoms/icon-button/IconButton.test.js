import React from 'react'
import renderer from 'react-test-renderer'
import { IconButton } from './IconButton'
import 'jest-styled-components'
import { render, cleanup, screen, fireEvent } from '@testing-library/react'

describe('Button', () => {
  it('renders primary Button + Icon correctly with default props', () => {
    const fn = jest.fn()
    const tree = renderer
      .create(
        <IconButton
          disabled={false}
          type='primary'
          icon='Close'
          isRounded={true}
          buttonSize='40px'
          iconSize={10}
          onClick={fn}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})

afterEach(cleanup)
beforeEach(() => {
  jest.clearAllMocks()
})

describe('Icon Button Component', () => {
  const onClickFn = jest.fn(() => {})
  let props = {
    icon: 'Add',
    iconSize: 24,
    buttonSize: null,
    onClick: onClickFn,
  }

  it('Check Icon Button exists', () => {
    render(<IconButton {...props} />)
    expect(screen.getByTestId('icon-Add')).toBeInTheDocument()
  })
})

describe('InfoIcon Component', () => {
  const onHoverFn = jest.fn(() => {})
  const onClickFn = jest.fn(() => {})
  let props = {
    icon: 'Info',
    iconSize: 24,
    onMouseOver: onHoverFn,
    onClick: onClickFn,
  }

  it('Check InfoIcon exists', () => {
    render(<IconButton {...props} />)
    expect(screen.getByTestId('icon-Info')).toBeInTheDocument()
  })

  it('Check onClick Event is called on InfoIcon', () => {
    render(<IconButton {...props} isPrimary />)
    const infoDiv = screen.getByTestId('icon-Info')
    fireEvent.click(infoDiv)
    expect(onClickFn).toBeCalledTimes(1)
  })

  it('Check onHover Event is called on InfoIcon', () => {
    render(<IconButton {...props} />)
    const infoDiv = screen.getByTestId('icon-Info')
    fireEvent.mouseOver(infoDiv)
    expect(onHoverFn).toBeCalledTimes(1)
  })
})
