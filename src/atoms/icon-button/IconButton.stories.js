import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { IconButton } from './IconButton'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import { ICON_BTN_DESIGN, ICON_BTN_PRESENTATION } from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Icon button',
  decorators: [withDesign],
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <IconButton {...args} />

export const primary = Template.bind({})
primary.args = {
  disabled: false,
  type: 'primary',
  icon: 'Add',
  isRounded: true,
  isPrimary: true,
  buttonSize: '40px',
  iconSize: 16,
  onClick: e => console.log('button clicked', e),
}
primary.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: ICON_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: ICON_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
export const secondary = Template.bind({})
secondary.args = {
  disabled: false,
  type: 'secondary',
  icon: 'Close',
  isRounded: true,
  isPrimary: false,
  buttonSize: '40px',
  iconSize: 16,
  onClick: e => console.log('button clicked', e),
}
secondary.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: ICON_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: ICON_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
