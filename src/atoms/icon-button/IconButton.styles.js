import styled from 'styled-components'
import { theme } from '../../shared/index'

export const StyledIconButton = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  font-size: 14px;
  line-height: 16px;
  font-family: Gilroy-Medium;
  text-align: center;
  background: transparent;
  border: 1px solid transparent;
  outline: none;
  cursor: pointer;
  color: ${theme.palette.brand.primary.charcoal};
  padding: 0px;
  .icon,
  .icon path {
    fill: ${theme.palette.brand.primary.white};
  }

  /** global */
  &:disabled {
    cursor: initial;
  }
  &.secondary {
    background-color: ${theme.palette.brand.primary.white};
    border-color: ${theme.palette.ui.neutral.grey4};
    .icon,
    .icon path {
      fill: ${theme.palette.brand.primary.charcoal};
    }

    &:hover {
      border: 0.8px ${theme.palette.ui.cta.charcoal} solid;
    }

    &:focus {
      border: 2px ${theme.palette.ui.cta.yellow} solid;
    }

    &:active {
      opacity: 1;
      background-color: ${theme.palette.ui.neutral.grey1};
      border-color: ${theme.palette.ui.neutral.grey1};
      .icon,
      .icon path {
        fill: ${theme.palette.brand.primary.white};
      }
    }
    &:disabled {
      background-color: ${theme.palette.ui.neutral.grey8};
      border-color: ${theme.palette.ui.neutral.grey8};
      pointer-events: none;
      .icon,
      .icon path {
        fill: ${theme.palette.ui.neutral.grey7};
      }
    }
  }

  &.primary {
    background-color: ${theme.palette.ui.cta.blue};
    border-color: ${theme.palette.ui.cta.blue};

    &:hover {
      opacity: 0.8;
    }

    &:focus {
      border: 2px ${theme.palette.ui.cta.yellow} solid;
    }

    &:active {
      opacity: 1;
      background-color: ${theme.palette.ui.states.pressed};
      border-color: ${theme.palette.ui.states.pressed};
    }
    &:disabled {
      opacity: 0.8;
      pointer-events: none;
    }
  }

  ${({ buttonSize }) => `
    width: ${buttonSize ? buttonSize : '40px'};
    height: ${buttonSize ? buttonSize : '40px'};
  `}

  ${({ isRounded }) => `
    ${
      isRounded &&
      `
      border-radius: 50%;
    `
    }
  `}
`
