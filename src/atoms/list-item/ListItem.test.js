import React from 'react'
import { render } from '@testing-library/react'
import { ListItem } from './'

describe('<ListItem/>', () => {
  it('should render correctly', () => {
    const { container } = render(<ListItem>List item content</ListItem>)
    expect(container).toMatchSnapshot()
  })
})
