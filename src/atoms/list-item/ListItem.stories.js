import React from 'react'
import { ListItem } from './index'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const Template = () => {
  return <ListItem>Basic list item</ListItem>
}

export const BasicTitle = Template.bind({})
export default {
  title: 'Modules/Cards/List/ListItem',
  component: ListItem,
}
BasicTitle.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
