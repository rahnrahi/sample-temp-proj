import styled from 'styled-components'
import { theme } from '../../shared'

export const ListItem = styled.div`
  ${theme.typography.h6}
  padding: 1rem 1rem;
  border-bottom: 1px ${theme.palette.ui.neutral.grey4} solid;
`
