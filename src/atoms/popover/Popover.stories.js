import React from 'react'
import { Popover } from './'
import { Link } from '../../atoms/link/Link'
import { PopoverContent } from './mock'
import PopoversDocs from '../../../docs/Popovers.mdx'
import { POPOVER_DESIGN, POPOVER_PRESENTATION } from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Popover',
  component: Popover,
  argTypes: {
    placement: {
      control: {
        type: 'select',
        options: ['bottom', 'top', 'left', 'right'],
      },
    },
  },
  args: {
    showArrow: true,
    className: 'primary',
    placement: 'bottom',
    target: <Link text='User Profile' href='/' />,
  },
  parameters: {
    docs: {
      page: PopoversDocs,
    },
  },
}

export const Default = args => (
  <div style={{ position: 'relative', width: '150px', margin: '300px auto' }}>
    <Popover {...args}>
      <PopoverContent />
    </Popover>
  </div>
)
Default.storyName = 'Popover'
Default.parameters = getDesignSpecifications(
  POPOVER_DESIGN,
  POPOVER_PRESENTATION
)
