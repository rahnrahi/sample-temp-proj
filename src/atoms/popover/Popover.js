import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import { StyledPopover } from './Popover.styles'

export const Popover = ({
  placement,
  children,
  className,
  showArrow = false,
  target,
  ...props
}) => {
  const [isVisible, setVisible] = useState(false)
  const poRef = useRef()

  useEffect(() => {
    if (poRef.current) {
      const elem = poRef.current
      elem.addEventListener('mouseenter', handleHover)
      elem.addEventListener('mouseleave', handleOut)
      return () => {
        elem.removeEventListener('mouseenter', handleHover)
        elem.removeEventListener('mouseleave', handleOut)
      }
    }
  }, [poRef])

  const handleHover = () => {
    setVisible(true)
  }

  const handleOut = () => {
    setVisible(false)
  }

  return (
    <StyledPopover
      className={`${isVisible ? 'active' : ''} ${className ? className : ''} ${
        placement ? placement : ''
      }`}
      {...props}
      ref={poRef}
    >
      {target}
      <div className='content'>
        <div className='content-inner'>
          {showArrow && <span className='arrows'></span>}
          {children && children}
        </div>
      </div>
    </StyledPopover>
  )
}
Popover.defaultProps = {
  placement: 'bottom',
}
Popover.propTypes = {
  /** to set visibility of popover */
  show: PropTypes.bool,
  /** custom popover view element/function can be passed here */
  children: PropTypes.node,
  /**class name*/
  className: PropTypes.string,
  showArrow: PropTypes.bool,
  /** A arrow position, left, right, top, bottom*/
  placement: PropTypes.string,
  style: PropTypes.object,
  /** A callback function*/
  onClick: PropTypes.func,
  /** Target: The element which need popover enabled that has to be passed as a target */
  target: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.node,
    PropTypes.func,
  ]),
}
