import React, { useEffect, useReducer, useState } from 'react'
import PropTypes from 'prop-types'
import { CKEditor } from 'ckeditor4-react'
import editorConfig from './_config'
import classnames from 'classnames'
import useLocaleDirection from '../../hooks/render-rtl'

export const HTMLeditor = ({
  text,
  onTextChange,
  onTextChangeHTML,
  classNames,
  localeCode,
  ...rest
}) => {
  const [editorValue, setEditorValue] = useState(text || '')
  const [editorInstance, setInstance] = useState(null)
  const [editorConfigData, setEditorConfigData] = useState(editorConfig)
  const [counterKey, setCounterKey] = useReducer(c => c + 1, 0)

  const CalculateDirection = code => {
    const direction = useLocaleDirection(code)
    const config = editorConfigData
    if (config.contentsLangDirection !== direction) {
      config.contentsLangDirection = direction
      setEditorConfigData(config)
      setCounterKey()
    }
  }

  useEffect(() => {
    CalculateDirection(localeCode)
  }, [localeCode])

  const getSimpleText = () => {
    return (
      editorInstance &&
      editorInstance.instances &&
      editorInstance.instances.customEditor &&
      editorInstance.instances.customEditor.document &&
      editorInstance.instances.customEditor.document.getBody() &&
      editorInstance.instances.customEditor.document.getBody().getText()
    )
  }

  const onEditorTextChange = e => {
    const data = e.editor.getData()
    setEditorValue(data)
  }

  useEffect(() => {
    // Simple text callback
    if (onTextChange && typeof onTextChange === 'function') {
      let simpleText = getSimpleText()

      if (!simpleText) {
        simpleText = editorValue
      }

      onTextChange(simpleText)
    }

    // HTML text callback
    if (onTextChangeHTML && typeof onTextChangeHTML === 'function') {
      onTextChangeHTML(editorValue)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [editorInstance, editorValue])

  return (
    <div
      className={
        classNames
          ? classnames('custom-editor-wrapper', classNames)
          : 'custom-editor-wrapper'
      }
    >
      <CKEditor
        key={counterKey}
        name='customEditor'
        config={editorConfigData}
        initData={editorValue}
        onBeforeLoad={instance => setInstance(instance)}
        onChange={onEditorTextChange}
        {...rest}
      />
    </div>
  )
}

HTMLeditor.defaultProps = {
  text: 'This is sample text',
  classNames: '',
}

HTMLeditor.propTypes = {
  /** Initial text data*/
  text: PropTypes.string,
  /** Function when data in changed. Called with 1 argument i.e HTML text data */
  onTextChangeHTML: PropTypes.func,
  /** Function when data in changed. Called with 1 argument i.e Simple text data */
  onTextChange: PropTypes.func,
  /** List of space separated class names for adding custom classes*/
  classNames: PropTypes.string,
  /** Locale Code to Render the content in that language */
  localeCode: PropTypes.string,
}
