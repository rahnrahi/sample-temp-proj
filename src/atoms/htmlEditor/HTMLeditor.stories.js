import React from 'react'
import { HTMLeditor } from './index'
import HTMLEditorDocs from '../../../docs/HTMLEditor.mdx'
import {
  HTMLEDITOR_DESIGN,
  HTMLEDITOR_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/HTML Editor',
  component: HTMLeditor,
  parameters: {
    docs: {
      page: HTMLEditorDocs,
    },
  },
}

const Template = args => <HTMLeditor {...args} />

export const HTMLEditor = Template.bind({})
HTMLEditor.args = {
  onTextChange: text => console.log('Text received', text),
  onTextChangeHTML: text => console.log('HTML received', text),
  localeCode: '',
}
HTMLEditor.parameters = getDesignSpecifications(
  HTMLEDITOR_DESIGN,
  HTMLEDITOR_PRESENTATION
)
