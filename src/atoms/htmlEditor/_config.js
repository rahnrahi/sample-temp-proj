export default {
  removePlugins: ['about', 'elementspath', 'removeformat'],
  removeButtons: 'Anchor',
  extraPlugins: 'justify',
}
