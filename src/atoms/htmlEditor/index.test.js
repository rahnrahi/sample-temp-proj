import React from 'react'
import { render } from '@testing-library/react'
import 'jest-styled-components'
import { HTMLeditor } from './index'

describe('<HTMLeditor/>', () => {
  it('should render the HTMLeditor component', async () => {
    const textChangeHandler = jest.fn()
    const textChangeHTMLHandler = jest.fn()
    const { container } = render(
      <HTMLeditor
        classNames={'editor'}
        text='Sample Text'
        onTextChange={textChangeHandler}
        onTextChangeHTML={textChangeHTMLHandler}
      />
    )

    expect(textChangeHandler).toHaveBeenCalledTimes(1)
    expect(textChangeHTMLHandler).toHaveBeenCalledTimes(1)
    expect(container).toMatchSnapshot()
  })
})
