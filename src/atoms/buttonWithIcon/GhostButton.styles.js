import styled from 'styled-components'
import { theme } from '../../shared/index'

export const StyledGhostButton = styled.button`
  display: flex;
  min-width: 101px;
  height: 32px;
  flex-direction: ${({ iconPosition }) =>
    iconPosition && iconPosition === 'right' ? 'row-reverse' : 'row'};
  align-items: center;
  justify-content: center;
  ${theme.typography.link.css};
  border-radius: ${({ borderRadius }) => (borderRadius ? borderRadius : '0px')};
  background-color: transparent;
  color: ${theme.palette.brand.primary.charcoal};
  border: 1px solid transparent;
  .icon {
    fill: ${theme.palette.brand.primary.charcoal};
    padding-right: ${({ iconPosition }) =>
      iconPosition === 'right' ? '0px' : '4px'};
    padding-left: ${({ iconPosition }) =>
      iconPosition === 'right' ? '4px' : '0px'};
  }

  ${({ disabled }) =>
    disabled &&
    `
     background-color: ${theme.palette.background};
     color: ${theme.palette.ui.neutral.grey7};
     pointer-events: none;

     .icon {
      fill: ${theme.palette.ui.neutral.grey7};
     }
  `};

  :hover {
    background-color: ${theme.palette.ui.states.disabled};
    color: ${theme.palette.ui.states.pressed};
    .icon {
      fill: ${theme.palette.ui.states.pressed};
    }
  }

  :active {
    // Todo: this color is not present in theme file atm so hard coding it
    background-color: #f3f8ff;
    color: ${theme.palette.ui.states.pressed};
    border: 1px solid transparent;
    .icon {
      fill: ${theme.palette.ui.states.pressed};
    }
  }

  :focus {
    border: 1px solid ${theme.palette.ui.cta.yellow};
    background-color: transparent;
  }
`
