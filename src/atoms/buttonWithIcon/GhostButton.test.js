import React from 'react'
import { render, screen } from '@testing-library/react'
import { GhostButton } from './GhostButton'
import 'jest-styled-components'

describe('<GhostButton />', () => {
  it('should render ghost button', () => {
    const { container } = render(<GhostButton text='Test Ghost Button' />)
    expect(container).toMatchSnapshot()
  })

  it('should render ghost button with correct text', () => {
    render(<GhostButton text='Test Ghost Button' />)
    const btn = screen.getByRole('button', { name: 'Test Ghost Button' })
    expect(btn).toBeInTheDocument()
  })

  it('should render disabled ghost button', () => {
    render(<GhostButton text='Test Ghost Button' disabled />)
    const btn = screen.getByRole('button', { name: 'Test Ghost Button' })
    expect(btn).toBeDisabled()
  })

  it('should render ghost button without icon', () => {
    render(<GhostButton text='Test Ghost Button' borderRadius='4px' />)
    const btn = screen.getByRole('button', { name: 'Test Ghost Button' })
    const iconElement = btn.getElementsByClassName('icon')[0]
    expect(iconElement).toBeUndefined()
  })

  it('should render ghost button with icon', () => {
    render(
      <GhostButton
        text='Test Ghost Button'
        showIcon
        icon='Close'
        iconPosition='left'
      />
    )
    const btn = screen.getByRole('button', { name: 'Test Ghost Button' })
    const iconElement = btn.getElementsByClassName('icon')[0]
    expect(iconElement).toBeInTheDocument()
  })
})
