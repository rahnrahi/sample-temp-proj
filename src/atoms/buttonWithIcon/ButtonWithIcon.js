import React from 'react'
import PropTypes from 'prop-types'
import { StyledIconButton } from './ButtonWithIcon.styles'
import { Icons } from '../icon/Icon'
import { theme } from '../../shared'
export const ButtonWithIcon = ({
  text,
  icon,
  iconPosition,
  isRoundIcon,
  iconSize,
  ...rest
}) => {
  const Icon = Icons[icon]

  if (isRoundIcon) {
    let size = icon && Icon && (Icon === 'Add' || Icon === 'Close') ? 14 : 16
    return (
      <StyledIconButton {...rest} isRoundIcon={isRoundIcon}>
        {Icon && <Icon className='icon' size={iconSize ? iconSize : size} />}
      </StyledIconButton>
    )
  }
  return (
    <StyledIconButton {...rest}>
      {iconPosition && iconPosition === 'left' && Icon && (
        <Icon className='icon left-icon' size={iconSize} />
      )}
      {text}
      {iconPosition && iconPosition === 'right' && Icon && (
        <Icon className='icon right-icon' size={iconSize} />
      )}
    </StyledIconButton>
  )
}

ButtonWithIcon.defaultProps = {
  disabled: false,
  isPrimary: true,
  theme: 'none',
  emphasis: 'none',
  isRoundIcon: false,
  isActive: false,
  buttonSize: '40px',
  iconSize: 16,
  primaryIconColor: theme.palette.brand.primary.white,
  secondaryIconColor: theme.palette.brand.primary.charcoal,
}

ButtonWithIcon.propTypes = {
  /** Button Text*/
  text: PropTypes.string,
  /** Function Callback*/
  onClick: PropTypes.func.isRequired,
  /** By Default isPrimary=true*/
  isPrimary: PropTypes.bool,
  /** By Default isisRoundIcon=true, default is false*/
  isRoundIcon: PropTypes.bool,
  /** Theme 'none',  'light'  & 'dark', default is 'none' */
  theme: PropTypes.oneOf(['none', 'dark', 'light']),
  /** Emphasis 'none',  'light'  & 'dark', default is 'none', it will apply for only for secondary button */
  emphasis: PropTypes.oneOf(['none', 'high', 'low']),
  /** Icon 'Add, Edit, Close, DownArrow', default null*/
  icon: PropTypes.string,
  /** Icon position 'left' and 'right', default undefined */
  iconPosition: PropTypes.string,
  /** Explicit Active State, only for rounded button with icon`*/
  isActive: PropTypes.bool,
  /** Icon Size, 10, 12, 14, 16 , 18 and 24 */
  iconSize: PropTypes.number,
  /** Primary Button Icon Color */
  primaryIconColor: PropTypes.string,
  /** Secondary button Icon Color */
  secondaryIconColor: PropTypes.string,
  /** Button size */
  buttonSize: PropTypes.string,
  /** Stroke Color eg: #ff0000. default transparent */
  strokeColor: PropTypes.string,
  /** True if the button is disabled; false otherwise */
  disabled: PropTypes.bool,
}
