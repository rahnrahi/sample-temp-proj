import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { ButtonWithIcon } from './ButtonWithIcon'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import { IconPosition } from '../button/Button.enum'
import {
  PRIMARY_BTN_DESIGN,
  PRIMARY_BTN_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Button/Primary/With Icon',
  component: ButtonWithIcon,
  decorators: [withDesign],
  argTypes: {
    text: { control: 'text' },
    onClick: { action: 'clicked' },
    theme: {
      control: {
        type: 'select',
        options: ['none', 'dark', 'light'],
      },
    },
    emphasis: {
      control: {
        type: 'select',
        options: ['none', 'high', 'low'],
      },
    },
    iconSize: {
      control: {
        type: 'select',
        options: [10, 12, 14, 16, 18, 24],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: [
          'Close',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'UpCaret',
          'LeftArrow',
          'RightArrow',
          'DownCaret',
          'RightCaret',
          'UpArrow',
          'Link',
          'Search',
          'Delete',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'Notification',
          'Eye',
          'Checkmark',
          'Cross',
          null,
        ],
      },
    },
    iconPosition: {
      control: {
        type: 'select',
        options: ['left', 'right'],
      },
    },
    primaryIconColor: {
      control: {
        type: 'color',
      },
    },
    secondaryIconColor: {
      control: {
        type: 'color',
      },
    },
    strokeColor: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <ButtonWithIcon {...args} />

export const WithIconLeft = Template.bind({})
WithIconLeft.args = {
  text: 'Primary',
  disabled: false,
  isPrimary: true,
  icon: 'Edit',
  iconPosition: IconPosition.LEFT,
}
WithIconLeft.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: PRIMARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: PRIMARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
export const WithIconRight = Template.bind({})
WithIconRight.args = {
  text: 'Primary',
  disabled: false,
  isPrimary: true,
  icon: 'Edit',
  iconPosition: IconPosition.RIGHT,
}
WithIconRight.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: PRIMARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: PRIMARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
