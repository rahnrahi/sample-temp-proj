import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { GhostButton } from './GhostButton'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import {
  TERTIARY_BTN_DESIGN,
  TERTIARY_BTN_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Button/Tertiary',
  component: GhostButton,
  decorators: [withDesign],
  argTypes: {
    iconSize: {
      control: {
        type: 'select',
        options: [10, 12, 14, 16, 18, 24],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: [
          'Close',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'UpCaret',
          'LeftArrow',
          'RightArrow',
          'DownCaret',
          'RightCaret',
          'UpArrow',
          'Link',
          'Search',
          'Delete',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'Notification',
          'Eye',
          'Checkmark',
          'Cross',
        ],
      },
    },
    iconPosition: {
      control: {
        type: 'select',
        options: ['left', 'right'],
      },
    },
  },
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <GhostButton {...args} />

export const Small = Template.bind({})
Small.args = {
  text: 'Button',
}
Small.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: TERTIARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: TERTIARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
