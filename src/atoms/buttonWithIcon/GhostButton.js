import React from 'react'
import PropTypes from 'prop-types'
import { StyledGhostButton } from './GhostButton.styles'
import { Icons } from '../icon/Icon'
import noop from 'lodash/noop'

export const GhostButton = ({
  showIcon,
  text,
  icon,
  iconPosition,
  iconSize,
  onClickHandler,
  ...rest
}) => {
  const Icon = Icons[icon]
  return (
    <StyledGhostButton
      iconPosition={iconPosition}
      {...rest}
      onClick={onClickHandler}
    >
      {showIcon && <Icon className='icon' size={iconSize} />}
      <span>{text}</span>
    </StyledGhostButton>
  )
}

GhostButton.defaultProps = {
  showIcon: false,
  text: 'Ghost Button',
  iconPosition: 'left',
  icon: 'Close',
  iconSize: 16,
  borderRadius: '5px',
  disabled: false,
  onClickHandler: noop,
}

GhostButton.propTypes = {
  showIcon: PropTypes.bool,
  text: PropTypes.string,
  borderRadius: PropTypes.string,
  icon: PropTypes.string,
  iconPosition: PropTypes.oneOf(['right', 'left']),
  iconSize: PropTypes.number,
  disabled: PropTypes.bool,
  onClickHandler: PropTypes.func,
}
