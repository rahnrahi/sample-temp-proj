import React from 'react'
import { Textarea } from './'
import { render, cleanup, screen } from '@testing-library/react'

afterEach(cleanup)

const limit = 100

const props = {
  className: 'multiline',
  label: 'Description',
  limit,
}

describe('<Textarea/>', () => {
  it('should render a Textarea component', () => {
    const { container } = render(<Textarea {...props} />)

    expect(container).toMatchSnapshot()
  })
  it('should render a disabled Textarea component', () => {
    const { getByRole, rerender } = render(<Textarea {...props} />)

    const textarea = getByRole('textbox', { name: 'Description' })

    expect(textarea).not.toBeDisabled()

    rerender(<Textarea {...props} disabled={true} />)

    expect(textarea).toBeDisabled()
  })
  it('should show the character count and limit', () => {
    let maxLengthString = ''

    for (let i = 0; i < limit; ++i) {
      maxLengthString += 'a'
    }

    const { queryByText, rerender } = render(<Textarea {...props} />)

    const limitContainer = queryByText(`/${limit}`).parentNode

    expect(limitContainer).toHaveTextContent(`0/${limit}`)

    rerender(
      <Textarea
        {...props}
        textareaProps={{ value: maxLengthString.substring(0, limit / 2) }}
      />
    )

    expect(limitContainer).toHaveTextContent(`${limit / 2}/${limit}`)
  })

  it('should render with edit button', () => {
    const onEditButtonClick = jest.fn()
    const { container } = render(
      <Textarea
        {...props}
        showEditButton={true}
        onEditButtonClick={onEditButtonClick}
      />
    )
    expect(container).toMatchSnapshot()
    const editButton = screen.getByTestId('textarea-edit-button')
    expect(editButton).toBeInTheDocument()

    editButton.click()
    expect(onEditButtonClick.mock.calls.length).toEqual(1)
  })

  it('should render with the label', () => {
    const { container } = render(<Textarea {...props} label={'Test'} />)
    expect(container).toMatchSnapshot()
  })

  it('should render with error and errorMessage', () => {
    const { container } = render(
      <Textarea {...props} error={true} errorMessage={'Something went wrong'} />
    )
    expect(container).toMatchSnapshot()
  })
})
