import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledTextareaComponent = styled.div`
  font-family: ${theme.typography.link.fontFamily};
  ${({ width }) => width && `width: ${width}`};

  .error {
    margin-top: 4px;
    margin-left: 1px;
    letter-spacing: 0.02em;
    font-size: 13px;
    color: ${theme.palette.ui.cta.red};
  }
`
export const StyledTextareaContainer = styled.div`
  .textarea_parent {
    width: 100%;
    height: 100%;
    position: relative;
    box-sizing: border-box;
    display: block;
  }

  .textarea {
    &__label {
      font-size: 13px;
      font-weight: normal;
      display: block;
      z-index: 1;
      text-overflow: ellipsis;
      box-sizing: border-box;
      color: ${({ error }) =>
        error
          ? theme.palette.ui.cta.red
          : theme.palette.brand.primary.charcoal};
      padding-bottom: 16px;
      letter-spacing: 0.02em;
      padding-top: 26px;

      &--hidden {
        position: absolute;
        text-indent: -9999px;
      }
    }
  }

  &.container_focus {
    .parent {
      border-top: none;
      border: 1px solid ${theme.palette.ui.states.active};
      padding: 16px;
    }
    .has-edit {
      padding-bottom: 25px;
    }
    .limit {
      display: block;
    }
    .textarea {
      &__label {
        color: ${({ error }) =>
          error ? theme.palette.ui.cta.red : theme.palette.brand.primary.gray};
      }
    }
  }
  &.filled {
    .textarea {
      &__label {
        color: ${({ error }) =>
          error ? theme.palette.ui.cta.red : theme.palette.brand.primary.gray};
      }
    }
  }
  &.filled:not(.container_focus) {
    .textarea {
      &__label {
        padding-bottom: 9px;
      }
    }
  }
  .has-content {
    padding: 0px 0px 16px 0px;
    border: none;
    border-bottom: ${({ disabled, error }) => {
      if (disabled) return `1px solid ${theme.palette.ui.neutral.grey8}`
      return `1px solid ${
        error ? theme.palette.ui.cta.red : theme.palette.brand.primary.charcoal
      }`
    }}
  &.disabled {
    textarea {
      pointer-events: none;
      opacity: 0.5;
      color: inherit;
    }
    .textarea__label {
      color: ${theme.palette.ui.neutral.grey7};
    }
  }
`

export const StyledMultiline = styled.div`
  position: relative;
  border-top: 1px solid
    ${({ disabled, error }) => {
      if (disabled) return theme.palette.ui.neutral.grey8
      return error
        ? theme.palette.ui.cta.red
        : theme.palette.brand.primary.charcoal
    }};

  textarea {
    max-width: 100%;
    box-sizing: border-box;
    width: 100%;
    resize: none;
    border: none;
    outline: none;
    font-size: 13px;
    padding: 0px;
    color: ${theme.palette.brand.primary.charcoal};
    font-family: ${theme.typography.link.fontFamily};
    display: block;
    ${({ isFocused, hasContent }) =>
      !isFocused &&
      !hasContent &&
      `
    height: 0px !important;
    `}

    &:disabled {
      color: ${theme.palette.ui.neutral.grey7};
      opacity: 1;
      background-color: transparent;
    }
  }

  .limit {
    display: flex;
    position: absolute;
    bottom: 3px;
    right: 6px;
    display: none;
    color: ${theme.palette.ui.neutral.grey2};
    font-size: 12px;
  }

  .edit-text-button {
    ${theme.typography.link.css};
    color: ${theme.palette.ui.cta.blue};
    cursor: pointer;
    width: min-content;
    position: absolute;
    bottom: 3px;
    left: 15px;
  }
`
