import React, { useState } from 'react'
import { Textarea } from '.'
import InputFieldsDocs from '../../../docs/InputFields.mdx'
import {
  INPUT_TEXTAREA_DESIGN,
  INPUT_TEXTAREA_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Components/Input Fields/Textarea',
  component: Textarea,
  parameters: {
    docs: {
      page: InputFieldsDocs,
    },
  },
}

const Template = args => {
  const [value, setValue] = useState('')

  return (
    <Textarea
      {...args}
      textareaProps={{
        onChange: e => setValue(e.target.value),
        value: value,
      }}
    />
  )
}

export const TextareaStory = Template.bind({})
TextareaStory.args = {
  width: '350px',
  className: 'multiline',
  label: 'Description',
  limit: 100,
}
TextareaStory.storyName = 'Textarea'
TextareaStory.parameters = getDesignSpecifications(
  INPUT_TEXTAREA_DESIGN,
  INPUT_TEXTAREA_PRESENTATION
)
