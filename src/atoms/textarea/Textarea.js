import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import autosize from 'autosize'
import {
  StyledMultiline,
  StyledTextareaContainer,
  StyledTextareaComponent,
} from './Textarea.styles'
import useLocaleDirection from '../../hooks/render-rtl'

const isFunction = x => typeof x === 'function'

const EditButton = ({ showEditButton, onEditButtonClick }) => {
  const editButtonClickHandler = e => {
    onEditButtonClick && isFunction(onEditButtonClick) && onEditButtonClick(e)
  }
  return showEditButton ? (
    <div
      data-testid='textarea-edit-button'
      className='edit-text-button'
      onClick={editButtonClickHandler}
    >
      Edit
    </div>
  ) : null
}

const ErrorComponent = ({ error, errorMessage, errorProps }) =>
  error ? (
    <div className='error' {...errorProps}>
      {errorMessage}
    </div>
  ) : null

const TextAreaLabel = ({ label, placeholder }) =>
  label ? (
    <span className='textarea__label'>{label}</span>
  ) : (
    // Used for ADA compliance
    <span className='textarea__label--hidden'>{placeholder}</span>
  )

const CharacterLimit = ({ isValidLimit, value, limit }) =>
  isValidLimit ? (
    <span className='limit'>
      <span>{value.length}</span>
      <span>/{limit}</span>
    </span>
  ) : null

export const Textarea = React.forwardRef(
  (
    {
      textareaProps,
      errorProps,
      label,
      className: containerClassName,
      error,
      errorMessage,
      width,
      showEditButton,
      onEditButtonClick,
      disabled,
      limit,
      localeCode,
      ...containerPropsRest
    },
    ref
  ) => {
    const [isFocused, setFocus] = useState(false)
    const dir = useLocaleDirection(localeCode)
    let tRef = useRef()
    useEffect(() => {
      if ((tRef && tRef.current) || (ref && ref.current)) {
        autosize(tRef.current || ref.current)
      }
    }, [tRef, ref])

    const handleBlur = event => {
      setFocus(false)

      if (isFunction(textareaProps.onBlur)) {
        textareaProps.onBlur(event)
      }
    }

    const handleFocus = event => {
      setFocus(true)

      if (isFunction(textareaProps.onFocus)) {
        textareaProps.onFocus(event)
      }
    }

    const {
      className: inputClassName,
      placeholder,
      boxed,
      value = '',
      ...textareaPropsRest
    } = textareaProps || {}

    const isValidLimit = limit && limit > 0

    if (isValidLimit) {
      textareaPropsRest.maxLength = limit
    }

    return (
      <StyledTextareaComponent className={containerClassName} width={width}>
        <StyledTextareaContainer
          className={cx({
            container_error: error,
            container_focus: isFocused || showEditButton,
            boxed: boxed,
            filled: value.length,
            disabled: disabled,
          })}
          disabled={disabled}
          error={error}
          {...containerPropsRest}
        >
          <label className='textarea_parent'>
            <TextAreaLabel label={label} placeholder={placeholder} />
            <StyledMultiline
              className={cx({
                [`has-content`]: value.length > 0,
                parent: true,
                ['has-edit']: showEditButton,
              })}
              isFocused={isFocused}
              disabled={disabled}
              error={error}
              hasContent={value.length > 0}
            >
              <textarea
                {...textareaPropsRest}
                placeholder={placeholder}
                onBlur={handleBlur}
                onFocus={handleFocus}
                className={inputClassName}
                value={value}
                ref={ref || tRef}
                rows={1}
                disabled={disabled}
                dir={dir}
              />
              <EditButton
                showEditButton={showEditButton}
                onEditButtonClick={onEditButtonClick}
              />
              <CharacterLimit
                value={value}
                limit={limit}
                isValidLimit={isValidLimit}
              />
            </StyledMultiline>
          </label>
        </StyledTextareaContainer>
        <ErrorComponent
          error={error}
          errorMessage={errorMessage}
          errorProps={errorProps}
        />
      </StyledTextareaComponent>
    )
  }
)

Textarea.propTypes = {
  /** expect true/false, if field has error */
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  label: PropTypes.string,
  iconLeft: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  textareaProps: PropTypes.shape({
    defaultValue: PropTypes.string,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onChange: PropTypes.func,
    type: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
  }),
  /** errorProps are used to pass extra param like data-testid */
  errorProps: PropTypes.object,
  /** width should be given with unit. eg: 100px */
  width: PropTypes.string,
  /** The maximum number of characters allowed. */
  limit: PropTypes.number,
  /**
   * **DEPRECATED:** No longer supported by the design system. Will be removed in
   * design system 4.0 upgrade.
   *
   * Show/Hide edit button
   */
  showEditButton: PropTypes.bool,
  /**
   * **DEPRECATED:** No longer supported by the design system. Will be removed in
   * design system 4.0 upgrade.
   *
   * Edit button callback
   */
  onEditButtonClick: PropTypes.func,
  /** Locale Code to render the Component in that language */
  localeCode: PropTypes.string,
}

EditButton.propTypes = {
  showEditButton: PropTypes.bool,
  onEditButtonClick: PropTypes.func,
}

ErrorComponent.propTypes = {
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  errorProps: PropTypes.object,
}

TextAreaLabel.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
}

CharacterLimit.propTypes = {
  value: PropTypes.string,
  limit: PropTypes.number,
  isValidLimit: PropTypes.bool,
}
