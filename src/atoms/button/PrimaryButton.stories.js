import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { Button } from './Button'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import { ButtonSize } from './Button.enum'
import {
  PRIMARY_BTN_DESIGN,
  PRIMARY_BTN_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Button/Primary',
  component: Button,
  decorators: [withDesign],
  argTypes: {
    text: { control: 'text' },
    onClick: { action: 'clicked' },
    size: {
      control: {
        type: 'select',
        options: ['small', 'large'],
      },
    },
    theme: {
      control: {
        type: 'select',
        options: ['none', 'dark'],
      },
    },
  },
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <Button {...args} />

export const Small = Template.bind({})
Small.args = {
  text: 'Button label',
  isPrimary: true,
  size: ButtonSize.SMALL,
}
Small.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: PRIMARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: PRIMARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Medium = Template.bind({})
Medium.args = {
  text: 'Button Label',
  disabled: false,
  isPrimary: true,
  size: 'medium',
}
Medium.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: PRIMARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: PRIMARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Large = Template.bind({})
Large.args = {
  text: 'Button Label',
  disabled: false,
  isPrimary: true,
  size: ButtonSize.LARGE,
}
Large.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: PRIMARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: PRIMARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
