import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { Button } from './Button'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import { ButtonSize, IconPosition } from './Button.enum'
import {
  SECONDARY_BTN_DESIGN,
  SECONDARY_BTN_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Button/Secondary',
  component: Button,
  decorators: [withDesign],
  argTypes: {
    text: { control: 'text' },
    onClick: { action: 'clicked' },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
    theme: {
      control: {
        type: 'select',
        options: ['none', 'dark'],
      },
    },
    iconSize: {
      control: {
        type: 'select',
        options: [8, 10, 12, 14, 16, 18, 24],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: [
          'Close',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'UpCaret',
          'LeftArrow',
          'RightArrow',
          'DownCaret',
          'RightCaret',
          'UpArrow',
          'Link',
          'Search',
          'Delete',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'Notification',
          'Eye',
          'Checkmark',
          'Cross',
        ],
      },
    },
    iconPosition: {
      control: {
        type: 'select',
        options: [IconPosition.LEFT, IconPosition.RIGHT],
      },
    },
  },
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <Button {...args} />

export const Small = Template.bind({})
Small.args = {
  text: 'Button Label',
  isPrimary: false,
  size: 'small',
}
Small.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: SECONDARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: SECONDARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Medium = Template.bind({})
Medium.args = {
  text: 'Button Label',
  isPrimary: false,
  size: 'medium',
}
Medium.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: SECONDARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: SECONDARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Large = Template.bind({})
Large.args = {
  text: 'Button label',
  isPrimary: false,
  size: ButtonSize.LARGE,
}
Large.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: SECONDARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: SECONDARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
