import React from 'react'
import PropTypes from 'prop-types'
import { StyledButton, StyledButtonWithIconWrapper } from './Button.styles'
import { ButtonSize, IconPosition } from './Button.enum'
import { Icons } from '../icon/Icon'

export const Button = ({
  text,
  icon,
  showIcon,
  isPrimary,
  iconSize,
  iconPosition,
  ...rest
}) => {
  const Icon = icon && Icons[icon]

  if (showIcon && !isPrimary) {
    return (
      <StyledButton {...rest}>
        <StyledButtonWithIconWrapper iconPosition={iconPosition}>
          {Icon && <Icon className='icon left-icon' size={iconSize} />}
          <div className='button-text'>{text}</div>
        </StyledButtonWithIconWrapper>
      </StyledButton>
    )
  }

  return (
    <StyledButton {...rest} isPrimary={isPrimary}>
      {text}
    </StyledButton>
  )
}

Button.defaultProps = {
  disabled: false,
  isPrimary: true,
  isTertiary: false,
  size: 'large',
  theme: 'none',
  onClick: () => {},
  showIcon: false,
  iconSize: 12,
  icon: 'Add',
  iconPosition: IconPosition.LEFT,
}

Button.propTypes = {
  /** Button Text*/
  text: PropTypes.oneOfType([PropTypes.string]).isRequired,
  /** Function Callback*/
  onClick: PropTypes.func.isRequired,
  /** By Default isPrimary=true*/
  isPrimary: PropTypes.bool,
  /** Size of the button 'small'  & 'large'*/
  size: PropTypes.oneOf([ButtonSize.SMALL, ButtonSize.LARGE]),
  /** Theme 'none',  'light'  & 'dark'*/
  theme: PropTypes.oneOf(['none', 'dark', 'light']),
  /** To Disable Button  default is false*/
  disabled: PropTypes.bool,
  /** To enable third button style, default is false*/
  isTertiary: PropTypes.bool,
  /**
   * Show/Hide Icon.
   * Only applicable on Secondary Button for now. */
  showIcon: PropTypes.bool,
  /** Icon 'Add, Edit, Close, DownArrow', default null*/
  icon: PropTypes.string,
  /** Icon Size. 8, 10, 12, 14, 16 , 18 and 24 */
  iconSize: PropTypes.number,
  /** Icon position */
  iconPosition: PropTypes.string,
}
