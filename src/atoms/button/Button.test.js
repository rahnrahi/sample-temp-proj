import React from 'react'
import renderer from 'react-test-renderer'
import { render, fireEvent, screen } from '@testing-library/react'
import { Button } from './Button'
import { IconPosition, ButtonSize } from './Button.enum'
import { QuaternaryButton } from './QuaternaryButton'
import 'jest-styled-components'

describe('Button', () => {
  it('renders primary Button correctly with default props', () => {
    const fn = jest.fn()
    const tree = renderer
      .create(
        <Button
          text='Button Label'
          isPrimary={true}
          size={ButtonSize.SMALL}
          onClick={fn}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders primary Button correctly with size:small, theme: dark, isPrimary:false', () => {
    const fn = jest.fn()
    const tree = renderer
      .create(
        <Button
          text='Button Label'
          isPrimary={true}
          size={ButtonSize.SMALL}
          onClick={fn}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders primary Button correctly with size:large, theme: light, isPrimary:false', () => {
    const fn = jest.fn()
    const tree = renderer
      .create(
        <Button
          text='Button Label'
          isPrimary={false}
          size={ButtonSize.LARGE}
          theme='light'
          onClick={fn}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with default props', () => {
    const fn = jest.fn()
    const { getByText } = render(
      <Button
        text='Button Label'
        isPrimary={true}
        size={ButtonSize.SMALL}
        onClick={fn}
      />
    )
    expect(getByText('Button Label')).toBeInTheDocument()
  })

  it('should call the click handlers correctly', () => {
    const clickHandler = jest.fn()
    const { getByText } = render(
      <Button
        text='Clickable Button'
        isPrimary={true}
        size={ButtonSize.SMALL}
        onClick={clickHandler}
      />
    )
    const clickableButton = getByText('Clickable Button')
    expect(clickableButton).toBeInTheDocument()
    fireEvent.click(clickableButton)
    expect(clickHandler).toHaveBeenCalledTimes(1)
  })

  it("shouldn't call the click handlers if the button is disabled", () => {
    const clickHandler = jest.fn()
    const { getByText } = render(
      <Button
        text='UnClickable Button'
        isPrimary={true}
        size={ButtonSize.SMALL}
        onClick={clickHandler}
        disabled
      />
    )
    const clickableButton = getByText('UnClickable Button')
    expect(clickableButton).toBeInTheDocument()
    fireEvent.click(clickableButton)
    expect(clickHandler).toHaveBeenCalledTimes(0)
  })

  it("shouldn't call the click handlers if the button is disabled", () => {
    const clickHandler = jest.fn()
    const { getByText } = render(
      <Button
        text='UnClickable Button'
        isPrimary={true}
        size={ButtonSize.SMALL}
        onClick={clickHandler}
        disabled
      />
    )
    const clickableButton = getByText('UnClickable Button')
    expect(clickableButton).toBeInTheDocument()
    fireEvent.click(clickableButton)
    expect(clickHandler).toHaveBeenCalledTimes(0)
  })

  it('renders quaternary Button correctly with default props', () => {
    const fn = jest.fn()
    const tree = renderer
      .create(<QuaternaryButton text='Button Label' onClickHandler={fn} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should call the click handlers of the quaternary button correctly', () => {
    const clickHandler = jest.fn()
    const { getByText } = render(
      <QuaternaryButton text='Clickable Button' onClickHandler={clickHandler} />
    )
    const clickableButton = getByText('Clickable Button')
    expect(clickableButton).toBeInTheDocument()
    fireEvent.click(clickableButton)
    expect(clickHandler).toHaveBeenCalledTimes(1)
  })

  it("shouldn't call the click handlers of the quaternary button if the button is disabled", () => {
    const clickHandler = jest.fn()
    const { getByText } = render(
      <QuaternaryButton
        text='UnClickable Button'
        onClickHandler={clickHandler}
        disabled
      />
    )
    const clickableButton = getByText('UnClickable Button')
    expect(clickableButton).toBeInTheDocument()
    fireEvent.click(clickableButton)
    expect(clickHandler).toHaveBeenCalledTimes(0)
  })

  it('should render quateranry button with icon on left', () => {
    render(
      <QuaternaryButton
        text='Test Quaternary Button'
        showIcon
        icon='Close'
        iconPosition={IconPosition.LEFT}
      />
    )
    const btn = screen.getByRole('button', { name: 'Test Quaternary Button' })
    const iconElement = btn.getElementsByClassName('icon')[0]
    expect(iconElement).toBeInTheDocument()
  })
  it('should render quateranry button with icon on right', () => {
    render(
      <QuaternaryButton
        text='Test Quaternary Button'
        showIcon
        icon='Close'
        iconPosition={IconPosition.RIGHT}
      />
    )
    const btn = screen.getByRole('button', { name: 'Test Quaternary Button' })
    const iconElement = btn.getElementsByClassName('icon')[0]
    expect(iconElement).toBeInTheDocument()
  })
})
