import styled, { css } from 'styled-components'
import { theme } from '../../shared/index'

const PrimaryNoThemeCss = css`
  &:hover {
    opacity: 0.8;
  }
  &:focus {
    border: 1px solid ${theme.palette.ui.cta.yellow};
  }
  &:active {
    background: ${theme.palette.ui.states.pressed};
    border: 1px solid ${theme.palette.ui.states.pressed};
    opacity: 1;
  }
  ${({ disabled }) => disabled && `opacity: 0.2;`}
`
const SecondaryNoThemeCss = css`
  .icon {
    fill: ${theme.palette.brand.primary.charcoal};
  }
  &:hover {
    border: 1px solid ${theme.palette.ui.neutral.charcoal};
    color: ${theme.palette.brand.primary.charcoal};

    .icon {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }
  &:focus {
    border: 2px solid ${theme.palette.ui.cta.yellow};
  }
  &:active {
    color: ${theme.palette.brand.primary.white};
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid ${theme.palette.ui.neutral.grey1};
  }
  ${({ disabled }) =>
    disabled &&
    `opacity: 1;
     background: ${theme.palette.ui.neutral.grey7};
     border: 1px solid ${theme.palette.ui.neutral.grey7};
     color: ${theme.palette.brand.primary.white};
     .icon {
      fill: ${theme.palette.brand.primary.white};
    }
  `}
`
const PrimaryDarkThemeCss = css`
  &:hover {
    background: ${theme.palette.brand.primary.white};
    color: ${theme.palette.brand.primary.charcoal};
    border: 1px solid ${theme.palette.brand.primary.white};
  }
  &:focus {
    background: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.cta.yellow};
    color: ${theme.palette.brand.primary.charcoal};
  }
  &:active {
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};
  }
  ${({ disabled }) =>
    disabled &&
    `opacity: 0.15;
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};
  `}
`
const SecondaryDarkThemeCss = css`
  color: ${theme.palette.brand.primary.white};
  background: ${theme.palette.brand.primary.charcoal};
  border: 1px solid ${theme.palette.brand.primary.white};
  .icon {
    fill: ${theme.palette.brand.primary.white};
  }
  &:hover {
    background: ${theme.palette.brand.primary.white};
    color: ${theme.palette.brand.primary.charcoal};
    border: 1px solid ${theme.palette.brand.primary.white};
    .icon {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }
  &:focus {
    background: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.cta.yellow};
    color: ${theme.palette.brand.primary.charcoal};
    .icon {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }
  &:active {
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};
    .icon {
      fill: ${theme.palette.brand.primary.white};
    }
  }
  ${({ disabled }) =>
    disabled &&
    `opacity: 0.2;
    background: ${theme.palette.brand.primary.charcoal};
    border: 1px solid ${theme.palette.ui.neutral.grey2};
    color: ${theme.palette.ui.neutral.grey2};
  `}
`
const TertiaryThemeCss = css`
  color: ${theme.palette.brand.primary.white};
  background: ${theme.palette.brand.primary.charcoal};
  border: 1px solid ${theme.palette.brand.primary.charcoal};
  &:hover {
    background: ${theme.palette.brand.primary.gray};
    border: 1px solid ${theme.palette.brand.primary.gray};
  }
  &:focus {
    background: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.cta.yellow};
    color: ${theme.palette.brand.primary.charcoal};
  }
  &:active {
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};
  }
  ${({ disabled }) =>
    disabled &&
    `background: ${theme.palette.ui.neutral.grey7};
    border: 1px solid ${theme.palette.ui.neutral.grey7};
  `}
`

const themePrimaryMap = new Map()
themePrimaryMap.set('none', PrimaryNoThemeCss)
themePrimaryMap.set('dark', PrimaryDarkThemeCss)
const themeSecondaryMap = new Map()
themeSecondaryMap.set('none', SecondaryNoThemeCss)
themeSecondaryMap.set('dark', SecondaryDarkThemeCss)
const themeTertiaryMap = new Map()
themeTertiaryMap.set('none', TertiaryThemeCss)

const PrimaryCss = css`
  color: ${theme.palette.brand.primary.white};
  background: ${theme.palette.ui.cta.blue};
  border: 1px solid ${theme.palette.ui.cta.blue};
  border-radius: 60px;
  cursor: pointer;
  outline: none;
  box-shadow: none;
  display: flex;
  justify-content: center;
  align-items: center;
  ${({ theme }) => themePrimaryMap.get(`${theme}`)};
`
const SecondaryCss = css`
  color: ${theme.palette.brand.primary.charcoal};
  background: ${theme.palette.brand.primary.white};
  border: 1px solid ${theme.palette.ui.neutral.grey3};
  border-radius: 60px;
  cursor: pointer;
  outline: none;
  box-shadow: none;
  display: flex;
  ${({ theme }) => themeSecondaryMap.get(`${theme}`)};
`
const TertiaryCss = css`
  color: ${theme.palette.brand.primary.white};
  background: ${theme.palette.brand.primary.charcoal};
  border: 1px solid ${theme.palette.brand.primary.charcoal};
  border-radius: 60px;
  cursor: pointer;
  outline: none;
  box-shadow: none;
  min-width: 100px;
  display: flex;
  align-items: center;
  ${({ theme }) => themeTertiaryMap.get(`${theme}`)};
`
const smallCss = css`
  ${theme.typography.link.css};
  min-width: 90px;
  height: 36px;
  padding: 8px 14px;
`

const mediumCss = css`
  ${({ isPrimary }) =>
    isPrimary
      ? `${theme.typography.link.css}; min-width: 105px; height: 36px; padding: 10px 16px;`
      : `${theme.typography.link.css}; min-width: 99px; height: 36px; padding: 10px 20px;`}
`
const largeCss = css`
  ${({ isPrimary }) =>
    isPrimary
      ? `${theme.typography.link.css}; min-width: 117px; height: 48px; padding: 15px 16px;`
      : `${theme.typography.link.css}; min-width: 103px; height: 48px; padding: 16px 20px;`}
`
const sizeMap = new Map()
sizeMap.set('small', smallCss)
sizeMap.set('medium', mediumCss)
sizeMap.set('large', largeCss)

export const StyledButton = styled.button`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  ${({ size, isPrimary }) => {
    if (size) return sizeMap.get(`${size}`)
    return isPrimary ? sizeMap.get('medium') : sizeMap.get('large')
  }};
  font-family: Gilroy-Medium;
  text-align: center;
  ${({ isPrimary, isTertiary }) => {
    if (isTertiary && !isPrimary) return TertiaryCss
    return isPrimary ? PrimaryCss : SecondaryCss
  }}
  ${({ disabled }) => disabled && `pointer-events: none;`}
`

export const StyledButtonWithIconWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: ${({ iconPosition }) =>
    iconPosition && iconPosition.toLowerCase() === 'left'
      ? 'row'
      : 'row-reverse'};
  align-items: center;

  .icon {
    ${({ iconPosition }) =>
      iconPosition && iconPosition.toLowerCase() === 'left'
        ? `padding-right: 10px`
        : `padding-left: 10px`};
  }
`
export const StyledQuaternaryButton = styled.button`
  display: flex;
  min-width: 100%;
  height: 72px;
  cursor: pointer;
  flex-direction: ${({ iconPosition }) =>
    iconPosition && iconPosition === 'right' ? 'row-reverse' : 'row'};
  align-items: center;
  justify-content: center;
  ${theme.typography.h6.css};
  border-radius: 4px;
  background-color: transparent;
  color: ${theme.palette.brand.primary.charcoal};
  border: 1px dashed ${theme.palette.ui.neutral.grey2};
  .icon {
    fill: ${theme.palette.brand.primary.charcoal};
    padding-right: ${({ iconPosition }) =>
      iconPosition === 'right' ? '0px' : '4px'};
    padding-left: ${({ iconPosition }) =>
      iconPosition === 'right' ? '4px' : '0px'};
  }

  ${({ disabled }) =>
    disabled &&
    `
     background-color: transparent;
     color: ${theme.palette.ui.neutral.grey7};
     pointer-events: none;

     .icon {
      fill: ${theme.palette.ui.neutral.grey7};
     }
  `};

  &:hover {
    background-color: ${theme.palette.ui.states.disabled};
    color: ${theme.palette.ui.states.pressed};
    border: 1px dashed ${theme.palette.ui.states.pressed};
    .icon {
      fill: ${theme.palette.ui.states.pressed};
    }
  }

  &:focus {
    border: 1px solid ${theme.palette.ui.cta.yellow};
    background-color: transparent;
  }

  &:active {
    // This color is not there in the existing theme.
    background-color: #e7f0ff;
    color: ${theme.palette.ui.states.pressed};
    border: 1px dashed ${theme.palette.ui.states.pressed};
    .icon {
      fill: ${theme.palette.ui.states.pressed};
    }
  }
`
