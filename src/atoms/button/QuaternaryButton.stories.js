import React from 'react'
import { withDesign } from 'storybook-addon-designs'
import { QuaternaryButton } from './QuaternaryButton'
import ButtonsDocs from '../../../docs/Buttons.mdx'
import {
  QUATERNARY_BTN_DESIGN,
  QUATERNARY_BTN_PRESENTATION,
} from '../../hooks/constants'

export default {
  title: 'Components/Buttons/Button/Quaternary',
  component: QuaternaryButton,
  decorators: [withDesign],
  argTypes: {
    iconSize: {
      control: {
        type: 'select',
        options: [10, 12, 14, 16, 18, 24],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: [
          'Close',
          'AssignedToMe',
          'Help',
          'Copy',
          'CircleDownArrow',
          'DownArrow',
          'UpCaret',
          'LeftArrow',
          'RightArrow',
          'DownCaret',
          'RightCaret',
          'UpArrow',
          'Link',
          'Search',
          'Delete',
          'Info',
          'Play',
          'Import',
          'Export',
          'Add',
          'Dots',
          'Edit',
          'GridView',
          'ListView',
          'Settings',
          'Notification',
          'Eye',
          'Checkmark',
          'Cross',
        ],
      },
    },
    iconPosition: {
      control: {
        type: 'select',
        options: ['left', 'right'],
      },
    },
  },
  parameters: {
    docs: {
      page: ButtonsDocs,
    },
  },
}

const Template = args => <QuaternaryButton {...args} />

export const XLarge = Template.bind({})
XLarge.args = {
  text: 'label',
}
XLarge.parameters = {
  design: [
    {
      name: 'Design',
      type: 'figma',
      url: QUATERNARY_BTN_DESIGN,
      allowFullscreen: true,
    },
    {
      name: 'Presentation',
      type: 'figma',
      url: QUATERNARY_BTN_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
