export const IconPosition = Object.freeze({
  LEFT: 'left',
  RIGHT: 'right',
})

export const ButtonSize = Object.freeze({
  LARGE: 'large',
  SMALL: 'small',
  MEDIUM: 'medium',
})
