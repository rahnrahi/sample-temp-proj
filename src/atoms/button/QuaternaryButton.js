import React from 'react'
import PropTypes from 'prop-types'
import { StyledQuaternaryButton } from './Button.styles'
import { IconPosition } from './Button.enum'
import { Icons } from '../icon/Icon'
import noop from 'lodash/noop'

export const QuaternaryButton = ({
  showIcon,
  text,
  icon,
  iconPosition,
  iconSize,
  onClickHandler,
  ...rest
}) => {
  const Icon = Icons[icon]
  return (
    <StyledQuaternaryButton
      iconPosition={iconPosition}
      {...rest}
      onClick={onClickHandler}
    >
      {showIcon && <Icon className='icon' size={iconSize} />}
      <span>{text}</span>
    </StyledQuaternaryButton>
  )
}

QuaternaryButton.defaultProps = {
  showIcon: false,
  text: 'label',
  iconPosition: IconPosition.LEFT,
  icon: 'Close',
  iconSize: 16,
  borderRadius: '5px',
  disabled: false,
  onClickHandler: noop,
}

QuaternaryButton.propTypes = {
  /** Control to show/hide the icon */
  showIcon: PropTypes.bool,
  /** Text on the Button */
  text: PropTypes.string,
  /** Icon 'Add, Edit, Close, DownArrow', default null */
  icon: PropTypes.string,
  /** Position of the icon left/right */
  iconPosition: PropTypes.oneOf([IconPosition.RIGHT, IconPosition.LEFT]),
  /** Icon Size, 10, 12, 14, 16 , 18 and 24  */
  iconSize: PropTypes.number,
  /** True if the button is disabled; false otherwise  */
  disabled: PropTypes.bool,
  /** onClick handler function  */
  onClickHandler: PropTypes.func,
}
