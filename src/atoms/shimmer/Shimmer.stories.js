import React from 'react'
import { Shimmer as ShimmerComponent } from './'
import LoadingDocs from '../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Shimmer',
  component: ShimmerComponent,
  args: {
    count: 2,
    className: 'primary',
    perRow: 3,
  },
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Template = args => <ShimmerComponent {...args} />

export const Shimmer = Template.bind({})
Shimmer.args = {}
Shimmer.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
