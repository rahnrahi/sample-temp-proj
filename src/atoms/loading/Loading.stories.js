import React from 'react'
import { Loading } from './'
import LoadingDocs from '../../../docs/Loading.mdx'
import {
  LOADING_DESIGN,
  LOADING_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Spinner',
  component: Loading,
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Template = args => <Loading {...args} />
export const LoadingSpinner = Template.bind({})
LoadingSpinner.args = {
  show: true,
}
LoadingSpinner.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: LOADING_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: LOADING_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
