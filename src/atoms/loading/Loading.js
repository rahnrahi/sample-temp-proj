import React from 'react'
import PropTypes from 'prop-types'
import { StyledLoading } from './Loading.styles'
import IconLoading from '../../svg/icon-loading'

export const Loading = ({
  className,
  show,
  size,
  strokeWidth,
  strokeColor,
  ...restProps
}) => {
  return show ? (
    <StyledLoading className={className} {...restProps}>
      <IconLoading
        size={size}
        strokeWidth={strokeWidth}
        strokeColor={strokeColor}
      />
    </StyledLoading>
  ) : null
}
Loading.defaultProps = {
  size: 44,
  show: true,
}
Loading.propTypes = {
  show: PropTypes.bool,
  className: PropTypes.string,
  size: PropTypes.number,
  strokeWidth: PropTypes.number,
  strokeColor: PropTypes.string,
}
