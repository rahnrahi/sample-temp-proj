import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { KeyValue } from './KeyValue'
import 'jest-styled-components'

afterEach(cleanup)

describe('<Link/>', () => {
  it('should render basic key-value pair', () => {
    const { container } = render(
      <KeyValue keyText='date of birth' value='14 April 1997' />
    )
    expect(container).toMatchSnapshot()
  })
})
