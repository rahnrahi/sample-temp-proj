import React from 'react'
import PropTypes from 'prop-types'
import { Root, Key, Value } from './KeyValue.style'

export const KeyValue = ({ keyText, value, children }) => {
  return (
    <Root>
      <Key>{keyText}</Key>
      <Value>{value ?? children}</Value>
    </Root>
  )
}

KeyValue.propTypes = {
  keyText: PropTypes.string,
  value: PropTypes.any,
  children: PropTypes.any,
}
