import styled from 'styled-components'
import theme from '../../shared/theme'

export const Root = styled.div`
  margin-bottom: 0.5rem;

  display: flex;
  flex-direction: column;
`

export const Key = styled.header`
  margin-bottom: 0.2rem;

  ${theme.typography.h6}
  text-transform: capitalize;
  color: ${theme.palette.brand.primary.gray};
`

export const Value = styled.div`
  ${theme.typography.h6}
  color: ${theme.palette.brand.primary.charcoal};
`
