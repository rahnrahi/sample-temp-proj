import React from 'react'
import PropTypes from 'prop-types'
import { Suggestion } from './Suggestion'
import { IntegrationRailContainerStyle } from './styles'
import { NavHeader } from '../../molecules'
import { Input } from '../../atoms'
export const Integration = ({
  position,
  onClickBack,
  suggestions = [],
  inputProps,
  integrationSearchResultsView,
  onClickAddSuggestion,
}) => {
  return (
    <IntegrationRailContainerStyle
      position={position}
      data-testid='integration-view'
    >
      <>
        <NavHeader
          showCloseIcon={false}
          onClickIcon={onClickBack}
          iconName='LeftArrow'
          label='Back'
          iconSize={21}
        />
      </>
      <div className='integration-content-container'>
        <label>Add integration</label>
        <Input
          className='search-local'
          icon='Search'
          inputProps={{ ...inputProps }}
          kind='md'
          label=''
        />
        {suggestions.length && (
          <label className='margin-top'>Suggested for you</label>
        )}
        {suggestions.map((item, index) => (
          <Suggestion {...item} key={index} onClickAdd={onClickAddSuggestion} />
        ))}
        {React.isValidElement(integrationSearchResultsView) &&
          integrationSearchResultsView}
      </div>
    </IntegrationRailContainerStyle>
  )
}
Integration.defaultProps = {
  suggestions: [],
  inputProps: { boxed: false },
}
Integration.propTypes = {
  onClickBack: PropTypes.func,
  position: PropTypes.string,
  suggestions: PropTypes.array,
  /** Integration search input props*/
  inputProps: PropTypes.object,
  integrationSearchResultsView: PropTypes.node,
  onClickAddSuggestion: PropTypes.func,
}
