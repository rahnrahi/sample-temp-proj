import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { ExpandedRail } from './ExpandedRail'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const expandedProps = {
  onClose: jest.fn(),
  position: 'right',
  inputProps: { boxed: false, placeholder: 'Search' },
  iconName: 'SkuLookUp',
  label: 'SKU Look Up',
}
describe('<ExpandedRail/>', () => {
  it('render ExpandedRail component correctly with props', () => {
    const { container } = render(<ExpandedRail {...expandedProps} />)
    expect(container).toMatchSnapshot()
  })
})
