import React from 'react'
import PropTypes from 'prop-types'
import { IntegrationRailContainerStyle } from './styles'
import { NavHeader } from '../../molecules'
import { Input } from '../../atoms'
export const ExpandedRail = ({
  position,
  inputProps,
  label,
  iconName,
  onClose,
  expandedSearchResultsView,
}) => {
  return (
    <IntegrationRailContainerStyle position={position}>
      <>
        <NavHeader
          showCloseIcon={true}
          label={label}
          iconName={iconName}
          onClose={onClose}
        />
      </>
      <div className='integration-content-container'>
        <Input
          className='search-local'
          icon='Search'
          inputProps={{ ...inputProps }}
          kind='md'
          label=''
        />
        {React.isValidElement(expandedSearchResultsView) &&
          expandedSearchResultsView}
      </div>
    </IntegrationRailContainerStyle>
  )
}
ExpandedRail.defaultProps = {
  inputProps: { boxed: false, placeholder: 'Search' },
}
ExpandedRail.propTypes = {
  onClose: PropTypes.func,
  position: PropTypes.string,
  /** Integration search input props*/
  inputProps: PropTypes.object,
  label: PropTypes.string,
  iconName: PropTypes.string,
  expandedSearchResultsView: PropTypes.node,
}
