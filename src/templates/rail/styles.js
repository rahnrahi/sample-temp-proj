import styled, { css } from 'styled-components'
import { theme } from '../../shared'

const rightPositionCss = css`
  right: 0;
  top: 0;
`
const leftPositionCss = css`
  left: 0;
  top: 0;
`
const positionMap = new Map()
positionMap.set('right', rightPositionCss)
positionMap.set('left', leftPositionCss)

export const RailContainerStyle = styled.div`
  background: ${theme.palette.brand.primary.white};
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  ${({ position }) => position && `${positionMap.get(position)}`};
  padding: 30px 20px;
  box-sizing: border-box;
`

export const RailItemsStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  flex: 1;

  .flot-tile {
    position: fixed;
    bottom: 30px;
  }
`
export const IntegrationRailContainerStyle = styled(RailContainerStyle)`
  width: 336px;
  height: 100%;
  z-index: ${theme.zIndex.rail};
  justify-content: flex-start;
  align-items: flex-start;
  ${theme.shadows.light.level1}
  ${({ position }) => position && `${positionMap.get(position)}`};
  padding: 50px 32px;
  overflow-y: auto;

  .integration-content-container {
    margin-top: 50px;
    display: flex;
    flex-direction: column;
    width: 100%;

    label {
      ${theme.typography.kicker};
      color: ${theme.palette.brand.primary.gray};
      margin-bottom: 29px;
    }
  }
`

export const SuggestionContainerStyle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${theme.palette.ui.neutral.grey1};
  button {
    border: solid 1px ${theme.palette.brand.primary.charcoal};
    padding: 8px 14px;
    height: 32px;
    &:focus {
      border: 1px solid ${theme.palette.brand.primary.charcoal};
    }
  }
`
