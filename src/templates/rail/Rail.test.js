import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Rail } from './Rail'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const railProps = {
  containerProps: { position: 'right' },
  expandedSearchInputProps: { boxed: false, placeholder: 'Search' },
  integrationSearchInputProps: { boxed: false, placeholder: 'Search' },
  iconName: 'SkuLookUp',
  label: 'SKU Look Up',
  features: [
    { iconName: 'SkuLookUp', label: 'SKU Look Up', id: 1 },
    { iconName: 'ProductPerformance', label: 'Product Performance', id: 2 },
  ],
  suggestions: [
    {
      isSelected: false,
      iconName: 'SkuLookUp',
      label: 'Sku Look Up',
      id: 1,
    },
    {
      isSelected: false,
      iconName: 'ProductPerformance',
      label: 'Product Performance',
      id: 2,
    },
  ],
  onSelectFeature: jest.fn(),
  onRemoveFeature: jest.fn(),
  onDragEnd: jest.fn(),
  showCustomization: true,
  onClickAddSuggestion: jest.fn(),
}
describe('<Rail/>', () => {
  it('render Rail component correctly with default Props', () => {
    const { container, getByTestId } = render(<Rail />)
    const customize = getByTestId('Customize')
    expect(customize).toBeVisible()
    fireEvent.click(customize)
    const integration = getByTestId('Add')
    expect(integration).toBeVisible()
    expect(container).toMatchSnapshot()
  })

  it('render Rail component correctly with  Props', () => {
    const { container, getByTestId } = render(<Rail {...railProps} />)
    const customize = getByTestId('Customize')
    expect(customize).toBeVisible()
    fireEvent.click(customize)
    const integration = getByTestId('Add')
    fireEvent.click(integration)
    expect(getByTestId('integration-view')).toBeVisible()
    expect(getByTestId('nav-header-Back')).toBeVisible()
    expect(getByTestId('nav-header-action-LeftArrow')).toBeVisible()
    expect(container).toMatchSnapshot()
  })
})
