import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Integration } from './Integration'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const integrationProps = {
  onClickBack: jest.fn(),
  onClickAddSuggestion: jest.fn(),
  inputProps: { boxed: false, placeholder: 'Search' },
  suggestions: [],
  position: 'right',
}

describe('<Integration/>', () => {
  it('render Integration component correctly with props', () => {
    const { container } = render(<Integration {...integrationProps} />)
    expect(container).toMatchSnapshot()
  })
})
