import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { RailItemsStyle } from './styles'
import { RailContainer } from './RailContainer'
import { IconTile } from './Tile'
import { Integration } from './Integration'
import { ExpandedRail } from './ExpandedRail'
import Tiles from './Tiles'

const noop = () => null

export const Rail = React.memo(
  ({
    containerProps,
    features,
    expandedSearchInputProps,
    integrationSearchInputProps,
    integrationSearchResultsView,
    expandedSearchResultsView,
    suggestions,
    onSelectFeature,
    onRemoveFeature,
    showCustomization,
    onDragEnd,
    onClickAddSuggestion,
  }) => {
    const [showIntegrationIcon, setShowIntegrationIcon] = useState(
      !showCustomization
    )
    const [showExpandedRail, setShowExpandedRail] = useState(false)
    const [showIntegration, setShowIntegration] = useState(false)
    const [selectedFeature, setSelectedFeature] = useState({})

    const onSetSelectedFeature = ({ iconName, label, id, ...rest }) => {
      if (iconName && label) {
        setShowExpandedRail(true)
        setSelectedFeature({ iconName, label, id })
        onSelectFeature({ iconName, label, id, ...rest })
      }
    }

    useEffect(() => {
      setShowIntegrationIcon(!showCustomization)
    }, [showCustomization])

    return (
      <RailContainer {...containerProps}>
        <RailItemsStyle>
          <Tiles
            features={features}
            onSelectFeature={onSetSelectedFeature}
            onRemoveFeature={onRemoveFeature}
            showCustomization={showIntegrationIcon}
            onDragEnd={onDragEnd}
            data-testid='features'
          />
        </RailItemsStyle>

        <RailItemsStyle jc='center'>
          {showIntegrationIcon && (
            <IconTile
              label={`ADD Integrations`}
              iconName='Add'
              fill='#121213'
              size={21}
              onClick={setShowIntegration}
              capitalize={true}
              className='flot-tile'
            />
          )}
          {!showIntegrationIcon && (
            <IconTile
              label={`Customize`}
              iconName='Customize'
              size={48}
              onClick={setShowIntegrationIcon}
              className='flot-tile'
            />
          )}
        </RailItemsStyle>
        {showIntegration && (
          <Integration
            {...containerProps}
            onClickBack={() => setShowIntegration(false)}
            suggestions={suggestions}
            inputProps={integrationSearchInputProps}
            integrationSearchResultsView={integrationSearchResultsView}
            onClickAddSuggestion={onClickAddSuggestion}
          />
        )}
        {showExpandedRail && (
          <ExpandedRail
            {...containerProps}
            {...selectedFeature}
            onClose={() => setShowExpandedRail(false)}
            inputProps={expandedSearchInputProps}
            expandedSearchResultsView={expandedSearchResultsView}
          />
        )}
      </RailContainer>
    )
  }
)
Rail.defaultProps = {
  showCustomization: true,
  containerProps: { position: 'right' },
  integrationSearchInputProps: { boxed: false, placeholder: 'Search' },
  expandedSearchInputProps: { boxed: false, placeholder: 'Search' },
  features: [],
  suggestions: [],
  onDragEnd: noop,
  onClickAddSuggestion: noop,
  onSelectFeature: noop,
  onRemoveFeature: noop,
}

Rail.propTypes = {
  /** Rail container css props*/
  containerProps: PropTypes.object,
  /** Rail features list in array of {iconName, label, id} */
  features: PropTypes.array,
  /** Search input component props*/
  integrationSearchInputProps: PropTypes.object,
  /** Search input component props*/
  expandedSearchInputProps: PropTypes.object,
  /** A custom view for expanded search Rail Component*/
  integrationSearchResultsView: PropTypes.node,
  /** A custom view for search Integration Rail Component*/
  expandedSearchResultsView: PropTypes.node,
  /** Integration suggestions list in array of */
  suggestions: PropTypes.arrayOf(
    PropTypes.shape({
      isSelected: PropTypes.bool.isRequired,
      iconName: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
  /** A Callback function for onSelect feature on Rail, and it return object*/
  onSelectFeature: PropTypes.func.isRequired,
  /** A Callback function for Remove a feature on Rail and it return object*/
  onRemoveFeature: PropTypes.func.isRequired,
  /** A Callback function for onDragEnd, and it will return sorted array of features*/
  onDragEnd: PropTypes.func.isRequired,
  /** To enable customize icon*/
  showCustomization: PropTypes.bool,
  /** A Callback function for add suggestions, and it return object */
  onClickAddSuggestion: PropTypes.func.isRequired,
}
