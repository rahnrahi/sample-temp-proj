import React from 'react'
import PropTypes from 'prop-types'
import { RailContainerStyle } from './styles'

export const RailContainer = ({ children, ...restProps }) => {
  return <RailContainerStyle {...restProps}>{children}</RailContainerStyle>
}
RailContainer.defaultProps = {
  width: '113px',
  height: '100%',
  position: 'right',
}
RailContainer.propTypes = {
  children: PropTypes.node,
  width: PropTypes.string,
  height: PropTypes.string,
  position: PropTypes.string,
}
