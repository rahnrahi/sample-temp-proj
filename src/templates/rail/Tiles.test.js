import React from 'react'
import { render, cleanup } from '@testing-library/react'
import Tiles from './Tiles'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
describe('<Tiles/>', () => {
  it('render IconTile component correctly', () => {
    const { container } = render(<Tiles />)
    expect(container).toMatchSnapshot()
  })
})
