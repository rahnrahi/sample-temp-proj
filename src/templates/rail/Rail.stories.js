import React from 'react'
import { Rail } from './Rail'
const IntegrationSearchResultsView = () => (
  <div>Custom Integration Search Results View</div>
)
const ExpandedSearchResultsView = () => (
  <div>Custom Expanded Search Results View</div>
)

export default {
  title: 'Archived/Rail',
  decorators: [
    Story => (
      <div style={{ height: '100vh', width: '600px', position: 'relative' }}>
        {Story()}
      </div>
    ),
  ],
  component: Rail,
  parameters: {
    backgrounds: { default: 'background' },
  },
  argTypes: {
    onSelectFeature: { action: 'click' },
    onRemoveFeature: { action: 'click' },
    onDragEnd: { action: 'click' },
    onClickAddSuggestion: { action: 'click' },
    containerProps: { type: { name: 'object', required: true } },
    showCustomization: {
      action: 'select',
      type: { name: 'boolean', required: false },
    },
  },
  args: {
    containerProps: {
      position: 'right',
    },
    features: [
      { iconName: 'SkuLookUp', label: 'SKU Look Up', id: 1 },
      { iconName: 'ProductPerformance', label: 'Product Performance', id: 2 },
    ],
    integrationSearchInputProps: { boxed: false, placeholder: 'Search' },
    expandedSearchInputProps: { boxed: false, placeholder: 'Search' },
    suggestions: [
      {
        isSelected: false,
        iconName: 'SkuLookUp',
        label: 'Sku Look Up',
        id: 1,
      },
      {
        isSelected: false,
        iconName: 'ProductPerformance',
        label: 'Product Performance',
        id: 2,
      },
    ],
  },
}

const Template = args => <Rail {...args} />

export const RightRail = Template.bind({})
export const LeftRail = Template.bind({})
LeftRail.args = {
  ...RightRail.args,
  containerProps: {
    position: 'left',
  },
}
export const CustomRail = Template.bind({})
CustomRail.args = {
  ...RightRail.args,
  containerProps: {
    position: 'left',
  },
  integrationSearchResultsView: <IntegrationSearchResultsView />,
  expandedSearchResultsView: <ExpandedSearchResultsView />,
}
CustomRail.storyName = 'Rail with custom Search Results View'
