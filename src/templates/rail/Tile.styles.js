import styled from 'styled-components'
import { theme } from '../../shared'

export const IconTileContainerStyle = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  justify-content: center;
  align-items: center;
  text-align: center;
  cursor: pointer;
  width: 98px;
  padding: 0 20px;
  margin-top: 10px;
  margin-bottom: 10px;
  ${({ isDragging }) =>
    isDragging &&
    `border: 2px dashed ${theme.palette.ui.cta.blue}; background: ${theme.palette.brand.primary.white}`};
  ${({ isDragging }) => isDragging && theme.shadows.light.level3};
  .label {
    margin-top: 5px;
    cursor: pointer;
    ${({ capitalize }) =>
      capitalize ? theme.typography.kicker : theme.typography.h6};
    white-space: break-spaces;
    color: ${theme.palette.brand.primary.charcoal};
  }
  .remove {
    ${theme.typography.h6};
    color: ${theme.palette.ui.cta.blue};
    margin-top: 8px;
  }
`
