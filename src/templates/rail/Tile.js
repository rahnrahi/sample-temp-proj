import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import { IconTileContainerStyle } from './Tile.styles'
import { Icon } from '../../atoms'
import { lineBreak } from '../../shared/utils'
export const IconTile = forwardRef(
  (
    {
      label,
      iconName,
      onRemove,
      onClick,
      fill,
      showCustomization,
      ...restProps
    },
    ref
  ) => {
    return (
      <IconTileContainerStyle {...restProps} ref={ref}>
        <IconTileContainerStyle onClick={onClick}>
          {fill ? (
            <Icon
              iconName={`${iconName}`}
              size={21}
              fill={fill}
              data-testid={`${iconName}`}
              style={{ cursor: 'pointer' }}
              onClick={onClick}
            />
          ) : (
            <Icon
              iconName={iconName}
              size={50}
              data-testid={`${iconName}`}
              style={{ cursor: 'pointer' }}
              onClick={onClick}
            />
          )}
          <div className='label'>{lineBreak(label)}</div>
        </IconTileContainerStyle>
        {showCustomization && (
          <span
            className='remove'
            onClick={onRemove}
            data-testid={`${label}-remove`}
          >
            Remove
          </span>
        )}
      </IconTileContainerStyle>
    )
  }
)

IconTile.propTypes = {
  label: PropTypes.string.isRequired,
  fill: PropTypes.string,
  iconName: PropTypes.string.isRequired,
  iconCustomProps: PropTypes.object,
  onClick: PropTypes.func,
  onRemove: PropTypes.func,
  showCustomization: PropTypes.bool,
  capitalize: PropTypes.bool,
}
