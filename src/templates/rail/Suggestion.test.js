import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Suggestion } from './Suggestion'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const suggestionProps = {
  iconName: 'SkuLookUp',
  label: 'SKU Look Up',
  id: 1,
  isSelected: true,
}
describe('<Suggestion/>', () => {
  it('render Suggestion component correctly', () => {
    const { container } = render(<Suggestion {...suggestionProps} />)
    expect(container).toMatchSnapshot()
  })
})
