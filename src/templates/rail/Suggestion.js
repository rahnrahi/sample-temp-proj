import React from 'react'
import PropTypes from 'prop-types'
import { SuggestionContainerStyle } from './styles'
import { IconTile } from './Tile'
import { ButtonWithIcon } from '../../atoms'

export const Suggestion = ({
  isSelected,
  iconName,
  label,
  onClickAdd,
  ...rest
}) => {
  return (
    <SuggestionContainerStyle>
      <IconTile iconName={iconName} label={label} />
      <ButtonWithIcon
        emphasis='high'
        icon='Add'
        iconPosition='left'
        isPrimary={false}
        showIcon
        onClick={() =>
          onClickAdd({ isSelected, iconName, label, onClickAdd, ...rest })
        }
        text={isSelected ? 'Added' : 'Add'}
        theme='light'
      />
    </SuggestionContainerStyle>
  )
}
Suggestion.defaultProps = {
  iconName: '',
  label: '',
  isSelected: false,
}
Suggestion.propTypes = {
  iconName: PropTypes.string,
  label: PropTypes.string,
  isSelected: PropTypes.bool,
  onClickAdd: PropTypes.func,
}
