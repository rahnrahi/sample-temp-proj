import React, { useEffect, useState } from 'react'
import { IconTile } from './Tile'
import PropTypes from 'prop-types'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

const Tiles = ({
  features,
  onSelectFeature,
  onRemoveFeature,
  showCustomization,
  onDragEnd,
}) => {
  const [tiles, setTiles] = useState([])
  useEffect(() => {
    setTiles(features)
  }, [features])

  const _tempTiles = tiles
  return (
    <DragDropContext
      onDragEnd={param => {
        const srcI = param.source.index
        const desI = param.destination?.index
        if (desI) {
          _tempTiles.splice(desI, 0, _tempTiles.splice(srcI, 1)[0])
          setTiles(_tempTiles)
          if ('function' == typeof onDragEnd) {
            onDragEnd(_tempTiles)
          }
        }
      }}
    >
      <Droppable droppableId='droppable-1' type='items'>
        {provided => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {tiles.map(({ iconName, label, id, ...rest }, i) => (
              <Draggable
                key={label}
                index={i}
                draggableId={`draggable-${id}`}
                isDragDisabled={!showCustomization}
              >
                {(provided, snapshot) => {
                  return (
                    <IconTile
                      iconName={iconName}
                      key={label}
                      label={label}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={{
                        ...provided.draggableProps.style,
                      }}
                      isDragging={snapshot.isDragging}
                      onClick={() =>
                        onSelectFeature({ iconName, label, id, ...rest })
                      }
                      onRemove={() =>
                        onRemoveFeature({ iconName, label, id, ...rest })
                      }
                      showCustomization={showCustomization}
                    />
                  )
                }}
              </Draggable>
            ))}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  )
}

Tiles.defaultProps = {
  features: [],
  onSelectFeature: () => null,
  onRemoveFeature: () => null,
  onDragEnd: () => null,
  showCustomization: false,
}
Tiles.propTypes = {
  features: PropTypes.array,
  onSelectFeature: PropTypes.func,
  onRemoveFeature: PropTypes.func,
  onDragEnd: PropTypes.func,
  showCustomization: PropTypes.bool,
}
export default Tiles
