import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { IconTile } from './Tile'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const tileProps = {
  iconName: 'SkuLookUp',
  label: 'SKU Look Up',
  id: 1,
}
describe('<IconTile/>', () => {
  it('render IconTile component correctly', () => {
    const { container } = render(<IconTile {...tileProps} />)
    expect(container).toMatchSnapshot()
  })
})
