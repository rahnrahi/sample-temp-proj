import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='-3 -3 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <circle opacity='0.8' cx='9' cy='9' r='9' fill='#121213' />
    <path
      d='M8.70711 5.70711C8.07714 5.07714 7 5.52331 7 6.41421L7 11.5858C7 12.4767 8.07714 12.9229 8.70711 12.2929L11.2929 9.70711C11.6834 9.31658 11.6834 8.68342 11.2929 8.29289L8.70711 5.70711Z'
      fill='white'
    />
  </svg>
)
