import React from 'react'
export default ({ size = 16, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 16 16'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <g clipPath='url(#clip0)'>
      <rect width='16' height='16' rx='2' fill='#0D62FF' />
      <path
        d='M4.28033 7.94163C3.98744 7.64874 3.51256 7.64874 3.21967 7.94163C2.92678 8.23452 2.92678 8.7094 3.21967 9.00229L5.94202 11.7246C6.23491 12.0175 6.70979 12.0175 7.00268 11.7246L13.447 5.28033C13.7399 4.98744 13.7399 4.51256 13.447 4.21967C13.1541 3.92678 12.6792 3.92678 12.3863 4.21967L6.47235 10.1337L4.28033 7.94163Z'
        fill='white'
      />
    </g>
    <defs>
      <clipPath id='clip0'>
        <path
          d='M0 1.6C0 0.716345 0.716344 0 1.6 0L14.4 0C15.2837 0 16 0.716344 16 1.6L16 14.4C16 15.2837 15.2837 16 14.4 16L1.6 16C0.716345 16 0 15.2837 0 14.4L0 1.6Z'
          fill='white'
        />
      </clipPath>
    </defs>
  </svg>
)
