import React from 'react'

export const Analytics = ({
	size= 22,
	width= size,
	height= size,
	fill='#121213',
	...restProps
}) => (
		<svg
			width={width}
			height={height}
			viewBox="0 0 14 22"
			fill={fill}
			xmlns="http://www.w3.org/2000/svg"
			{...restProps}
		>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 0 0.00737247 0.995702 16.0931 15.9154)" fill="#121213"/>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 0 0.00737247 0.995702 9.0941 15.9154)" fill="#121213"/>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 -7.9395e-08 0.00737238 0.995702 9.0941 8.91535)" fill="#121213"/>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 -7.9395e-08 0.00737238 0.995702 16.0912 8.91535)" fill="#121213"/>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 0 0.00737247 0.995702 1.90709 15.9154)" fill="#121213"/>
			<ellipse rx="1.79238" ry="1.80985" transform="matrix(1.00425 -7.9395e-08 0.00737238 0.995702 16.0892 2.08332)" fill="#121213"/>
		</svg>
)
