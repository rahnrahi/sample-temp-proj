import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739303 0.999973 12.0001 11.7286)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739303 0.999973 8.23792 7.36072)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739303 0.999973 8.23792 15.6996)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739303 0.999973 15.7618 15.6996)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739303 0.999973 15.7618 7.36072)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739299 0.999973 19.1056 11.7286)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739299 0.999973 4.89417 11.7286)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00739299 0.999973 12.0001 19.2734)" fill="white"/>
		<ellipse rx="1.88096" ry="1.78691" transform="matrix(1 0 0.00702336 0.999975 11.9993 3.78687)" fill="white"/>
	</svg>
)
