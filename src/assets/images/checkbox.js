import React from 'react'
export default ({ size = 16, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 16 16'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <rect x='0.5' y='0.5' width='15' height='15' rx='1.5' stroke='#384454' />
  </svg>
)
