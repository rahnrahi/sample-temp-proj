import React from 'react'

export default ({ size = 12, width = size, height = size, fill, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 12 12'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <path
      d='M5.70711 2.70711C5.07714 2.07714 4 2.52331 4 3.41421L4 8.58579C4 9.47669 5.07714 9.92286 5.70711 9.29289L8.29289 6.70711C8.68342 6.31658 8.68342 5.68342 8.29289 5.29289L5.70711 2.70711Z'
      fill={fill? fill: '#121213'}
    />
  </svg>
)
