import React from 'react'
export const CircleClose = ({
  size = 40,
  width = size,
  height = size,
  ...restProps
}) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 40 40'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <circle
      cx='20'
      cy='20'
      r='19.6'
      transform='rotate(90 20 20)'
      stroke='#121213'
      strokeWidth='0.8'
    />
    <path
      d='M18.868 20L15.668 16.8L16.7994 15.6686L19.9994 18.8686L23.1994 15.6686L24.3308 16.8L21.1307 20L24.3307 23.2L23.1994 24.3313L19.9994 21.1313L16.7994 24.3313L15.668 23.2L18.868 20Z'
      fill='black'
    />
  </svg>
)
