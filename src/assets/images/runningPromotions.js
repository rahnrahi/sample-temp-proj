import React from 'react'
export default ({ size = 50, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 50 50'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <path
      d='M25.1535 16.1581C25.3146 15.997 25.5402 15.9179 25.7667 15.943L30.873 16.5104C31.2217 16.5492 31.4969 16.8244 31.5356 17.173L32.103 22.2794C32.1281 22.5058 32.049 22.7314 31.8879 22.8925L22.5453 32.2351C22.2524 32.528 21.7775 32.528 21.4846 32.2351L15.8109 26.5614C15.518 26.2685 15.518 25.7937 15.8109 25.5008L25.1535 16.1581Z'
      stroke='#121213'
      strokeWidth='1.5'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
    <circle
      cx='27.614'
      cy='20.3625'
      r='0.8625'
      fill='#121213'
      stroke='#121213'
      strokeWidth='1.125'
    />
    <path
      d='M34.7148 23.4204L34.717 23.4359C34.7838 23.9033 34.6266 24.3748 34.2928 24.7087L22.9933 36.0081C22.4075 36.5939 21.4578 36.5939 20.872 36.0081L14.3719 29.508C14.1666 29.3027 14.1666 28.9698 14.3719 28.7644V28.7644'
      stroke='#737F8F'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </svg>
)
