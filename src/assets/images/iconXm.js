import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<ellipse rx="1.65093" ry="1.60102" transform="matrix(1 0 0.00724234 0.999974 15.6677 8.0115)" fill="white"/>
		<ellipse rx="1.65093" ry="1.60102" transform="matrix(1 0 0.00688024 0.999976 19.3357 4.60098)" fill="white"/>
		<ellipse rx="1.60102" ry="1.65093" transform="matrix(-4.50741e-08 1 -0.999977 0.00681102 15.656 14.8452)" fill="white"/>
		<ellipse rx="1.60102" ry="1.65093" transform="matrix(-4.50741e-08 1 -0.999977 0.00681102 19.3245 18.4878)" fill="white"/>
		<ellipse rx="1.60102" ry="1.65093" transform="matrix(-4.50741e-08 1 -0.999977 0.00681102 11.9226 11.4346)" fill="white"/>
		<ellipse rx="1.65093" ry="1.60102" transform="matrix(-1 0 -0.00724234 0.999974 8.32991 8.0115)" fill="white"/>
		<ellipse rx="1.65093" ry="1.60102" transform="matrix(-1 0 -0.00688024 0.999976 4.66191 4.60098)" fill="white"/>
		<ellipse rx="1.60102" ry="1.65093" transform="matrix(4.50741e-08 1 0.999977 0.00681102 8.34156 14.8452)" fill="white"/>
		<ellipse rx="1.60102" ry="1.65093" transform="matrix(4.50741e-08 1 0.999977 0.00681102 4.67311 18.4878)" fill="white"/>
	</svg>
)
