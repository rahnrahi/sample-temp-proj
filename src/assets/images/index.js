import Cross from './cross'
import { DownArrow } from './downArrow'
import DownCaret from './downCaret'
import RightArrow from './rightArrow'
import EditPen from './editPen'
import LeftArrow from './leftArrow'
import WhiteDownArrow from './whiteDownArrow'
import { Add } from './add'
import Comment from './comment'
import { Close } from './close'
import { Edit } from './edit'
import Checkmark from './checkmark'
import Dots from './dots'
import BlueCaret from './blueCaret'
import Search from './iconSearch'
import Overview from './iconOverview'
import PIM from './iconPim'
import OMS from './iconOms'
import XM from './iconXm'
import Offers from './iconOffers'
import LoyaltyManagement from './iconLoyaltyManagement'
import Subscriptions from './iconSubscriptions'
import Dropship from './iconDropship'
import POS from './iconPOS'
import FabricLogo from './fabricLogo'
import FabricLogoName from './fabricLogoName'
import SkuLookUp from './skuLookUp'
import ProductPerformance from './productPerformance'
import UserBehaviors from './userBehaviors'
import RunningPromotions from './runningPromotions'
import AssignedToMe from './assignedToMe'
import Help from './help'
import Copy from './actionCopy'
import CircleDownArrow from './circleDownArrow'
import UpCaret from './upCaret'
import RightCaret from './rightCaret'
import { UpArrow } from './upArrow'
import Link from './link'
import Delete from './delete'
import Info from './info'
import Play from './play'
import Calculator from './calculator'
import Calendar from './calendar'
import Lock from './lock'
import Import from './import'
import Export from './export'
import GridView from './grid'
import ListView from './list'
import Settings from './settings'
import Radio from './radio'
import RadioSelected from './radioSelected'
import Checkbox from './checkbox'
import CheckboxSelected from './checkboxSelected'
import Notification from './iconNotification'
import { CircleClose } from './circleClose'
import Customize from './customize'
import { Partial } from './partial'
import Eye from './eye'
import { Pay } from './pay'
import { Analytics } from './analytics'
import { Customer } from './customer'
import CSR from './iconCSR'
import Refresh from './refresh'
import { AllInclusive } from './allInclusive'
import Flash from './flash'
import Clock from './clock'
import DragAndDrop from './dragAndDrop'
import ActionEye from './actionEye'
import DigitalAsset from './DigitalAsset'

export {
  Cross,
  DownArrow,
  DownCaret,
  RightArrow,
  EditPen,
  LeftArrow,
  WhiteDownArrow,
  Add,
  Close,
  Edit,
  Checkmark,
  Dots,
  BlueCaret,
  Overview,
  PIM,
  OMS,
  XM,
  Offers,
  LoyaltyManagement,
  Subscriptions,
  POS,
  Dropship,
  FabricLogo,
  FabricLogoName,
  SkuLookUp,
  ProductPerformance,
  UserBehaviors,
  RunningPromotions,
  AssignedToMe,
  Help,
  Copy,
  CircleDownArrow,
  UpCaret,
  RightCaret,
  UpArrow,
  Link,
  Search,
  Delete,
  Refresh,
  Info,
  Play,
  Calculator,
  Calendar,
  Lock,
  Import,
  Export,
  GridView,
  ListView,
  Settings,
  Radio,
  RadioSelected,
  CheckboxSelected,
  Checkbox,
  Notification,
  CircleClose,
  Customize,
  Partial,
  Eye,
  Pay,
  Analytics,
  Customer,
  CSR,
  AllInclusive,
  Flash,
  Clock,
  Copy as ActionCopy,
  DragAndDrop,
  ActionEye,
  DigitalAsset,
  Comment,
}

export const icons = {
  Search,
}
