import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(1 0 0.0070234 0.999975 12.0002 11.9769)'
      fill='#121213'
    />
    <circle
      r='1.71326'
      transform='matrix(1 0 0.00702336 0.999975 11.9992 4.21321)'
      fill='#121213'
    />
    <circle
      r='1.71326'
      transform='matrix(-1 -8.74228e-08 -0.00702327 -0.999975 11.9993 19.7868)'
      fill='#121213'
    />
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(-0.809017 -0.587785 0.582089 -0.813125 7.42274 18.305)'
      fill='#121213'
    />
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(0.809017 0.587785 -0.582089 0.813125 16.5782 5.70481)'
      fill='#121213'
    />
    <circle
      r='1.71326'
      transform='matrix(0.809017 -0.587785 0.593453 0.804869 7.4233 5.7059)'
      fill='#121213'
    />
    <circle
      r='1.71326'
      transform='matrix(-0.809017 0.587785 -0.593453 -0.804869 16.5767 18.298)'
      fill='#121213'
    />
    <circle
      r='1.71326'
      transform='matrix(-0.309017 -0.951057 0.948863 -0.315689 4.59524 14.4098)'
      fill='#121213'
    />
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(0.309017 0.951057 -0.948863 0.315689 19.4048 9.59213)'
      fill='#121213'
    />
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(0.309017 -0.951057 0.953203 0.30233 4.59464 9.5956)'
      fill='#121213'
    />
    <ellipse
      rx='1.71326'
      ry='1.71326'
      transform='matrix(-0.309017 0.951057 -0.953203 -0.30233 19.4044 14.4025)'
      fill='#121213'
    />
  </svg>
)
