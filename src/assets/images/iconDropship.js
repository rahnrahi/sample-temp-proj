import React from 'react'
export default ({
	size = 24,
	width = size,
	height = size,
	...restProps
}) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<circle r="1.80029" transform="matrix(-1 0 -0.0070234 -0.999975 12.0025 6.19975)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.0070234 -0.999975 15.6002 10.5806)" fill="#121213"/>
		<circle r="1.80029" transform="matrix(-1 0 -0.0070234 -0.999975 15.5997 1.80132)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.0070234 -0.999975 8.4229 1.80132)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.00702341 -0.999975 8.398 10.5806)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.00702335 -0.999975 5.20269 6.19975)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.00702335 -0.999975 1.99664 1.80131)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.00702335 -0.999975 18.8013 6.19975)" fill="#121213"/>
		<ellipse rx="1.80029" ry="1.80029" transform="matrix(-1 0 -0.00702335 -0.999975 22.003 1.80131)" fill="#121213"/>
		<circle r="1.80029" transform="matrix(-1 0 -0.00702336 -0.999975 12.0025 14.1998)" fill="#121213"/>
	</svg>
)
