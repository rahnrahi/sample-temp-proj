import React from 'react'
export default ({ size = 14, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 14 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...restProps}
  >
    <path d="M12.4 1.41057H11.725V0.262695H10.375V1.41057H3.625V0.262695H2.275V1.41057H1.6C0.8575 1.41057 0.25 1.92711 0.25 2.55844V11.7414C0.25 12.3727 0.8575 12.8893 1.6 12.8893H12.4C13.1425 12.8893 13.75 12.3727 13.75 11.7414V2.55844C13.75 1.92711 13.1425 1.41057 12.4 1.41057ZM12.4 11.7414H1.6V5.42812H12.4V11.7414ZM12.4 4.28024H1.6V2.55844H12.4V4.28024Z" fill="#121213"/>
  </svg>
)
