import React from 'react'
export default ({
  size = 16,
  width = size,
  height = size,
  fill,
  ...restProps
}) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 16 16'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <rect
      x='1'
      y='1'
      width='6.22222'
      height='6.22222'
      rx='1'
      fill={fill ? fill : '#121213'}
    />
    <rect
      x='1'
      y='8.77734'
      width='6.22222'
      height='6.22222'
      rx='1'
      fill={fill ? fill : '#121213'}
    />
    <rect
      x='8.77734'
      y='8.77734'
      width='6.22222'
      height='6.22222'
      rx='1'
      fill={fill ? fill : '#121213'}
    />
    <rect
      x='8.77734'
      y='1'
      width='6.22222'
      height='6.22222'
      rx='1'
      fill={fill ? fill : '#121213'}
    />
  </svg>
)
