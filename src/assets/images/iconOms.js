import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 1.59474e-07 0.00737265 0.995702 11.6175 11.9004)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 1.59474e-07 0.00737265 0.995702 19.2669 11.9004)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 0 0.00737247 0.995702 8.17509 15.1691)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 0 0.00737247 0.995702 15.8245 15.1691)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 0 0.00737247 0.995702 4.7332 18.8039)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 0 0.00737247 0.995702 12.3826 18.8039)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 -7.9395e-08 0.00737238 0.995702 8.17509 8.26696)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 -7.9395e-08 0.00737238 0.995702 15.8245 8.26696)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 8.42937e-08 0.00700395 0.995705 4.73259 4.635)" fill="white"/>
		<ellipse rx="1.7138" ry="1.64206" transform="matrix(1.00425 8.42937e-08 0.00700395 0.995705 12.3819 4.635)" fill="white"/>
	</svg>
)
