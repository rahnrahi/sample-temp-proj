import React from 'react'
export const Partial = ({ size = 10, width = size, height = 2, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 10 2'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <rect width='10' height='2' rx='1' fill='white' />
  </svg>
)
