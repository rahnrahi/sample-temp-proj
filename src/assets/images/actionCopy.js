import React from 'react'

export default ({
  size = 24,
  width = size,
  height = size,
  fill = '#121213',
  ...restProps
}) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width}
      height={height}
      fill={fill}
      viewBox='0 0 24 24'
    >
      <path
        fill={fill}
        d='M16.77 2.607A.607.607 0 0016.163 2H5.396A1.903 1.903 0 003.5 3.912v12.766a.615.615 0 001.23 0V4.214a1 1 0 011-1h10.433a.607.607 0 00.607-.607z'
      ></path>
      <path
        fill={fill}
        stroke='#fff'
        strokeWidth='0.1'
        d='M18.884 20.566h.05V6.209H7.725V20.566h11.159zM8.177 4.846h10.427c1.02 0 1.846.834 1.846 1.863V20.09a1.853 1.853 0 01-1.846 1.862H8.177a1.853 1.853 0 01-1.846-1.862V6.71c0-1.03.827-1.862 1.846-1.862z'
      ></path>
    </svg>
  )
}
