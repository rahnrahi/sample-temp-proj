import React from 'react'
export default ({ size = 32, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 32 26'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <path
      d='M1 3C1 1.89543 1.83947 1 2.875 1H29.125C30.1605 1 31 1.89543 31 3V23C31 24.1046 30.1605 25 29.125 25H2.875C1.83947 25 1 24.1046 1 23V3Z'
      stroke='#121213'
      stroke-width='2'
    />
    <path
      d='M1 18.5944L5.4594 14.135L10.5599 19.2354C13.9717 15.8236 20.7953 9 20.7953 9C23.9694 11.9371 30.3176 18.5223 30.3176 18.5223'
      stroke='#121213'
    />
    <circle cx='10.5' cy='8.5' r='4' stroke='#121213' />
  </svg>
)
