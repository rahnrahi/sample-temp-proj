import React from 'react'

export default ({ size = 18, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 18 18'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <circle cx='9' cy='9' r='9' fill='#121213' />
    <path
      d='M5.70711 8.70711C5.07714 8.07714 5.52331 7 6.41421 7L11.5858 7C12.4767 7 12.9229 8.07714 12.2929 8.70711L9.70711 11.2929C9.31658 11.6834 8.68342 11.6834 8.29289 11.2929L5.70711 8.70711Z'
      fill='white'
    />
  </svg>
)
