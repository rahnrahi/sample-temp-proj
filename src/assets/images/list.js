import React from 'react'
export default ({
  size = 16,
  width = size,
  height = size,
  fill,
  ...restProps
}) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 16 16'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
    <rect x='1' y='1' width='14' height='3.11111' rx='1' fill={fill? fill: '#121213'} />
    <rect x='1' y='6.44531' width='14' height='3.11111' rx='1' fill={fill? fill: '#121213'} />
    <rect x='1' y='11.8887' width='14' height='3.11111' rx='1' fill={fill? fill: '#121213'} />
  </svg>
)
