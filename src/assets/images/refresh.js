import React from 'react'

export default ({ size = 24, width = size, height = size, ...restProps }) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width}
      height={height}
      fill='none'
      viewBox='0 0 20 20'
      {...restProps}
    >
      <path
        stroke={restProps.stroke ?? '#292929'}
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='1.5'
        d='M10 1a9 9 0 11-5.657 2'
      ></path>
      <path
        d='M1 2.49988H5V5.49988'
        stroke={restProps.stroke ?? '#292929'}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}
