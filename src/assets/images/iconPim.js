import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00702339 0.999975 4.80947 4.70756)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 4.8101 11.5379)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 4.8101 18.3683)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00702339 0.999975 11.9993 4.70756)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 12 11.5379)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 12 18.3683)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00702339 0.999975 19.1892 4.70756)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 19.19 11.5379)" fill="white"/>
		<ellipse rx="1.79748" ry="1.70761" transform="matrix(1 0 0.00739302 0.999973 19.19 18.3683)" fill="white"/>
	</svg>
)
