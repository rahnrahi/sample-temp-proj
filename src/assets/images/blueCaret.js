import React from 'react'

export default props => {
  return (
    <svg
      {...props}
      width='12'
      height='12'
      viewBox='0 0 12 12'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M9.29289 6.29289C9.92286 6.92286 9.47669 8 8.58579 8L3.41421 8C2.52331 8 2.07714 6.92286 2.70711 6.29289L5.29289 3.70711C5.68342 3.31658 6.31658 3.31658 6.70711 3.70711L9.29289 6.29289Z'
        fill='#0D62FF'
      />
    </svg>
  )
}
