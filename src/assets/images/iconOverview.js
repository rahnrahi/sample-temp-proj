import React from 'react'
export default ({ size = 24, width = size, height = size, ...restProps }) => (
  <svg
    width={width}
    height={height}
    viewBox='0 0 24 24'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...restProps}
  >
		<ellipse cx="19" cy="11.5499" rx="2" ry="1.9" fill="white"/>
		<ellipse cx="12" cy="11.5499" rx="2" ry="1.9" fill="white"/>
		<ellipse cx="5" cy="11.5499" rx="2" ry="1.9" fill="white"/>
		<ellipse cx="12" cy="4.9" rx="2" ry="1.9" fill="white"/>
		<ellipse cx="12" cy="18.2001" rx="2" ry="1.9" fill="white"/>
	</svg>
)
