import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { theme } from '../../shared'

export const Typo = ({ type = 'h1', text }) => {
  const Typo = styled.div`
    ${theme.typography[type]}
  `
  return (
    <>
      <Typo>{text}</Typo>
    </>
  )
}

Typo.propTypes = {
  type: PropTypes.oneOf([
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'subtitle1',
    'subtitle2',
    'subtitle3',
  ]),
  text: PropTypes.string,
}
