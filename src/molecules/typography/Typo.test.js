import React from 'react'
import { render } from '@testing-library/react'
import { Typo } from './Typo'
import 'jest-styled-components'

describe('Typo', () => {
  it('renders Typo correctly with default props', () => {
    const { getByText } = render(<Typo text='Some Text' type='subtitle1' />)
    const textSample = getByText('Some Text')
    expect(textSample).toBeInTheDocument()
  })
})
