import React from 'react'
import { render } from '@testing-library/react'
import { StyledH1, StyledH2 } from './Typography.styles'
import 'jest-styled-components'

describe('StyledH1', () => {
  it('renders StyledH1 correctly with default props', () => {
    const { getByText } = render(<StyledH1>Some Text</StyledH1>)
    const textSample = getByText('Some Text')
    expect(textSample).toBeInTheDocument()
  })
})

describe('StyledH2', () => {
  it('renders StyledH2 correctly with default props', () => {
    const { getByText } = render(<StyledH2>Some Text</StyledH2>)
    const textSample = getByText('Some Text')
    expect(textSample).toBeInTheDocument()
  })
})
