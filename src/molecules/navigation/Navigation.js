import React from 'react'
import PropTypes from 'prop-types'
import { StyledNav, StyledUL, StyledHead, StyledH1 } from './Navigation.styles'
import cx from 'classnames'
import { Item } from './Item'
let level = 0
export const Navigation = ({
  links,
  orientation,
  className,
  onClick,
  title,
  total,
  ...props
}) => {
  return (
    <StyledNav
      className={cx({
        [orientation]: orientation,
        [className]: className,
      })}
      {...props}
    >
      {title ? (
        <StyledHead>
          <StyledH1>
            {title} {total ? <span>({total})</span> : null}
          </StyledH1>
        </StyledHead>
      ) : null}
      <StyledUL className={cx(`level-0`)}>
        {links.map((link, index) => {
          if (link.children) level += 1
          return (
            <Item
              {...link}
              key={link.id ? link.id : index}
              index={index}
              level={level}
              onClick={onClick}
            />
          )
        })}
      </StyledUL>
    </StyledNav>
  )
}

Navigation.propTypes = {
  orientation: PropTypes.string,
  /** If a component is provided in the linkComponent attribute then the url and label attributes are not required. */
  links: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.string,
  /** A callback handler which will return event, label and id. Only used when
   * the client does not provide a component for the links. */
  onClick: PropTypes.func,
  title: PropTypes.string,
  total: PropTypes.number,
}
