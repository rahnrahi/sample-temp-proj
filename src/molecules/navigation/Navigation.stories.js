import React from 'react'
import { Navigation } from '.'
import {
  dataVertical,
  dataHorizontal,
  dataVerticalWithLinkComponent,
} from './mock'
import TabsDocs from '../../../docs/Tabs.mdx'
import {
  NAVIGATION_DESIGN,
  NAVIGATION_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Tabs/Navigation',
  component: Navigation,
  parameters: {
    actions: { argTypesRegex: '^on.*' },
    docs: {
      page: TabsDocs,
    },
  },
  argTypes: {
    orientation: {
      control: {
        type: 'select',
        options: ['horizontal', 'vertical'],
      },
    },
  },
  args: {},
}

const design = getDesignSpecifications(
  NAVIGATION_DESIGN,
  NAVIGATION_PRESENTATION
)['design']

const Template = args => <Navigation {...args} />

export const Horizontal = Template.bind({})
Horizontal.args = {
  ...dataHorizontal,
  orientation: 'horizontal',
  className: 'primary',
  onClick: (e, data) => {
    e.preventDefault()
    console.log(e, data)
  },
}
Horizontal.parameters = { design }

export const Vertical = Template.bind({})
Vertical.args = {
  ...dataVertical,
  orientation: 'vertical',
  className: 'secondary',
  onClick: (e, data) => {
    e.preventDefault()
    console.log(e, data)
  },
}
Vertical.parameters = { design }

export const VerticalWithLinkComponent = Template.bind({})
VerticalWithLinkComponent.args = {
  ...dataVerticalWithLinkComponent,
  orientation: 'vertical',
  className: 'secondary',
  onClick: (e, data) => {
    e.preventDefault()
    console.log(e, data)
  },
}
VerticalWithLinkComponent.parameters = { design }
