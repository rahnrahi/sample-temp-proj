import React from 'react'
import PropTypes from 'prop-types'

const Link = ({ component, url, label, id, count, handleClick }) => {
  if (component) {
    return component
  }

  return (
    <a href={url} onClick={e => handleClick(e, { url, label, id })}>
      {label} {count ? <span className='count'>({count})</span> : null}
    </a>
  )
}

Link.propTypes = {
  component: PropTypes.object,
  url: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  count: PropTypes.number,
  handleClick: PropTypes.func,
}

export default Link
