import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { StyledUL, StyledLI } from './Navigation.styles'
import cx from 'classnames'
import { Icon } from '../../atoms'
import Link from './Link'

/**
 * Renders a tree of nav items recursively.
 * if passing a child component in as the link the following props can be used:
 *   Item.propTypes = {
 *     id: PropTypes.number,
 *     linkComponent: PropTypes.object, // The child component
 *     active: PropTypes.bool,
 *     children: PropTypes.arrayOf(PropTypes.object),
 *     count: PropTypes.number,
 *     disabled: PropTypes.bool,
 *   }
 * Otherwise (for backwards compatibility) the following props can be used:
 *   Item.propTypes = {
 *     id: PropTypes.number,
 *     url: PropTypes.string,
 *     label: PropTypes.string,
 *     active: PropTypes.bool,
 *     children: PropTypes.arrayOf(PropTypes.object),
 *     index: PropTypes.number,
 *     level: PropTypes.number,
 *     onClick: PropTypes.func,
 *     count: PropTypes.number,
 *     disabled: PropTypes.bool,
 *   }
 * @param {*} param0
 * @returns {Object}
 */
export const Item = ({
  url,
  linkComponent = null,
  label,
  active,
  children,
  level,
  count,
  disabled,
  onClick,
  id,
}) => {
  const [toggle, setToggle] = useState(false)

  useEffect(() => {
    children && children.some(child => child.active) ? setToggle(true) : ''
  }, [children])

  const handleClick = (e, link) => {
    if (!disabled) {
      onClick(e, link)
    }
  }

  const handleToggle = e => {
    setToggle(!toggle)
    onClick && onClick(e)
  }

  const labelWithIndex = (
    <React.Fragment>
      {label} {count ? <span className='count'>({count})</span> : null}{' '}
    </React.Fragment>
  )

  const caret = toggle ? (
    <Icon iconName='UpCaret' size={12} />
  ) : (
    <Icon iconName='DownCaret' size={12} />
  )

  const isLeaf = typeof linkComponent == 'object' || typeof url == 'string'
  const hasChildren = Array.isArray(children) && children.length

  return (
    <StyledLI
      className={cx({
        active: active,
        ['has-child']: children,
        expanded: toggle,
        ['disabled']: disabled,
      })}
    >
      {hasChildren && (
        <React.Fragment>
          <span onClick={e => handleToggle(e)}>
            {labelWithIndex}
            {caret}
          </span>
          <StyledUL className={cx({ ['child']: children }, `level-${++level}`)}>
            {children.map((link, index) => {
              return (
                <Item
                  {...link}
                  key={link.id ? link.id : index}
                  index={index}
                  onClick={e => handleClick(e, link)}
                />
              )
            })}
          </StyledUL>
        </React.Fragment>
      )}
      {isLeaf && !hasChildren && (
        // Including the !hasChildren check here maintains the original functionality for backward compatability
        <Link
          component={linkComponent}
          url={url}
          label={label}
          id={id}
          handleClick={handleClick}
        />
      )}
    </StyledLI>
  )
}

Item.propTypes = {
  id: PropTypes.number,
  linkComponent: PropTypes.object,
  url: PropTypes.string,
  label: PropTypes.string,
  active: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.object),
  index: PropTypes.number,
  level: PropTypes.number,
  onClick: PropTypes.func,
  count: PropTypes.number,
  disabled: PropTypes.bool,
}
