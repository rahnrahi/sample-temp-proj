import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledNav = styled.div`
  background: ${theme.palette.ui.neutral.grey5};
  padding: 10px;

  &.horizontal {
    ul {
      display: flex;

      li {
        ${theme.typography.h5.css};
        position: relative;
        font-size: 16px;
        line-height: 19px;
        padding: 0px 14px;

        .count {
          margin-left: 7px;
        }

        &:first-child {
          padding-left: 0px;
        }

        &:last-child {
          padding-right: 0px;
        }

        &.has-child {
          ul {
            flex-direction: column;
            position: absolute;
            left: 0px;
          }
        }
      }
    }
  }

  &.vertical {
    .level-0 {
      li {
        padding: 17px 0px;
        a,
        span {
          font-family: ${theme.typography.body.fontFamily};
        }
      }
    }
    li.has-child {
      &.expanded {
        padding-bottom: 0px;

        span {
          color: ${theme.palette.brand.primary.charcoal};
          font-family: ${theme.typography.body.fontFamily};
        }
      }

      ul {
        padding: 15px 0px 10px 15px;

        li {
          padding: 15px 0px;
        }
      }
    }
  }
`
export const StyledUL = styled.ul`
  list-style: none;
  margin: 0px;
  padding: 0px;
  z-index: 9;

  li.has-child {
    ul {
      display: none;
    }

    &.expanded {
      ul {
        display: block;
      }
    }
  }
`
export const StyledLI = styled.li`
  font-size: 18px;
  font-family: ${theme.typography.link.fontFamily};

  &.active {
    a,
    span {
      color: ${theme.palette.brand.primary.charcoal};

      &:after {
        content: '';
        display: block;
        height: 2px;
        background-color: ${theme.palette.ui.cta.blue};
        position: absolute;
        bottom: -8px;
        width: 100%;
      }

      svg {
        fill: ${theme.palette.brand.primary.charcoal};
      }
    }
  }

  &.disabled {
    cursor: no-drop;
    pointer-events: none;

    a,
    span {
      color: ${theme.palette.ui.neutral.grey7};
    }

    svg {
      fill: ${theme.palette.ui.neutral.grey7};
    }
  }

  &:hover,
  &:hover a,
  &:hover span {
    color: ${theme.palette.brand.primary.charcoal};
  }
  &:hover {
    svg {
      fill ${theme.palette.brand.primary.charcoal};
    }
  }

  &.has-child:hover {
    ul > li.active > * {
      color: ${theme.palette.brand.primary.charcoal};
    }

    ul > li > * {
      color: ${theme.palette.brand.primary.gray};
    }

    ul > li:hover > * {
      color: ${theme.palette.brand.primary.charcoal};
    }
  }

  a {
    text-decoration: none;
    color: ${theme.palette.brand.primary.gray};
    display: inline-block;
    position: relative;
  }
  span {
    color: ${theme.palette.brand.primary.gray};
    cursor: pointer;

    svg {
      position: relative;
      left: 18px;
      fill: ${theme.palette.brand.primary.gray};
    }
  }
`
export const StyledHead = styled.div`
  padding-bottom: 24px;
`
export const StyledH1 = styled.h1`
  color: ${theme.palette.brand.primary.charcoal};
  margin: 0px;
  padding: 0px;
  font-size: 24px;
  line-height: 29px;
`
