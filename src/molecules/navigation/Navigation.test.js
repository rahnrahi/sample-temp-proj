import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import { Navigation } from './'
import 'jest-styled-components'
import { dataVertical, dataHorizontal, MockLink } from './mock'

afterEach(cleanup)
describe('<Navigation/>', () => {
  it('renders navigation horizontal component correctly', () => {
    const { container } = render(<Navigation {...dataHorizontal} />)
    expect(container).toMatchSnapshot()
  })
  it('renders navigation vertical component correctly', () => {
    const { container } = render(<Navigation {...dataVertical} />)
    expect(container).toMatchSnapshot()
  })
  it('renders a single nav option', () => {
    const links = [
      {
        id: 1,
        url: '/home/alone',
        label: 'Home Alone',
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const link = screen.getByText('Home Alone')
    expect(link).toBeInTheDocument()
  })
  it('renders a single nav option when it passed a component as a link', () => {
    const links = [
      {
        id: 1,
        linkComponent: <MockLink label={'Home Alone'} />,
      },
    ]

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
      />
    )

    const link = screen.getByText('Home Alone')
    expect(link).toBeInTheDocument()
  })
  it('does not register click events when a component is provided for links', () => {
    const links = [
      {
        id: 1,
        linkComponent: <MockLink label={'Home Alone'} />,
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const link = screen.getByText('Home Alone')
    link.click()
    expect(onClick).toHaveBeenCalledTimes(0)
  })
  it('renders enabled nav options and registers click events, but does not register click events for disabled items', () => {
    const links = [
      {
        id: 1,
        url: '/home/alone',
        label: 'Home Alone',
      },
      {
        id: 2,
        url: '/home/alone/2',
        label: 'Home Alone 2',
        disabled: true,
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const original = screen.getByText('Home Alone')
    expect(original).toBeInTheDocument()
    const sequel = screen.getByText('Home Alone 2')
    expect(sequel).toBeInTheDocument()
    sequel.click()
    expect(onClick).toHaveBeenCalledTimes(0)
    original.click()
    expect(onClick).toHaveBeenCalledTimes(1)
  })
  it('renders multiple nav options without children', () => {
    const links = [
      {
        id: 1,
        url: '/home/alone',
        label: 'Home Alone',
      },
      {
        id: 2,
        url: '/home/alone/2',
        label: 'Home Alone 2',
      },
      {
        id: 3,
        url: '/home/alone',
        label: 'Home Alone 3',
      },
      {
        id: 4,
        url: '/die/hard',
        label: 'Die Hard',
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const homeAlone = screen.getByText('Home Alone')
    expect(homeAlone).toBeInTheDocument()
    const homeAlone2 = screen.getByText('Home Alone 2')
    expect(homeAlone2).toBeInTheDocument()
    const homeAlone3 = screen.getByText('Home Alone 3')
    expect(homeAlone3).toBeInTheDocument()
    const dieHard = screen.getByText('Die Hard')
    expect(dieHard).toBeInTheDocument()
  })
  it('renders multiple nav options when they are nested', () => {
    const links = [
      {
        id: 1,
        label: 'Home Alone (Series)',
        children: [
          {
            id: 1,
            label: 'Home Alone',
            url: '/home-alone',
          },
          {
            id: 2,
            label: 'Home Alone 2',
            url: '/home-alone-2',
          },
          {
            id: 3,
            label: 'Home Alone 3',
            url: '/home-alone-3',
          },
        ],
      },
      {
        id: 2,
        label: 'Die Hard (Series)',
        children: [
          {
            id: 1,
            label: 'Die Hard',
            url: '/die-hard',
          },
          {
            id: 2,
            label: 'Die Hard 2',
            url: '/die-hard 2',
          },
          {
            id: 3,
            label: 'Die Hard with a Vengeance',
            url: '/die-hard-with-a-vengeance',
          },
          {
            id: 4,
            label: 'Live Free or Die Hard',
            url: '/live-free-or-die-hard',
          },
        ],
      },
      {
        id: 3,
        url: '/inception',
        label: 'Inception',
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const topLevelLabels = links.map(entry => entry.label)
    for (const label of topLevelLabels) {
      const menuItem = screen.getByText(label)
      expect(menuItem).toBeInTheDocument()
    }

    // Submenu items are exposed on click
    const secondMenuLabel = screen.getByText(topLevelLabels[1])
    secondMenuLabel.click()
    const submenuItemLabels = links[1].children.map(entry => entry.label)
    for (const label of submenuItemLabels) {
      const menuItem = screen.getByText(label)
      expect(menuItem).toBeInTheDocument()
    }
  })
  it('renders multiple nav options as link components when they are nested', () => {
    const links = [
      {
        id: 1,
        label: 'Home Alone (Series)',
        children: [
          {
            id: 1,
            linkComponent: <MockLink label={'Home Alone'} />,
          },
          {
            id: 2,
            linkComponent: <MockLink label={'Home Alone 2'} />,
          },
          {
            id: 3,
            linkComponent: <MockLink label={'Home Alone 3'} />,
          },
        ],
      },
      {
        id: 2,
        label: 'Die Hard (Series)',
        children: [
          {
            id: 1,
            linkComponent: <MockLink label={'Die Hard'} />,
          },
          {
            id: 2,
            linkComponent: <MockLink label={'Die Hard 2'} />,
          },
          {
            id: 3,
            linkComponent: <MockLink label={'Die Hard with a Vengeance'} />,
          },
          {
            id: 4,
            linkComponent: <MockLink label={'Live Free or Die Hard'} />,
          },
        ],
      },
      {
        id: 3,
        linkComponent: <MockLink label={'Inception'} />,
      },
    ]
    const onClick = jest.fn()

    render(
      <Navigation
        orientation={'vertical'}
        className={'primary'}
        links={links}
        onClick={onClick}
      />
    )

    const topLevelLabels = ['Home Alone', 'Die Hard', 'Inception']
    for (const label of topLevelLabels) {
      const menuItem = screen.getByText(label)
      expect(menuItem).toBeInTheDocument()
    }

    // Submenu items are exposed on click
    const secondMenuLabel = screen.getByText(topLevelLabels[1])
    secondMenuLabel.click()
    const submenuItemLabels = [
      'Die Hard',
      'Die Hard 2',
      'Die Hard with a Vengeance',
      'Live Free or Die Hard',
    ]
    for (const label of submenuItemLabels) {
      const menuItem = screen.getByText(label)
      expect(menuItem).toBeInTheDocument()
    }
  })
})
