import React from 'react'
import PropTypes from 'prop-types'

export const dataVertical = {
  orientation: 'horizontal',
  className: 'primary',
  links: [
    {
      url: '/',
      label: 'Items',
      id: 1,
    },
    {
      url: '/',
      label: 'Attributes',
      id: 2,
    },
    {
      url: '/',
      label: 'Hierarchies',
      id: 3,
      children: [
        {
          url: '/',
          label: 'Master',
          id: 1,
          active: true,
        },
        {
          url: '/',
          label: 'Published',
          id: 2,
        },
      ],
    },
    {
      url: '/',
      label: 'Reports',
      id: 3,
    },
    {
      url: '/',
      label: 'Settings',
      id: 3,
    },
  ],
}

export const MockLink = ({ label }) => {
  return (
    <a
      aria-current={'page'}
      href={'/items'}
      onClick={event => {
        event.preventDefault()
        console.log('Click event', event)
      }}
    >
      {label}
    </a>
  )
}

MockLink.propTypes = {
  label: PropTypes.string,
}

export const dataVerticalWithLinkComponent = {
  orientation: 'horizontal',
  className: 'primary',
  links: [
    {
      id: 1,
      linkComponent: <MockLink label={'Items'} />,
    },
    {
      id: 2,
      linkComponent: <MockLink label={'Attributes'} />,
    },
    {
      label: 'Hierarchies',
      id: 3,
      children: [
        {
          id: 1,
          linkComponent: <MockLink label={'Master'} />,
          active: true,
        },
        {
          id: 2,
          linkComponent: <MockLink label={'Published'} />,
        },
      ],
    },
    {
      id: 4,
      linkComponent: <MockLink label={'Reports'} />,
    },
    {
      id: 5,
      linkComponent: <MockLink label={'Settings'} />,
      disabled: true,
    },
  ],
}

export const dataHorizontal = {
  orientation: 'horizontal',
  className: 'primary',
  title: 'All items',
  total: 200,
  links: [
    {
      url: '/',
      label: 'Live item',
      id: 1,
      active: true,
      count: 50,
    },
    {
      url: '/',
      label: 'Ready to review',
      id: 2,
      count: 50,
      disabled: true,
    },
    {
      url: '/',
      label: 'In progress',
      id: 3,
      count: 50,
    },
    {
      url: '/',
      label: 'Archived',
      id: 3,
      count: 50,
    },
  ],
}
