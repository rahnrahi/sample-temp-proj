import React from 'react'
import styled from 'styled-components'
import { TableLoader as TableLoaderComponent } from './TableLoader'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Table Loader',
  component: TableLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  width: 872px;
  background: white;
  border: 30px solid rgba(0, 0, 0, 0.025);
`

const Template = args => {
  return (
    <Container>
      <TableLoaderComponent {...args} />
    </Container>
  )
}

export const TableLoader = Template.bind({})
TableLoader.args = {
  rowCount: 12,
  columnCount: 5,
  columns: [
    'Product ID',
    'Product name',
    'Total Variants',
    'Created',
    'Status',
  ],
  theme: 'light',
}
TableLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
