import React from 'react'
import PropTypes from 'prop-types'
import { StyleTableLoader, StyledRow, StyledHead } from './TableLoader.style'
import { StyledListItem } from '../LoadingScreen.styles'

export const TableLoader = ({
  columns,
  columnCount,
  rowCount,
  theme,
  ...props
}) => {
  return (
    <StyleTableLoader {...props}>
      {columns.length !== 0 && (
        <StyledRow>
          {columns.map((c, i) => (
            <StyledHead
              key={`head-cell-${i}`}
              index={'' + i}
              className='table-item'
              length={columns.length}
            >
              {c}
            </StyledHead>
          ))}
        </StyledRow>
      )}
      {Array.from({ length: rowCount }).map((r, i) => {
        return (
          <StyledRow key={`row-${i}`}>
            {Array.from({
              length: columns.length > 0 ? columns.length : columnCount,
            }).map((c, j) =>
              columns.length > 0 ? (
                <StyledListItem
                  className='table-item'
                  key={`row-${i}-col-${j}`}
                  width={j === 1 ? '40%' : `calc(60%/${columns.length - 1})`}
                  height='16px'
                  theme={theme}
                />
              ) : (
                <StyledListItem
                  className='table-item'
                  key={`row-${i}-col-${j}`}
                  width={j === 1 ? '40%' : `calc(60%/${columnCount})`}
                  height='16px'
                  theme={theme}
                />
              )
            )}
          </StyledRow>
        )
      })}
    </StyleTableLoader>
  )
}

TableLoader.defaultProps = {
  columnCount: 5,
  rowCount: 12,
  columns: [],
}

TableLoader.propTypes = {
  /**
   *  column count for table loader
   * */
  columnCount: PropTypes.number.isRequired,
  /**
   * row count for  table laoder
   */
  rowCount: PropTypes.number.isRequired,
  /**
   *  header data for table loader
   * */
  columns: PropTypes.arrayOf(PropTypes.string),
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
}
