import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { TableLoader } from './TableLoader'

afterEach(cleanup)

describe('<TableLoader/>', () => {
  const columns = [
    'Product ID',
    'Product name',
    'Total Variants',
    'Created',
    'Status',
  ]

  it('should render table loader with default prop values', () => {
    const { container } = render(<TableLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render table loader with rowCount:5 columnCount:12', () => {
    const { container } = render(<TableLoader rowCount={5} columnCount={12} />)
    expect(container).toMatchSnapshot()
  })
  it('should render table loader with header data and theme:light', () => {
    const { container } = render(
      <TableLoader columns={columns} theme='light' />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render table loader with header data and theme:dark', () => {
    const { container } = render(<TableLoader columns={columns} theme='dark' />)
    expect(container).toMatchSnapshot()
  })
})
