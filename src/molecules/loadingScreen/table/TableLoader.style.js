import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyleTableLoader = styled.div`
  display: flex;
  background: ${theme.palette.brand.primary.white};
  flex-direction: column;
  .table-item {
    margin-right: 88px;
    :last-child {
      margin: 0;
    }
  }
`

export const StyledRow = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  padding: 27px 58px 27px 112px;
  align-items: center;
  box-shadow: inset 0px 1px 0px rgba(115, 127, 143, 0.1);
  justify-content: space-evenly;
`

export const StyledHead = styled.div`
  position: relative;
  font-size: 13px;
  line-height: 16px;
  letter-spacing: 0.02em;
  color: ${theme.palette.brand.primary.gray};
  width: ${({ index, length }) =>
    index === '1' ? '40%' : `calc(60%/${length - 1})`};
  height: ${({ height }) => height && height};
  display: inline-flex;
  align-items: center;
  word-wrap: break-word;
  :first-child {
    :before {
      content: '#';
      position: absolute;
      left: -50px;
      top: 0px;
    }
  }
`
