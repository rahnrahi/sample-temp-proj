import React from 'react'
import styled from 'styled-components'
import { SearchLoader as SearchLoaderComponent } from './Search'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Search Loader',
  component: SearchLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  background: #737f8f1a;
  width: 872px;
  padding: 20px;
`

const Template = args => {
  return (
    <Container>
      <SearchLoaderComponent {...args} />
    </Container>
  )
}

export const SearchLoader = Template.bind({})
SearchLoader.args = {
  theme: 'dark',
}
SearchLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
