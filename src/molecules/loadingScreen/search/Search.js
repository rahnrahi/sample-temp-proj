import React from 'react'
import PropTypes from 'prop-types'
import { StyledListItem } from '../LoadingScreen.styles'
import { StyledSearchLoader, StyledRow } from './Search.style'

export const SearchLoader = ({ theme, ...props }) => {
  return (
    <StyledSearchLoader {...props}>
      <StyledRow>
        <StyledListItem width='87%' height='40px' theme={theme} />
        <StyledListItem width='36px' height='36px' theme={theme} />
        <StyledListItem width='36px' height='36px' theme={theme} />
      </StyledRow>
    </StyledSearchLoader>
  )
}

SearchLoader.propTypes = {
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
}
