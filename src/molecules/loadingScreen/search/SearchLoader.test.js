import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { SearchLoader } from './Search'

afterEach(cleanup)

describe('<SearchLoader/>', () => {
  it('should render Search loader correctly', () => {
    const { container } = render(<SearchLoader />)
    expect(container).toMatchSnapshot()
  })

  it('should render Search loader with theme:light', () => {
    const { container } = render(<SearchLoader theme='light' />)
    expect(container).toMatchSnapshot()
  })

  it('should render Search loader with theme:dark', () => {
    const { container } = render(<SearchLoader theme='dark' />)
    expect(container).toMatchSnapshot()
  })
})
