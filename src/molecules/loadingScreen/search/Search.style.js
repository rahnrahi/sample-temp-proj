import styled from 'styled-components'

export const StyledSearchLoader = styled.div`
  display: flex;
  flex-direction: column;
`

export const StyledRow = styled.div`
  display: flex;
  align-items: center;
  div:first-child {
    margin-right: 29px;
  }
  div:last-child {
    margin-left: 12px;
  }
`
