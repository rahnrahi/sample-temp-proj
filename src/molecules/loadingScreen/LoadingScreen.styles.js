import styled, { keyframes, css } from 'styled-components'
import { theme } from '../../shared'

const DarkThemeCss = css`
  background: ${theme.palette.ui.neutral.grey4};
  background-image: linear-gradient(
    to right,
    ${theme.palette.ui.neutral.grey4} 0%,
    #dfdee0 20%,
    ${theme.palette.ui.neutral.grey4} 30%,
    ${theme.palette.ui.neutral.grey4} 100%
  );
`

const LightThemeCss = css`
  background: ${theme.palette.ui.neutral.grey5};
  background-image: linear-gradient(
    to right,
    ${theme.palette.ui.neutral.grey5} 0%,
    #e8eaed 20%,
    ${theme.palette.ui.neutral.grey5} 30%,
    ${theme.palette.ui.neutral.grey5} 100%
  );
`
const listItem = new Map()

listItem.set('light', LightThemeCss)
listItem.set('dark', DarkThemeCss)

const loading = keyframes`
  0% {
    background-position: -468px 0;
  }

  100% {
    background-position: 468px 0;
  }
`

export const StyledLoadingWrapper = styled.div`
  width: 100%;
  display: flex;
  background: transparent;
  flex-direction: column;
  overflow: hidden;
`

export const StyledListItem = styled.div`
  box-sizing: border-box;
  width: ${({ width }) => width && width};
  height: ${({ height }) => (height ? height : '16px')};
  border-radius: ${({ radius }) => (radius ? radius : '60px')};
  ${({ theme }) => listItem.get(theme)};
  ${({ loaderColor, theme }) =>
    loaderColor &&
    `
			background: ${loaderColor};
			background-image: linear-gradient(
				to right,
				${loaderColor} 0%,
				${theme === 'dark' ? '#dfdee0' : '#e8eaed'} 20%,
				${loaderColor} 30%,
				${loaderColor} 100%
			);
		`};
  background-repeat: no-repeat;
  background-size: 800px 104px;
  animation: ${loading} 1s linear infinite forwards;
`
