import styled from 'styled-components'

export const StyledLeftRail = styled.div`
  div {
    margin-bottom: 37px;
    :nth-child(4) {
      margin-bottom: 20px;
    }
    :nth-child(5) {
      width: 50%;
    }
  }
`
