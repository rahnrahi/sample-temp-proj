import React from 'react'
import styled from 'styled-components'
import { LeftRailLoader as LeftRailLoaderComponent } from './LeftRail'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Left Rail Loader',
  component: LeftRailLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  width: 150px;
  height: 100vh;
  padding: 20px;
  background: #737f8f1a;
`

const Template = args => {
  return (
    <Container>
      <LeftRailLoaderComponent {...args} />
    </Container>
  )
}

export const LeftRailLoader = Template.bind({})
LeftRailLoader.args = {
  rowCount: 7,
  theme: 'dark',
}
LeftRailLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
