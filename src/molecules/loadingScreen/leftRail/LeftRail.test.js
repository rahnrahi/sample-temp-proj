import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { LeftRailLoader } from './LeftRail'

afterEach(cleanup)

describe('<LeftRailLoader/>', () => {
  it('should render LeftRail loader with default prop values', () => {
    const { container } = render(<LeftRailLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render LeftRail loader with rowCount:10 theme:light', () => {
    const { container } = render(<LeftRailLoader rowCount={10} theme='light' />)
    expect(container).toMatchSnapshot()
  })
  it('should render LeftRail loader with rowCount:10 theme:dark', () => {
    const { container } = render(<LeftRailLoader rowCount={10} theme='dark' />)
    expect(container).toMatchSnapshot()
  })
})
