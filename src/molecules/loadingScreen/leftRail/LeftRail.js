import React from 'react'
import PropTypes from 'prop-types'
import { StyledLeftRail } from './LeftRail.style'
import { StyledListItem } from '../LoadingScreen.styles'

export const LeftRailLoader = ({ rowCount, theme, ...props }) => {
  return (
    <StyledLeftRail {...props}>
      {Array.from({ length: rowCount }).map((v, i) => (
        <StyledListItem key={i} theme={theme} />
      ))}
    </StyledLeftRail>
  )
}

LeftRailLoader.defaultProps = {
  rowCount: 7,
  theme: 'dark',
}

LeftRailLoader.propTypes = {
  /**
   * number of rows in  left rail loader
   */
  rowCount: PropTypes.number,
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
}
