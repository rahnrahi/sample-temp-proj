import React from 'react'
import styled from 'styled-components'
import { RightRailLoader as RightRailLoaderComponent } from './RightRail'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Right Rail Loader',
  component: RightRailLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  width: 113px;
  height: 700px;
  padding: 20px;
  background: #737f8f1a;
  box-shadow: 0 0 0 12px rgba(0, 0, 0, 0.025);
`

const Template = args => {
  return (
    <Container>
      <RightRailLoaderComponent {...args} />
    </Container>
  )
}

export const RightRailLoader = Template.bind({})
RightRailLoader.args = {
  rowCount: 5,
  theme: 'light',
}
RightRailLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
