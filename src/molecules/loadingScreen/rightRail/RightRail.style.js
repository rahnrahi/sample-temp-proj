import styled from 'styled-components'

export const StyledRightRail = styled.div`
  box-sizing: border-box;
  background: white;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  div {
    margin-top: 29px;
    :first-child {
      margin-top: 36px;
    }
    :nth-child(2) {
      margin-top: 88px;
    }
  }
`
