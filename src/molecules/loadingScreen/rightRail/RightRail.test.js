import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { RightRailLoader } from './RightRail'

afterEach(cleanup)

describe('<RightRailLoader/>', () => {
  it('should render RightRail loader with default prop values', () => {
    const { container } = render(<RightRailLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render RightRail loader with rowCount:10 theme:light', () => {
    const { container } = render(
      <RightRailLoader rowCount={10} theme='light' />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render RightRail loader with rowCount:10 theme:dark', () => {
    const { container } = render(<RightRailLoader rowCount={10} theme='dark' />)
    expect(container).toMatchSnapshot()
  })
})
