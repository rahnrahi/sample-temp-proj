import React from 'react'
import PropTypes from 'prop-types'
import { StyledRightRail } from './RightRail.style'
import { StyledListItem } from '../LoadingScreen.styles'

export const RightRailLoader = ({ rowCount, theme, ...props }) => {
  return (
    <StyledRightRail {...props}>
      {Array.from({ length: rowCount }).map((v, i) => (
        <StyledListItem key={i} width='36px' height='36px' theme={theme} />
      ))}
    </StyledRightRail>
  )
}

RightRailLoader.defaultProps = {
  rowCount: 5,
}

RightRailLoader.propTypes = {
  /**
   * number of rows in right rail loader
   */
  rowCount: PropTypes.number,
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
}
