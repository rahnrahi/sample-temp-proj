import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { ButtonLoader } from './ButtonLoader'

afterEach(cleanup)

describe('<ButtonLoader/>', () => {
  it('should render button loader with default prop values', () => {
    const { container } = render(<ButtonLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render button loader with theme:light', () => {
    const { container } = render(
      <ButtonLoader width='100px' height='30px' radius='30px' theme='light' />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render button loader with theme:dark', () => {
    const { container } = render(
      <ButtonLoader width='100px' height='30px' radius='30px' theme='dark' />
    )
    expect(container).toMatchSnapshot()
  })
})
