import React from 'react'
import styled from 'styled-components'
import { ButtonLoader as ButtonLoaderComponent } from './ButtonLoader'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Button Loader',
  component: ButtonLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const StyledContainer = styled.div`
  background: white;
  width: 250px;
  padding: 20px;
  border: 30px solid rgba(0, 0, 0, 0.025);
`

const Template = args => {
  return (
    <StyledContainer>
      <ButtonLoaderComponent {...args} />
    </StyledContainer>
  )
}

export const ButtonLoader = Template.bind({})
ButtonLoader.args = {
  width: '76px',
  height: '30px',
  radius: '30px',
  theme: 'light',
}
ButtonLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
