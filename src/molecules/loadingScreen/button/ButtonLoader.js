import React from 'react'
import PropTypes from 'prop-types'

import { StyledListItem } from '../LoadingScreen.styles'

export const ButtonLoader = ({ width, height, radius, theme, ...props }) => {
  return (
    <StyledListItem
      {...props}
      width={width}
      height={height}
      radius={radius}
      theme={theme}
    />
  )
}

ButtonLoader.defaultProps = {
  width: '76px',
  height: '30px',
  radius: '30px',
}

ButtonLoader.propTypes = {
  /**
   *  button loader width
   * */
  width: PropTypes.string.isRequired,
  /**
   *  button loader height
   * */
  height: PropTypes.string.isRequired,
  /**
   *  button loader radius
   * */
  radius: PropTypes.string.isRequired,
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
  /** Color for loader */
  loaderColor: PropTypes.string,
}
