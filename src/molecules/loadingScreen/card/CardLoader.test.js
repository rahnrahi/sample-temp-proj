import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { CardLoader } from './CardLoader'

afterEach(cleanup)

describe('<CardLoader/>', () => {
  it('should render card loader with default prop values', () => {
    const { container } = render(<CardLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render card loader with rowCount:5 theme:light', () => {
    const { container } = render(<CardLoader rowCount={5} theme='light' />)
    expect(container).toMatchSnapshot()
  })
  it('should render card loader with rowCount:5 theme:dark', () => {
    const { container } = render(<CardLoader rowCount={5} theme='dark' />)
    expect(container).toMatchSnapshot()
  })
})
