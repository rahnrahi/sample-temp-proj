import React from 'react'
import PropTypes from 'prop-types'
import { StyledCardLoader, StyledCardRow } from './CardLoader.style'
import { StyledListItem } from '../LoadingScreen.styles'

export const CardLoader = ({ rowCount, theme, ...props }) => {
  return (
    <StyledCardLoader {...props}>
      <StyledListItem width='43%' height='16px' theme={theme} />
      {Array.from({ length: rowCount }).map((v, i) => (
        <StyledCardRow key={i}>
          <StyledListItem width='70%' height='16px' theme={theme} />
          <StyledListItem width='30%' height='30px' theme={theme} />
        </StyledCardRow>
      ))}
    </StyledCardLoader>
  )
}

CardLoader.defaultProps = {
  rowCount: 4,
}

CardLoader.propTypes = {
  /**
   * number of rows in card loader
   */
  rowCount: PropTypes.number,
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
}
