import styled from 'styled-components'

export const StyledCardLoader = styled.div`
  display: flex;
  flex-direction: column;
`
export const StyledCardRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 32px;
  box-sizing: border-box;
  /* border: 2px solid red; */
  &:nth-child(2) {
    margin-top: 24px;
  }
  > div {
    :first-child {
      margin-right: 24px;
    }
  }
`
