import React from 'react'
import styled from 'styled-components'
import { CardLoader as CardLoaderComponent } from './CardLoader'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Card Loader',
  component: CardLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  background: white;
  width: 337px;
  padding: 20px;
  border: 30px solid rgba(0, 0, 0, 0.025);
`

const Template = args => {
  return (
    <Container>
      <CardLoaderComponent {...args} />
    </Container>
  )
}

export const CardLoader = Template.bind({})
CardLoader.args = {
  rowCount: 4,
  theme: 'light',
}
CardLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
