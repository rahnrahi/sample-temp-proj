import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledFormLoader = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${theme.palette.ui.neutral.grey4};
  border-radius: 4px;
`
export const StyledCardRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  box-sizing: border-box;
  height: 40px;
  box-shadow: inset 0px 1px 0px ${theme.palette.ui.neutral.grey4};
  padding: 0 24px;
  > div {
    :first-child {
      margin-right: 97px;
    }
  }
  ${({ shouldRemoveBorder }) => shouldRemoveBorder && `box-shadow: none;`}
`
export const StyledHeader = styled.div`
  padding: 32px 24px 8px;
  > div {
    :first-child {
      margin-bottom: 16px;
    }
  }
`
