import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { FormLoader } from './FormLoader'

afterEach(cleanup)

describe('<FormLoader/>', () => {
  it('should render form loader with default prop values', () => {
    const { container } = render(<FormLoader />)
    expect(container).toMatchSnapshot()
  })
  it('should render form loader with rowCount:5 theme:light', () => {
    const { container } = render(<FormLoader rowCount={5} theme='light' />)
    expect(container).toMatchSnapshot()
  })
  it('should render form loader with rowCount:5 theme:dark', () => {
    const { container } = render(<FormLoader rowCount={5} theme='dark' />)
    expect(container).toMatchSnapshot()
  })
  it('should render form loader with rowCount:5 isRowFullWidth:true', () => {
    const { container } = render(
      <FormLoader rowCount={5} isRowFullWidth={true} />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render form loader with rowCount:5 showHeader:false', () => {
    const { container } = render(<FormLoader rowCount={5} showHeader={false} />)
    expect(container).toMatchSnapshot()
  })
})
