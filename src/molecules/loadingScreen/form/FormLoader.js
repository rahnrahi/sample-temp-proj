import React from 'react'
import PropTypes from 'prop-types'
import {
  StyledFormLoader,
  StyledCardRow,
  StyledHeader,
} from './FormLoader.style'
import { StyledListItem } from '../LoadingScreen.styles'

export const FormLoader = ({
  rowCount,
  theme,
  showHeader,
  isRowFullWidth,
  ...props
}) => {
  const getRowWidth = i => {
    if (isRowFullWidth) {
      return 'calc(100% - 250px)'
    }
    if (i % 3 === 0) return 'max(362px, 100% - 764px)'
    return 'max(602px , 100% - 524px)'
  }
  return (
    <StyledFormLoader {...props}>
      {showHeader && (
        <StyledHeader>
          <StyledListItem
            width='min(100%, 251px)'
            height='16px'
            theme={theme}
          />
          <StyledListItem
            width='min(100%, 147px)'
            height='16px'
            theme={theme}
          />
        </StyledHeader>
      )}
      {Array.from({ length: rowCount }).map((_v, i) => (
        <StyledCardRow key={i} shouldRemoveBorder={i === 0 && !showHeader}>
          <StyledListItem width='min(30%, 153px)' height='16px' theme={theme} />
          <StyledListItem width={getRowWidth(i)} height='16px' theme={theme} />
        </StyledCardRow>
      ))}
    </StyledFormLoader>
  )
}

FormLoader.defaultProps = {
  rowCount: 7,
  showHeader: true,
  theme: 'light',
  isRowFullWidth: false,
}

FormLoader.propTypes = {
  /**
   * number of rows in card loader
   */
  rowCount: PropTypes.number,
  /**
   * white background :'light' ,
   * dark background:'dark'
   */
  theme: PropTypes.oneOf(['light', 'dark']),
  /**
   * should show header loader
   * @default true
   * @type boolean
   */
  showHeader: PropTypes.bool,
  /**
   * is item row value loader full width
   * @default false
   * @type boolean
   */
  isRowFullWidth: PropTypes.bool,
}
