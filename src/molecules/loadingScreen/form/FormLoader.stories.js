import React from 'react'
import styled from 'styled-components'
import { FormLoader as FormLoaderComponent } from './FormLoader'
import LoadingDocs from '../../../../docs/Loading.mdx'
import {
  SHIMMER_DESIGN,
  SHIMMER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../../hooks/constants'

export default {
  title: 'Modules/Loading/Loading Skeleton/Form Loader',
  component: FormLoaderComponent,
  args: {},
  parameters: {
    docs: {
      page: LoadingDocs,
    },
  },
}

const Container = styled.div`
  background: white;
  width: calc(100% - 60px);
  padding: 20px;
`

const Template = args => {
  return (
    <Container>
      <FormLoaderComponent {...args} />
    </Container>
  )
}

export const FormLoader = Template.bind({})
FormLoader.args = {
  rowCount: 7,
  theme: 'light',
}
FormLoader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SHIMMER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SHIMMER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
