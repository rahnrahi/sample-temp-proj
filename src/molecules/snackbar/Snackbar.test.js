import React from 'react'
import { Snackbar } from '.'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

const successProp = {
  alertIcon: 'icon-success',
  dismissable: true,
  kind: 'success',
  label: 'Image added succesfully',
  show: true,
  withIcon: true,
  onDismiss: () => {},
}

const alertProp = {
  alertIcon: 'icon-alert',
  dismissable: true,
  kind: 'alert',
  label: 'Something went wrong. Please try again',
  show: true,
  withIcon: true,
}

const msgProps = {
  dismissable: true,
  kind: 'message',
  label: 'New updates on PIM available',
  show: true,
}

const ntProps = {
  dismissable: true,
  kind: 'message',
  show: true,
  label: 'New updates on PIM available',
}

const fProps = {
  show: true,
  dismissable: true,
  kind: 'floating',
  label: 'Order cancelled successfully',
  textColor: '#ffffff',
  backgroundColor: '#121213',
  isHighEmphasis: true,
  isFloating: true,
  position: 'top-center',
}

describe('<Snake Bar/>', () => {
  it('renders success ui', () => {
    const { container } = render(<Snackbar {...successProp} />)
    expect(container).toMatchSnapshot()
  })

  it('renders alert ui', () => {
    const { container, getByTestId } = render(<Snackbar {...alertProp} />)
    const elem = getByTestId('sb-dismiss')
    fireEvent.click(elem)
    expect(container).toMatchSnapshot()
  })
  it('renders message ui', () => {
    const { container, getByTestId } = render(<Snackbar {...msgProps} />)
    const elem = getByTestId('sb-dismiss')
    fireEvent.click(elem)
    expect(container).toMatchSnapshot()
  })
  it('renders notification ui', () => {
    const { container, getByTestId } = render(<Snackbar {...ntProps} />)
    const elem = getByTestId('sb-dismiss')
    fireEvent.click(elem)
    expect(container).toMatchSnapshot()
  })
  it('renders Floating Alert', () => {
    const { container, getByTestId } = render(<Snackbar {...fProps} />)
    const elem = getByTestId('sb-dismiss')
    fireEvent.click(elem)
    expect(container).toMatchSnapshot()
  })
})
