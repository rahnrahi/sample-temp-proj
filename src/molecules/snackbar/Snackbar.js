import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { StyledSnackbar } from './Snackbar.styles'
import Icon from './Icon'
import { IconButton } from '../../atoms'
import cx from 'classnames'
import { theme } from '../../shared'
import ReactDOM from 'react-dom'

export const Snackbar = ({
  kind,
  children,
  dismissable,
  label,
  onDismiss,
  show,
  withIcon,
  className,
  height,
  width,
  backgroundColor,
  textColor = theme.palette.brand.primary.white,
  isFloating = false,
  isHighEmphasis = false,
  position = 'top-center',
  enablePortal,
  zIndex,
  ...others
}) => {
  const [alertVisible, setVisible] = useState(false)
  const [slideIn, setSlideIn] = useState(false)
  const [fadeOut, setFadeOut] = useState(false)

  const bgColor = {
    success: theme.palette.brand.primary.charcoal,
    alert: theme.palette.ui.cta.red,
    notification: theme.palette.brand.primary.charcoal,
    message: theme.palette.brand.primary.charcoal,
  }

  useEffect(() => {
    if (show) {
      setTimeout(() => setSlideIn(true), 500)
    }
  }, [show])

  const handleAlertDismiss = () => {
    if (typeof onDismiss == 'function') onDismiss()
    setFadeOut(true)
    setTimeout(() => {
      setVisible(false)
      setSlideIn(false)
      setFadeOut(false)
    }, 300)
  }

  const visible = typeof show === 'undefined' ? alertVisible : show

  if (!visible) return <span />

  let iconColumn
  if (withIcon) {
    iconColumn = (
      <div className='icon'>
        <Icon kind={kind} />
      </div>
    )
  }

  let dismissableColumn
  if (dismissable) {
    dismissableColumn = (
      <IconButton
        isRounded
        type='primary'
        icon='Close'
        data-testid='sb-dismiss'
        onClick={handleAlertDismiss}
      />
    )
  }

  const getSnackbarBody = () => {
    return (
      <StyledSnackbar
        className={cx({
          [kind]: kind,
          [className]: className,
          animate: slideIn,
          fadeout: fadeOut,
          [position]: position,
        })}
        {...others}
        height={height}
        width={width}
        show={show}
        isFloating={isFloating}
        textColor={textColor}
        isHighEmphasis={isHighEmphasis}
        backgroundColor={backgroundColor ? backgroundColor : bgColor[kind]}
        position={position}
        zIndex={zIndex}
        kind={kind}
      >
        <div className='row'>
          {iconColumn}
          <div className='col col-middle'>
            <div>
              {label && <span className='label'>{label}</span>}
              {children}
            </div>
          </div>
          {dismissableColumn}
        </div>
      </StyledSnackbar>
    )
  }

  return enablePortal
    ? ReactDOM.createPortal(getSnackbarBody(), document.body)
    : getSnackbarBody()
}

Snackbar.propTypes = {
  closeLabel: PropTypes.node,
  dismissable: PropTypes.bool,
  kind: PropTypes.oneOf([
    'alert',
    'success',
    'message',
    'notification',
    'floating',
  ]),
  label: PropTypes.string,
  onDismiss: PropTypes.func,
  show: PropTypes.bool,
  withIcon: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  backgroundColor: PropTypes.string,
  isFloating: PropTypes.bool,
  textColor: PropTypes.string,
  isHighEmphasis: PropTypes.bool,
  position: PropTypes.oneOf(['top-left', 'top-center', 'top-right']),
  /** Enable disable portal functionality */
  enablePortal: PropTypes.bool,
  /** set z-index */
  zIndex: PropTypes.number,
}

Snackbar.defaultProps = {
  closeLabel: 'Close alert',
  dismissable: false,
  withIcon: false,
  enablePortal: false,
  zIndex: theme.zIndex.snackbar,
}
