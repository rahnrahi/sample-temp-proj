import React, { useState } from 'react'
import { Snackbar } from '.'
import { Button } from '../../atoms'
import { theme } from '../../shared'
import NotificationsDocs from '../../../docs/Notifications.mdx'
import { TOASTR_DESIGN, TOASTR_PRESENTATION } from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Toast Message & Snackbar/Snackbar',
  component: Snackbar,
  args: {
    height: '60px',
    width: '600px',
  },
  parameters: {
    docs: {
      page: NotificationsDocs,
    },
  },
}

const Template = args => {
  const [toggle, setToggle] = useState(true)
  const handleClick = () => {
    setToggle(!toggle)
  }
  return <Snackbar {...args} onDismiss={() => handleClick()} show={toggle} />
}

export const MessageUI = Template.bind({})
MessageUI.args = {
  dismissable: true,
  kind: 'message',
  label: 'New updates on PIM available',
  isHighEmphasis: true,
}
MessageUI.parameters = getDesignSpecifications(
  TOASTR_DESIGN,
  TOASTR_PRESENTATION
)

export const AlertUI = Template.bind({})
AlertUI.args = {
  dismissable: true,
  kind: 'alert',
  label: 'Something went wrong. Please try again',
}
AlertUI.parameters = getDesignSpecifications(TOASTR_DESIGN, TOASTR_PRESENTATION)

export const SuccessUI = Template.bind({})
SuccessUI.args = {
  dismissable: true,
  kind: 'success',
  label: 'Image added succesfully',
}
SuccessUI.parameters = getDesignSpecifications(
  TOASTR_DESIGN,
  TOASTR_PRESENTATION
)

export const NotificationUI = args => (
  <Snackbar {...args}>
    <div className='sb-custom'>
      <div className='col left'>
        <p>10 SKU prices have been updated.</p>
      </div>
      <div className='col right'>
        <Button
          text='View items in Offers'
          size='small'
          isPrimary={false}
          emphasis='high'
          theme='dark'
        />
        <Button text='OK' size='small' />
      </div>
    </div>
  </Snackbar>
)
NotificationUI.args = {
  kind: 'message',
}
NotificationUI.parameters = getDesignSpecifications(
  TOASTR_DESIGN,
  TOASTR_PRESENTATION
)

export const FloatingUI = args => {
  const [toggle, setToggle] = useState(false)
  const handleClick = () => {
    setToggle(!toggle)
  }
  return (
    <>
      <Button text='Click me :)' size='small' onClick={() => handleClick()} />
      <Snackbar
        {...args}
        onDismiss={() => setTimeout(() => setToggle(false), 300)}
        show={toggle}
      />
    </>
  )
}

FloatingUI.args = {
  dismissable: true,
  kind: 'floating',
  label: 'Order cancelled successfully',
  backgroundColor: theme.palette.brand.primary.charcoal,
  isHighEmphasis: true,
  isFloating: true,
  position: 'top-center',
}
FloatingUI.parameters = getDesignSpecifications(
  TOASTR_DESIGN,
  TOASTR_PRESENTATION
)

export const SnackbarWithPortal = Template.bind({})
SnackbarWithPortal.args = {
  label: "I'm snackbar in React Portal.",
  enablePortal: true,
  backgroundColor: theme.palette.brand.primary.charcoal,
}
SnackbarWithPortal.parameters = getDesignSpecifications(
  TOASTR_DESIGN,
  TOASTR_PRESENTATION
)
