import styled from 'styled-components'
import { theme } from '../../shared'

const generateHoverBGColor = kind =>
  kind === 'alert'
    ? theme.palette.ui.error.two
    : theme.palette.ui.neutral.grey10

const generateActiveAndFocusBGColor = kind =>
  kind === 'alert'
    ? theme.palette.ui.error.three
    : theme.palette.ui.neutral.grey11

export const StyledSnackbar = styled.div`
  font-family: 'Gilroy';
  padding: 10px 24px;
  box-sizing: border-box;
  border-radius: 10px;
  font-size: 1rem;
  letter-spacing: 0.02em;
  min-height: 60px;
  display: flex;
  align-items: center;
  ${({ backgroundColor }) => `background-color: ${backgroundColor};`};
  ${({ textColor }) => `color: ${textColor};`};
  ${({ height }) =>
    `
      height: ${height};
  `};

  ${({ zIndex }) => zIndex && `z-index: ${zIndex}`};

  ${({ width }) =>
    `
      width: ${width};
  `};

  ${({ isHighEmphasis, textColor }) =>
    `
    .dismiss {
      path {
        fill: ${
          isHighEmphasis
            ? '#ffffff'
            : textColor || theme.palette.brand.primary.charcoal
        };
      }
    }
  `};

  ${({ isFloating, show, zIndex }) =>
    isFloating &&
    `
    position: absolute;
    opacity: 0;
    top: -200px;
    ${zIndex && `z-index: ${zIndex}`};

    &.top {
      &-left {
        left: 20px;
      }
      &-center {
        left: 50%;
        transform: translateX(-50%);
      }
      &-right {
        right: 20px;
      }
    }

    ${
      show &&
      `
      &.animate {
        opacity: 1;
        transition-property: opacity, top, bottom, left, right, width, margin, border-radius;
        transition-duration: 0.5s;
        transition-timing-function: ease;
        top: 70px;

        &.fadeout {
          opacity: 0;
        }
      }
    `
    }
  `};

  &.notification,
  &.message {
    box-shadow: 0px 5px 20px rgba(115, 127, 143, 0.1);
  }

  .row {
    display: flex;
    align-items: center;
    height: 100%;
    width: 100%;
    justify-content: space-between;

    svg {
      vertical-align: top;
    }
  }

  .sb-custom {
    display: flex;
    align-items: center;

    .col {
      &.left {
        flex-basis: 100%;

        p {
          margin: 0px;
          color: #ffffff;
        }
      }
      &.right {
        display: flex;
        align-items: center;
        justify-content: space-between;

        button {
          margin-left: 8px;
          white-space: nowrap;
        }
      }
    }
  }

  button.primary {
    .icon,
    .icon path {
      fill: ${theme.palette.brand.primary.white};
    }

    background-color: transparent;
    border: 1px ${theme.palette.brand.primary.white} solid;

    :hover {
      border-color: transparent;
      background-color: ${({ kind }) => generateHoverBGColor(kind)};
      .icon,
      .icon path {
        fill: ${theme.palette.brand.primary.white};
      }
    }

    :active {
      border-color: transparent;
      background-color: ${({ kind }) => generateActiveAndFocusBGColor(kind)};
    }

    :focus {
      .icon,
      .icon path {
        fill: ${theme.palette.brand.primary.white};
      }
      background-color: ${({ kind }) => generateActiveAndFocusBGColor(kind)};
    }
  }
`
