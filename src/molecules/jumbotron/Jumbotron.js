import React from 'react'
import PropTypes from 'prop-types'
import { StyledJumbotronWrapper } from './Jumbotron.styles'
import { Button } from '../../index'

export const Jumbotron = ({
  primaryText,
  secondaryText,
  backgroundColor,
  width,
  height,
  buttonProps,
  onClick,
}) => {
  const style = {
    backgroundColor,
    width,
    height,
  }

  return (
    <StyledJumbotronWrapper style={style}>
      {primaryText && <div className='primary-text'>{primaryText}</div>}
      {secondaryText && <div className='secondary-text'>{secondaryText}</div>}
      {buttonProps && (
        <div>
          <Button {...buttonProps} onClick={onClick} />
        </div>
      )}
    </StyledJumbotronWrapper>
  )
}

Jumbotron.defaultProps = {
  width: '100%',
  height: '100%',
}

Jumbotron.propTypes = {
  primaryText: PropTypes.string,
  secondaryText: PropTypes.string,
  backgroundColor: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  buttonProps: PropTypes.object,
  onClick: PropTypes.func.isRequired,
}

export default Jumbotron
