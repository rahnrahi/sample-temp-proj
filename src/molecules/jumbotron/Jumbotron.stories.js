import React from 'react'
import Jumbotron from './Jumbotron'
import InformationMessageDocs from '../../../docs/InformativeMessage.mdx'
import {
  INFO_MESSAGE_DESIGN,
  INFO_MESSAGE_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Informative Message/Jumbotron',
  component: Jumbotron,
  parameters: {
    docs: {
      page: InformationMessageDocs,
    },
  },
}

const Template = args => <Jumbotron {...args} />

export const FullSize = Template.bind({})
FullSize.args = {
  primaryText: 'Registration Successful!',
  secondaryText: 'You have successfully registered with XM',
  buttonProps: {
    text: 'Continue',
    size: 'large',
  },
  onClick: () => {},
}
FullSize.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: INFO_MESSAGE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: INFO_MESSAGE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
