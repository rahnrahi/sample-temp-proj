import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledJumbotronWrapper = styled.aside`
  background-color: ${theme.palette.ui.neutral.grey4};
  font-family: ${theme.typography.body};
  padding: 120px 0px;
  display: flex;
  flex-direction: column;
  align-items: center;

  .primary-text {
    color: ${theme.palette.brand.primary.charcoal};
    font-size: 34px;
    line-height: 29px;
    padding-bottom: 24px;
    white-space: pre-wrap;
  }

  .secondary-text {
    font-family: ${theme.typography.kicker.fontFamily};
    text-transform: ${theme.typography.kicker.textTransform};
    font-size: ${theme.typography.kicker.fontSize};
    letter-spacing: ${theme.typography.kicker.letterSpacing};
    line-height: ${theme.typography.kicker.lineHeight};
    color: ${theme.palette.brand.primary.gray};
    padding-bottom: 24px;
    white-space: pre-wrap;
  }
`
