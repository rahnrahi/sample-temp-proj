import React from 'react'
import { Jumbotron } from './index'
import { render, cleanup } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)
const args = {
  primaryText: 'Registration Successful!',
  secondaryText: 'You have successfully registered with XM',
  buttonProps: {
    text: 'Continue',
    size: 'large',
  },
  onClick: () => {},
}
describe('<Jumbotron/>', () => {
  it('renders jumbotron placeholder', () => {
    const { container } = render(<Jumbotron {...args} />)
    expect(container).toMatchSnapshot()
  })
})
