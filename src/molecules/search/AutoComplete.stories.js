import React, { useState, useEffect } from 'react'
import { AutoComplete } from '.'
import { skuProps } from './mock'
import SearchDocs from '../../../docs/Search.mdx'
import {
  SEARCH_DESIGN,
  SEARCH_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Search/Primary',
  component: AutoComplete,
  parameters: {
    actions: { argTypesRegex: '^on.*' },
    docs: { page: SearchDocs },
  },
}

export const Primary = () => {
  const [value, setValue] = useState('')
  const [show, setShow] = useState(false)
  const handleChange = e => {
    setValue(e.target.value)
  }

  useEffect(() => {
    if (value.length > 3) setShow(true)
  }, [value])

  return (
    <AutoComplete
      inputProps={{
        icon: 'Search',
        className: 'search-autocomplete',
        isFloatedLabel: false,
        inputProps: {
          placeholder: 'Search by SKU Number, Category, or Collections',
          onChange: e => handleChange(e),
          value: value,
        },
      }}
      autoCompleteProps={{
        data: skuProps,
        isLoading: false,
        show: show,
        toggleSearchAll: true,
        className: 'autocomplete-popup',
        onSearchAll: event => console.log(event),
        onSelect: data => console.log(data, 'data...'),
        onClearSearch: (event, iconState) => {
          console.log(event, iconState, 'event')
          setShow(false)
          setValue('')
        },
        onEscPress: () => setShow(false),
        onBodyClick: () => setShow(false),
      }}
    />
  )
}
Primary.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: SEARCH_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: SEARCH_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
