import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledCSearchContainer = styled.div`
  display: flex;
  &:first-child {
    width: 100%;
  }

  .actions {
    padding-left: 10px;
  }
`

export const StyledCSearch = styled.div`
  display: flex;
  box-shadow: 0px 5px 20px rgba(115, 127, 143, 0.1);
  background-color: #ffffff;
  border-radius: 50px;
  box-sizing: border-box;
  padding: 5px 25px;
  width: 100%;
  align-items: center;
  position: relative;
  height: 50px;

  .ac-input-wrapper {
    width: 100%;
    position: relative;

    .ac-icon-right {
      position: absolute;
      right: 0px;
      top: 50%;
      transform: translateY(-50%);
    }
  }

  input {
    border: none;
  }

  .column {
    &.left {
      flex-basis: 100%;
      position: relative;
    }
    &.right {
      flex-basis: 300px;

      .option-label {
        text-align: right;
        padding-right: 25px;
      }

      .ctx-search {
        > div:nth-child(3) {
          right: -25px;
          top: 50px;
          border: 1px solid ${theme.palette.ui.neutral.grey4};
          border-radius: 4px;
        }
      }

      & > div {
        width: auto;
        margin: auto;

        > div:nth-of-type(2) {
          box-shadow: none;
          padding-bottom: 12px;
        }
      }
    }
  }
`
export const StyledAutoComplete = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 52px;
  background-color: ${theme.palette.brand.primary.white};
  border: 1px solid ${theme.palette.ui.neutral.grey5};
  box-sizing: border-box;
  box-shadow: 0px 6px 16px rgba(115, 127, 143, 0.16);
  border-radius: 4px;
  padding: 32px;
  z-index: 9;

  .autocomplete-loader {
    text-align: center;
  }

  .head {
    display: flex;

    .col {
      flex: 1;

      &.right {
        text-align: right;
      }
    }
  }

  .content {
    .rows {
      display: flex;
      align-items: center;
      border-bottom: 1px solid ${theme.palette.ui.neutral.grey4};
      padding: 16px 0px;
      cursor: pointer;
      font-size: 13px;

      .cols {
        padding-right: 20px;

        img {
          vertical-align: top;
          border-radius: 4px;
        }
        &.img {
          min-width: 36px;
        }
        &.sku {
          color: ${theme.palette.brand.primary.charcoal};
        }
        &.name {
          color: ${theme.palette.ui.cta.blue};
        }
        &.tags {
          color: ${theme.palette.brand.primary.gray};
        }
        &:last-child {
          padding-right: 0px;
        }
      }
    }
  }
`
