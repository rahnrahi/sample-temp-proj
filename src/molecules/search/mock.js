export const skuProps = {
  sectionTitle: 'SKU',
  results: [
    {
      image: 'https://via.placeholder.com/36x36',
      sku: '0200388292',
      name: 'Energy and Metabolism',
      tags: 'PIM / Women / Multivitamins',
    },
    {
      image: 'https://via.placeholder.com/36x36',
      sku: '0200388292',
      name: 'Energy and Metabolism Plus',
      tags: 'PIM / Women / Multivitamins',
    },
    {
      image: 'https://via.placeholder.com/36x36',
      sku: '0200388292',
      name: 'Energy and Metabolism Sport',
      tags: 'PIM / Women / Multivitamins',
    },
  ],
}

export const options = [
  { id: 1, name: 'Josh Weir' },
  { id: 2, name: 'Sarah Weir' },
  { id: 3, name: 'Alicia Weir' },
  { id: 4, name: 'Ashutosh Tiwari' },
]
