import React, { useCallback, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { Input } from '../../atoms/input'
import { Dropdown } from '../dropdown'
import { StyledCSearch, StyledAutoComplete } from './Search.styles'
import { Loading, Link } from '../../'
import useKeyPress from '../../hooks/key-press'
import useClickOutside from '../../hooks/click-outside'
import { IconButton } from '../../atoms/icon-button'

export const ContextualSearch = ({
  inputProps,
  dropdownProps,
  showDropdown = true,
}) => {
  return (
    <StyledCSearch>
      <div className='column left'>
        <Input {...inputProps} />
      </div>
      {showDropdown ? (
        <div className='column right'>
          <Dropdown {...dropdownProps} />
        </div>
      ) : null}
    </StyledCSearch>
  )
}

ContextualSearch.propTypes = {
  inputProps: PropTypes.shape({
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    label: PropTypes.string,
    icon: PropTypes.string,
    boxed: PropTypes.bool,
    className: PropTypes.string,
    inputProps: PropTypes.shape({
      defaultValue: PropTypes.string,
      onBlur: PropTypes.func,
      onFocus: PropTypes.func,
      onChange: PropTypes.func,
      type: PropTypes.string,
      value: PropTypes.string,
      placeholder: PropTypes.string,
    }),
    errorProps: PropTypes.object,
    kind: PropTypes.string,
  }),
  dropdownProps: PropTypes.shape({
    isDefault: PropTypes.bool,
    titleLabel: PropTypes.string.isRequired,
    className: PropTypes.string,
    options: PropTypes.array.isRequired,
    value: PropTypes.object,
    onSelect: PropTypes.func,
  }),
  showDropdown: PropTypes.bool,
}

export const AutoComplete = ({ inputProps, autoCompleteProps, children }) => {
  const {
    isLoading,
    data,
    show,
    onSelect,
    onSearchAll,
    toggleSearchAll,
    onClearSearch,
    onEscPress,
    onBodyClick,
    ...rest
  } = autoCompleteProps

  const { value } = inputProps.inputProps
  const escPress = useKeyPress('Escape')
  const clickRef = useRef()

  useEffect(() => {
    if (escPress) {
      if (typeof onEscPress === 'function') onEscPress()
    }
  }, [escPress, onEscPress])

  useClickOutside(
    clickRef,
    () => {
      if (typeof onBodyClick === 'function') onBodyClick()
    },
    [onBodyClick]
  )

  const handleClick = useCallback(
    (event, item) => {
      event.preventDefault()
      if (typeof onSelect === 'function') {
        onSelect(item)
      }
    },
    [onSelect]
  )

  const handleClear = e => {
    onClearSearch(e)
  }

  const showLoading = () => {
    return isLoading ? (
      <Loading className='autocomplete-loader' show={true} />
    ) : (
      <>
        <div className='head'>
          <div className='col left'>{data.sectionTitle}</div>
          <div className='col right'>
            {toggleSearchAll ? (
              <Link
                text='Seach All'
                href=''
                type='secondary'
                onClick={event => onSearchAll(event)}
              />
            ) : null}
          </div>
        </div>
        <div className='content'>
          {data.results.map((item, index) => {
            return (
              <div
                className='rows'
                key={index}
                onClick={event => handleClick(event, item)}
              >
                {item.image && (
                  <div className='cols img'>
                    <img src={item.image} alt={item.name} />
                  </div>
                )}
                {item.sku && <div className='cols sku'>{item.sku}</div>}
                {item.name && <div className='cols name'>{item.name}</div>}
                {item.tags && <div className='cols tags'>{item.tags}</div>}
              </div>
            )
          })}
        </div>
      </>
    )
  }

  return (
    <StyledCSearch ref={clickRef}>
      <div className='ac-input-wrapper'>
        <Input {...inputProps} />
        {value.length > 0 && (
          <div className='ac-icon-right'>
            <IconButton
              icon='Close'
              iconSize={14}
              isRounded
              buttonSize='24px'
              onClick={e => handleClear(e)}
            />
          </div>
        )}
      </div>
      {show ? (
        <StyledAutoComplete {...rest}>
          {children ? children : showLoading()}
        </StyledAutoComplete>
      ) : null}
    </StyledCSearch>
  )
}

AutoComplete.propTypes = {
  inputProps: PropTypes.shape({
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    label: PropTypes.string,
    icon: PropTypes.string,
    boxed: PropTypes.bool,
    className: PropTypes.string,
    inputProps: PropTypes.shape({
      defaultValue: PropTypes.string,
      onBlur: PropTypes.func,
      onFocus: PropTypes.func,
      onChange: PropTypes.func,
      type: PropTypes.string,
      value: PropTypes.string,
      placeholder: PropTypes.string,
      localeCode: PropTypes.string,
    }),
    errorProps: PropTypes.object,
    kind: PropTypes.string,
  }),
  autoCompleteProps: PropTypes.shape({
    isLoading: PropTypes.bool,
    show: PropTypes.bool,
    onSelect: PropTypes.func,
    data: PropTypes.object,
    onSearchAll: PropTypes.func,
    toggleSearchAll: PropTypes.bool,
    onClearSearch: PropTypes.func,
    onEscPress: PropTypes.func,
    onBodyClick: PropTypes.func,
    rest: PropTypes.object,
  }),
  children: PropTypes.element,
}
