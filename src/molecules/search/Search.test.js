import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import { ContextualSearch, AutoComplete } from '../../'
import { options, skuProps } from './mock'
import 'jest-styled-components'

afterEach(cleanup)

const ctxSearchProp = {
  inputProps: {
    icon: 'Search',
    className: 'search-contextual',
    inputProps: {
      placeholder: 'Search by SKU Number, Category, or Collections',
    },
  },
  dropdownProps: {
    isDefault: true,
    options: [...options],
    value: { id: 0, name: 'All Item Types' },
    className: 'ctx-search',
    onSelect: data => console.log(data),
  },
}

const autoCompleteProps = {
  inputProps: {
    icon: 'Search',
    className: 'search-autocomplete',
    inputProps: {
      placeholder: 'Search by SKU Number, Category, or Collections',
      value: '',
    },
  },
  autoCompleteProps: {
    data: skuProps,
    isLoading: false,
    show: false,
    onSelect: data => console.log(data),
  },
}

describe('<ContextualSearch/>', () => {
  it('renders', () => {
    const { container } = render(<ContextualSearch {...ctxSearchProp} />)
    expect(container).toMatchSnapshot()
  })
})
describe('<AutoComplete/>', () => {
  it('renders', () => {
    const { container } = render(<AutoComplete {...autoCompleteProps} />)
    expect(container).toMatchSnapshot()
  })
  it('should show pop up given value length > 0', () => {
    autoCompleteProps.inputProps.value = 'something'
    autoCompleteProps.autoCompleteProps.show = true
    const { container } = render(<AutoComplete {...autoCompleteProps} />)
    expect(container).toMatchSnapshot()
  })
  it('should show loading pop up', () => {
    autoCompleteProps.inputProps.value = 'something'
    autoCompleteProps.autoCompleteProps.isLoading = true
    autoCompleteProps.autoCompleteProps.show = true
    const { container } = render(<AutoComplete {...autoCompleteProps} />)
    expect(container).toMatchSnapshot()
  })
  it('should show the custom children', () => {
    autoCompleteProps.inputProps.value = 'something'
    autoCompleteProps.autoCompleteProps.show = true
    const { container } = render(
      <AutoComplete {...autoCompleteProps}>
        <h1 data-testid='testing-search'>Testing</h1>
      </AutoComplete>
    )
    expect(container).toMatchSnapshot()
  })
})
