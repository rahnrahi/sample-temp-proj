import React from 'react'
import Flyout from './Flyout.example'
import { getFlyoutPlacements } from '../../shared/utils'
import FlyoutDocs from '../../../docs/Flyouts.mdx'
import {
  DROPDOWN_FLYOUT_DESIGN,
  DROPDOWN_FLYOUT_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Dropdowns/Flyout/With Text List',
  component: Flyout,
  argTypes: {
    onSelect: { action: 'click' },
    placement: {
      control: {
        type: 'select',
        options: getFlyoutPlacements(),
      },
    },
    icon: {
      control: {
        type: 'select',
        options: ['DownCaret', 'Add', 'Close', 'Edit', 'Export', 'Import'],
      },
    },
  },
  args: {
    show: false,
    className: 'primary',
    offset: 10,
    id: 'fl-example-1',
    placement: 'bottomLeft',
    items: [{ label: 'Item' }, { label: 'Item' }],
    width: '200px',
    buttonText: 'Add new',
    icon: 'Add',
  },
  parameters: {
    docs: {
      page: FlyoutDocs,
    },
  },
}

export const WithTextList = args => <Flyout {...args} />
WithTextList.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: DROPDOWN_FLYOUT_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: DROPDOWN_FLYOUT_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
