const checkWindow = () => !!window

const getWindowPosition = axis => {
  if (!checkWindow()) return 0
  return axis == 'X' ? window.scrollX : window.scrollY
}

export const setStyledBasedOnPosition = (
  event,
  placement,
  styles,
  flyoutDetails,
  offset,
  params
) => {
  if (!params || !flyoutDetails) {
    styles.left = (event && event.clientX) || 0
    styles.top = (event && event.clientY) || 0
    return styles
  }

  const leftPosition = params.x + getWindowPosition('X')
  switch (placement) {
    case 'leftTop':
      styles.left = leftPosition - flyoutDetails.width - offset
      styles.top = params.y + getWindowPosition('Y')
      break
    case 'leftCenter':
      styles.left = leftPosition - flyoutDetails.width - offset
      styles.top =
        params.y +
        params.height / 2 -
        flyoutDetails.height / 2 +
        getWindowPosition('Y')
      break
    case 'leftBottom':
      styles.left = leftPosition - flyoutDetails.width - offset
      styles.top =
        params.y + params.height - flyoutDetails.height + getWindowPosition('Y')
      break

    case 'topLeft':
      styles.left = leftPosition
      styles.top =
        params.y - flyoutDetails.height - offset + getWindowPosition('Y')
      break
    case 'topCenter':
      styles.left = leftPosition + params.width / 2 - flyoutDetails.width / 2
      styles.top =
        params.y - flyoutDetails.height - offset + getWindowPosition('Y')
      break
    case 'topRight':
      styles.left = leftPosition + params.width - flyoutDetails.width
      styles.top =
        params.y - flyoutDetails.height - offset + getWindowPosition('Y')
      break

    case 'rightTop':
      styles.left = leftPosition + params.width + offset
      styles.top = params.y + getWindowPosition('Y')
      break
    case 'rightCenter':
      styles.left = leftPosition + params.width + offset
      styles.top =
        params.y +
        params.height / 2 -
        flyoutDetails.height / 2 +
        getWindowPosition('Y')
      break
    case 'rightBottom':
      styles.left = leftPosition + params.width + offset
      styles.top =
        params.y + params.height - flyoutDetails.height + getWindowPosition('Y')
      getWindowPosition('Y')
      break

    case 'bottomCenter':
      styles.left = leftPosition + params.width / 2 - flyoutDetails.width / 2
      styles.top = params.y + params.height + offset + getWindowPosition('Y')
      break
    case 'bottomRight':
      styles.left = leftPosition + params.width - flyoutDetails.width
      styles.top = params.y + params.height + offset + getWindowPosition('Y')
      break

    default:
      styles.left = leftPosition
      styles.top = params.y + params.height + offset + getWindowPosition('Y')
  }
}
