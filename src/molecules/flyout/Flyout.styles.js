import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledFlyout = styled.div`
  position: relative;
  ${({ width }) => `width: ${width}`};
  z-index: ${theme.zIndex.context};
  overflow: hidden;

  .custom-scrollbar {
    scrollbar-width: thin;
    scrollbar-color: transparent transparent;
    transition: scrollbar-color 0.3s;

    &:hover {
      scrollbar-color: ${theme.palette.ui.neutral.grey1} transparent;
    }

    &:not(:hover)::-webkit-scrollbar-thumb {
      background: transparent;
    }

    &::-webkit-scrollbar {
      width: 8px;
    }

    &::-webkit-scrollbar-thumb {
      border-radius: 5px;
      transition: background 0.3s;
      background: ${theme.palette.ui.neutral.grey1};
    }
  }
  .content {
    background-color: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.neutral.grey2};
    box-shadow: 0px 6px 16px rgba(115, 127, 143, 0.16);
    border-radius: 4px;
    max-height: 200px;
    overflow-y: scroll;
    overflow-x: hidden;

    .content-inner {
      box-sizing: border-box;
      border-radius: 4px;
      overflow: ${({ overflow }) => (overflow ? overflow : 'hidden')};
    }
  }
`
export const StyledFlyoutItem = styled.div`
  font-size: 13px;
  line-height: 16px;
  letter-spacing: 0.02em;
  text-decoration: none;
  color: ${theme.palette.brand.primary.charcoal};
  padding: 10px 23px;
  box-sizing: border-box;
  transition: all 0.2s ease-in-out;
  cursor: pointer;

  &:hover {
    background-color: ${theme.palette.ui.neutral.grey5};
  }
`
