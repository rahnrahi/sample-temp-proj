import React, { useEffect, useState } from 'react'
import { Flyout as Example } from '.'
import PropTypes from 'prop-types'
import { ButtonWithIcon } from '../../atoms'

export const Flyout = props => {
  const {
    className,
    placement,
    offset,
    items,
    width,
    buttonText,
    icon,
    show,
  } = props

  const [toggle, setToggle] = useState(show)
  useEffect(() => {
    setToggle(show)
  }, [show])

  return (
    <>
      <ButtonWithIcon
        disabled={false}
        emphasis='high'
        icon={icon}
        iconPosition='left'
        isPrimary={false}
        isRoundIcon={false}
        onClick={() => setToggle(!toggle)}
        text={buttonText}
        showIcon
        theme='light'
        id='target1'
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      />
      <div style={{ width: '2000px' }}>&nbsp;</div>
      <Example
        show={toggle}
        id='target1'
        className={className}
        handleToggle={setToggle}
        placement={placement}
        offset={offset}
        items={items}
        onSelect={function (item) {
          console.log('selected item', item)
        }}
        width={width}
      />
    </>
  )
}

Flyout.propTypes = {
  show: PropTypes.bool,
  className: PropTypes.string,
  placement: PropTypes.string,
  offset: PropTypes.number,
  items: PropTypes.arrayOf(PropTypes.object),
  width: PropTypes.string,
  buttonText: PropTypes.string,
  icon: PropTypes.oneOf([
    'DownCaret',
    'Add',
    'Close',
    'Edit',
    'Export',
    'Import',
  ]),
}

export default Flyout
