import React, {
  forwardRef,
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react'
import { setStyledBasedOnPosition } from './Flyout.util'
import PropTypes from 'prop-types'
import { StyledFlyout, StyledFlyoutItem } from './Flyout.styles'
import useClickOutside from '../../hooks/click-outside'
import cx from 'classnames'
import ReactDOM from 'react-dom'

export const Flyout = forwardRef(
  (
    {
      children,
      className,
      id,
      show,
      onOutsideClick,
      placement,
      offset = 0,
      items,
      onSelect,
      width,
      ...props
    },
    ref
  ) => {
    const [styles, setStyles] = useState({})

    const fRef = useRef(null)
    useImperativeHandle(ref, () => fRef.current)

    useClickOutside(
      fRef,
      () => {
        if (typeof onOutsideClick === 'function') onOutsideClick()
      },
      show
    )

    const getPosition = useCallback(
      (event, id) => {
        let styles = {}
        const element = document.getElementById(id)
        const params = element && element.getBoundingClientRect()
        const flyoutDetails =
          fRef.current && fRef.current.getBoundingClientRect()

        setStyledBasedOnPosition(
          event,
          placement,
          styles,
          flyoutDetails,
          offset,
          params
        )
        return styles
      },
      [offset, placement]
    )

    const handleClick = useCallback(
      event => {
        event.preventDefault()
        setTimeout(() => {
          if (fRef.current) {
            const positionStyle = getPosition(event, id)
            setStyles({
              position: 'absolute',
              ...positionStyle,
            })
          }
        }, 0)
      },
      [id, getPosition]
    )

    useEffect(() => {
      const element = document.getElementById(id)
      if (element) {
        element.addEventListener('click', handleClick)

        return () => {
          element.removeEventListener('click', handleClick)
        }
      }
    }, [id, handleClick])

    const handleOnSelect = (event, item) => {
      event.stopPropagation()
      onSelect(item)
    }

    return show
      ? ReactDOM.createPortal(
          <StyledFlyout
            {...props}
            className={cx({
              active: show,
              [className]: className,
            })}
            ref={fRef}
            style={styles}
            width={width}
            data-target={id}
          >
            <div className='content custom-scrollbar'>
              <div className='content-inner'>
                {items && Array.isArray(items) && items.length
                  ? items.map((item, index) => (
                      <StyledFlyoutItem
                        onClick={event => handleOnSelect(event, item)}
                        key={index}
                      >
                        {item.label}
                      </StyledFlyoutItem>
                    ))
                  : children && children}
              </div>
            </div>
          </StyledFlyout>,
          document.body
        )
      : null
  }
)

Flyout.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  id: PropTypes.string,
  show: PropTypes.bool,
  onOutsideClick: PropTypes.func,
  placement: PropTypes.string,
  offset: PropTypes.number,
  items: PropTypes.array,
  onSelect: PropTypes.func,
  width: PropTypes.string,
}
