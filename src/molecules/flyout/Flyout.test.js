import React, { createRef } from 'react'
import { cleanup, fireEvent, render } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import { Flyout } from './'
import { FlyoutContent } from './mock'
import 'jest-styled-components'
import { getFlyoutPlacements } from '../../shared/utils'
import { setStyledBasedOnPosition } from './Flyout.util'

afterEach(cleanup)

const prop = {
  show: true,
  id: 'target1',
  className: 'primary',
  placement: 'bottomLeft',
  offset: 10,
}

const mockEvent = {
  clientX: 0,
  clientY: 0,
}

const placements = getFlyoutPlacements()
const flyoutDetailsMock = {
  height: 20,
  width: 50,
}

const paramsMock = { ...flyoutDetailsMock, x: 0, y: 0 }

describe('<Flyout/>', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should render', () => {
    const { container } = render(
      <Flyout {...prop}>
        <FlyoutContent />
      </Flyout>
    )
    expect(container).toMatchSnapshot()
  })

  it('should allow clicks on a menu item', () => {
    const selectHandler = jest.fn()
    const { getByText } = render(
      <Flyout
        {...prop}
        onSelect={selectHandler}
        items={[{ label: 'Item A' }, { label: 'Item B' }]}
      />
    )
    const menuItem = getByText('Item A')
    fireEvent.click(menuItem)
    expect(selectHandler).toHaveBeenCalledTimes(1)
    expect(menuItem).toBeInTheDocument()
  })

  it('should forward ref', () => {
    const ref = createRef()
    render(
      <Flyout
        ref={ref}
        {...prop}
        items={[{ label: 'Item A' }, { label: 'Item B' }]}
      />
    )
    expect(ref.current).not.toBeNull()
  })

  it('should test flyout.util with param', () => {
    placements.forEach((placement, index) => {
      const styles = {}
      setStyledBasedOnPosition(
        mockEvent,
        placement,
        styles,
        flyoutDetailsMock,
        index,
        paramsMock
      )
      expect(Object.keys(styles).length).toBeGreaterThan(0)
    })
  })
  it('should test flyout.util without param', () => {
    placements.forEach((placement, index) => {
      const styles = {}
      setStyledBasedOnPosition(
        mockEvent,
        placement,
        styles,
        flyoutDetailsMock,
        index
      )
      expect(styles).toEqual({
        left: 0,
        top: 0,
      })
    })
  })
})
