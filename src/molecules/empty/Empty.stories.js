import React from 'react'
import { Empty as Emptycomponent } from './index'
import InformationMessageDocs from '../../../docs/InformativeMessage.mdx'
import {
  INFO_MESSAGE_DESIGN,
  INFO_MESSAGE_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Informative Message',
  component: Emptycomponent,
  parameters: {
    docs: {
      page: InformationMessageDocs,
    },
  },
}

const Template = args => <Emptycomponent {...args} />
export const Empty = Template.bind({})
Empty.args = {
  className: 'empty',
  primaryText: 'You have no master hierarchy',
  secondaryText: 'Start by adding your master h’s and add\nany published h’s',
  buttonProps: {
    text: 'Import Master hierarchy',
    size: 'small',
  },
  footerContent:
    '###### Some Footer Text with [react markdown](https://www.npmjs.com/package/react-markdown)',
  markdown: true,
}
Empty.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: INFO_MESSAGE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: INFO_MESSAGE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const Error = Template.bind({})
Error.args = {
  className: 'empty',
  primaryText: 'Error 500',
  secondaryText:
    'Seems like we can’t find the page you are\nlooking for. You can try going back. ',
  buttonProps: {
    text: 'Go back',
    size: 'small',
  },
}
Error.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: INFO_MESSAGE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: INFO_MESSAGE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
