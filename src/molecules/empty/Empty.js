import React from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'
import { StyledEmptyWrapper } from './Empty.styles'
import { Button } from '../../atoms/button/Button'
import { lineBreak } from '../../shared/utils'

export const Empty = ({
  className,
  primaryText,
  secondaryText,
  buttonProps,
  children,
  footerContent,
  markdown,
}) => {
  return (
    <StyledEmptyWrapper className={className}>
      <div className='empty-inner'>
        {primaryText && (
          <div className='primary-text'>{lineBreak(primaryText)}</div>
        )}
        {secondaryText && (
          <div className='secondary-text'>
            {markdown ? (
              <ReactMarkdown>{secondaryText}</ReactMarkdown>
            ) : (
              lineBreak(secondaryText)
            )}
          </div>
        )}
        {buttonProps && (
          <div className='button-block'>
            <Button {...buttonProps} />
          </div>
        )}
        {footerContent && (
          <div className='footer-content'>
            {React.isValidElement(footerContent) ? (
              footerContent
            ) : (
              <ReactMarkdown>{footerContent}</ReactMarkdown>
            )}
          </div>
        )}
        {children && children}
      </div>
    </StyledEmptyWrapper>
  )
}

Empty.defaultProps = {
  footerContent: '',
  markdown: true,
}
Empty.propTypes = {
  /** Primary Text for Empty Component*/
  primaryText: PropTypes.string,
  /** Secondary Text for Empty Component, by default markdown is enabled for string*/
  secondaryText: PropTypes.string,
  /** custom class can be passed here for Empty container */
  className: PropTypes.string,
  /** All Button props can be passed here, for more info refer atoms/buttons*/
  buttonProps: PropTypes.object,
  /** The Children should be a React element*/
  children: PropTypes.element,
  /** The footerContent should be anything that can be rendered by React — a number, string, element, or array (or fragment) containing these types, by default markdown is enabled for string*/
  footerContent: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
  /** markdown for both secondaryText and footerContent(only for string type)*/
  markdown: PropTypes.bool,
}
