import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledEmptyWrapper = styled.div`
  background-color: ${theme.palette.ui.neutral.grey4};
  text-align: center;
  font-family: ${theme.typography.body};
  padding: 120px 0px;

  p {
    margin: 0;
  }
  h4,
  h5,
  h6 {
    margin: 0;
  }
  h4 {
    ${theme.typography.h4}
  }
  h5 {
    ${theme.typography.h5}
  }
  h6 {
    ${theme.typography.h6}
  }

  a {
    color: ${theme.palette.ui.cta.blue};
    text-decoration: none;
  }

  .primary-text {
    font-family: ${theme.typography.kicker.fontFamily};
    text-transform: ${theme.typography.kicker.textTransform};
    font-size: ${theme.typography.kicker.fontSize};
    letter-spacing: ${theme.typography.kicker.letterSpacing};
    line-height: ${theme.typography.kicker.lineHeight};
    color: ${theme.palette.brand.primary.gray};
    padding-bottom: 16px;
    white-space: pre-wrap;
  }

  .secondary-text {
    color: ${theme.palette.brand.primary.charcoal};
    font-size: 24px;
    line-height: 29px;
    padding-bottom: 54px;
    white-space: pre-wrap;
  }

  .button-block {
    padding-bottom: 140px;
    button {
      display: inline-block;
    }
  }

  .footer-content {
    font-family: ${theme.typography.caption};
    line-height: 16px;
    font-size: 13px;
  }
`
