import React from 'react'
import ListCollapsibleItem from './index'
import Collapse from '../../atoms/collapse'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const Template = () => {
  return (
    <Collapse>
      <ListCollapsibleItem title={'Header title'} id={'sectionTrackingKey'}>
        The content
      </ListCollapsibleItem>
    </Collapse>
  )
}

export const Basic = Template.bind({})
export default {
  title: 'Modules/Cards/List/ListCollapsibleItem',
  component: ListCollapsibleItem,
}
Basic.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
