import styled from 'styled-components'
import { ListItem } from '../../atoms/list-item'
import Collapse from '../../atoms/collapse'
import { theme } from '../../shared'

export const HeaderRoot = styled(ListItem)`
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  border-bottom: 1px transparent solid;
`
export const PanelRoot = styled(Collapse.Panel)`
  &:not(:last-child) {
    border-bottom: 1px ${theme.palette.ui.neutral.grey4} solid;
  }
`
export const PanelContent = styled.div`
  padding: 0 1rem 1rem;
`
