import React from 'react'
import { render, screen } from '@testing-library/react'
import ListCollapsibleItem from './'
import Collapse from '../../atoms/collapse'

describe('<ListCollapsibleItem/>', () => {
  it('should render correctly expanded with props', () => {
    const { container } = render(
      <Collapse defaultExpandedSections={['section-id']}>
        <ListCollapsibleItem id={'section-id'} title={'Header title'}>
          List item content
        </ListCollapsibleItem>
      </Collapse>
    )
    expect(screen.getByText('List item content')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
  it('should render correctly collapsed with props', () => {
    const { container } = render(
      <Collapse>
        <ListCollapsibleItem id={'section-id'} title={'Header title'}>
          List item content
        </ListCollapsibleItem>
      </Collapse>
    )
    expect(screen.queryByText('List item content')).not.toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
