import React from 'react'
import { Icon } from '../../atoms/icon/Icon'
import PropTypes from 'prop-types'
import { HeaderRoot, PanelContent, PanelRoot } from './styles'
import Collapse from '../../atoms/collapse'

const Header = ({ isExpanded, children }) => {
  return (
    <HeaderRoot>
      <span>{children}</span>
      {isExpanded ? (
        <Icon iconName={'UpArrow'} />
      ) : (
        <Icon iconName={'DownArrow'} />
      )}
    </HeaderRoot>
  )
}
Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  isExpanded: PropTypes.bool,
}

const ListCollapsibleItem = ({ id, children, title, ...otherProps }) => {
  return (
    <PanelRoot header={<Header>{title}</Header>} id={id} {...otherProps}>
      <PanelContent>{children}</PanelContent>
    </PanelRoot>
  )
}
ListCollapsibleItem.propTypes = {
  ...Collapse.Panel.propTypes,
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
export default ListCollapsibleItem
