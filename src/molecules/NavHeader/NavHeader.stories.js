import React from 'react'
import { NavHeader } from './NavHeader'

export default {
  title: 'Archived/Nav Header',
  component: NavHeader,
  parameters: { actions: { argTypesRegex: '^on.*' } },
  argTypes: {
    orientation: {
      showCloseIcon: {
        type: 'boolean',
      },
    },
  },
  args: {},
}

const Template = args => <NavHeader {...args} />

export const DefaultNavHeader = Template.bind({})
