import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { NavHeader } from './NavHeader'
import 'jest-styled-components'
import 'regenerator-runtime/runtime'

afterEach(cleanup)
const tileProps = {
  showCloseIcon: true,
  onClickIcon: jest.fn(),
  onClose: jest.fn(),
  iconName: 'SkuLookUp',
  label: 'SKU Look Up',
  iconSize: 50,
}
describe('<NavHeader/>', () => {
  it('render NavHeader component correctly', () => {
    const { container } = render(<NavHeader {...tileProps} />)
    expect(container).toMatchSnapshot()
  })
})
