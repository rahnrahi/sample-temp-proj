import styled from 'styled-components'
import { theme } from '../../shared'

export const NavHeaderStyle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  .left {
    display: flex;
    align-items: center;
    cursor: pointer;
    svg {
      width: ${({ iconSize }) => iconSize}px;
      height: ${({ iconSize }) => iconSize}px;
    }
    label {
      margin-left: 5px;
      margin-right: 24px;
      ${theme.typography.body};
      color: ${theme.palette.brand.primary.charcoal};
    }
  }
`
