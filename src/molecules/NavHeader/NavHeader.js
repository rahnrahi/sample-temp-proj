import React from 'react'
import PropTypes from 'prop-types'
import { Icon, theme } from '../../atoms'
import { NavHeaderStyle } from './NavHeader.styles'

export const NavHeader = ({
  showCloseIcon,
  onClickIcon,
  iconName,
  label,
  onClose,
  iconSize,
}) => {
  return (
    <NavHeaderStyle iconSize={iconSize} data-testid={`nav-header-${label}`}>
      <div
        className='left'
        onClick={onClickIcon}
        data-testid={`nav-header-action-${iconName}`}
      >
        <Icon iconName={iconName} size={iconSize} />
        <label>{label}</label>
      </div>
      {showCloseIcon && (
        <Icon
          iconName='CircleClose'
          size={40}
          fill={theme.palette.brand.primary.white}
          onClick={onClose}
        />
      )}
    </NavHeaderStyle>
  )
}
NavHeader.defaultProps = {
  showCloseIcon: true,
  iconSize: 50,
}
NavHeader.propTypes = {
  showCloseIcon: PropTypes.bool,
  onClickIcon: PropTypes.func,
  iconName: PropTypes.string,
  label: PropTypes.string,
  onClose: PropTypes.func,
  iconSize: PropTypes.number,
}
