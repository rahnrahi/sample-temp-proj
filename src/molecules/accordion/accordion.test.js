import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { Accordion } from './accordion'
import { Pill } from '../../'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

const propsDefault = {
  collapsed: true,
  render: () => (
    <Pill
      text='DRAFT'
      color='#D5D5D5'
      variant='warning'
      className='pill-draft mt-12 ml-20'
    />
  ),
  onClick: (_, state) => console.log(state),
  title: 'Accordion',
  count: 5,
  icon: 'DownCaret',
  buttonProps: {
    onClick: () => console.log('onclick'),
    text: 'Edit',
  },
}

describe('<Accordion/>', () => {
  it('renders correctly', () => {
    const { container, getByText } = render(<Accordion {...propsDefault} />)
    expect(container).toMatchSnapshot()
    const wrapper = container.querySelector('.collapse-content')
    fireEvent.click(getByText('Edit'))
    wrapper.classList.contains('expanded')
  })
})
