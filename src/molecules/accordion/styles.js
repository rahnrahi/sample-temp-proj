import styled, { css } from 'styled-components'
import { theme } from '../../shared'

const StyledDataSet = css`
  .dataset-wrapper {
    .ds {
      &-row {
        display: flex;
      }
      &-col {
        flex: 1;
      }
      &-count {
        ${theme.typography.h6}
        color: ${theme.palette.brand.primary.gray};
      }
    }
  }
  .dataset {
    color: ${theme.palette.brand.primary.charcoal};

    &-title {
      padding: 16px 24px;
      box-sizing: border-box;
      ${theme.typography.h5}
    }
    &-row {
      border-bottom: 1px solid ${theme.palette.ui.neutral.grey5};
      &:first-child {
        border-top: 1px solid ${theme.palette.ui.neutral.grey5};
      }
      &.blbr {
        border-left: 1px solid ${theme.palette.ui.neutral.grey5};
        border-right: 1px solid ${theme.palette.ui.neutral.grey5};
      }
    }
    &-key,
    &-value {
      ${theme.typography.h6}
      color: ${theme.palette.brand.primary.gray};
      padding: 12px 12px 12px 24px;
      flex: 1;
    }
    &-value {
      padding: 12px 78px 12px 12px;
      text-align: right;
      color: ${theme.palette.brand.primary.charcoal};
    }
  }
`

export const StyledAccordion = styled.div`
  width: 100%;
  border: 1px solid ${theme.palette.ui.neutral.grey5};
  margin-bottom: 16px;

  &:last-child {
    margin-bottom: 0px;
  }

  border-radius: 4px;

  .collapse-card-header {
    min-height: 75px;
    .right {
      padding: 24px;
      cursor: pointer;
      display: flex;
      flex: 1;
      justify-content: flex-start;
      .card-icon {
        display: flex;
        align-items: center;
        .icon {
          transform: rotate(0deg);
          transition: transform 0.4s;
        }
        .icon.rotate {
          transform: rotate(180deg);
        }
      }
      .card-title {
        font-family: ${theme.typography.caption.fontFamily};
        font-size: 18px;
        line-height: 21px;
        margin-left: 5px;
        display: flex;
        align-items: center;

        span {
          ${theme.typography.h6}
          color:${theme.palette.brand.primary.gray};
          margin-left: 9px;
          line-height: initial;
        }
      }
    }
    .left {
      display: flex;
      justify-content: flex-end;
      padding: 24px;
    }
  }

  .collapse-button {
    display: block;
    width: 100%;
  }

  .collapse-content.collapsed {
    display: none;
  }

  .collapsed-content.expanded {
    display: block;
  }

  .renderer {
    padding-top: 12px;
  }

  &.with-icon {
    .renderer {
      padding-left: 20px;
    }
  }

  ${StyledDataSet}
`

export const StyledDataCount = styled.span`
  ${theme.typography.h6}
  color: ${theme.palette.brand.primary.gray};
`

export const Grid = styled.div``
export const Row = styled.div`
  display: flex;
  ${({ justifyContent }) =>
    justifyContent && `justify-content: ${justifyContent};`}
  ${({ alignItems }) => alignItems && `align-items: ${alignItems}; `}
  ${({ flexDirection }) => flexDirection && `flex-direction: ${flexDirection};`}
`

export const Col = styled.div`
  ${({ flex }) => flex && `flex: ${flex};`}
`
