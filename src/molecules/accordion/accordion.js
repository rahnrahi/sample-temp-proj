import React from 'react'
import { Icon, Button } from '../../index'
import cx from 'classnames'
import PropTypes from 'prop-types'
import { StyledAccordion, Row, Col } from './styles'

export const Accordion = ({
  collapsed,
  children,
  title,
  count,
  onClick,
  buttonProps,
  icon,
  render,
}) => {
  const [isCollapsed, setIsCollapsed] = React.useState(collapsed)

  const setIsCollapsedWrapper = (e, state) => {
    e.preventDefault()
    setIsCollapsed(!isCollapsed)
    if (typeof onClick === 'function') onClick(e, state)
  }

  return (
    <StyledAccordion
      className={cx({
        'with-render': render,
        'with-icon': icon,
      })}
    >
      <Row className='collapse-card-header'>
        <Col
          className='right'
          onClick={e => setIsCollapsedWrapper(e, isCollapsed)}
        >
          <Row justifyContent='center' flexDirection='column'>
            <Row>
              {icon && (
                <Col className='card-icon'>
                  <Icon
                    iconName={icon}
                    size={16}
                    className={isCollapsed ? 'icon' : 'icon rotate'}
                  />
                </Col>
              )}
              <Col className='card-title'>
                {title} {count && <span>({count})</span>}
              </Col>
            </Row>
            {render && <Row className='renderer'>{render()}</Row>}
          </Row>
        </Col>
        {buttonProps && (
          <Col className='left'>
            <Button isPrimary={false} size='small' {...buttonProps} />
          </Col>
        )}
      </Row>
      <div
        className={`collapse-content ${isCollapsed ? 'collapsed' : 'expanded'}`}
        aria-expanded={isCollapsed}
      >
        {children && children}
      </div>
    </StyledAccordion>
  )
}

Accordion.defaultProps = {
  collapsed: true,
}

Accordion.propTypes = {
  collapsed: PropTypes.bool,
  children: PropTypes.element,
  title: PropTypes.string,
  count: PropTypes.number,
  onClick: PropTypes.func,
  buttonProps: PropTypes.object,
  icon: PropTypes.string,
  /** render: is used to add additional content below accordion title eg: Pill */
  render: PropTypes.func,
}
