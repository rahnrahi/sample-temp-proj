import React from 'react'
import { Accordion } from './accordion'
import { Pill } from '../../index'
import CardsDocs from '../../../docs/Cards.mdx'
import {
  CARD_ACCORDIAN_DESIGN,
  CARD_ACCORDIAN_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Cards/Accordion',
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => (
  <Accordion {...args}>
    <div className='dataset-wrapper'>
      <div className='dataset-title ds-grid'>
        Initial product setup <span className='ds-count'>(5)</span>
      </div>
      <div>
        <div className='dataset-row ds-row'>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Vendor</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Manufacture type</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
        </div>
        <div className='dataset-row ds-row'>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Vendor</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Manufacture type</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className='mt-16 mb-48 dataset-wrapper'>
      <div className='dataset-title'>
        Product setup <span className='ds-count'>(3)</span>
      </div>
      <div>
        <div className='dataset-row ds-row'>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Vendor</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
          <div flex='1' className='ds-col'>
            <div className='ds-row'>
              <div className='dataset-key'>Manufacture type</div>
              <div className='dataset-value'>Value</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Accordion>
)

export const Primary = Template.bind({})
Primary.args = {
  collapsed: true,
  render: () => (
    <Pill
      text='DRAFT'
      color='#D5D5D5'
      variant='warning'
      className='pill-draft mt-12 ml-20'
    />
  ),
  onClick: (_, state) => console.log(state),
  title: 'Accordion',
  count: 5,
  icon: 'DownCaret',
  buttonProps: {
    onClick: () => console.log('onclick'),
    text: 'Edit',
  },
}
Primary.parameters = getDesignSpecifications(
  CARD_ACCORDIAN_DESIGN,
  CARD_ACCORDIAN_PRESENTATION
)
