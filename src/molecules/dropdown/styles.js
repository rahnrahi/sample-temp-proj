import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledDropdownWrapper = styled.div`
  width: ${({ width, isDefault }) => {
    if (width) return width
    return isDefault ? `346px` : `auto`
  }};
  outline: none;
  .dropdown-header-filled {
    padding: 8px 0 17px 0;
  }
  .dropdown-header-empty {
    padding: 0px;
  }
  :hover {
    .dropdown-header {
      ${({ error }) =>
        error
          ? `box-shadow: inset 0 -1px 0 ${theme.palette.ui.cta.red}`
          : `box-shadow: inset 0 -1px 0 ${theme.palette.brand.primary.charcoal};`};

      ${({ disabled }) =>
        disabled &&
        `box-shadow: inset 0 -1px 0 ${theme.palette.ui.neutral.grey7};`};
    }
  }
  :focus {
    .dropdown-header {
      ${({ error }) =>
        error
          ? `box-shadow: inset 0 -1px 0 ${theme.palette.ui.cta.red}`
          : `box-shadow: inset 0 -1px 0 ${theme.palette.ui.cta.blue};`};

      ${({ disabled }) =>
        disabled &&
        `box-shadow: inset 0 -1px 0 ${theme.palette.ui.neutral.grey7};`};
    }
  }
`

export const DropDownContainer = styled('div')`
  display: inline-block;
  width: ${({ width, isDefault }) => {
    if (width) return width
    return isDefault ? `346px` : `auto`
  }};
  position: relative;
  font-family: ${theme.typography.caption.fontFamily};
  ${({ disabled }) => disabled && `pointer-events: none`};

  .label-row {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 15px;
  }
  .label-row-empty {
    height: 15px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-top: 25px;
    padding-bottom: 16px;
  }
`
export const StyledDropdownLabel = styled('div')`
  .dd-lable {
    ${theme.typography.h6.css};
    color: ${({ error }) =>
      error ? theme.palette.ui.cta.red : theme.palette.brand.primary.gray};
    ${({ disabled }) =>
      disabled &&
      `
            color: ${theme.palette.ui.neutral.grey7};
            pointer-events: none;
    `}
  }
`
export const StyledDropdownHeader = styled('div')`
  text-align: center;
  align-items: center;
  box-shadow: inset 0 -1px 0 ${theme.palette.brand.primary.gray};
  ${({ errorState }) =>
    errorState && `box-shadow: inset 0 -1px 0 ${theme.palette.ui.cta.red}`};
  ${({ disabled }) =>
    disabled &&
    `
      box-shadow: inset 0 -1px 0 ${theme.palette.ui.neutral.grey7};
      pointer-events: none;
    `};
  display: flex;
  justify-content: center;
  box-sizing: border-box;

  &,
  * {
    cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer !important')};
  }

  .content-wrapper {
    .icon {
      right: 0;
      transform-origin: 50% 50%;
      transition: all 0.2s ease;
      svg {
        fill: ${({ disabled }) =>
          disabled
            ? theme.palette.ui.neutral.grey7
            : theme.palette.brand.primary.gray};
      }
    }
    flex: 1;
    display: flex;
    justify-content: left;
    align-items: baseline;
    label {
      display: inline-block;
      ${props =>
        props.isDefault ? `width:100%` : `width:auto;margin-right:10px`};
      text-align: left;
      color: ${theme.palette.brand.primary.charcoal};
    }
    .option-label {
      font-family: ${theme.typography.subtitle1.fontFamily};
      font-size: 13px;
      line-height: 16px;
      letter-spacing: 0.02em;
      ${({ disabled }) =>
        disabled && `color: ${theme.palette.ui.neutral.grey7}`};
    }
  }
`
export const DropDownListContainer = styled('div')`
  position: absolute;
  font-size: ${theme.typography.h6.fontSize};
  line-height: ${theme.typography.h6.lineHeight};
  letter-spacing: ${theme.typography.h6.letterSpacing};
  cursor: pointer;
  width: ${({ isDefault }) => (isDefault ? 'inherit' : '200px')};
  z-index: 3;
  ${({ isDefault, placement }) => {
    if (isDefault || !placement) return
    if (placement == 'right') return 'right: 0px'
    return placement === 'left' && 'left: 0px'
  }};
`
export const DropDownList = styled('ul')`
  ::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 4px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${theme.palette.brand.primary.gray};
    box-shadow: 0 0 0.0625rem rgba(255, 255, 255, 0.5);
  }
  padding: 0;
  margin: 0;
  max-height: 190px;
  overflow-y: auto;
  box-sizing: border-box;
  ${props =>
    props.isDefault
      ? ` box-shadow: 0 5px 20px rgba(115, 127, 143, 0.1);`
      : ` border: 1px solid ${theme.palette.ui.neutral.grey2};
      box-shadow: 0px 6px 16px rgba(115, 127, 143, 0.16);
      `};
  border-radius: 4px;
  background: ${theme.palette.brand.primary.white};
  .active {
    background: ${theme.palette.ui.neutral.grey5};
  }
  .selected {
    background: ${theme.palette.ui.neutral.grey5};
  }
`
export const ListItem = styled('li')`
  text-align: left;
  list-style: none;
  cursor: pointer;
  color: ${theme.palette.brand.primary.charcoal};
  padding: 14px 20px 14px 24px;

  &:hover {
    background-color: ${theme.palette.ui.neutral.grey5};
  }
  :first-child {
    padding-top: 14px;
    padding-bottom: 14px;
  }
  :last-child {
    padding-top: 14px;
    padding-bottom: 14px;
  }
`

export const StyledErrorMessage = styled.div`
  ${theme.typography.caption.css};
  color: ${theme.palette.ui.cta.red};
  margin: 4px 0px 0px 1px;
`
