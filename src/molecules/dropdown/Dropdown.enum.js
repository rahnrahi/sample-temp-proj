export const DropdownType = Object.freeze({
  SMALL: 'small',
  LARGE: 'large',
})
