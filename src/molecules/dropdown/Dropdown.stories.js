import React from 'react'
import { dropdownPropsWithTrue } from './mock'
import { Dropdown } from './Dropdown'
import DropdownDocs from '../../../docs/Dropdowns.mdx'
import {
  DROPDOWN_SINGLE_DESIGN,
  DROPDOWN_SINGLE_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Dropdowns/Single Select',
  component: Dropdown,
  decorators: [
    Story => <div style={{ height: '300px', width: '100%' }}>{Story()}</div>,
  ],
  argTypes: {
    titleLabel: { control: 'text' },
    className: { control: 'text' },
    width: { control: 'text' },
  },
  parameters: {
    docs: {
      page: DropdownDocs,
    },
  },
}

const Template = args => <Dropdown {...args} />

export const Large = Template.bind({})

Large.args = dropdownPropsWithTrue
Large.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: DROPDOWN_SINGLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: DROPDOWN_SINGLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
