import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import {
  dropdownPropsWithTrue,
  dropdownPropsWithFalse,
  dropdownWithErrorMessage,
  dropdownDisabled,
} from './mock'
import { Dropdown } from './Dropdown'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 1234),
  }
})

afterEach(cleanup)

describe('<Dropdown/>', () => {
  it('renders correctly', () => {
    const { container } = render(<Dropdown {...dropdownPropsWithTrue} />)
    expect(container).toMatchSnapshot()
  })
  it('renders correctly when isDefault is false', () => {
    const { container } = render(<Dropdown {...dropdownPropsWithFalse} />)
    expect(container).toMatchSnapshot()
  })

  it('check for Dropdown options length', async () => {
    const { container, getByText } = render(
      <Dropdown {...dropdownPropsWithTrue} />
    )
    const dropdown = container.querySelector('.content-wrapper')
    const selectedOption = container.querySelector('.option-label')
    fireEvent.click(dropdown)
    expect(getByText('List Item 1')).toBeInTheDocument()
    fireEvent.click(getByText('List Item 1'))
    expect(selectedOption).toHaveTextContent('List Item 1')
    expect(container).toMatchSnapshot()
  })

  it('render correctly with error enabled and error message', () => {
    const { container } = render(<Dropdown {...dropdownWithErrorMessage} />)
    expect(container).toMatchSnapshot()
  })

  it('render disabled dropdown', () => {
    const { container } = render(<Dropdown {...dropdownDisabled} />)
    expect(container).toMatchSnapshot()
  })
})
