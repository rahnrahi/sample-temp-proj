import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { Icon } from '../../atoms'
import { useClickOutside } from './helper'
import useKeyPress from '../../hooks/key-press'
import {
  StyledDropdownHeader,
  StyledDropdownLabel,
  DropDownListContainer,
  DropDownList,
  DropDownContainer,
  ListItem,
  StyledErrorMessage,
  StyledDropdownWrapper,
} from './styles'
import isEmpty from 'lodash/isEmpty'
import { Tooltip } from '../../atoms/tooltip/Tooltip'
import { DropdownType } from './Dropdown.enum'

const DropdownError = ({ errorState, errorMessage }) =>
  errorState ? (
    <StyledErrorMessage id='dropdown-error'>{errorMessage}</StyledErrorMessage>
  ) : null

const DownArrowIcon = () => (
  <div className='icon'>
    <Icon
      iconName={'DownArrow'}
      size={16}
      data-testid='dropdown-trigger-icon'
    />
  </div>
)

const ContentWrapper = ({ selectedOption }) => (
  <div className='content-wrapper'>
    <SelectedOptionLabel selectedOption={selectedOption} />
    <DownArrowIcon />
  </div>
)

const SelectedOptionLabel = ({ selectedOption }) => (
  <label className='option-label'>
    {(!isEmpty(selectedOption) && selectedOption.name) || ''}
  </label>
)

const DropdownLabel = ({
  type,
  selectedOption,
  disabled,
  errorState,
  titleLabel,
}) =>
  type == DropdownType.LARGE ? (
    <div className={selectedOption ? 'label-row' : 'label-row-empty'}>
      <StyledDropdownLabel disabled={disabled} error={errorState}>
        <label className='dd-lable'>{titleLabel}</label>
      </StyledDropdownLabel>
    </div>
  ) : null

const DropdownTooltip = ({ selectedOption, type, isOpen, titleLabel }) =>
  !isEmpty(selectedOption) && type === DropdownType.SMALL ? (
    <Tooltip size={DropdownType.SMALL} position={'bottom'}>
      {!isOpen ? titleLabel : ''}
    </Tooltip>
  ) : null

const DropdownList = ({
  type,
  optionsPlacement,
  customOptionsView,
  options,
  isOpen,
  setIsOpen,
  setSelectedOption,
  onSelect,
}) => {
  const downPress = useKeyPress('ArrowDown')
  const upPress = useKeyPress('ArrowUp')
  const enterPress = useKeyPress('Enter')
  const [cursor, setCursor] = useState(-1)
  useEffect(() => {
    if (options.length && isOpen && downPress) {
      setCursor(prevState => Math.min(prevState + 1, options.length - 1))
    }
  }, [downPress, isOpen, options.length])

  useEffect(() => {
    if (options.length && isOpen && upPress) {
      setCursor(prevState => Math.max(0, prevState - 1))
    }
  }, [upPress, isOpen, options.length])

  useEffect(() => {
    if (options.length && enterPress && cursor > -1) {
      setSelectedOption(options[cursor])
      setIsOpen(false)
    }
  }, [cursor, enterPress, options, setIsOpen, setSelectedOption])

  const onOptionClicked = (value, index) => () => {
    setSelectedOption(value)
    setIsOpen(false)
    setCursor(index)
    if (typeof onSelect === 'function') onSelect(value)
  }

  return (
    <DropDownListContainer
      isDefault={type === DropdownType.LARGE}
      placement={optionsPlacement}
      data-testid='dropdown-list-container'
    >
      <DropDownList
        data-testid='dropdown-list-items'
        isDefault={type === DropdownType.LARGE}
      >
        {React.isValidElement(customOptionsView)
          ? customOptionsView
          : options.map((option, index) => (
              <ListItem
                onClick={onOptionClicked(option, index)}
                key={`dropdown_list_item_${index}`}
                className={index === cursor && 'active'}
                data-testid={`dropdown-list-item-${index}`}
              >
                {option.name}
              </ListItem>
            ))}
      </DropDownList>
    </DropDownListContainer>
  )
}

export const Dropdown = React.forwardRef(
  (
    {
      type,
      options = [],
      value,
      titleLabel,
      onSelect,
      customOptionsView,
      isOptionsVisible,
      errorState,
      errorMessage,
      disabled,
      optionsPlacement,
      ...props
    },
    ref
  ) => {
    const [isOpen, setIsOpen] = useState(false)
    const [selectedOption, setSelectedOption] = useState(value)
    const clickRef = useRef()

    useEffect(() => {
      setIsOpen(isOptionsVisible)
    }, [isOptionsVisible])

    useEffect(() => {
      setSelectedOption(value)
    }, [value])

    useClickOutside(
      clickRef,
      () => {
        setIsOpen(false)
      },
      isOpen
    )
    const ariaProps = {}
    const restProps = {}

    const toggling = () => setIsOpen(!isOpen)

    for (const [key, value] of Object.entries({ ...props })) {
      if (key.startsWith('aria')) ariaProps[key] = value
      else restProps[key] = value
    }

    return (
      <StyledDropdownWrapper
        ref={ref}
        tabIndex={0}
        {...ariaProps}
        error={errorState}
        width={restProps.width}
        isDefault={type === DropdownType.LARGE}
        disabled={disabled}
      >
        <DropDownContainer
          {...restProps}
          isDefault={type === DropdownType.LARGE}
          ref={clickRef}
          disabled={disabled}
          isSelected={!isEmpty(selectedOption) && selectedOption.name}
        >
          <DropdownLabel
            type={type}
            selectedOption={selectedOption}
            disabled={disabled}
            errorState={errorState}
            titleLabel={titleLabel}
          />

          <StyledDropdownHeader
            className={
              selectedOption
                ? 'dropdown-header dropdown-header-filled'
                : 'dropdown-header-empty'
            }
            onClick={toggling}
            isDefault={type === DropdownType.LARGE}
            errorState={errorState}
            disabled={disabled}
            type={type}
          >
            {' '}
            <ContentWrapper selectedOption={selectedOption} />
            <DropdownTooltip
              type={type}
              selectedOption={selectedOption}
              isOpen={isOpen}
              titleLabel={titleLabel}
            />
          </StyledDropdownHeader>

          {isOpen && (
            <DropdownList
              type={type}
              optionsPlacement={optionsPlacement}
              customOptionsView={customOptionsView}
              options={options}
              onSelect={onSelect}
              isOpen={isOpen}
              setIsOpen={setIsOpen}
              setSelectedOption={setSelectedOption}
            />
          )}

          <DropdownError errorState={errorState} errorMessage={errorMessage} />
        </DropDownContainer>
      </StyledDropdownWrapper>
    )
  }
)

SelectedOptionLabel.propTypes = {
  selectedOption: PropTypes.object,
}

DropdownList.propTypes = {
  type: PropTypes.oneOf(['small', 'large']),
  customOptionsView: PropTypes.element,
  optionsPlacement: PropTypes.oneOf(['left', 'right']),
  options: PropTypes.array.isRequired,
  onSelect: PropTypes.func,
  isOpen: PropTypes.bool,
  setIsOpen: PropTypes.func,
  setSelectedOption: PropTypes.func,
}

DropdownError.propTypes = {
  errorState: PropTypes.bool,
  errorMessage: PropTypes.string,
}

ContentWrapper.propTypes = {
  selectedOption: PropTypes.object,
}

DropdownLabel.propTypes = {
  type: PropTypes.oneOf(['small', 'large']),
  selectedOption: PropTypes.object,
  disabled: PropTypes.bool,
  errorState: PropTypes.bool,
  titleLabel: PropTypes.string,
}

DropdownTooltip.propTypes = {
  isOpen: PropTypes.bool,
  type: PropTypes.oneOf(['small', 'large']),
  titleLabel: PropTypes.string,
  selectedOption: PropTypes.object,
}

Dropdown.propTypes = {
  type: PropTypes.oneOf(['small', 'large']),
  titleLabel: PropTypes.string,
  className: PropTypes.string,
  options: PropTypes.array.isRequired,
  value: PropTypes.object,
  onSelect: PropTypes.func,
  /** The Custom component for custom dropdown options*/
  customOptionsView: PropTypes.element,
  isOptionsVisible: PropTypes.bool,
  /** Error state */
  errorState: PropTypes.bool,
  /** Error message */
  errorMessage: PropTypes.string,
  /** Disabled state */
  disabled: PropTypes.bool,
  /** Width of the dropdown component (e.g 484px) */
  width: PropTypes.oneOfType([PropTypes.string]),
  /** Options dropdown position [Only applicable in small dropdown]*/
  optionsPlacement: PropTypes.oneOf(['left', 'right']),
}

Dropdown.defaultProps = {
  type: 'large',
  isOptionsVisible: false,
  optionsPlacement: 'left',
}
