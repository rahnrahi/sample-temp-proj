import { useEffect } from 'react'

export const useClickOutside = (ref, callback, isOpen) => {
  const handleClick = e => {
    if (ref.current && !ref.current.contains(e.target)) {
      callback()
    }
  }
  useEffect(() => {
    if (isOpen) {
      document.body.addEventListener('click', handleClick)
      return () => {
        document.body.removeEventListener('click', handleClick)
      }
    }
  }, [isOpen])
}
