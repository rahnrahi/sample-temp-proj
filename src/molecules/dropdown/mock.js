export const options = [
  { id: 1, name: 'List Item 1' },
  { id: 2, name: 'List Item 2' },
  { id: 3, name: 'List Item 3' },
  { id: 4, name: 'List Item 4' },
  { id: 5, name: 'List Item 5' },
  { id: 6, name: 'List Item 6' },
  { id: 7, name: 'List Item 7' },
  { id: 8, name: 'List Item 8' },
  { id: 9, name: 'List Item 9' },
  { id: 10, name: 'List Item 10' },
]
export const dropdownPropsWithTrue = {
  type: 'large',
  options: [...options],
  titleLabel: 'Field Label',
  value: { id: 0, name: 'Menu Title' },
}
export const dropdownPropsWithFalse = {
  type: 'small',
  options: [...options],
  isOptionsVisible: false,
  titleLabel: 'Field Label',
  value: { id: 0, name: 'Menu Title' },
}

export const dropdownWithErrorMessage = {
  type: 'large',
  option: [...options],
  titleLable: 'Field Label',
  value: { id: 0, name: 'Menu Title' },
  errorState: true,
  errorMessage: 'Error Message',
}

export const dropdownDisabled = {
  type: 'large',
  option: [...options],
  titleLable: 'Field Label',
  value: { id: 0, name: 'Menu Title' },
  disabled: true,
}
