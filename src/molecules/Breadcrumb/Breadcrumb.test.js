import React from 'react'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import { Breadcrumb } from './Breadcrumb'

afterEach(cleanup)
beforeEach(() => {
  jest.clearAllMocks()
})

describe('<Breadcrumb />', () => {
  const onLabelClick = jest.fn(() => {})
  const backCaretHandler = jest.fn(() => {})
  const props = {
    data: [
      { label: 'label1', onLabelClick },
      { label: 'label2' },
      { label: 'label3' },
      { label: 'label4', onLabelClick },
      { label: 'label5' },
      { label: 'label6' },
      { label: 'label7', onLabelClick },
    ],
    backCaretHandler,
  }

  it('renders Breadcrumb component', () => {
    render(<Breadcrumb {...props} />)
    expect(screen.getByTestId('breadcrumb-div')).toBeInTheDocument()
  })

  it('checks for click on back caret icon', () => {
    render(<Breadcrumb {...props} />)
    let backCaretDiv = screen.getByTestId('backcaret-div')
    expect(backCaretDiv).toBeInTheDocument()

    fireEvent(
      backCaretDiv,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      })
    )
    expect(backCaretHandler).toHaveBeenCalledWith('label6')
  })

  it('checks when only one data is passed', () => {
    props.data = [{ label: 'label1', onLabelClick }]
    render(<Breadcrumb {...props} />)
    let backCaretDiv = screen.getByTestId('backcaret-div')
    expect(backCaretDiv).toBeInTheDocument()

    fireEvent(
      backCaretDiv,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      })
    )
    expect(backCaretHandler).toHaveBeenCalledWith('label1')
  })

  it('checks when no data is passed', () => {
    props.data = []
    render(<Breadcrumb {...props} />)
    expect(screen.getByTestId('breadcrumb-div').firstElementChild).toBeNull()
  })
})
