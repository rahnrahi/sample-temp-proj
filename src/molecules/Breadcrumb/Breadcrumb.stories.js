import React from 'react'
import { Breadcrumb } from './Breadcrumb'
import BreadcrumbsDocs from '../../../docs/Breadcrumbs.mdx'
import {
  BREADCRUMB_DESIGN,
  BREADCRUMB_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Modules/Breadcrumb',
  component: Breadcrumb,
  argTypes: {
    data: [],
    backCaretHandler: () => {
      // This is intentional
    },
  },
  parameters: {
    docs: {
      page: BreadcrumbsDocs,
    },
  },
}

const Template = args => <Breadcrumb {...args} />
export const BreadcrumbComponent = Template.bind({})
BreadcrumbComponent.args = {
  data: [
    {
      label: 'label1',
      onLabelClick: () => {
        // This is intentional
      },
    },
    { label: 'label2' },
    { label: 'label3' },
    {
      label: 'label4',
      onLabelClick: () => {
        // This is intentional
      },
    },
    { label: 'label5' },
    { label: 'label6' },
    {
      label: 'label7',
      onLabelClick: () => {
        // This is intentional
      },
    },
  ],
  backCaretHandler: () => {
    // This is intentional
  },
}
BreadcrumbComponent.storyName = 'Breadcrumb'
BreadcrumbComponent.parameters = getDesignSpecifications(
  BREADCRUMB_DESIGN,
  BREADCRUMB_PRESENTATION
)
