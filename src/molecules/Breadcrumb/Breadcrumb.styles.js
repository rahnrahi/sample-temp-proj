import styled from 'styled-components'

export const StyledBreadCrumbDiv = styled.div`
  display: flex;
`

export const StyledBackCaretDiv = styled.div`
  margin-right: 2px;
  padding: 2px;
`

export const StyledBreadCrumbLabelContainerDiv = styled.div`
  display: flex;
  margin: 2px 3px;
`
