import React from 'react'
import { LeftArrow } from '../../assets/images'
import {
  StyledBreadCrumbDiv,
  StyledBackCaretDiv,
  StyledBreadCrumbLabelContainerDiv,
} from './Breadcrumb.styles'
import PropTypes from 'prop-types'
import { BreadcrumbNavLink } from '../../atoms'

export const Breadcrumb = ({ data, backCaretHandler }) => {
  return (
    <StyledBreadCrumbDiv data-testid='breadcrumb-div'>
      {data.length > 0 && (
        <StyledBackCaretDiv
          onClick={() =>
            /**
             * Teritiary operator is for getting the second last nav link
             * in case there are more than one nav links.
             * If there is only one, get the only link.
             * */
            backCaretHandler(
              data.length > 1 ? data[data.length - 2].label : data[0].label
            )
          }
          data-testid='backcaret-div'
        >
          <LeftArrow />
        </StyledBackCaretDiv>
      )}
      {data.length > 0 &&
        data.map((item, id) => {
          return (
            <StyledBreadCrumbLabelContainerDiv key={item.label + id}>
              <BreadcrumbNavLink
                label={item.label}
                onClick={item.onLabelClick}
                data-testid='breadcrumb-nav'
              />
              <div>{id !== data.length - 1 && '/'}</div>
            </StyledBreadCrumbLabelContainerDiv>
          )
        })}
    </StyledBreadCrumbDiv>
  )
}

Breadcrumb.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      onLabelClick: PropTypes.func,
    })
  ).isRequired,
  /** This function would be called onClick of the back caret icon*/
  backCaretHandler: PropTypes.func.isRequired,
}
