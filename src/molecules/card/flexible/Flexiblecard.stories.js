import React from 'react'
import { FlexibleCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  EXPANDABLE_CARD_IMAGE_DESIGN,
  EXPANDABLE_CARD_IMAGE_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Expandable/with image',
  component: FlexibleCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <FlexibleCard {...args} />

export const simpleFlexibleCard = Template.bind({})
simpleFlexibleCard.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_IMAGE_DESIGN,
  EXPANDABLE_CARD_IMAGE_PRESENTATION
)
simpleFlexibleCard.args = {
  title: 'Payment Details',
  data: [
    // First nested card
    {
      imageUrl:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/1000px-Mastercard-logo.svg.png',
      keyValues: { Mastercard: '', ending: '3746', expiration: '02/23' },
      hideShowNumber: 1,
    },
    // Second nested card
    {
      imageUrl: 'http://pngimg.com/uploads/visa/visa_PNG28.png',
      keyValues: {
        Visa: '',
        ending: '**** **** **** 3746',
        expiration: '02/23',
      },
      hideShowNumber: 1,
    },
    // Third nested card
    {
      imageUrl: 'https://img.icons8.com/ios/452/gift-card.png',
      keyValues: { 'Gift Card': '', 'ID': '4055180211303' },
      hideShowNumber: 1,
    },
  ],
  width: '337px',
  iconName: 'DownArrow',
  showBorder: true,
  background: '#FFFFFF',
}
