import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { FlexibleCard } from './index'

afterEach(cleanup)

describe('<FlexibleCard/>', () => {
  it('should render Flexible card with 2 nested cards', () => {
    const { container } = render(
      <FlexibleCard
        title='Payment Details'
        data={[
          // First nested card
          {
            title: 'Mastercard',
            imageUrl:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/1000px-Mastercard-logo.svg.png',
            keyValues: { ending: '3746', expiration: '02/23' },
          },
          // Second nested card
          {
            title: 'Visa',
            imageUrl:
              'http://assets.stickpng.com/thumbs/58482363cef1014c0b5e49c1.png',
            keyValues: { ending: '**** **** **** 3746', expiration: '02/23' },
          },
        ]}
      />
    )
    expect(container).toMatchSnapshot()
  })
})
