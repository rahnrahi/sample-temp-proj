import React, { useState } from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  StyledFlexibleCardDiv,
  StyledFlexibleCardTitle,
  StyledInnerCardDiv,
  StyledInnerCardLeftSection,
  StyledInnerCardImg,
  StyledInnerCardMiddleSection,
  StyledInnerCardInfo,
  StyledInnerCardInfoKey,
  StyledInnerCardInfoValue,
  StyledInnerCardRightSection,
} from './style'
import { Icon } from '../../../atoms'

export const FlexibleCard = ({ title, data, width, iconName, ...rest }) => {
  return (
    <StyledFlexibleCardDiv width={width} {...rest}>
      <StyledFlexibleCardTitle>{title}</StyledFlexibleCardTitle>
      {data &&
        data.map((cardData, index) => (
          <InnerCard
            iconName={iconName}
            width={width}
            key={index}
            title={cardData.title}
            imageUrl={cardData.imageUrl}
            keyValues={cardData.keyValues}
            hideShowNumber={cardData.hideShowNumber}
          />
        ))}
    </StyledFlexibleCardDiv>
  )
}

const InnerCard = ({
  imageUrl,
  keyValues,
  hideShowNumber,
  width,
  iconName,
}) => {
  const [expanded, setExpanded] = useState(true)
  return (
    <StyledInnerCardDiv className='flexible-inner-card' width={width}>
      <StyledInnerCardLeftSection showImage={imageUrl}>
        <StyledInnerCardImg src={imageUrl} />
      </StyledInnerCardLeftSection>
      <StyledInnerCardMiddleSection expanded={expanded}>
        {keyValues &&
          Object.keys(keyValues).map((key, index) =>
            keyValues[key] ? (
              <StyledInnerCardInfo
                key={index}
                expanded={expanded}
                className={ClassNames({
                  shouldHideShow: index >= hideShowNumber,
                })}
              >
                <StyledInnerCardInfoKey>{`${key
                  .charAt(0)
                  .toUpperCase()
                  .concat(key.slice(1))}: `}</StyledInnerCardInfoKey>
                <StyledInnerCardInfoValue>
                  {keyValues[key]}
                </StyledInnerCardInfoValue>
              </StyledInnerCardInfo>
            ) : (
              <StyledInnerCardInfo
                key={index}
                expanded={expanded}
                className={ClassNames({
                  shouldHideShow: index >= hideShowNumber,
                })}
              >
                <StyledInnerCardInfoValue>{key}</StyledInnerCardInfoValue>
              </StyledInnerCardInfo>
            )
          )}
      </StyledInnerCardMiddleSection>
      <StyledInnerCardRightSection
        expanded={expanded}
        onClick={() => {
          setExpanded(expanded => !expanded)
        }}
        iconName={iconName}
      >
        <Icon iconName={iconName} />
      </StyledInnerCardRightSection>
    </StyledInnerCardDiv>
  )
}

FlexibleCard.propTypes = {
  /** Main title of card */
  title: PropTypes.string,
  /** Nested cards array. Note:
   * To show something as title only in nested card and not as key value pair
   * then keep value of that key in keyValues as empty. hideShowNumber
   * represents number of lines in card info to show/hide. 0 means all, 1 means
   * skip first and so on etc*/
  data: PropTypes.arrayOf(
    PropTypes.shape({
      imageUrl: PropTypes.string,
      keyValues: PropTypes.object,
      hideShowNumber: PropTypes.number,
    })
  ),
  /** Width of card */
  width: PropTypes.string,
  /** Icon name */
  iconName: PropTypes.string,
  /** Show/hide card's outer border/outline */
  showBorder: PropTypes.bool,
  /** Main card background color */
  background: PropTypes.string,
}

InnerCard.propTypes = {
  /** Image url for card image */
  imageUrl: PropTypes.string,
  /** Key value pairs for card info */
  keyValues: PropTypes.object,
  /** Number of lines in card info to show/hide. 0 means all, 1 means skip
   * first and so on */
  hideShowNumber: PropTypes.number,
  /** Width of card */
  width: PropTypes.string,
  /** Icon name */
  iconName: PropTypes.string,
}
