import styled from 'styled-components'
import { theme } from '../../../shared'
import { hexToRgba } from '../../../shared/utils'

const StyledInnerCardDivBorderColor = hexToRgba(
  theme.palette.brand.primary.gray,
  0.5
)

export const StyledFlexibleCardDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 24px 30px;
  max-width: ${({ width }) => (width ? width : '337px')};
  min-width: 337px;
  border: ${({ showBorder }) =>
    showBorder ? `1px solid ${theme.palette.ui.neutral.grey4}` : 'none'};
  border-radius: 4px;
  ${({ background }) => background && `background: ${background}`}
`

export const StyledFlexibleCardTitle = styled.p`
  ${theme.typography.body.css};
  margin: 0;
`

export const StyledInnerCardDiv = styled.div`
  border-bottom: 1px solid
    rgba(
      ${StyledInnerCardDivBorderColor.red},
      ${StyledInnerCardDivBorderColor.green},
      ${StyledInnerCardDivBorderColor.blue},
      ${StyledInnerCardDivBorderColor.opacity}
    );
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding-top: 17px;
  padding-bottom: 22px;
  width: ${({ width }) => (width ? width : '337px')};
  max-width: ${({ width }) => (width ? width : '337px')};
  min-width: 337px;

  &:last-child {
    border: none;
  }
`

export const StyledInnerCardTitle = styled.div`
  ${theme.typography.h6.css};
  padding-bottom: 3px;
`

export const StyledInnerCardLeftSection = styled.div`
  min-width: 34px;
  display: ${({ showImage }) => (showImage ? 'block' : 'none')};
`

export const StyledInnerCardImg = styled.img`
  width: 34px;
  height: 34px;
  object-fit: contain;
  display: ${({ src }) => (src ? 'block' : 'none')};
  align-items: center;
`

export const StyledInnerCardMiddleSection = styled.div`
  min-width: 228px;
`

export const StyledInnerCardInfo = styled.div`
  @keyframes show {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  ${theme.typography.h6.css};
  padding-bottom: 3px;
  animation: show 0.5s ease-in-out both;

  &.shouldHideShow {
    display: ${({ expanded }) => (expanded ? 'block' : 'none')};
  }

  &:last-child {
    padding-bottom: 0;
  }
`

export const StyledInnerCardInfoKey = styled.span`
  ${theme.typography.h6.css};
  color: ${theme.palette.brand.primary.gray};
`

export const StyledInnerCardInfoValue = styled.span`
  ${theme.typography.h6.css};
`

export const StyledInnerCardRightSection = styled.div`
  display: flex;
  cursor: pointer;
  transition: all 0.3s ease-in-out;
  transform-origin: center center;
  ${({ expanded }) => expanded && `transform: rotateZ(-180deg);`};
`
