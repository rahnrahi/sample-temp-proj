import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  StyledCarouselCard,
  StyledCardContent,
  StyledImg,
  StyledContent,
  StyledTitle,
  StyledSubtitle,
} from './style'

export const CarouselCard = ({
  title,
  subtitle,
  imgUrl,
  imgAltText = 'CardImage',
  ...props
}) => {
  const [selected, setSelected] = useState(false)

  const handleCardSelect = () => {
    setSelected(select => !select)
  }

  return (
    <StyledCarouselCard
      {...props}
      selected={selected}
      onClick={handleCardSelect}
    >
      <StyledCardContent tabIndex='-1'>
        {imgUrl && <StyledImg src={imgUrl} alt={imgAltText} />}
        <StyledContent>
          {title && <StyledTitle>{title}</StyledTitle>}
          {subtitle && <StyledSubtitle>{subtitle}</StyledSubtitle>}
        </StyledContent>
      </StyledCardContent>
    </StyledCarouselCard>
  )
}

CarouselCard.propTypes = {
  /** card title */
  title: PropTypes.string.isRequired,
  /** card subtitle */
  subtitle: PropTypes.string.isRequired,
  /** card image url */
  imgUrl: PropTypes.string.isRequired,
  /** card image alt text */
  imgAltText: PropTypes.string.isRequired,
}
