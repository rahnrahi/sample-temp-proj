import React from 'react'
import { HierarchyTreeCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  CARD_HIERARCHY_DESIGN,
  CARD_HIERARCHY_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Hierarchy tree',
  component: HierarchyTreeCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <HierarchyTreeCard {...args} tabIndex='0' />

export const withArrow = Template.bind({})
withArrow.args = {
  title: 'Fabric.inc',
  showArrow: true,
  isEnabled: false,
  handleOptionClick: () => {},
  customOnClick: () => {
    console.log('Custom on click func called')
  },
  options: [
    {
      name: 'Rename',
      onClickHandler: () => {
        console.log('Rename clicked')
      },
    },
    {
      name: 'Hide',
      onClickHandler: () => {
        console.log('Hide clicked')
      },
    },
    {
      name: 'Duplicate',
      onClickHandler: () => {
        console.log('Duplicate clicked')
      },
    },
  ],
}
withArrow.parameters = getDesignSpecifications(
  CARD_HIERARCHY_DESIGN,
  CARD_HIERARCHY_PRESENTATION
)

export const withoutArrow = Template.bind({})
withoutArrow.args = {
  title: 'Fabric.inc',
  showArrow: false,
  isEnabled: false,
  handleOptionClick: () => {},
}
withoutArrow.parameters = getDesignSpecifications(
  CARD_HIERARCHY_DESIGN,
  CARD_HIERARCHY_PRESENTATION
)

export const WithEnabledState = Template.bind({})
WithEnabledState.args = {
  title: 'Fabric.inc',
  moreOption: true,
  isEnabled: true,
  handleOptionClick: () => {},
}
WithEnabledState.parameters = getDesignSpecifications(
  CARD_HIERARCHY_DESIGN,
  CARD_HIERARCHY_PRESENTATION
)

export const withoutMoreOption = Template.bind({})
withoutMoreOption.args = {
  title: 'Fabric.inc',
  showArrow: false,
  moreOption: false,
  isEnabled: false,
  handleOptionClick: () => {},
}
withoutMoreOption.parameters = getDesignSpecifications(
  CARD_HIERARCHY_DESIGN,
  CARD_HIERARCHY_PRESENTATION
)
