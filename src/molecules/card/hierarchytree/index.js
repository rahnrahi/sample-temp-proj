import React, { useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { BlueCaret, Dots as MoreOption } from '../../../assets/images'
import { v4 as uuidv4 } from 'uuid'
import {
  StyledArrow,
  StyledOption,
  StyledTitle,
  StyledTreeCard,
  StyledEditDiv,
  StyledActionContainer,
} from './style'
import { Flyout } from '../../flyout'
import { ButtonWithIcon, Input } from '../../../atoms'
import useLocaleDirection from '../../../hooks/render-rtl'
import { RTL } from '../../../hooks/constants'

export const HierarchyTreeCard = ({
  title,
  showArrow,
  moreOption,
  isEnabled,
  handleOptionClick,
  customOnClick,
  onSave,
  onCancel,
  options,
  rename,
  placeholder,
  localeCode,
  ...props
}) => {
  const [selected, setSelected] = useState(false)
  const [isHover, setIsHover] = useState(false)
  const [showOptions, setShowOptions] = useState(false)
  const [editMode, setEditMode] = useState(rename)
  const [cardTitle, setCardTitle] = useState(title)
  const [previousTitle, setPreviousTitle] = useState(title)
  const id = useMemo(() => uuidv4(), [])
  const dir = useLocaleDirection(localeCode)
  const isRTL = dir === RTL

  const handleOptionsButtonClick = event => {
    event.stopPropagation()
    handleOptionClick()
    setShowOptions(!showOptions)
  }

  const handleCardSelect = () => {
    setSelected(select => !select)
  }

  const externalOnClickHandler = () => {
    // Call old internal on click function for backward compatibility
    handleCardSelect()

    // Call externally passed onClick method then for extending functionality
    customOnClick()
  }

  const handleOptionSelect = ({ label }) => {
    /*
    	Convert card to edit mode when rename option is clicked otherwise use
    	corresponding user provided option's on click implementation.
    */
    if (label.toUpperCase() === 'RENAME') {
      handleRename()
    }
    const requiredOption = options.find(option => option.name === label)
    if (requiredOption && typeof requiredOption.onClickHandler === 'function') {
      requiredOption.onClickHandler()
    }
    setShowOptions(false)
  }

  const handleRename = () => {
    setEditMode(true)
  }

  const handleSaveEdit = event => {
    event.stopPropagation()
    setEditMode(false)
    setPreviousTitle(cardTitle)
    setShowOptions(false)
    onSave && onSave(cardTitle)
  }

  const handleCancelEdit = event => {
    event.stopPropagation()
    setEditMode(false)
    setCardTitle(previousTitle)
    setShowOptions(false)
    onCancel && onCancel(previousTitle)
  }

  return (
    <StyledTreeCard
      {...props}
      data-testid='tree-card'
      selected={selected}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => {
        setIsHover(false)
        setShowOptions(false)
      }}
      onClick={customOnClick ? externalOnClickHandler : handleCardSelect}
      editMode={editMode}
      isRTL={isRTL}
    >
      {editMode && (
        <StyledEditDiv>
          <Input
            className='primary editText'
            inputProps={{
              disabled: false,
              value: cardTitle,
              placeholder: placeholder,
            }}
            showBorderWithFloatedLabel={false}
            onChange={e => setCardTitle(e.target.value)}
            localeCode={localeCode}
          />
          <StyledActionContainer isRTL={isRTL}>
            <ButtonWithIcon
              disabled={!cardTitle || cardTitle.trim().length == 0}
              className='saveButton'
              icon='Checkmark'
              isRoundIcon
              showIcon
              onClick={handleSaveEdit}
            />
            <ButtonWithIcon
              className='cancelButton'
              icon='Close'
              isRoundIcon
              showIcon
              onClick={handleCancelEdit}
            />
          </StyledActionContainer>
        </StyledEditDiv>
      )}
      {!editMode && (
        <StyledTitle>
          {showArrow && (
            <StyledArrow data-testid='arrow' isEnabled={isEnabled}>
              <BlueCaret />
            </StyledArrow>
          )}
          {cardTitle}
          {isHover && moreOption && (
            <>
              <StyledOption
                id={id}
                data-testid='more-option'
                onClick={event => handleOptionsButtonClick(event)}
              >
                <MoreOption />
              </StyledOption>
              <Flyout
                show={showOptions}
                className='primary'
                id={id}
                items={
                  options &&
                  options.map(option => {
                    return {
                      label: option.name,
                    }
                  })
                }
                offset={10}
                onSelect={e => handleOptionSelect(e)}
                placement='leftTop'
                width='200px'
              />
            </>
          )}
        </StyledTitle>
      )}
    </StyledTreeCard>
  )
}

HierarchyTreeCard.defaultProps = {
  showArrow: true,
  moreOption: true,
  isEnabled: false,
}

HierarchyTreeCard.propTypes = {
  /** card title */
  title: PropTypes.string.isRequired,
  /** show or hide arrow */
  showArrow: PropTypes.bool,
  /** show or hide more option (ie three dots) */
  moreOption: PropTypes.bool,
  /** enable state which changes arrow color */
  isEnabled: PropTypes.bool,
  /** handler for more option (ie three dots) click */
  handleOptionClick: PropTypes.func,
  /** handler for extending on click functionality externally */
  customOnClick: PropTypes.func,
  /** handle onSave callback function for rename card title, return updated title */
  onSave: PropTypes.func,
  /** handle onCancel callback function for rename card title, return previous title */
  onCancel: PropTypes.func,
  /** Dynamic list of options for context menu along with onclick implementation of each option*/
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      onClickHandler: PropTypes.func.isRequired,
    })
  ),
  /** Rename card state - enable/disable */
  rename: PropTypes.bool,
  /** Placeholder for input text field*/
  placeholder: PropTypes.string,
  /** Locale Code Value to Support the Directional Input */
  localeCode: PropTypes.string,
}
