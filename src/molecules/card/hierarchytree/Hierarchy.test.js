import React from 'react'
import { render, fireEvent, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { HierarchyTreeCard } from './index'

afterEach(cleanup)

const fn = jest.fn()

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 1),
  }
})

describe('<HierarchyTreeCard/>', () => {
  it('should render HierarchyTreeCard with arrow', () => {
    const { container, getByTestId } = render(
      <HierarchyTreeCard title='Walmart' showArrow={true} />
    )
    fireEvent.mouseEnter(getByTestId('tree-card'))
    expect(getByTestId('arrow')).toBeInTheDocument()
    expect(getByTestId('more-option')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('should render HierarchyTree Card without arrow', () => {
    const { container } = render(
      <HierarchyTreeCard title='Walmart' showArrow={false} />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render HierarchyTreeCard with enabled state', () => {
    const { container } = render(
      <HierarchyTreeCard
        title='Walmart'
        showArrow={true}
        moreOption={true}
        isEnabled={true}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render HierarchyTreeCard without more option', () => {
    const { container } = render(
      <HierarchyTreeCard title='Walmart' showArrow={false} moreOption={false} />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render HierarchyTreeCard with handleOptionClick', () => {
    const { container, getByTestId } = render(
      <HierarchyTreeCard
        title='Walmart'
        showArrow={false}
        handleOptionClick={fn}
      />
    )
    fireEvent.click(getByTestId('tree-card'))
    expect(container).toMatchSnapshot()
  })

  it('should render HierarchyTreeCard in edit mode', () => {
    const { getByTestId, getByText, getByDisplayValue } = render(
      <HierarchyTreeCard
        title='Fabric inc.'
        showArrow={false}
        handleOptionClick={fn}
        options={[{ name: 'Rename' }]}
      />
    )

    fireEvent.mouseEnter(getByTestId('tree-card'))
    fireEvent.click(getByTestId('more-option'))
    fireEvent.click(getByText('Rename'))
    expect(getByDisplayValue('Fabric inc.')).toHaveValue('Fabric inc.')
  })

  it('should render HierarchyTreeCard in edit mode and update title', () => {
    const { getByTestId, getByText, getByDisplayValue } = render(
      <HierarchyTreeCard
        title='Fabric inc.'
        showArrow={false}
        handleOptionClick={fn}
        options={[{ name: 'Rename' }]}
      />
    )

    fireEvent.mouseEnter(getByTestId('tree-card'))
    fireEvent.click(getByTestId('more-option'))
    fireEvent.click(getByText('Rename'))
    expect(getByDisplayValue('Fabric inc.')).toHaveValue('Fabric inc.')

    fireEvent.change(getByDisplayValue('Fabric inc.'), {
      target: { value: 'Fabric Incorporation' },
    })

    expect(getByDisplayValue('Fabric Incorporation')).toBeInTheDocument()
  })

  it('should render hierarchy tree card with placeholder', () => {
    const { container } = render(
      <HierarchyTreeCard title='' rename={true} placeholder='Enter name..' />
    )

    expect(container).toMatchSnapshot()
  })
})
