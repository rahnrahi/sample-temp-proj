import styled from 'styled-components'
import { theme } from '../../../shared'

const getCardPadding = (editMode, isRTL) => {
  if (editMode) {
    return isRTL ? '0 16px 0 24px' : '0 16px'
  }
  return `16px 6px 16px 16px`
}

export const StyledTreeCard = styled.div`
  box-sizing: border-box;
  width: 276px;
  padding: ${({ editMode, isRTL }) => getCardPadding(editMode, isRTL)};
  ${theme.typography.link.css};
  background: ${theme.palette.brand.primary.white};
  border-radius: 10px;
  box-shadow: 0px 5px 20px rgba(115, 127, 143, 0.1);
  border: 2px solid transparent;
  word-break: break-all;
  ${({ editMode }) => !editMode && `cursor: pointer;`}
  :focus {
    outline: none;
    background: ${theme.palette.brand.primary.white};
    ${({ selected }) =>
      !selected && `border: 2px solid ${theme.palette.ui.cta.yellow};`}
  }
  ${({ selected }) =>
    selected && `border: 2px solid ${theme.palette.ui.cta.blue};`}
`

export const StyledEditDiv = styled.div`
  display: flex;
  align-items: center;

  .editText input {
    border-bottom: none;
  }

  .editText ::placeholder {
    ${theme.typography.link.css};
  }

  .saveButton {
    margin-right: 5px;
    width: 32px;
    height: 32px;
  }

  .cancelButton {
    background-color: #fff;
    border-color: #000;
    width: 32px;
    height: 32px;
  }

  .cancelButton .icon path {
    fill: ${theme.palette.brand.primary.charcoal};
  }
`

export const StyledTitle = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  color: ${theme.palette.brand.primary.charcoal};
  padding-right: 10px;
  font-family: ${theme.typography.link.fontFamily};
  font-size: ${theme.typography.link.fontSize};
  line-height: ${theme.typography.h6.lineHeight};
`

export const StyledArrow = styled.span`
  margin-right: 10px;
  transition: all 0.2s ease;
  ${({ isEnabled }) =>
    !isEnabled &&
    `
		svg > path{
			fill:${theme.palette.brand.primary.charcoal};
		}
		transform:rotate(180deg);
	`}
`
export const StyledOption = styled.span`
  position: absolute;
  right: 0px;
  display: inline-block;
  z-index: 1;
  padding: 10px;
  cursor: pointer;
`

export const StyledActionContainer = styled.div`
  ${({ isRTL }) => isRTL && `padding-left: 16px;`}
`
