import React from 'react'
import PropTypes from 'prop-types'
import { RightArrow } from '../../../assets/images'

import {
  StyledItemGeneral,
  StyledThumbnail,
  StyledContent,
  StyledTitle,
  StyledSubtitle,
  StyledArrow,
  StyledCategoryItem,
  StyledText,
  StyledValue,
} from './style'

export const GeneralItem = ({
  title,
  subtitle,
  imgUrl,
  imgAltText = 'item-img',
  ...props
}) => {
  return (
    <StyledItemGeneral {...props}>
      <StyledThumbnail>
        {imgUrl && <img src={imgUrl} alt={imgAltText} />}
      </StyledThumbnail>
      <StyledContent>
        <StyledTitle>{title}</StyledTitle>
        <StyledSubtitle>{subtitle}</StyledSubtitle>
      </StyledContent>
    </StyledItemGeneral>
  )
}

GeneralItem.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  imgUrl: PropTypes.string.isRequired,
  imgAltText: PropTypes.string.isRequired,
}

export const ActionItem = ({
  title,
  subtitle,
  imgUrl,
  imgAltText,
  onItemClick,
  showArrow,
  ...props
}) => {
  return (
    <StyledItemGeneral {...props} isActionItem={true}>
      <StyledThumbnail isActionItem={true}>
        {imgUrl && <img src={imgUrl} alt={imgAltText} />}
      </StyledThumbnail>
      <StyledContent isActionItem={true}>
        <StyledTitle>
          {typeof title === 'function' ? title() : title}
        </StyledTitle>
        <StyledSubtitle>{subtitle}</StyledSubtitle>
      </StyledContent>
      {showArrow && (
        <StyledArrow
          onClick={() => onItemClick({ title, subtitle, imgUrl, imgAltText })}
        >
          <RightArrow />
        </StyledArrow>
      )}
    </StyledItemGeneral>
  )
}

ActionItem.defaultProps = {
  showArrow: true,
}

ActionItem.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
  subtitle: PropTypes.string.isRequired,
  imgUrl: PropTypes.string.isRequired,
  imgAltText: PropTypes.string.isRequired,
  onItemClick: PropTypes.func,
  showArrow: PropTypes.bool,
}

export const CategoryItem = ({ label, value, ...props }) => {
  return (
    <StyledCategoryItem {...props}>
      <StyledText>{label}</StyledText>
      <StyledValue>{value}</StyledValue>
    </StyledCategoryItem>
  )
}

CategoryItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
}
