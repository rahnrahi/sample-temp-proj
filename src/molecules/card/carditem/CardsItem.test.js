import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { GeneralItem, ActionItem, CategoryItem } from './index'

afterEach(cleanup)

const fn = jest.fn()

describe('<CardItem/>', () => {
  const imgUrl =
    'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
  it('renders General Card item correctly', () => {
    const { container } = render(
      <GeneralItem
        title='Value 1'
        subtitle='subTitle'
        imgUrl={imgUrl}
        imgAltText='item-img'
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('renders Action Card Item with showArrow:false correctly', () => {
    const { container } = render(
      <ActionItem
        title='Value 1'
        subtitle='subTitle'
        imgUrl={imgUrl}
        imgAltText='item-img'
        onItemClick={fn}
        showArrow={false}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('renders Action Card Item with showArrow:true correctly', () => {
    const { container } = render(
      <ActionItem
        title='Value 1'
        subtitle='subTitle'
        imgUrl={imgUrl}
        imgAltText='item-img'
        onItemClick={fn}
        showArrow={true}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('renders Category Card Item correctly', () => {
    const { container } = render(
      <CategoryItem label='Subcategories' value='51' />
    )
    expect(container).toMatchSnapshot()
  })
})
