export const testCard = {
  expirationDate: '21 June 2023',
  billingAddress:
    '100 1st Northeast Kentucky Industrial Parkway Street. Seattle WA, 98338-124 U.S.',
  cardImage:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/1000px-Mastercard-logo.svg.png',
  cardNumber: '**** **** 3456',
}
