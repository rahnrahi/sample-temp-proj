import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { PaymentDetailsCard } from './PaymentDetailsCard'
import 'jest-styled-components'

import { testCard } from './mock'

afterEach(cleanup)

describe('<PaymentDetailsCard/>', () => {
  it('should render basic payment info', () => {
    const { container, getByText, getByTestId } = render(
      <PaymentDetailsCard {...testCard} />
    )

    expect(getByText(testCard.expirationDate)).toBeInTheDocument()
    expect(getByText(testCard.billingAddress)).toBeInTheDocument()
    expect(getByText(testCard.cardNumber)).toBeInTheDocument()
    expect(getByTestId('undefined-image').src).toBe(testCard.cardImage)
    expect(container).toMatchSnapshot()
  })
})
