import styled from 'styled-components'
import theme from '../../../shared/theme'

export const Root = styled.div`
  padding: 1rem;

  background-color: ${theme.palette.brand.primary.white};

  display: flex;
  flex-direction: column;
`

export const Header = styled.header`
  margin-bottom: 1rem;

  ${theme.typography.h5}

  color: ${theme.palette.brand.primary.charcoal};
`

export const Background = styled.div`
  width: 700px;
  padding: 1rem;
  padding-right: 2rem;

  background: #ddd;
  > * {
    margin-top: 1rem;
  }
`
