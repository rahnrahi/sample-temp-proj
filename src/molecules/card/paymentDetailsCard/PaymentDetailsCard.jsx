import React from 'react'
import PropTypes from 'prop-types'

import { KeyValue, PaymentCardNumber } from '../../../atoms'
import { Root, Header } from './PaymentDetailsCard.style'

export const PaymentDetailsCard = ({
  expirationDate,
  billingAddress,
  cardImage,
  cardNumber,
  children,
  ...rest
}) => {
  return (
    <Root {...rest}>
      <Header>Payment Details</Header>
      <PaymentCardNumber cardImage={cardImage} cardNumber={cardNumber} />
      <KeyValue keyText='expiry date' value={expirationDate} />
      <KeyValue keyText='billing address' value={billingAddress} />
      {children}
    </Root>
  )
}

PaymentDetailsCard.propTypes = {
  expirationDate: PropTypes.string,
  billingAddress: PropTypes.string,
  cardImage: PropTypes.string,
  cardNumber: PropTypes.string,
  children: PropTypes.any,
}
