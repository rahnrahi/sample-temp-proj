import React, { useState } from 'react'
import { Switch } from '../../../atoms/switch/Switch'
import { PaymentDetailsCard } from './PaymentDetailsCard'
import { Background } from './PaymentDetailsCard.style'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  CARD_STANDARD_DESIGN,
  CARD_STANDARD_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Standard',
  component: PaymentDetailsCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = ({ ...args }) => {
  const [showChild, setShowChild] = useState(false)

  const toggleShowChild = () => setShowChild(value => !value)

  return (
    <Background>
      <Switch
        label='show child element'
        initialState={showChild}
        ontoggle={toggleShowChild}
      />

      <PaymentDetailsCard {...args}>
        {showChild && <div> Example qnother element </div>}
      </PaymentDetailsCard>
    </Background>
  )
}

export const Standard = Template.bind({})
Standard.args = {
  expirationDate: '21 June 2023',
  billingAddress:
    '100 1st Northeast Kentucky Industrial Parkway Street. Seattle WA, 98338-124 U.S.',
  cardImage:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/1000px-Mastercard-logo.svg.png',
  cardNumber: '**** **** 3456',
}
Standard.parameters = getDesignSpecifications(
  CARD_STANDARD_DESIGN,
  CARD_STANDARD_PRESENTATION
)
