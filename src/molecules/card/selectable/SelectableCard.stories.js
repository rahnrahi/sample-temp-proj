import React from 'react'
import { SelectableCard } from './index'
import CardDocs from '../../../../docs/Cards.mdx'
import { Icons } from '../../../atoms/icon/Icon'
import { SELECTABLE_CARD_DESIGN } from '../../../hooks/constants'
export default {
  title: 'Modules/Cards/Card/Selectable',
  component: SelectableCard,
  argTypes: {
    text: {
      control: {
        type: 'text',
      },
    },
    iconName: {
      control: {
        type: 'select',
        options: Object.keys(Icons),
      },
    },
    selected: {
      control: {
        type: 'boolean',
      },
    },
    onClick: {
      control: {
        type: 'event',
      },
    },
    disabled: {
      control: {
        type: 'boolean',
      },
    },
  },
  parameters: {
    design: {
      name: 'Design',
      type: 'figma',
      url: SELECTABLE_CARD_DESIGN,
      allowFullscreen: true,
    },
    docs: {
      page: CardDocs,
    },
  },
}

const Template = args => <SelectableCard {...args} tabIndex='0' />

export const Default = Template.bind({})
Default.args = {
  text: 'This is going to be the text',
  iconName: 'Calendar',
  selected: false,
  onClick: () => {},
  disabled: false,
}
