import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { SelectableCard } from './index'
import 'jest-styled-components'

describe('SelectableCard', () => {
  it('renders selectable card with default props', () => {
    const { container } = render(
      <SelectableCard
        text='This is going to the the testing item'
        selected={false}
        iconName='Calendar'
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('renders selectable card correctly in the disabled state', () => {
    const { container } = render(
      <SelectableCard
        text='This is going to the the testing item'
        iconName='Calendar'
        disabled={false}
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('renders selectable card correctly with search icon', () => {
    const { container } = render(
      <SelectableCard
        text='This is a relatable text'
        iconName='Search'
        disabled={false}
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('Should render selectable card woth default params on page', () => {
    render(
      <SelectableCard text={'Selectable card Test'} iconName={'Calendar'} />
    )
    const cardText = screen.getByText('Selectable card Test')
    expect(cardText).toBeInTheDocument()
  })
  it('Should click on card just once', () => {
    const onCardClickFunc = jest.fn()
    render(
      <SelectableCard
        text={'Selectable card Test'}
        iconName={'Calendar'}
        onClick={onCardClickFunc}
      />
    )
    const card = screen.getByTestId('selectable-card')
    fireEvent.click(card)
    expect(onCardClickFunc).toBeCalledTimes(1)
  })
})
