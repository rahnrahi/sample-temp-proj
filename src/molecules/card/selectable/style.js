import styled from 'styled-components'
import { theme } from '../../../shared'
import {
  StyledCardBorderColor,
  StyledCardDisabledBorderColor,
} from '../expandable/style'

export const StyledCard = styled.div`
  ${theme.typography.body.css};
  width: 148px;
  cursor: pointer;
  height: 118px;
  padding: 30px 8px 0px 8px;
  display: flex;
  flex-direction: column;
  border: 1px solid rgba(
    ${StyledCardDisabledBorderColor.red},
    ${StyledCardDisabledBorderColor.green},
    ${StyledCardDisabledBorderColor.blue},
    ${StyledCardDisabledBorderColor.opacity});

  ${({ selected }) =>
    selected &&
    `border: 1px solid ${theme.palette.ui.colors.blue1}`
  };

  border-radius: 4px;
  background: ${theme.palette.brand.primary.white};
  box-sizing: border-box;
  color: ${({ disabled }) =>
    disabled
      ? `${theme.palette.ui.neutral.grey7}`
      : `${theme.palette.brand.primary.charcoal}`};
  ${({ disabled }) =>
    disabled &&
    `.icon, path {
      fill: ${theme.palette.ui.neutral.grey8};
    }
		pointer-events: none;
    border: 1px solid rgba(
      ${StyledCardBorderColor.red},
      ${StyledCardBorderColor.green},
      ${StyledCardBorderColor.blue},
      ${StyledCardBorderColor.opacity}
    );
	`};
  :hover {
    background: ${theme.palette.ui.colors.blue5};
  }
  :focus {
    outline: none;
    background: ${theme.palette.brand.primary.white};
    border: 1px solid ${theme.palette.ui.colors.blue1};
    outline: ${theme.palette.ui.colors.blue3} solid 4px;
  }
  :active {
    background: ${theme.palette.ui.colors.blue5};
    border: 1px solid ${theme.palette.ui.colors.blue1};
    color: ${theme.palette.ui.colors.blue1};
    .icon,
    path {
      fill: ${theme.palette.ui.colors.blue1};
    }
    outline: none;
  }
`
export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  :active {
    color: ${theme.palette.ui.colors.blue1};
  }
`
export const TextContainer = styled.div`
  margin-top: 12px;
  font-size: ${theme.typography.text14.fontSize};
  display: flex;
  justify-content: center;
  text-align: center;
  line-height: 16px;
  :active {
    color: ${theme.palette.ui.colors.blue1};
  }
`
