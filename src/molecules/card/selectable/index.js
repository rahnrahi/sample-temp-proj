import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { Icon } from '../../../atoms'
import { IconContainer, StyledCard, TextContainer } from './style'
export const SelectableCard = ({
  iconName,
  text,
  selected,
  onClick,
  disabled = false,
}) => {
  const selectedCardRef = useRef()
  const [cardSelected, setSelected] = useState(selected)
  const renderText = typeof text === 'string' ? text?.substring(0, 37) : ''

  useEffect(() => {
    setSelected(selected)
  }, [selected])

  return (
    <StyledCard
      data-testid={`selectable-card`}
      ref={selectedCardRef}
      selected={cardSelected}
      disabled={disabled}
      onClick={() => {
        setSelected(true)
        onClick()
      }}
    >
      <IconContainer className={'icon'}>
        <Icon size={32} iconName={iconName} />
      </IconContainer>
      <TextContainer>{renderText}</TextContainer>
    </StyledCard>
  )
}
SelectableCard.defaultProps = {
  iconName: '',
  text: '',
  selected: false,
  onClick: () => {},
  disabled: false,
}
SelectableCard.propTypes = {
  /** This is the name of the icon that is supposed to be shown */
  iconName: PropTypes.string.isRequired,
  /** This is the text that will be shown below the icon.<b> Max length supported is 38 characters </b>*/
  text: PropTypes.string.isRequired,
  /** Prop to toggle the card to a selected mode */
  selected: PropTypes.bool,
  /** Handler to execute when the card is clicked */
  onClick: PropTypes.func,
  /** Prop to mark the card as disabled */
  disabled: PropTypes.bool,
}
