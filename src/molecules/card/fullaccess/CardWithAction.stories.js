import React from 'react'
import { CardWithActionItem } from './index'
import { Link } from '../../../atoms'
import CardsDocs from '../../../../docs/Cards.mdx'
import { design } from '../card-utils'

export default {
  title: 'Modules/Cards/Card/Standard/with action items',
  component: CardWithActionItem,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <CardWithActionItem {...args} tabIndex='0' />

export const CardWithactionItem = Template.bind({})
CardWithactionItem.args = {
  heading: 'Recent Activity',
  width: '337px',
  height: '350px',
  data: [
    {
      title: () => (
        <span>
          Charlie Lee added <Link text='14 items' href='/' type='primary' /> to
          The Fall Cords Line
        </span>
      ),
      subtitle: 'Sept 7 2:38 PM PST',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
    {
      title: () => {
        return (
          <span>
            Charlie Lee added <Link text='14 items' href='/' type='primary' />{' '}
            to The Fall Cords Line.
          </span>
        )
      },
      subtitle: 'Sept 7 2:38 PM PST',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
    {
      title: 'Charlie Lee added 14 items to The Fall Cords Line.',
      subtitle: 'Sept 7 2:38 PM PST',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
  ],
  onItemClick: obj => {
    console.log('value', obj)
  },
  showArrow: true,
}
CardWithactionItem.parameters = { design }
