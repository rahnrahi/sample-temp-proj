import React from 'react'
import PropTypes from 'prop-types'
import { Tab, TabItem } from '../../../atoms'
import { GeneralItem, ActionItem } from '../carditem'
import { StyledCard, StyledCardHeading } from './style'

/** Full Access Card */
export const FullAccessCard = ({
  heading,
  data,
  width,
  height,
  onItemClick,
  children,
  ...props
}) => {
  return (
    <StyledCard {...props} width={width} height={height}>
      <StyledCardHeading>{heading}</StyledCardHeading>
      {children ? (
        children
      ) : (
        <>
          {data.length > 0 &&
            data.map((obj, i) => (
              <GeneralItem key={i} {...obj} onClick={() => onItemClick(obj)} />
            ))}
        </>
      )}
    </StyledCard>
  )
}

FullAccessCard.defaultProps = {
  width: '300px',
}

FullAccessCard.propTypes = {
  /**  card heading  */
  heading: PropTypes.string,
  /** width of card*/
  width: PropTypes.string,
  /** height of card*/
  height: PropTypes.string,
  /** pass data as a prop with same structure */
  data: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
      imgUrl: PropTypes.string,
      imgAltText: PropTypes.string,
    })
  ),
  /** use children for custom content */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /** handler function for item arrow clicking */
  onItemClick: PropTypes.func,
}

/** Card with Action Items */
export const CardWithActionItem = ({
  heading,
  data,
  width,
  height,
  onItemClick,
  showArrow,
  children,
  ...props
}) => {
  return (
    <StyledCard {...props} width={width} height={height}>
      <StyledCardHeading>{heading}</StyledCardHeading>
      {children ? (
        children
      ) : (
        <>
          {data &&
            data.length > 0 &&
            data.map((obj, i) => (
              <ActionItem
                key={i}
                {...obj}
                onItemClick={onItemClick}
                showArrow={showArrow}
              />
            ))}
        </>
      )}
    </StyledCard>
  )
}

CardWithActionItem.defaultProps = {
  showArrow: true,
  width: '337px',
}

CardWithActionItem.propTypes = {
  /**  card heading  */
  heading: PropTypes.string,
  /** width of card*/
  width: PropTypes.string,
  /** height of card*/
  height: PropTypes.string,
  /** pass data as a prop with same structure */
  data: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
      subtitle: PropTypes.string,
      imgUrl: PropTypes.string,
      imgAltText: PropTypes.string,
    })
  ),
  showArrow: PropTypes.bool,
  /** use children for custom content */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /** handler function for item arrow clicking */
  onItemClick: PropTypes.func,
}

/** Card with Tab */

export const CardWithTab = ({
  heading,
  data,
  width,
  height,
  onItemClick,
  showArrow,
  children,
  tabChangeCustomHandler,
  ...props
}) => {
  return (
    <StyledCard {...props} width={width} height={height}>
      <StyledCardHeading className='card-heading'>{heading}</StyledCardHeading>
      {children
        ? children
        : data &&
          data.length > 0 && (
            <Tab
              navClassName='tab-mb'
              tabChangeHandler={tabChangeCustomHandler}
            >
              {data.map((obj, i) => (
                <TabItem key={i} title={obj.tabName}>
                  {obj.render !== undefined
                    ? obj.render()
                    : obj.data.length > 0 &&
                      obj.data.map((item, j) => (
                        <ActionItem
                          key={j}
                          {...item}
                          onItemClick={onItemClick}
                          showArrow={showArrow}
                        />
                      ))}
                </TabItem>
              ))}
            </Tab>
          )}
    </StyledCard>
  )
}

CardWithTab.defaultProps = {
  showArrow: true,
  width: '337px',
}

CardWithTab.propTypes = {
  /**  card heading  */
  heading: PropTypes.string,
  /** width of card*/
  width: PropTypes.string,
  /** height of card*/
  height: PropTypes.string,
  /** pass data as a prop with same structure */
  data: PropTypes.arrayOf(
    PropTypes.shape({
      tabName: PropTypes.string,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          title: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
          subtitle: PropTypes.string,
          imgUrl: PropTypes.string,
          imgAltText: PropTypes.string,
        })
      ),
    })
  ),
  showArrow: PropTypes.bool,
  /** use children for custom content */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /** handler function for item arrow clicking */
  onItemClick: PropTypes.func,
  /** handler function for extending functionality on tab change */
  tabChangeCustomHandler: PropTypes.func,
}
