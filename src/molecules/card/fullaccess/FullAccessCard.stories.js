import React from 'react'
import { FullAccessCard as FullaccessCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import { design } from '../card-utils'

export default {
  title: 'Modules/Cards/Card/Standard/with image list',
  component: FullaccessCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <FullaccessCard {...args} tabIndex='0' />

export const FullAccessCard = Template.bind({})
FullAccessCard.args = {
  heading: 'Quick Items',
  width: '300px',
  height: '250px',
  data: [
    {
      title: 'Value 1',
      subtitle: 'Subtitle 2',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
    {
      title: 'Value 2',
      subtitle: 'Subtitle 2',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
    {
      title: 'Value 3',
      subtitle: 'Subtitle 3',
      imgUrl:
        'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
      imgAltText: 'item-img',
    },
  ],
  onItemClick: obj => {
    console.log('value', obj)
  },
}
FullAccessCard.parameters = { design }
