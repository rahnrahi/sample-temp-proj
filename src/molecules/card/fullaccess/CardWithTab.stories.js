import React from 'react'
import { CardWithTab } from './index'
import { Link } from '../../../atoms'
import CardsDocs from '../../../../docs/Cards.mdx'
import { design } from '../card-utils'

export default {
  title: 'Modules/Cards/Card/Standard/with image list & tabs',
  component: CardWithTab,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <CardWithTab {...args} tabIndex='0' />

export const WithActionItems = Template.bind({})
WithActionItems.args = {
  heading: 'PIM Alerts',
  data: [
    {
      tabName: 'Errors',
      data: [
        {
          title: () => (
            <span>
              Charlie Lee added <Link text='14 items' href='/' type='primary' />{' '}
              to The Fall Cords Line
            </span>
          ),
          subtitle: 'Sept 7 2:38 PM PST',
          imgUrl:
            'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
          imgAltText: 'item-img',
        },
        {
          title: () => (
            <span>
              <span>
                Charlie Lee added{' '}
                <Link text='14 items' href='/' type='primary' />
                to The Fall Cords Line
              </span>
            </span>
          ),
          subtitle: 'Sept 7 2:38 PM PST',
          imgUrl:
            'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
          imgAltText: 'item-img',
        },
      ],
    },
    {
      tabName: 'Needs',
      data: [
        {
          title: () => (
            <span>
              Charlie Lee added <Link text='14 items' href='/' type='primary' />{' '}
              to The Fall Cords Line
            </span>
          ),
          subtitle: 'Sept 7 2:38 PM PST',
          imgUrl:
            'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
          imgAltText: 'item-img',
        },
      ],
    },
  ],
  onItemClick: obj => {
    console.log('value', obj)
  },
  width: '337px',
  height: '300px',
  showArrow: true,
}
WithActionItems.parameters = { design }

export const WithCustomContent = Template.bind({})
WithCustomContent.args = {
  heading: 'PIM Alerts',
  data: [
    {
      tabName: 'Errors',
      render: () => {
        return <div>Errors</div>
      },
    },
    {
      tabName: 'Needs',
      render: () => {
        return <div>Needs</div>
      },
    },
  ],
  onItemClick: obj => {
    console.log('value', obj)
  },
  width: '337px',
  height: '300px',
  showArrow: true,
  tabChangeCustomHandler: index => console.log(`Switched to tab ${index}`),
}
WithCustomContent.parameters = { design }
