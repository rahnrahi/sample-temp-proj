import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledCard = styled.div`
  box-sizing: border-box;
  width: ${({ width }) => width && width};
  height: ${({ height }) => (height ? height : 'auto')};
  background: ${theme.palette.brand.primary.white};
  border: 2px solid transparent;
  border-radius: 4px;
  padding: 16px 30px 24px 30px;
  :focus {
    outline: none;
    border: 2px solid ${theme.palette.ui.cta.yellow};
  }
  .tab-mb {
    margin-bottom: 20px;
    font-size: ${theme.typography.h6.fontSize};
    line-height: ${theme.typography.h6.lineHeight};
    letter-spacing: 0.02em;
  }
`

export const StyledCardHeading = styled.div`
  ${theme.typography.body};
  color: ${theme.palette.brand.primary.charcoal};
  margin-bottom: 16px;
  text-align: left;
`
