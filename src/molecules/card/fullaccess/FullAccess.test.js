import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-styled-components'
import { FullAccessCard, CardWithActionItem, CardWithTab } from './index'
import { Link } from '../../../atoms'

afterEach(cleanup)

const fn = jest.fn()
const imgUrl =
  'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'

describe('<FullAccessCard/>,<CardWithActionItem/> and <CardWithTab/>', () => {
  it('should render FullAccessCard correctly', () => {
    const FullAccessCardData = [
      {
        title: 'Value 1',
        subtitle: 'Subtitle 2',
        imgUrl: imgUrl,
        imgAltText: 'item-img',
      },
      {
        title: 'Value 2',
        subtitle: 'Subtitle 2',
        imgUrl: imgUrl,
        imgAltText: 'item-img',
      },
    ]
    const { container } = render(
      <FullAccessCard
        heading='Card'
        width='300px'
        height='300px'
        data={FullAccessCardData}
        onItemClick={fn}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render CardWithActionItem correctly', () => {
    const CardWithActionData = [
      {
        title: () => (
          <span>
            Charlie Lee added <Link text='14 items' href='/' type='primary' />{' '}
            to The Fall Cords Line
          </span>
        ),
        subtitle: 'Sept 7 2:38 PM PST',
        imgUrl: imgUrl,
        imgAltText: 'item-img',
      },
    ]
    const { container } = render(
      <CardWithActionItem
        heading='Card'
        width='300px'
        height='300px'
        data={CardWithActionData}
        onItemClick={fn}
        showArrow={false}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render Card With Tab with action items correctly', () => {
    const CardWithTabData = [
      {
        tabName: 'Needs',
        data: [
          {
            title: () => (
              <span>
                Charlie Lee added{' '}
                <Link text='14 items' href='/' type='primary' /> to The Fall
                Cords Line
              </span>
            ),
            subtitle: 'Sept 7 2:38 PM PST',
            imgUrl: imgUrl,
            imgAltText: 'item-img',
          },
        ],
      },
    ]
    const { container } = render(
      <CardWithTab
        heading='Card'
        width='300px'
        height='300px'
        data={CardWithTabData}
        onItemClick={fn}
        showArrow={false}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render CardWithTab with custom tab data correctly', () => {
    const CardWithTabwithcustomData = [
      {
        tabName: 'Needs',
        render: () => <div>custom data</div>,
      },
      {
        tabName: 'Errors',
        render: () => <div>custom error data</div>,
      },
    ]
    const { container } = render(
      <CardWithTab
        heading='Card'
        width='300px'
        height='300px'
        data={CardWithTabwithcustomData}
        onItemClick={fn}
        showArrow={false}
      />
    )
    expect(container).toMatchSnapshot()
  })
})
