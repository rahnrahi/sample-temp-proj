import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { DownArrow } from '../../../assets/images'
import {
  StyledCard,
  StyledCardHeader,
  StyledHeading,
  StyledCardContent,
  StyledCaret,
} from './style'

export const ExpandableCard = ({
  heading,
  width,
  height,
  collapse: cardState,
  children,
  disabled,
  ...props
}) => {
  const [collapse, setCollapse] = useState(cardState)

  useEffect(() => {
    setCollapse(cardState)
  }, [cardState])

  const handleCollapse = () => {
    setCollapse(val => !val)
  }
  return (
    <StyledCard
      {...props}
      collapse={collapse}
      width={width}
      disabled={disabled}
    >
      <StyledCardHeader>
        <StyledHeading>{heading}</StyledHeading>
        <StyledCaret
          data-testid='downarrow'
          collapse={collapse}
          onClick={handleCollapse}
          disabled={disabled}
        >
          <DownArrow />
        </StyledCaret>
      </StyledCardHeader>
      <StyledCardContent collapse={collapse} height={height}>
        {children}
      </StyledCardContent>
    </StyledCard>
  )
}

ExpandableCard.defaultProps = {
  width: '337px',
  initialValue: false,
  disabled: false,
}

ExpandableCard.propTypes = {
  /** card heading */
  heading: PropTypes.string.isRequired,
  /** card width */
  width: PropTypes.string,
  /** card height */
  height: PropTypes.string,
  collapse: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /** Enable/Disable component */
  disabled: PropTypes.bool,
}
