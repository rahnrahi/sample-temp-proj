import styled from 'styled-components'
import { theme } from '../../../shared'
import { hexToRgba } from '../../../shared/utils'

export const StyledCardBorderColor = hexToRgba(
  theme.palette.brand.primary.gray,
  0.2
)

export const StyledCardDisabledBorderColor = hexToRgba(
  theme.palette.ui.neutral.grey7,
  0.2
)

export const StyledCard = styled.div`
  ${theme.typography.body.css};
  width: ${({ width }) => width && width};
  padding: 16px 27px 0px 32px;
  display: flex;
  flex-direction: column;
  border: ${({ disabled }) =>
    disabled
      ? `1px solid rgba(
      	${StyledCardBorderColor.red},
      	${StyledCardBorderColor.green},
      	${StyledCardBorderColor.blue},
      	${StyledCardBorderColor.opacity}
			);`
      : `2px solid rgba(
      	${StyledCardDisabledBorderColor.red},
      	${StyledCardDisabledBorderColor.green},
      	${StyledCardDisabledBorderColor.blue},
      	${StyledCardDisabledBorderColor.opacity});`};
  border-radius: 4px;
  background: ${theme.palette.brand.primary.white};
  box-sizing: border-box;
  color: ${({ disabled }) =>
    disabled
      ? `${theme.palette.ui.neutral.grey7}`
      : `${theme.palette.brand.primary.charcoal}`};
  ${({ disabled }) =>
    disabled &&
    `
		pointer-events: none;

	`};
  :hover {
    background: ${`rgba(
      	${StyledCardBorderColor.red},
      	${StyledCardBorderColor.green},
      	${StyledCardBorderColor.blue},
      	${StyledCardBorderColor.opacity})`};
  }
  :focus {
    outline: none;
    background: ${theme.palette.brand.primary.white};
    border: 2px solid ${theme.palette.ui.cta.yellow};
  }
  :active {
    background: ${theme.palette.brand.primary.white};
    border: 2px solid ${`rgba(
      	${StyledCardBorderColor.red},
      	${StyledCardBorderColor.green},
      	${StyledCardBorderColor.blue},
      	${StyledCardBorderColor.opacity})`};
    outline: none;
  }
`

export const StyledCaret = styled.div`
  display: flex;
  transition: all 0.3s ease-in-out;
  transform-origin: center center;
  cursor: pointer;
  ${({ collapse }) =>
    collapse &&
    `
    transform:rotateZ(180deg);
  `};

  svg {
    ${({ disabled }) => disabled && `fill: ${theme.palette.ui.neutral.grey7}`};
  }
`

export const StyledCardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: ${theme.typography.body.fontSize};
  line-height: ${theme.typography.body.lineHeight};
  padding-bottom: 14px;
`

export const StyledHeading = styled.div`
  word-break: break-all;
`

export const StyledCardContent = styled.div`
  box-sizing: border-box;
  padding-bottom: 16px;
  height: ${({ height }) => (height ? height : 'auto')};
  display: ${({ collapse }) => (collapse ? 'block' : 'none')};
`
