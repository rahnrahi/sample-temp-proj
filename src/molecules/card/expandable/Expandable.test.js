import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import { ExpandableCard } from './index'

afterEach(cleanup)

describe('<ExpandableCard/>', () => {
  it('renders Expandable Card correctly', () => {
    const { container } = render(
      <ExpandableCard heading='Card' width='337px' height='200px' collapse />
    )
    expect(container).toMatchSnapshot()
  })

  it('expand card correctly', () => {
    const { getByTestId, getByText } = render(
      <ExpandableCard heading='Card' width='337px' height='200px'>
        <span> expandable card </span>
      </ExpandableCard>
    )
    const downArrow = getByTestId('downarrow')
    fireEvent.click(downArrow)
    expect(getByText('expandable card')).toBeInTheDocument()
  })
})
