import React from 'react'
import { ExpandableCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Expandable/expandable card',
  component: ExpandableCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const ExpandableCardTemplate = args => <ExpandableCard {...args} tabIndex='0' />

export const Expandablecard = ExpandableCardTemplate.bind({})
Expandablecard.args = {
  heading: 'Cards',
  width: '337px',
  height: '200px',
}
Expandablecard.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)

export const Disabled = ExpandableCardTemplate.bind({})
Disabled.args = {
  heading: 'Cards',
  disabled: true,
}
Disabled.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
