import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledRootCard = styled.div`
  width: ${({ width }) => width && width};
  height: ${({ height }) => (height ? height : 'auto')};
  background: ${theme.palette.brand.primary.white};
  border: 2px solid transparent;
  border-radius: 4px;
  box-sizing: border-box;
  :hover {
    ${({ hoverState }) =>
      hoverState && `background: ${theme.palette.ui.neutral.grey5};`}
  }
  :focus {
    outline: none;
    background: ${theme.palette.brand.primary.white};
    ${({ selected }) =>
      !selected &&
      `
    border: 2px solid ${theme.palette.ui.cta.yellow};
		`}
  }
  ${({ selected }) =>
    selected && `border: 2px solid ${theme.palette.ui.cta.blue}`};
`

export const StyledRootHeader = styled.div`
  font-family: ${theme.typography.link.fontFamily};
  padding-bottom: ${theme.typography.link.fontSize};
  margin-bottom: ${theme.typography.link.lineHeight};
  ${({ borderBottom }) =>
    borderBottom &&
    `
      border-bottom: 1px solid ${theme.palette.brand.primary.gray}30;
    `};
`

export const StyledHeader = styled.div`
  ${theme.typography.body.css};
  color: ${theme.palette.brand.primary.charcoal};
  margin-bottom: 11px;
`
export const StyledTitle = styled.div`
  ${theme.typography.h5.css};
  color: ${theme.palette.brand.primary.charcoal};
  margin-bottom: 4px;
`

export const StyledSubtitle = styled.div`
  ${theme.typography.h6.css};
  color: ${theme.palette.brand.primary.gray};
`
export const StyledContent = styled.div`
  box-sizing: border-box;
  padding: 24px;
  &:focus {
    outline: none;
    border: none;
  }
`
