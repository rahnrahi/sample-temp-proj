import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import { RootCategoryCard } from './index'

afterEach(cleanup)

const fn = jest.fn()

describe('<RootCategoryCard/>', () => {
  const data = [
    {
      label: 'Subcategories',
      value: '51',
    },
    {
      label: 'Attributes',
      value: '23',
    },
  ]
  it('should render RootCategory Card with separator ', () => {
    const { container } = render(
      <RootCategoryCard
        header='Header'
        title='Shirts'
        subtitle='Updated 9/8 12:15PM PST'
        width='270px'
        height='300px'
        data={data}
        showLineSeparator={true}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render RootCategoryCard without separator', () => {
    const { container, getByText } = render(
      <RootCategoryCard
        header='Header'
        title='Shirts'
        subtitle='Updated 9/8 12:15PM PST'
        width='270px'
        height='300px'
        data={data}
        onClick={fn}
        showLineSeparator={false}
      />
    )
    fireEvent.click(getByText('Shirts'))
    expect(container).toMatchSnapshot()
  })
})
