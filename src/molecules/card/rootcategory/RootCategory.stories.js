import React from 'react'
import { RootCategoryCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Folder/primary',
  component: RootCategoryCard,
  argTypes: {
    onSelect: {
      type: 'select',
    },
  },
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <RootCategoryCard {...args} tabIndex='0' />

export const WithSeparator = Template.bind({})
WithSeparator.args = {
  title: 'Shirts',
  subtitle: 'Updated 9/8  12:15PM PST',
  width: '271px',
  data: [
    {
      label: 'Subcategories',
      value: '51',
    },
    {
      label: 'Attributes',
      value: '23',
    },
    {
      label: 'Total SKUs',
      value: '4,392',
    },
  ],
  showLineSeparator: true,
}
WithSeparator.parameters = getDesignSpecifications(
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION
)

export const WithoutSeparator = Template.bind({})
WithoutSeparator.args = {
  title: 'Shirts',
  subtitle: 'Updated 9/8  12:15PM PST',
  width: '271px',
  enableHover: true,
  data: [
    {
      label: 'Subcategories',
      value: '51',
    },
    {
      label: 'Attributes',
      value: '23',
    },
    {
      label: 'Total SKUs',
      value: '4,392',
    },
  ],
  showLineSeparator: false,
}
WithoutSeparator.parameters = getDesignSpecifications(
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION
)

export const WithHeaderText = Template.bind({})
WithHeaderText.args = {
  header: 'Header',
  hoverState: false,
  title: 'Shirts',
  subtitle: 'Updated 9/8  12:15PM PST',
  width: '271px',
  onSelect: data => console.log(data),
  data: [
    {
      label: 'Subcategories',
      value: '51',
    },
    {
      label: 'Attributes',
      value: '23',
    },
    {
      label: 'Total SKUs',
      value: '4,392',
    },
  ],
  showLineSeparator: true,
}
WithHeaderText.parameters = getDesignSpecifications(
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION
)
