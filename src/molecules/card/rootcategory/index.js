import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { CategoryItem } from '../carditem'
import {
  StyledRootCard,
  StyledContent,
  StyledRootHeader,
  StyledTitle,
  StyledSubtitle,
  StyledHeader,
} from './style'
import isFunction from 'lodash/isFunction'

export const RootCategoryCard = props => {
  const {
    header,
    title,
    subtitle,
    width,
    height,
    data,
    children,
    showLineSeparator,
    hoverState,
    onSelect,
    isSelected,
    ...rest
  } = props
  const [selected, setSelected] = useState(isSelected)

  const handleCardSelect = () => {
    isFunction(onSelect) && onSelect(props)
  }

  useEffect(() => {
    setSelected(isSelected)
  }, [isSelected])

  return (
    <StyledRootCard
      {...rest}
      hoverState={hoverState}
      selected={selected}
      width={width}
      height={height}
      onClick={handleCardSelect}
    >
      <StyledContent tabIndex='-1'>
        <StyledRootHeader borderBottom={showLineSeparator}>
          {header && <StyledHeader>{header}</StyledHeader>}
          {title && <StyledTitle>{title}</StyledTitle>}
          {subtitle && <StyledSubtitle>{subtitle}</StyledSubtitle>}
        </StyledRootHeader>
        <>
          {children
            ? children
            : data &&
              data.length > 0 &&
              data.map((obj, i) => <CategoryItem key={i} {...obj} />)}
        </>
      </StyledContent>
    </StyledRootCard>
  )
}

RootCategoryCard.defaultProps = {
  showLineSeparator: true,
  width: '270px',
  hoverState: true,
  isSelected: false,
}

RootCategoryCard.propTypes = {
  /** Header Text for card */
  header: PropTypes.string.isRequired,
  /** title for card */
  title: PropTypes.string.isRequired,
  /** subtitle for card */
  subtitle: PropTypes.string.isRequired,
  /** width of card */
  width: PropTypes.string,
  /** height of card */
  height: PropTypes.string,
  /** hoverState will enable hover state for Card, default: true */
  hoverState: PropTypes.bool,
  /** onSelect is callback function, which will enable selection of each card  */
  onSelect: PropTypes.func,
  /** isSelected is Bool to set selected border   */
  isSelected: PropTypes.bool,
  /** use data prop with same structure */
  data: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, value: PropTypes.string })
  ),
  /** show or hide line separator */
  showLineSeparator: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
