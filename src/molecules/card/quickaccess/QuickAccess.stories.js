import React from 'react'
import { QuickAccessCard } from './index'
import CardsDocs from '../../../../docs/Cards.mdx'
import {
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Cards/Card/Folder/secondary',
  component: QuickAccessCard,
  argTypes: {},
  parameters: {
    docs: {
      page: CardsDocs,
    },
  },
}

const Template = args => <QuickAccessCard {...args} tabIndex='0' />

export const QuickaccessCard = Template.bind({})
QuickaccessCard.args = {
  title: '23987532950 - The Fleet Fox',
  subtitle: 'Shirts/Flannel',
  imgUrl:
    'https://images.unsplash.com/photo-1603881568692-1429786f5ce1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
  imgAltText: 'img-txt',
}
QuickaccessCard.parameters = getDesignSpecifications(
  CARD_FOLDER_DESIGN,
  CARD_FOLDER_PRESENTATION
)
