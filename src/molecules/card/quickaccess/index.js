import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  StyledQuickAccessCard,
  StyledCardContent,
  StyledThumbnail,
  StyledContent,
  StyledTitle,
  StyledSubtitle,
} from './style'

export const QuickAccessCard = ({
  title,
  subtitle,
  imgUrl,
  imgAltText,
  ...props
}) => {
  const [selected, setSelected] = useState(false)

  const handleCardSelect = () => {
    setSelected(select => !select)
  }
  return (
    <StyledQuickAccessCard
      {...props}
      selected={selected}
      onClick={handleCardSelect}
    >
      <StyledCardContent tabIndex='-1'>
        <StyledThumbnail>
          <img src={imgUrl} alt={imgAltText} />
        </StyledThumbnail>
        <StyledContent>
          <StyledTitle>{title}</StyledTitle>
          <StyledSubtitle>{subtitle}</StyledSubtitle>
        </StyledContent>
      </StyledCardContent>
    </StyledQuickAccessCard>
  )
}

QuickAccessCard.propTypes = {
  /** card title */
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  /** card subtitle */
  subtitle: PropTypes.string.isRequired,
  /** Image url */
  imgUrl: PropTypes.string.isRequired,
  /** Image alt text */
  imgAltText: PropTypes.string.isRequired,
}
