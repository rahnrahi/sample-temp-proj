import {
  CARD_ACTION_DESIGN,
  CARD_ACTION_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export const design = getDesignSpecifications(
  CARD_ACTION_DESIGN,
  CARD_ACTION_PRESENTATION
)['design']
