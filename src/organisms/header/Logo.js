import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from '../../atoms'

const Logo = props => {
  const {
    logo: { url, width, height },
    title,
  } = props
  return (
    <div className='logo'>
      {url ? (
        <div className='logo-inner-wrap'>
          <img
            src={url}
            alt={title}
            width={width}
            height={height}
            className='logo-margin'
          />
          {title}
        </div>
      ) : (
        <div className='logo-inner-wrap'>
          <Icon iconName='FabricLogoName' className='logo-margin' />
        </div>
      )}
    </div>
  )
}

Logo.propTypes = {
  logo: PropTypes.shape({
    width: PropTypes.string.required,
    height: PropTypes.string.required,
    url: PropTypes.string.required,
  }),
  title: PropTypes.string,
}
Logo.defaultProps = {
  logo: {
    url: '',
    width: '18px',
    height: '18px',
  },
}

export default Logo
