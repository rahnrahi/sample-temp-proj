import React from 'react'
import PropTypes from 'prop-types'
import { StyledUserAction, StyledPopoverContent, StyledAvatar } from './styles'
import { Popover, theme, Link } from '../../../atoms'
import { initials } from '../../../shared/utils'

const UserNav = ({ userName, avatar }) => {
  return (
    <div className='user-actions'>
      <div className='avatar'>
        {avatar ? (
          <img src={avatar} alt={userName} />
        ) : (
          <StyledAvatar
            size='28px'
            round='28px'
            color={theme.palette.brand.primary.charcoal}
            bgColor={theme.palette.brand.primary.white}
          >
            {userName && initials(userName)}
          </StyledAvatar>
        )}
      </div>
      <div className='user-name'>{userName}</div>
    </div>
  )
}

UserNav.propTypes = {
  userName: PropTypes.string,
  avatar: PropTypes.string,
}

const UserAction = ({ userName, avatar, userEmail, userNav }) => {
  return (
    <StyledUserAction>
      <Popover
        showArrow={true}
        className='profile'
        placement='bottom'
        target={<UserNav userName={userName} avatar={avatar} />}
      >
        <StyledPopoverContent>
          <div className='header'>
            <div className={avatar ? 'avatar' : ''}>
              <span>
                {avatar ? (
                  <img src={avatar} alt={userName} />
                ) : (
                  <StyledAvatar
                    size='40px'
                    round='40px'
                    color={theme.palette.brand.primary.white}
                    bgColor={theme.palette.brand.primary.charcoal}
                    fSize='14px'
                  >
                    {userName && initials(userName)}
                  </StyledAvatar>
                )}
              </span>
            </div>
            <div className='title'>
              <strong>{userName}</strong>
              <p>{userEmail}</p>
            </div>
          </div>
          <ul>
            {userNav &&
              userNav.map((nav, id) => (
                <li key={id}>
                  <Link
                    href={nav.link}
                    text={nav.label}
                    type='secondary'
                    isLeft={true}
                  />
                </li>
              ))}
          </ul>
        </StyledPopoverContent>
      </Popover>
    </StyledUserAction>
  )
}

UserAction.propTypes = {
  userNav: PropTypes.array,
  avatar: PropTypes.string,
  userName: PropTypes.string,
  userEmail: PropTypes.string,
}

export default React.memo(UserAction)
