import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledUserAction = styled.div`
  .profile {
    .user-actions {
      display: flex;
      align-items: center;
      cursor: default;

      .avatar {
        width: 28px;
        height: 28px;
        overflow: hidden;

        img {
          max-width: 100%;
          height: auto;
          border-radius: 50%;
        }
      }
      .user-name {
        font-size: ${theme.typography.link.fontSize};
        line-height: ${theme.typography.link.lineHeight};
        padding-left: 22px;
      }
    }
    .content {
      right: 0px;
      left: auto !important;
      transform: unset !important;

      .arrows {
        left: 61%;
      }
    }
  }
`

export const StyledPopoverContent = styled.div`
  padding: 36px 36px 15px;
  box-sizing: border-box;
  font-family: 'Gilroy-Regular';

  .header {
    display: flex;
    padding: 0px 0px 30px;

    .avatar {
      width: 40px;

      img {
        max-width: 100%;
        height: auto;
      }

      span {
        display: block;
        background-color: ${theme.palette.ui.neutral.grey2};
        border-radius: 50%;
        width: 40px;
        height: 40px;
        overflow: hidden;
      }
    }
    .title {
      padding-left: 20px;

      strong {
        font-family: ${theme.typography.body.fontFamily};
        font-size: ${theme.typography.body.fontSize};
        line-height: ${theme.typography.body.lineHeight};
        color: ${theme.palette.brand.primary.charcoal};
      }
      p {
        margin: 0px;
        color: ${theme.palette.brand.primary.gray};
        font-size: ${theme.typography.h6.fontSize};
        line-height: ${theme.typography.h6.lineHeight};
        letter-spacing: ${theme.typography.h6.letterSpacing};
      }
    }
  }

  ul {
    list-style: none;
    padding: 0px;
    margin: 0px;

    li {
      a {
        font-size: ${theme.typography.h5.fontSize};
        line-height: ${theme.typography.h5.lineHeight};
        letter-spacing: ${theme.typography.h5.letterSpacing};
        text-decoration: none;
        color: ${theme.palette.brand.primary.charcoal};
        padding: 10px 0px;
        display: block;
      }
    }
  }
`

export const StyledAvatar = styled.div`
  width: ${({ size }) => (size ? size : '28px')};
  height: ${({ size }) => (size ? size : '28px')};
  border-radius: ${({ round }) => (round ? round : '')};
  background: ${({ bgColor }) =>
    bgColor ? bgColor : theme.palette.brand.primary.charcoal};
  color: ${({ color }) => (color ? color : theme.palette.brand.primary.white)};
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Gilroy-SemiBold;
  font-size: ${({ fSize }) => (fSize ? fSize : '10px')};
`
