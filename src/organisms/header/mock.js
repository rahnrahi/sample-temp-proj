export const links = [
  {
    text: 'Overview',
    icon: 'Overview',
    href: '/overview',
  },
  {
    text: 'PIM',
    icon: 'PIM',
    href: '/pim',
    active: true,
  },
  {
    text: 'XM',
    icon: 'XM',
    href: '/xm',
  },
  {
    text: 'OFFERS',
    icon: 'Offers',
    href: '/offers',
  },
  {
    text: 'OMS',
    icon: 'OMS',
    href: '/oms',
  },
  {
    text: 'Subscriptions',
    icon: 'Subscriptions',
    href: '/subscriptions',
  },
  {
    text: 'CSR',
    icon: 'CSR',
    href: '/csr',
  },
]

export const userData = {
  userName: 'Tiffany Doe',
  userEmail: 'tiffany.doe@jcrew.com',
  userNav: [
    {
      label: 'Edit Profile',
      link: '/',
    },
    {
      label: 'Preferences',
      link: '/',
    },
    {
      label: 'Logout',
      link: '/',
    },
  ],
}
