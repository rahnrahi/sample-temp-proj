import React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import { Header } from './'
import { links, userData } from './mock'
import 'jest-styled-components'

afterEach(cleanup)

const props = {
  company: 'SPORTS & CO',
  productLinks: links,
  showNotification: false,
  userData: userData,
}

describe('<Global Header/>', () => {
  it('renders', () => {
    const { container } = render(<Header {...props} />)
    expect(container).toMatchSnapshot()
  })
})
