import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from '../../atoms'

const ProductLinks = props => {
  const { productLinks } = props

  return (
    <div className='product-links'>
      {productLinks &&
        productLinks.map((link, index) => {
          const { onClick } = link
          return (
            <div
              className={`product-link ${link.active ? 'active' : ''}`}
              key={index}
            >
              <a href={link.href} title={link.text} onClick={onClick}>
                <span className='icon'>
                  <Icon iconName={link.icon} />
                </span>
                <span className='text'>{link.text}</span>
              </a>
            </div>
          )
        })}
    </div>
  )
}

ProductLinks.propTypes = {
  productLinks: PropTypes.array.isRequired,
}

export default ProductLinks
