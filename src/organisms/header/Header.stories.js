import React from 'react'
import { Header } from './'
import { links, userData } from './mock'
import {
  HEADER_DESIGN,
  HEADER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Archived/Header',
  component: Header,
  argTypes: {
    onClick: { action: 'click' },
  },
  args: {
    company: 'fabric',
    productLinks: links,
    showNotification: false,
    userData: userData,
    logo: {
      width: '24px',
      height: '24px',
      url: '',
    },
  },
}

const Template = args => <Header {...args} />

export const Index = Template.bind({})
Index.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: HEADER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: HEADER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
