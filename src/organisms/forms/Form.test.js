import React from 'react'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import Form from './Form'

afterEach(cleanup)
beforeEach(() => {
  jest.clearAllMocks()
})

describe('<Form />', () => {
  const submitCallback = jest.fn(() => {})

  const props = {
    editMode: true,
    submitCallback,
  }

  it('renders Form component', () => {
    render(
      <Form {...props}>
        {() => {
          return (
            <div>
              <span>HELLO</span>
              <input data-testid='submit-button' type='submit' />
            </div>
          )
        }}
      </Form>
    )
    document.querySelector('form').onsubmit = submitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(submitCallback).toBeCalled()
    expect(screen.getByTestId('form-div')).toBeInTheDocument()
  })
})
