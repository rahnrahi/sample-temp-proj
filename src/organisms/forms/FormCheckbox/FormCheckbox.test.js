import React from 'react'
import { FormCheckbox } from './FormCheckbox'
import Form from '../Form'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

describe('<FormCheckbox/>', () => {
  const onChange = jest.fn(() => {})

  const submitCallback = jest.fn(() => ({
    name: new Date('2022-02-23T05:33:53.018Z'),
  }))
  const props = {
    editMode: true,
    submitCallback,
  }

  it('should render checkbox', () => {
    const { getByTestId } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormCheckbox
            label='Name'
            name='name'
            value='John Doe'
            onChange={onChange}
            hooks={hooks}
            formProps={formProps}
            field='name'
          />
        )}
      </Form>
    )
    const checkbox = getByTestId('checkbox')
    expect(checkbox).toBeInTheDocument()
  })

  it('should check for states in checkbox', () => {
    const { getByTestId } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormCheckbox
            label='Name'
            name='name'
            value='John Doe'
            onChange={onChange}
            hooks={hooks}
            formProps={formProps}
            checked={false}
            field='name'
          />
        )}
      </Form>
    )
    const checkbox = getByTestId('checkbox')
    fireEvent.click(checkbox)
    expect(onChange).toBeCalled()
    expect(checkbox).toBeChecked()
    fireEvent.click(checkbox)
    expect(checkbox).not.toBeChecked()
  })
})
