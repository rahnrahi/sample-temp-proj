import React, { useEffect, useState } from 'react'
import { Checkbox } from '../../../atoms/checkbox/Checkbox'
import PropTypes from 'prop-types'
import { assignValue, extractValue, formattedFieldName } from '../utils'

export const FormCheckbox = params => {
  const checkboxRef = React.createRef()

  const [checkboxState, setCheckboxState] = useState(
    extractValue(params) || params?.checked
  )
  useEffect(() => {
    setCheckboxState(extractValue(params))
  }, [params, params?.checked])

  return (
    <Checkbox
      ref={checkboxRef}
      data-testid='checkbox'
      {...params}
      {...params.hooks.register(formattedFieldName(params), {
        value: checkboxState,
        required: Boolean(params.isRequired),
      })}
      disabled={!params.formProps.editMode}
      checked={checkboxState}
      onChange={e => {
        assignValue(params, e.target.checked, 'boolean')
        setCheckboxState(e.target.checked)
        params?.onChange && params?.onChange(e)
      }}
    />
  )
}

FormCheckbox.PropTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequire,
  name: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  isPartial: PropTypes.bool,
  className: PropTypes.string,
  otherProps: PropTypes.object,
}
