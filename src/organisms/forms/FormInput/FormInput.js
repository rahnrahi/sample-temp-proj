import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Input } from '../../../atoms/input/Input'
import {
  extractValue,
  assignValue,
  formattedFieldName,
  extractError,
  doesErrorExist,
} from '../utils'

export const FormInput = params => {
  const [inputValue, setInputValue] = useState(
    params?.inputProps?.defaultValue ||
      params?.inputProps?.value ||
      extractValue(params) ||
      ''
  )

  return (
    <Input
      data-testid='form-input'
      {...params}
      {...params.hooks.register(formattedFieldName(params), {
        value: inputValue,
        required: Boolean(params.isRequired),
      })}
      label={!params.enableFloatingLabel && params.label}
      inputProps={{
        ...params.inputProps,
        disabled: !params?.formProps?.editMode,
        defaultValue: inputValue,
        value: inputValue,
        onChange: e => {
          setInputValue(e.target.value)
          assignValue(params, e.target.value)
          params.hooks.clearErrors(params.field)
          params?.inputProps?.onChange && params?.inputProps?.onChange(e)
        },
        type: params?.inputProps?.type,
      }}
      errorMessage={extractError(params)}
      error={doesErrorExist(params)}
    />
  )
}

FormInput.PropTypes = {
  label: PropTypes.string,
  enableFloatingLabel: PropTypes.bool,
  isRequired: PropTypes.bool,
  inputProps: PropTypes.shape({
    value: PropTypes.string | PropTypes.number | PropTypes.Date,
    defaultValue: PropTypes.string | PropTypes.number | PropTypes.Date,
    type: PropTypes.string,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onKeyPress: PropTypes.func,
  }),
  field: PropTypes.string,
}
