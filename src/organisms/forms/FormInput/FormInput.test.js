import React from 'react'
import { FormInput } from './FormInput'
import Form from '../Form'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})
const primaryProps = {
  className: 'primary',
  label: 'Field Label',
  isFloatedLabel: true,
  field: 'field',
  width: '300px',
  inputProps: {
    isControlled: true,
    onChange: jest.fn(() => {}),
  },
}

describe('<FormInput/>', () => {
  const submitCallback = jest.fn(() => ({
    field: 'test',
  }))
  const nestedSubmitCallback = jest.fn(() => ({
    test: { field: 'test' },
  }))
  const props = {
    editMode: true,
    submitCallback,
  }
  const nestedProps = {
    editMode: true,
    submitCallback: nestedSubmitCallback,
  }
  it('renders FormInput component', () => {
    render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormInput {...primaryProps} hooks={hooks} formProps={formProps} />
        )}
      </Form>
    )
    const input = screen.getByTestId('form-input')
    expect(input).toBeInTheDocument()
  })

  it('should render the changed value', () => {
    const { container } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormInput {...primaryProps} hooks={hooks} formProps={formProps} />
        )}
      </Form>
    )
    const input = container.querySelector('input')
    fireEvent.focus(input)
    fireEvent.change(input, { target: { value: 'fabric' } })
    fireEvent.blur(input)
    expect(input.value).toBe('fabric')
    expect(primaryProps.inputProps.onChange).toBeCalled()
  })

  it('renders FormInput with a default value', () => {
    primaryProps['inputProps']['value'] = 'test'
    const { getByLabelText } = render(
      <Form>
        {(hooks, formProps) => (
          <FormInput {...primaryProps} hooks={hooks} formProps={formProps} />
        )}
      </Form>
    )
    const input = getByLabelText('Field Label')
    expect(input.value).toBe('test')
  })

  it('renders FormInput and Form submission is done', () => {
    render(
      <Form>
        {(hooks, formProps) => (
          <>
            <FormInput {...primaryProps} hooks={hooks} formProps={formProps} />
            <input data-testid='submit-button' type='submit' />
          </>
        )}
      </Form>
    )
    document.querySelector('form').onsubmit = submitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(submitCallback).toBeCalled()
  })

  it('renders FormInput passing a group name', () => {
    primaryProps['groupName'] = 'test'
    primaryProps['inputProps']['value'] = 'test'
    nestedProps['submitCallback'] = nestedSubmitCallback
    render(
      <Form {...nestedProps}>
        {(hooks, formProps) => (
          <>
            <FormInput {...primaryProps} hooks={hooks} formProps={formProps} />
            <input data-testid='submit-button' type='submit' />
          </>
        )}
      </Form>
    )
    document.querySelector('form').onsubmit = nestedSubmitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(nestedProps.submitCallback).toReturnWith({
      test: { field: 'test' },
    })
  })
})
