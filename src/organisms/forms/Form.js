import React, { useEffect, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { joiResolver } from '@hookform/resolvers/joi'
import Joi from 'joi'
import { isFunction } from '../../shared/utils'

export default function Form(formProps) {
  const [formValues, setFormValues] = useState({})

  const methods = useForm({
    resolver: joiResolver(
      formProps?.validationSchema || Joi.object({}).unknown()
    ),
    mode: 'onSubmit',
    reValidateMode: 'onChange',
    shouldUseNativeValidation: true,
    shouldFocusError: true,
    shouldUnregister: true,
    defaultValues: formProps.defaultValues || {},
  })

  const {
    handleSubmit,
    watch,
    formState,
    formState: { errors },
    register,
    getValues,
    setValue,
    unregister,
    control,
    trigger,
    reset,
    setError,
    clearErrors,
    resetField,
  } = methods

  useEffect(() => {
    setFormValues(getValues())
  }, [])

  const onSubmit = handleSubmit(data => {
    isFunction(formProps.submitCallback) && formProps.submitCallback(data)
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={handleSubmit(onSubmit)} data-testid='form-div'>
        {formProps?.children(
          {
            getValues,
            register,
            unregister,
            setValue,
            watch,
            control,
            formState,
            errors,
            trigger,
            reset,
            setError,
            clearErrors,
            resetField,
            formValues,
          },
          formProps
        )}
      </form>
    </FormProvider>
  )
}
