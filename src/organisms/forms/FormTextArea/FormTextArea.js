import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Textarea } from '../../../atoms/textarea'
import { ErrorMessage } from '../../../atoms/error/Error'
import {
  extractValue,
  assignValue,
  formattedFieldName,
  extractError,
  doesErrorExist,
} from '../utils'

export const FormTextarea = params => {
  const inputRef = useRef('textarea')
  const [inputValue, setInputValue] = useState(
    extractValue(params) ||
      params?.textareaProps?.defaultValue ||
      params?.textareaProps?.value ||
      ''
  )

  useEffect(() => {
    setInputValue(extractValue(params) || params?.textareaProps?.value || '')
  }, [params, params?.textareaProps?.value])

  return (
    <>
      <Textarea
        data-testid='form-textarea'
        {...params}
        {...params.hooks.register(formattedFieldName(params), {
          value: inputValue,
          required: params.isRequired,
        })}
        disabled={!params.formProps.editMode}
        textareaProps={{
          onChange: e => {
            assignValue(params, e.target.value)
            setInputValue(e.target.value)
            params?.textareaProps?.onChange &&
              params?.textareaProps?.onChange(e)
          },
          value: inputValue,
        }}
        ref={inputRef}
      />
      {doesErrorExist(params) && <ErrorMessage text={extractError(params)} />}
    </>
  )
}

FormTextarea.PropTypes = {
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  label: PropTypes.string,
  iconLeft: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  textareaProps: PropTypes.shape({
    defaultValue: PropTypes.string,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onChange: PropTypes.func,
    type: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    limit: PropTypes.number,
    rows: PropTypes.number,
  }),
  errorProps: PropTypes.object,
  width: PropTypes.string,
  limit: PropTypes.number,
  showEditButton: PropTypes.bool,
  onEditButtonClick: PropTypes.func,
  field: PropTypes.string,
  isRequired: PropTypes.bool,
}
