import React from 'react'
import { FormTextarea } from './FormTextArea'
import Form from '../Form'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('<FormTextArea/>', () => {
  const submitCallback = jest.fn(() => ({
    test: { 'Field Label': 'test' },
  }))
  const formProps = {
    editMode: true,
    submitCallback,
  }

  const props = {
    className: 'multiline',
    label: 'Description',
    textareaProps: {
      limit: 100,
      rows: 5,
      onChange: jest.fn(() => {}),
    },
  }

  it('renders Textarea component', () => {
    const { getByTestId } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormTextarea {...props} hooks={hooks} formProps={formProps} />
        )}
      </Form>
    )
    const textAreaElement = getByTestId('form-textarea')
    expect(textAreaElement).toBeInTheDocument()
  })
  it('renders the changed value', () => {
    const { container } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormTextarea {...props} hooks={hooks} formProps={formProps} />
        )}
      </Form>
    )
    const textAreaElement = container.querySelector('textarea')
    fireEvent.focus(textAreaElement)
    fireEvent.change(textAreaElement, { target: { value: 'fabric' } })
    expect(textAreaElement.value).toBe('fabric')
  })
})
