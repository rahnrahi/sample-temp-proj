import React from 'react'
import { FormCalendar } from './FormCalendar'
import Form from '../Form'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})

const calendarProps = {
  yearsArray: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027],
  popperPlacement: 'bottom-end',
  initialValue: new Date('2022-02-23T05:33:53.018Z'),
  onDateChange: () => {},
  field: 'calendar',
}

describe('<FormCalendar/>', () => {
  const submitCallback = jest.fn(() => ({
    calendar: new Date('2022-02-23T05:33:53.018Z'),
  }))
  const props = {
    editMode: true,
    submitCallback,
  }

  it('should render FormCalendar with initial value correctly', () => {
    const { container } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormCalendar
            hooks={hooks}
            formProps={formProps}
            initialValue={new Date('2020-10-10 00:00')}
          />
        )}
      </Form>
    )
    const input = container.querySelector('input')
    expect(input.value).toBe('10/10/2020')
  })

  it('should render FormCalendar correctly', () => {
    const { container } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormCalendar
            hooks={hooks}
            formProps={formProps}
            {...calendarProps}
          />
        )}
      </Form>
    )
    const input = container.querySelector('input')
    fireEvent.change(input, { target: { value: '10/10/2020' } })
    fireEvent.click(input)
    expect(input.value).toBe('10/10/2020')
  })

  it('should render FormCalendar on submit', () => {
    render(
      <Form {...props}>
        {(hooks, formProps) => (
          <>
            <FormCalendar hooks={hooks} formProps={formProps} />
            <input data-testid='submit-button' type='submit' />
          </>
        )}
      </Form>
    )
    document.querySelector('form').onsubmit = submitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(props.submitCallback).toReturnWith({
      calendar: new Date('2022-02-23T05:33:53.018Z'),
    })
  })
})
