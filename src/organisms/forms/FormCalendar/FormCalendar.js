import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Calendar } from '../../../atoms/calendar/Calendar'
import { ErrorMessage } from '../../../atoms/error/Error'
import {
  assignValue,
  extractError,
  extractValue,
  formattedFieldName,
} from '../utils'

export const FormCalendar = params => {
  const [inputValue, setInputValue] = useState(
    extractValue(params) || params?.initialValue || ''
  )

  useEffect(() => {
    setInputValue(extractValue(params) || params?.initialValue || '')
  }, [params, params?.initialValue])

  let errorMessage = extractError(params)
  return (
    <>
      <Calendar
        {...params}
        {...params.hooks.register(formattedFieldName(params), {
          value: inputValue,
          required: Boolean(params.isRequired),
        })}
        initialValue={inputValue}
        disabled={!params.formProps.editMode}
        onDateChange={date => {
          assignValue(params, date)
          setInputValue(date)
          params.hooks.clearErrors(params.field)
          params?.onDateChange && params?.onDateChange(date)
        }}
      />
      {errorMessage && <ErrorMessage text={errorMessage} />}
    </>
  )
}

FormCalendar.PropTypes = {
  onDateChange: PropTypes.func,
  fixedHeight: PropTypes.bool,
  numberOfYears: PropTypes.number,
  yearsArray: PropTypes.array,
  popperPlacement: PropTypes.string,
  minDate: PropTypes.date,
  maxDate: PropTypes.date,
  initialValue: PropTypes.date,
  customInput: PropTypes.func,
  disabled: PropTypes.bool,
  zIndex: PropTypes.number,
}
