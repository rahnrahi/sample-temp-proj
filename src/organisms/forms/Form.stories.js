import React, { useState } from 'react'
import Form from './Form'
import { FormInput } from './FormInput/FormInput'
import { FormCheckbox } from './FormCheckbox/FormCheckbox'
import { FormGroup } from './FormGroup/FormGroup'
import { FormCalendar } from './FormCalendar/FormCalendar'
import { FormTimePicker } from './FormTimePicker/FormTimePicker'
import { FormDropdown } from './FormDropdown/FormDropdown'
import { FormRadio } from './FormRadio/FormRadio'
import {
  StyledLabel,
  StyleRow,
  StyleHeader,
  StyleButtonRow,
  InputStyleRow,
  StyledDropDownLabel,
} from './Form.styles'
import { Button } from '../../atoms/button/Button'
import { Tab, TabItem } from '../../atoms/tab/Tab'
import Joi from 'joi'
import FormDocs from '../../../docs/Form.mdx'
import {
  FORM_DESIGN,
  FORM_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Templates/Form',
  component: Form,
  argTypes: {},
  args: {
    editMode: true,
    submitCallback: data => alert(JSON.stringify(data, '', 4)),
  },
  parameters: {
    docs: {
      page: FormDocs,
    },
  },
}

const design = [
  {
    name: DESIGN_TAB_MOCKUP,
    type: 'figma',
    url: FORM_DESIGN,
    allowFullscreen: true,
  },
  {
    name: DESIGN_TAB_PRESENTATION,
    type: 'figma',
    url: FORM_PRESENTATION,
    allowFullscreen: true,
  },
]

const validationSchema = Joi.object({
  organizationName: Joi.string().required(),
  street: Joi.string().required(),
  phoneNumber: Joi.number().required(),
  email: Joi.string().required(),
  city: Joi.string().required(),
  state: Joi.string().required(),
  zipCode: Joi.string().required(),
  country: Joi.object().required(),
  checkbox: Joi.bool(),
  taxExempt: Joi.string(),
  taxId: Joi.string(),
})

let calendarProps = {
  yearsArray: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027],
  popperPlacement: 'bottom-end',
  initialValue: new Date(),
  onDateChange: () => {},
  field: 'calendar',
  // date: new Date(),
}

let timepickerprops = {
  label: 'Tax exempt start time',
  width: '188px',
  timeIntervals: 15,
  date: new Date(),
  onChange: () => {},
  disabled: false,
  field: 'startTime',
  enableFloatingLabel: false,
}

let radioProps = {
  label: 'Radio',
  value: '',
  checked: true,
  name: 'Radio',
  tabIndex: 1,
}

const BaseForm = args => (
  <Form {...args}>
    {(hooks, formProps) => {
      return (
        <div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              marginBottom: 10,
            }}
          >
            <Button
              style={{
                height: 36,
                width: 100,
              }}
              text='submit'
              isPrimary
            >
              <input type='submit' />
            </Button>
          </div>
          <FormGroup hooks={hooks} formProps={formProps}>
            {FormGroupProps => (
              <>
                <StyleHeader> Add Organization</StyleHeader>
                <FormCheckbox
                  label={'checkbox'}
                  value={'true'}
                  hooks={hooks}
                  formProps={formProps}
                  checked={true}
                  field='checkbox'
                  onChange={() => {}}
                />
                <FormRadio
                  {...radioProps}
                  hooks={hooks}
                  formProps={formProps}
                  field='radio'
                />
                <StyleRow>
                  <StyledLabel>Organization Name</StyledLabel>
                  <FormInput
                    field='organizationName'
                    label='Organization Name'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                {/* <StyleRow  >
                  <StyledLabel>About Organization</StyledLabel>
                  <FormTextarea
                    {...textAreaProps}
                    hooks={hooks}
                    formProps={formProps}
                  />
                </StyleRow> */}
                <StyleRow>
                  <StyledLabel>Phone number (optional)</StyledLabel>
                  <FormInput
                    field='phoneNumber'
                    label='Phone number (optional)'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                    inputProps={{ type: 'number' }}
                    groupName={FormGroupProps?.groupName || ''}
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Street</StyledLabel>
                  <FormInput
                    field='street'
                    label='Street'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                    groupName={FormGroupProps?.groupName || ''}
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Contact email</StyledLabel>
                  <FormInput
                    field='email'
                    label='Contact email'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>City</StyledLabel>
                  <FormInput
                    field='city'
                    label='City'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>State</StyledLabel>
                  <FormInput
                    field='state'
                    label='State'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>ZipCode</StyledLabel>
                  <FormInput
                    field='zipCode'
                    label='ZipCode'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Country</StyledLabel>
                  <div style={{ width: '100%', margin: 'auto' }}>
                    <FormDropdown
                      hooks={hooks}
                      formProps={formProps}
                      field='country'
                      titleLabel={'Country'}
                      options={[
                        {
                          id: 1,
                          name: 'Canada',
                        },
                        {
                          id: 2,
                          name: 'USA',
                        },
                        {
                          id: 3,
                          name: 'India',
                        },
                        {
                          id: 4,
                          name: 'UK',
                        },
                      ]}
                      value={{
                        id: 2,
                        name: 'USA',
                      }}
                    />
                  </div>
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Tax exempt</StyledLabel>
                  <FormInput
                    field='taxExempt'
                    label='Tax exempt'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Tax ID</StyledLabel>
                  <FormInput
                    field='taxId'
                    label='Tax ID'
                    hooks={hooks}
                    formProps={formProps}
                    enableFloatingLabel
                  />
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Tax exempt start time</StyledLabel>
                  <div style={{ width: '100%' }}>
                    <FormTimePicker
                      {...timepickerprops}
                      hooks={hooks}
                      formProps={formProps}
                    />
                  </div>
                </StyleRow>
                <StyleRow>
                  <StyledLabel>Tax exempt end date</StyledLabel>
                  <div style={{ width: '100%', margin: 'auto' }}>
                    <FormCalendar
                      fixedHeight={false}
                      numberOfYears={0}
                      minDate={undefined}
                      maxDate={undefined}
                      customInput={() => {}}
                      disabled={false}
                      zIndex={0}
                      {...calendarProps}
                      hooks={hooks}
                      formProps={formProps}
                    />
                  </div>
                </StyleRow>
              </>
            )}
          </FormGroup>
        </div>
      )
    }}
  </Form>
)

const MultiFormTemplate = args => {
  const [tab, setTab] = useState(0)

  const clickHandler = val => {
    setTab(val)
  }

  return (
    <div>
      <Form {...args}>
        {(hooks, formProps) => (
          <>
            <Tab
              variant='horizontal'
              tabChangeHandler={val => clickHandler(val)}
            >
              <TabItem title='Tab 1'></TabItem>
              <TabItem title='Tab 2'></TabItem>
            </Tab>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row-reverse',
                marginBottom: 10,
              }}
            >
              <Button
                style={{
                  height: 36,
                  width: 100,
                }}
                text='submit'
                isPrimary
                icon={undefined}
                showIcon={undefined}
                iconSize={undefined}
                iconPosition={undefined}
              >
                <input type='submit' />
              </Button>
            </div>
            {tab === 0 && (
              <FormGroup
                hooks={hooks}
                formProps={formProps}
                groupName='organisationDetails'
              >
                {FormGroupProps => (
                  <>
                    <StyleHeader> Organization Details</StyleHeader>
                    <StyleRow>
                      <StyledLabel>Organization Name</StyledLabel>
                      <FormInput
                        field='organizationName'
                        label='Organization Name'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Phone number (optional)</StyledLabel>
                      <FormInput
                        field='phoneNumber'
                        label='Phone number (optional)'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Street</StyledLabel>
                      <FormInput
                        field='street'
                        label='Street'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Contact email</StyledLabel>
                      <FormInput
                        field='email'
                        label='Contact email'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>City</StyledLabel>
                      <FormInput
                        field='city'
                        label='City'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>State</StyledLabel>
                      <FormInput
                        field='state'
                        label='State'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>ZipCode</StyledLabel>
                      <FormInput
                        field='zipCode'
                        label='ZipCode'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Country</StyledLabel>
                      <div style={{ width: '100%', margin: 'auto' }}>
                        <FormDropdown
                          hooks={hooks}
                          formProps={formProps}
                          groupName={FormGroupProps?.groupName || ''}
                          field='country'
                          titleLabel='Country'
                          options={[
                            {
                              id: 1,
                              name: 'Canada',
                            },
                            {
                              id: 2,
                              name: 'USA',
                            },
                            {
                              id: 3,
                              name: 'India',
                            },
                            {
                              id: 4,
                              name: 'UK',
                            },
                          ]}
                          value={{
                            id: 2,
                            name: 'USA',
                          }}
                        />
                      </div>
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Tax exempt</StyledLabel>
                      <FormInput
                        field='taxExempt'
                        label='Tax exempt'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Tax ID</StyledLabel>
                      <FormInput
                        field='taxId'
                        label='Tax ID'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Tax exempt start date</StyledLabel>
                      <div style={{ width: '100%' }}>
                        <FormTimePicker
                          onChange={() => {}}
                          disabled={false}
                          {...timepickerprops}
                          hooks={hooks}
                          formProps={formProps}
                        />
                      </div>
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Tax exempt end date</StyledLabel>
                      <div style={{ width: '100%', margin: 'auto' }}>
                        <FormCalendar
                          fixedHeight={false}
                          numberOfYears={0}
                          minDate={undefined}
                          maxDate={undefined}
                          customInput={() => {}}
                          disabled={false}
                          zIndex={0}
                          {...calendarProps}
                          hooks={hooks}
                          formProps={formProps}
                          groupName={FormGroupProps?.groupName || ''}
                        />
                      </div>
                    </StyleRow>
                  </>
                )}
              </FormGroup>
            )}
            {tab === 1 && (
              <FormGroup
                hooks={hooks}
                formProps={formProps}
                groupName='address'
              >
                {FormGroupProps => (
                  <>
                    <StyleHeader> Address Details</StyleHeader>
                    <StyleRow>
                      <StyledLabel>Address Type</StyledLabel>
                      <FormInput
                        field='addressType'
                        label='Address Type'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Street Address</StyledLabel>
                      <FormInput
                        field='streetAddress'
                        label='Street Address'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>City</StyledLabel>
                      <FormInput
                        field='city'
                        label='City'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>State</StyledLabel>
                      <FormInput
                        field='state'
                        label='State'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>ZipCode</StyledLabel>
                      <FormInput
                        field='zipCode'
                        label='ZipCode'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Country</StyledLabel>
                      <div style={{ width: '100%', margin: 'auto' }}>
                        <FormDropdown
                          hooks={hooks}
                          formProps={formProps}
                          groupName={FormGroupProps?.groupName || ''}
                          field='country'
                          options={[
                            {
                              id: 1,
                              name: 'Canada',
                            },
                            {
                              id: 2,
                              name: 'USA',
                            },
                            {
                              id: 3,
                              name: 'India',
                            },
                            {
                              id: 4,
                              name: 'UK',
                            },
                          ]}
                        />
                      </div>
                    </StyleRow>
                    <StyleRow>
                      <StyledLabel>Group</StyledLabel>
                      <FormInput
                        field='group'
                        label='Group'
                        hooks={hooks}
                        formProps={formProps}
                        enableFloatingLabel
                        groupName={FormGroupProps?.groupName || ''}
                      />
                    </StyleRow>
                  </>
                )}
              </FormGroup>
            )}
          </>
        )}
      </Form>
    </div>
  )
}

const ValidationForm = args => {
  return (
    <Form {...args} validationSchema={validationSchema}>
      {(hooks, formProps) => {
        return (
          <div>
            <StyleButtonRow>
              <input
                type='reset'
                onClick={() => {
                  let keys = Object.keys(hooks.formValues)
                  keys.forEach(data => {
                    hooks.resetField(data)
                  })
                }}
              />
              <Button
                style={{
                  height: 36,
                  width: 100,
                }}
                text='submit'
                isPrimary
              >
                <input type='submit' />
              </Button>
            </StyleButtonRow>
            <FormGroup hooks={hooks} formProps={formProps}>
              {FormGroupProps => (
                <>
                  <StyleHeader> Add Organization</StyleHeader>
                  <FormCheckbox
                    label={'checkbox'}
                    value={'true'}
                    hooks={hooks}
                    formProps={formProps}
                    checked={true}
                    field='checkbox'
                    onChange={() => {}}
                  />
                  <div>
                    <FormInput
                      field='organizationName'
                      label='Organization Name'
                      hooks={hooks}
                      inputType='Field'
                      formProps={formProps}
                      isRequired={true}
                    />
                  </div>
                  <div>
                    <FormInput
                      field='phoneNumber'
                      label='Phone number (optional)'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                      groupName={FormGroupProps?.groupName || ''}
                    />
                  </div>
                  <div>
                    <FormInput
                      field='email'
                      label='Contact email'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <div>
                    <FormInput
                      field='street'
                      label='Street'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <div>
                    <FormInput
                      field='city'
                      label='City'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <div>
                    <FormInput
                      field='state'
                      label='State'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <div>
                    <FormInput
                      field='zipCode'
                      label='ZipCode'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <InputStyleRow>
                    <StyledDropDownLabel>Country</StyledDropDownLabel>
                    <div style={{ width: '100%', margin: 'auto' }}>
                      <FormDropdown
                        hooks={hooks}
                        formProps={formProps}
                        field='country'
                        options={[
                          {
                            id: 1,
                            name: 'Canada',
                          },
                          {
                            id: 2,
                            name: 'USA',
                          },
                          {
                            id: 3,
                            name: 'India',
                          },
                          {
                            id: 4,
                            name: 'UK',
                          },
                        ]}
                        value={{
                          id: 4,
                          name: 'UK',
                        }}
                      />
                    </div>
                  </InputStyleRow>
                  <div>
                    <FormInput
                      field='taxExempt'
                      label='Tax exempt'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                  <div>
                    <FormInput
                      field='taxId'
                      label='Tax ID'
                      hooks={hooks}
                      formProps={formProps}
                      inputType='Field'
                    />
                  </div>
                </>
              )}
            </FormGroup>
          </div>
        )
      }}
    </Form>
  )
}

const ViewForm = args => {
  let formValues = {
    organizationName: 'My Organization',
    street: 'Baker street',
    phoneNumber: 99000222222,
    email: 'fabric@fabric.inc',
    city: 'Seattle',
    state: 'Washington',
    zipCode: '98101',
    country: 'USA',
    checkbox: false,
    taxExempt: 'Yes',
    taxId: '00221A1S11134',
    calendar: new Date(),
  }

  return (
    <Form {...args} validationSchema={validationSchema} formValues={formValues}>
      {(hooks, formProps) => {
        return (
          <div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row-reverse',
                marginBottom: 10,
              }}
            >
              <Button
                style={{
                  height: 36,
                  width: 100,
                }}
                text='submit'
                isPrimary
                icon={undefined}
                showIcon={undefined}
                iconSize={undefined}
                iconPosition={undefined}
              >
                <input type='submit' />
              </Button>
            </div>
            <StyleHeader> Add Organization</StyleHeader>
            <FormCheckbox
              label={'checkbox'}
              value={'true'}
              hooks={hooks}
              formProps={formProps}
              checked={formValues.checkbox}
              onChange={() => {}}
            />
            <StyleRow>
              <StyledLabel>Organization Name</StyledLabel>
              <FormInput
                field='organizationName'
                label='Organization Name'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                isRequired={true}
                inputProps={{
                  value: formProps.formValues['organizationName'],
                  type: 'text',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Phone number (optional)</StyledLabel>
              <FormInput
                field='phoneNumber'
                label='Phone number (optional)'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['phoneNumber'],
                  type: 'number',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Contact email</StyledLabel>
              <FormInput
                field='email'
                label='Contact email'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['email'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Street</StyledLabel>
              <FormInput
                field='street'
                label='Street'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['street'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>City</StyledLabel>
              <FormInput
                field='city'
                label='City'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['city'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>State</StyledLabel>
              <FormInput
                field='state'
                label='State'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['state'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>ZipCode</StyledLabel>
              <FormInput
                field='zipCode'
                label='ZipCode'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['zipCode'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Country</StyledLabel>
              <div style={{ width: '100%', margin: 'auto' }}>
                <FormDropdown
                  hooks={hooks}
                  formProps={formProps}
                  field='country'
                  options={[
                    {
                      id: 1,
                      name: 'Canada',
                    },
                    {
                      id: 2,
                      name: 'USA',
                    },
                    {
                      id: 3,
                      name: 'India',
                    },
                    {
                      id: 4,
                      name: 'UK',
                    },
                  ]}
                  value={{
                    id: 2,
                    name: 'USA',
                  }}
                />
              </div>
            </StyleRow>
            <StyleRow>
              <StyledLabel>Tax exempt</StyledLabel>
              <FormInput
                field='taxExempt'
                label='Tax exempt'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['taxExempt'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Tax ID</StyledLabel>
              <FormInput
                field='taxId'
                label='Tax ID'
                hooks={hooks}
                formProps={formProps}
                enableFloatingLabel
                inputProps={{
                  value: formProps.formValues['taxId'],
                  type: 'string',
                }}
              />
            </StyleRow>
            <StyleRow>
              <StyledLabel>Tax exempt start time</StyledLabel>
              <div style={{ width: '100%' }}>
                <FormTimePicker
                  onChange={() => {}}
                  disabled={false}
                  {...timepickerprops}
                  hooks={hooks}
                  formProps={formProps}
                />
              </div>
            </StyleRow>
            <StyleRow>
              <StyledLabel>Tax exempt end date</StyledLabel>
              <div style={{ width: '100%', margin: 'auto' }}>
                <FormCalendar
                  {...calendarProps}
                  hooks={hooks}
                  formProps={formProps}
                />
              </div>
            </StyleRow>
          </div>
        )
      }}
    </Form>
  )
}
export const BasicForm = BaseForm.bind({})
BasicForm.parameters = { design }
export const MultipleForms = MultiFormTemplate.bind({})
MultipleForms.args = {
  editMode: true,
  submitCallback: data => alert(JSON.stringify(data, '', 4)),
}
MultipleForms.parameters = { design }
export const FormWithValidations = ValidationForm.bind({})
FormWithValidations.args = {
  defaultValues: {
    organizationName: 'Testing 123',
    phoneNumber: '123232',
    checkbox: true,
    email: 'testmail@gmail.com',
    street: '3rd Street',
    city: 'Northern',
    state: 'Cali',
    zipCode: '34009',
    country: {
      id: 2,
      name: 'USA',
    },
    taxExempt: 'Yes',
    taxId: '11232',
  },
}
FormWithValidations.parameters = { design }
export const FormWithValues = ViewForm.bind({})
FormWithValues.args = {
  editMode: false,
}
FormWithValues.parameters = { design }
