//constants
export const Type = Object.freeze({
  TEXT: 'text',
  PASSWORD: 'password',
  NUMBER: 'number',
  DATE: 'date',
})

/**
 *
 * @param params
 * @returns boolean
 * @description This function checks for any error in the form
 */
export const doesErrorExist = params => {
  let groupName = params?.groupName
  let fieldName = params?.field
  if (groupName) {
    return params?.hooks?.errors?.[groupName]?.[fieldName]?.message
      ? true
      : false
  }
  return params?.hooks?.errors?.[fieldName]?.message ? true : false
}

/**
 *
 * @param params
 * @returns string
 * @description This function helps get the error message
 */
export const extractError = params => {
  let groupName = params?.groupName
  let fieldName = params?.field
  if (groupName && doesErrorExist(params)) {
    return params?.hooks?.errors[groupName][fieldName]?.message || ''
  }
  return params?.hooks?.errors[fieldName]?.message || ''
}

/**
 *
 * @param params
 * @returns any
 * @description This function helps get the form value using getValues hook
 */
export const extractValue = params => {
  let groupName = params?.groupName
  let fieldName = params?.field
  if (groupName && params.hooks.getValues()?.[groupName]?.[fieldName]) {
    return params.hooks.getValues()[groupName][fieldName]
  }
  return params.hooks.getValues()[fieldName]
}

/**
 * @param params
 * @param value
 * @param type - string by default; used as a flag for boolean types
 * @description This function helps link element values to the form
 */
export const assignValue = (params, value, type = '') => {
  let groupName = params?.groupName
  let fieldName = params?.field
  let valueToAssign = value
  valueToAssign = type == 'boolean' ? value : valueToAssign
  if (groupName) {
    let existingValues = params.hooks.getValues()[groupName] || {}
    params.hooks.setValue(`${groupName}`, {
      ...existingValues,
      [`${fieldName}`]: valueToAssign,
    })
  } else {
    params.hooks.setValue(`${fieldName}`, valueToAssign)
  }
}

/**
 *
 * @param params
 * @returns string
 * @description This function helps register the element name to form
 */
export const formattedFieldName = params => {
  return params?.groupName
    ? `${params.groupName || ''}.${params.field}`
    : `${params.field}`
}
