import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Radio } from '../../../atoms/radio'
import { assignValue, extractValue, formattedFieldName } from '../utils'

export const FormRadio = params => {
  const [radioxState, setRadioState] = useState(
    extractValue(params) || params?.checked
  )

  useEffect(() => {
    setRadioState(extractValue(params))
  }, [params, params?.checked])

  params.hooks.setValue(params.name, params.checked)
  return (
    <Radio
      data-testid='radio'
      {...params}
      {...params.hooks.register(formattedFieldName(params), {
        value: radioxState,
        required: Boolean(params.isRequired),
      })}
      checked={radioxState}
      disabled={!params.formProps.editMode}
      onChange={e => {
        assignValue(params, e.target.checked)
        setRadioState(e.target.checked)
        params?.onChange && params?.onChange(e)
      }}
    />
  )
}

FormRadio.PropTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  id: PropTypes.string,
  className: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  name: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  tabIndex: PropTypes.number,
  children: PropTypes.element | PropTypes.arrayOf(PropTypes.element),
  withHover: PropTypes.bool,
  withFocus: PropTypes.bool,
  isRequired: PropTypes.bool,
}
