import React from 'react'
import { FormRadio } from './FormRadio'
import Form from '../Form'

import {
  render,
  cleanup,
  fireEvent,
  screen,
  waitFor,
} from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('<FormRadio/>', () => {
  const submitCallback = jest.fn(() => ({
    test: { 'Field Label': 'test' },
  }))
  const formProps = {
    editMode: true,
    submitCallback,
  }

  const props = {
    label: 'Radio Label',
    value: 'default',
    checked: false,
    name: 'default',
    tabIndex: 0,
    id: 'radio',
    onChange: jest.fn(() => {}),
  }

  it('renders FormRadio component', () => {
    const { getByTestId } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormRadio
            {...props}
            hooks={hooks}
            formProps={formProps}
            field='radio'
          />
        )}
      </Form>
    )
    const radioElement = getByTestId('radio')
    expect(radioElement).toBeInTheDocument()
  })

  it('checks state of radio', async () => {
    const { container } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormRadio
            {...props}
            hooks={hooks}
            formProps={formProps}
            field='radio'
          />
        )}
      </Form>
    )
    await waitFor(() => screen.getByText(/Radio Label/i))
    const radio = container.querySelector('.radio-input')
    fireEvent.click(radio)
    expect(radio.checked).toBe(true)
  })
})
