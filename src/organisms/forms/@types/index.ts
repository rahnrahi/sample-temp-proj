import React from "react";

interface FormProps {
  // Added any intentionally
    hooks: any
    formProps: any
    field: string
    isRequired?: boolean
    groupName?: string
}

export interface Options {
    id?: number,
    name?: string
}

export interface FormInputElements extends FormProps {
  label?: string
  enableFloatingLabel?: boolean
  isRequired?: boolean
  inputProps?: Partial<{
    value: string | number | Date 
    defaultValue: string | number | Date 
    type: string
    onBlur: (e: React.SyntheticEvent) => void
    onChange: (e: React.SyntheticEvent) => void
    onFocus: (e: React.SyntheticEvent) => void
    onKeyPress: (e: React.SyntheticEvent) => void
  }>
  field: string
}

export interface FormCheckboxElements extends FormProps {
  label: string
  value: string
  onChange: (e: React.SyntheticEvent) => void
  name?: string,
  checked?: boolean,
  disabled?: boolean,
  isPartial?: boolean,
  className?: string,
  otherProps?: object,
}

export interface FormDropdownElements extends FormProps{
    type?: string
    titleLabel?: string,
    className?: string,
    options: Options[],
    value?: Options,
    onSelect?: (e: React.SyntheticEvent) => void,
    customOptionsView?: JSX.Element,
    isOptionsVisible?: boolean,
    errorState?: boolean,
    errorMessage?: string,
    disabled?: boolean,
    width?: string,
    optionsPlacement?: string
    field?: string,
    isRequired?: boolean
    enableFloatingLabel?: boolean
}

export interface FormRadioElements extends FormProps {
    label: string,
    value: string,
    id?: string,
    className?: string,
    checked: boolean,
    name?: string,
    onChange?: (e: React.SyntheticEvent) => void,
    disabled?: boolean,
    tabIndex?: number,
    children?: JSX.Element | JSX.Element[]
    withHover?: boolean,
    withFocus?: boolean,
    isRequired?: boolean
}

export interface FormTextAreaElements extends FormProps {
    error?: boolean,
    errorMessage?: string,
    label?: string,
    iconLeft?: string,
    className?: string,
    disabled?: boolean,
    textareaProps?: Partial<{
        defaultValue: string,
        onBlur: (e: React.SyntheticEvent) => void,
        onFocus: (e: React.SyntheticEvent) => void,
        onChange: (e: React.SyntheticEvent) => void,
        type: string,
        value: string,
        placeholder: string,
        limit: number,
        rows: number
    }>
    errorProps?: object,
    width?: string,
    limit?: number,
    showEditButton?: boolean,
    onEditButtonClick?: (e: React.SyntheticEvent) => void,
    field?: string
    isRequired?: boolean
}

export interface FormTimePickerElements extends FormProps {
    label: string,
    onChange: (e: React.SyntheticEvent) => void,
    date?: Date
    timeIntervals?: number,
    width?: string,
    disabled?: boolean,
    intervals?: number,
    enableFloatingLabel?: boolean,
}

export interface FormCalendarElements extends FormProps {
    onDateChange?: (e: React.SyntheticEvent) => void,
    fixedHeight?: boolean,
    numberOfYears?: number,
    yearsArray?: any[],
    popperPlacement?: string
    minDate?: Date
    maxDate?: Date
    initialValue?: Date
    customInput?: (e: React.SyntheticEvent) => void,
    disabled?: boolean,
    zIndex?: number,
}
