import React from 'react'
import { FormTimePicker } from './FormTimePicker'
import Form from '../Form'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('<FormTimePicker/>', () => {
  const submitCallback = jest.fn(() => ({
    test: { 'Field Label': 'test' },
  }))
  const onChange = jest.fn(() => {})
  const formProps = {
    editMode: true,
    submitCallback,
  }
  it('should render Timepicker input correctly', () => {
    const { container } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormTimePicker
            hooks={hooks}
            formProps={formProps}
            label='Start-time'
            date={new Date('2017-01-01 00:00')}
            onChange={onChange}
            field={'Start-time'}
            enableFloatingLabel
          />
        )}
      </Form>
    )
    const input = container.querySelector('input')
    expect(input).toBeInTheDocument()
  })

  it('should render Timepicker with timelist correctly', () => {
    const { container } = render(
      <Form {...formProps}>
        {(hooks, formProps) => (
          <FormTimePicker
            hooks={hooks}
            formProps={formProps}
            label='Start-time'
            date={new Date('2017-01-01 00:00')}
            intervals={15}
            onChange={onChange}
            field={'Start-time'}
          />
        )}
      </Form>
    )
    const input = container.querySelector('input')
    fireEvent.focus(input)
    let classVal = container.querySelectorAll('li')[1]
    fireEvent.click(classVal)
    expect(input.value).toEqual('12:15 AM')
    expect(onChange).toBeCalled()
  })
})
