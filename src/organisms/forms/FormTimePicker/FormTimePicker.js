import React, { useState, useEffect } from 'react'
import { Timepicker } from '../../../atoms/timepicker/Timepicker'
import PropTypes from 'prop-types'
import { ErrorMessage } from '../../../atoms/error/Error'
import {
  assignValue,
  extractValue,
  formattedFieldName,
  extractError,
} from '../utils'

export const FormTimePicker = params => {
  const [inputValue, setInputValue] = useState(
    extractValue(params) || params?.date || ''
  )

  useEffect(() => {
    setInputValue(extractValue(params) || params?.date || '')
  }, [params, params?.date])

  let errorMessage = extractError(params)
  return (
    <>
      <Timepicker
        data-testid='form-timepicker'
        {...params}
        label={params.enableFloatingLabel ? params.label : ''}
        {...params.hooks.register(formattedFieldName(params), {
          value: inputValue,
          required: Boolean(params.isRequired),
        })}
        date={inputValue}
        disabled={!params.formProps.editMode}
        onChange={date => {
          assignValue(params, date)
          setInputValue(date)
          params?.onChange && params?.onChange(date)
        }}
      />
      {errorMessage && <ErrorMessage text={errorMessage} />}
    </>
  )
}

FormTimePicker.PropTypes = {
  hooks: PropTypes.any.isRequired,
  formProps: PropTypes.any.isRequired,
  field: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  groupName: PropTypes.string,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  date: PropTypes.date,
  timeIntervals: PropTypes.number,
  width: PropTypes.string,
  disabled: PropTypes.bool,
  intervals: PropTypes.number,
  enableFloatingLabel: PropTypes.bool,
}
