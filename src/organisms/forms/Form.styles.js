import styled from 'styled-components'

export const StyledLabel = styled.div`
  font-family: Gilroy-Medium;
  font-size: 13px;
  line-height: 16px;
  text-align: left;
  margin: auto;
  padding: 10px;
  margin-right: 80px;
  min-width: 160px;
  color: #737f8f;
`

export const StyledDropDownLabel = styled.div`
  font-family: Gilroy-Medium;
  font-size: 13px;
  line-height: 16px;
  text-align: left;
  margin-right: 80px;
  color: #737f8f;
  margin-right: 16px;
  flex: 1 1 auto;
  min-width: 20%;
`

export const StyleRow = styled.div`
  display: flex;
  height: 70px;
  left: 0px;
  top: 0px;
  border: 1px solid #e3e5e9;
  background: #ffffff;
  box-shadow: inset 0px 1px 0px #e3e5e9, inset 0px -1px 0px #e3e5e9;
`
export const InputStyleRow = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  position: relative;
  border: 1px solid #e3e5e9;
  background: #ffffff;
  border-right-width: 0px;
  border-left-width: 0px;
`

export const StyleHeader = styled.div`
  left: 2.04%;
  right: 85.6%;
  top: 4.91%;
  bottom: 91.65%;
  font-family: Gilroy-Medium;
  font-size: 18px;
  line-height: 21px;
  display: flex;
  align-items: center;
  color: #121213;
  margin-bottom: 20px;
`

export const StyleButtonRow = styled.div`
  display: flex;
  flex-direction: row-reverse;
  margin-bottom: 10px;
`
