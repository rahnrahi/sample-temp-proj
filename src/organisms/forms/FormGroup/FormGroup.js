import React from 'react'

export const FormGroup = params => {
  return <div data-testid='form-group'>{params.children(params)}</div>
}
