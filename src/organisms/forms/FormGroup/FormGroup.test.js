import React from 'react'
import { FormGroup } from './FormGroup'
import { FormInput } from '../FormInput/FormInput'
import Form from '../Form'

import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})
const primaryProps = {
  className: 'primary',
  label: 'Field Label',
  isFloatedLabel: true,
  width: '300px',
  field: 'field',
  inputProps: {
    isControlled: true,
    value: 'fabric',
  },
}

describe('<FormGroup/>', () => {
  const submitCallback = jest.fn(() => ({
    test: { 'Field Label': 'test' },
  }))
  const props = {
    editMode: true,
    submitCallback,
  }

  it('renders FormGroup component', () => {
    const { getByTestId } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormGroup hooks={hooks} formProps={formProps} groupName='test'>
            {formGroupProps => (
              <>
                <FormInput
                  {...primaryProps}
                  hooks={hooks}
                  formProps={formProps}
                  groupName={formGroupProps?.groupName || ''}
                />
                <input data-testid='submit-button' type='submit' />
              </>
            )}
          </FormGroup>
        )}
      </Form>
    )
    const input = getByTestId('form-group')
    expect(input).toBeInTheDocument()
  })

  it('submit FormGroup component', () => {
    render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormGroup hooks={hooks} formProps={formProps} groupName='test'>
            {formGroupProps => (
              <>
                <FormInput
                  {...primaryProps}
                  hooks={hooks}
                  formProps={formProps}
                  groupName={formGroupProps?.groupName || ''}
                />
                <input data-testid='submit-button' type='submit' />
              </>
            )}
          </FormGroup>
        )}
      </Form>
    )
    document.querySelector('form').onsubmit = submitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(props.submitCallback).toReturnWith({
      test: { 'Field Label': 'test' },
    })
  })
})
