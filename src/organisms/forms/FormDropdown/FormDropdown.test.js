import React from 'react'
import { FormDropdown } from './FormDropdown'
import Form from '../Form'
import { render, cleanup, fireEvent, screen } from '@testing-library/react'
import 'regenerator-runtime/runtime'
import 'jest-styled-components'

afterEach(cleanup)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('<FormDropDown/>', () => {
  const submitCallback = jest.fn(() => ({
    'Field Label': {
      id: 1,
      name: 'List Item 1',
    },
  }))
  const submitCallbackWithGroup = jest.fn(() => ({
    test: {
      'Field Label': {
        id: 1,
        name: 'List Item 1',
      },
    },
  }))
  let props = {
    editMode: true,
    submitCallback,
  }
  let groupProps = {
    editMode: true,
    submitCallback: submitCallbackWithGroup,
  }

  const options = [
    { id: 1, name: 'List Item 1' },
    { id: 2, name: 'List Item 2' },
    { id: 3, name: 'List Item 3' },
    { id: 4, name: 'List Item 4' },
    { id: 5, name: 'List Item 5' },
    { id: 6, name: 'List Item 6' },
    { id: 7, name: 'List Item 7' },
    { id: 8, name: 'List Item 8' },
    { id: 9, name: 'List Item 9' },
    { id: 10, name: 'List Item 10' },
  ]
  const dropDownProps = {
    type: 'large',
    options: [...options],
    titleLabel: 'Field Label',
    value: { id: 0, name: 'Menu Title' },
    field: 'Field Label',
    onSelect: () => {},
  }

  it('should render dropdown', () => {
    const { container, getByText } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <FormDropdown
            {...dropDownProps}
            hooks={hooks}
            formProps={formProps}
          />
        )}
      </Form>
    )
    const dropdown = container.querySelector('.content-wrapper')
    const selectedOption = container.querySelector('.option-label')
    fireEvent.click(dropdown)
    expect(getByText('List Item 1')).toBeInTheDocument()
    fireEvent.click(getByText('List Item 1'))
    expect(selectedOption).toHaveTextContent('List Item 1')
  })

  it('should submit selected dropdown value', () => {
    const { container, getByText } = render(
      <Form {...props}>
        {(hooks, formProps) => (
          <>
            <FormDropdown
              {...dropDownProps}
              hooks={hooks}
              formProps={formProps}
            />
            <input data-testid='submit-button' type='submit' />
          </>
        )}
      </Form>
    )
    const dropdown = container.querySelector('.content-wrapper')
    const selectedOption = container.querySelector('.option-label')
    fireEvent.click(dropdown)
    expect(getByText('List Item 1')).toBeInTheDocument()
    fireEvent.click(getByText('List Item 1'))
    expect(selectedOption).toHaveTextContent('List Item 1')
    document.querySelector('form').onsubmit = submitCallback
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(props.submitCallback).toReturnWith({
      'Field Label': {
        id: 1,
        name: 'List Item 1',
      },
    })
  })

  it('should submit selected dropdown value while grouped', () => {
    dropDownProps['groupName'] = 'test'
    const { container, getByText } = render(
      <Form {...groupProps}>
        {(hooks, formProps) => (
          <>
            <FormDropdown
              {...dropDownProps}
              hooks={hooks}
              formProps={formProps}
            />
            <input data-testid='submit-button' type='submit' />
          </>
        )}
      </Form>
    )
    const dropdown = container.querySelector('.content-wrapper')
    const selectedOption = container.querySelector('.option-label')
    fireEvent.click(dropdown)
    expect(getByText('List Item 1')).toBeInTheDocument()
    fireEvent.click(getByText('List Item 1'))
    expect(selectedOption).toHaveTextContent('List Item 1')
    document.querySelector('form').onsubmit = submitCallbackWithGroup
    fireEvent.submit(screen.getByTestId('submit-button'))
    expect(groupProps.submitCallback).toReturnWith({
      test: {
        'Field Label': {
          id: 1,
          name: 'List Item 1',
        },
      },
    })
  })
})
