import React, { useRef, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Dropdown } from '../../../molecules/dropdown'
import {
  assignValue,
  doesErrorExist,
  extractError,
  extractValue,
  formattedFieldName,
} from '../utils'

export const FormDropdown = params => {
  const dropdownRef = useRef(null)

  const [inputValue, setInputValue] = useState(
    extractValue(params) || params?.value || {}
  )

  useEffect(() => {
    setInputValue(extractValue(params) || params?.value)
  }, [params, params?.value])
  return (
    <Dropdown
      {...params}
      {...params.hooks.register(formattedFieldName(params), {
        value: inputValue,
        required: Boolean(params.isRequired),
      })}
      ref={dropdownRef}
      disabled={!params.formProps.editMode}
      label={!params.enableFloatingLabel && params.field}
      onSelect={val => {
        assignValue(params, val)
        setInputValue(val)
        params.hooks.clearErrors(params.field)
        params?.onSelect && params?.onSelect(val)
      }}
      value={inputValue}
      errorMessage={extractError(params)}
      errorState={doesErrorExist(params)}
    />
  )
}

FormDropdown.PropTypes = {
  type: PropTypes.string,
  titleLabel: PropTypes.string,
  className: PropTypes.string,
  options: PropTypes.array.isRequired,
  value: PropTypes.object,
  onSelect: PropTypes.func,
  customOptionsView: PropTypes.element,
  isOptionsVisible: PropTypes.bool,
  errorState: PropTypes.bool,
  errorMessage: PropTypes.string,
  disabled: PropTypes.bool,
  width: PropTypes.string,
  optionsPlacement: PropTypes.string,
  field: PropTypes.string,
  isRequired: PropTypes.bool,
  enableFloatingLabel: PropTypes.bool,
}
