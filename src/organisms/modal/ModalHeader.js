import React from 'react'
import { StyledModalHeader } from './styles'
import PropTypes from 'prop-types'
import { Button, ButtonWithIcon } from '../../atoms'
export const ModalHeader = ({ headerButtons, headerText }) =>
  (headerButtons.length > 0 || headerText) && (
    <StyledModalHeader>
      <div className='header-text'>{headerText}</div>
      <div className='buttons-section'>
        {headerButtons.map(({ isPrimary, text, onClick, ...rest }, index) =>
          !isPrimary ? (
            <ButtonWithIcon
              emphasis='high'
              iconPosition='left'
              isPrimary={false}
              onClick={onClick}
              text={text}
              theme='light'
              key={index}
              showIcon
              {...rest}
            />
          ) : (
            <Button
              data-dismiss='modal'
              aria-label='Close'
              size='small'
              onClick={onClick}
              text={text}
              key={index}
              {...rest}
            />
          )
        )}
      </div>
    </StyledModalHeader>
  )
ModalHeader.propTypes = {
  headerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
    })
  ),
  headerText: PropTypes.string,
}
