import React from 'react'
import { Modal } from './modal.example'
import ModalsDocs from '../../../docs/Modals.mdx'
import {
  MODAL_DESIGN,
  MODAL_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Modals/Small',
  component: Modal,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: [null, 'small', 'medium', 'large', 'custom'],
      },
    },
    width: {
      control: {
        type: 'text',
      },
    },
    onClose: {
      control: {
        type: 'event',
      },
    },
  },
  parameters: {
    docs: {
      page: ModalsDocs,
    },
  },
}

const Template = args => <Modal {...args} />

export const ModalSmall = Template.bind({})
ModalSmall.args = {
  showCloseButton: true,
  size: 'small',
  portalize: true,
  portalTarget: 'body',
  padding: '20px 40px 20px 40px',
  headerButtons: [],
}
ModalSmall.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: MODAL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: MODAL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ModalSmallWidthRender = Template.bind({})
ModalSmallWidthRender.args = {
  showCloseButton: true,
  size: 'small',
  portalize: true,
  portalTarget: 'body',
  padding: '20px 40px 20px 40px',
  headerButtons: [],
  render: () => <div>render data goes here a renderProp</div>,
}
ModalSmallWidthRender.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: MODAL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: MODAL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
