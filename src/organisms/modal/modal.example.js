import React, { useState } from 'react'
import ModalExample from './Modal'
import { Button } from '../../atoms'
import PropTypes from 'prop-types'
export const Modal = props => {
  const [visible, setVisible] = useState(false)

  return (
    <>
      <Button
        text='View Modal'
        onClick={() => setVisible(!visible)}
        size='small'
      />
      {visible && (
        <ModalExample
          {...props}
          onClose={() => setVisible(false)}
          onBackdropClick={() => setVisible(false)}
        />
      )}
    </>
  )
}

Modal.propTypes = {
  /**A  callback function for backdropClick event*/
  onBackdropClick: PropTypes.func,
  /** to set visibility of close Icon*/
  showCloseButton: PropTypes.bool,
  /** Custom width for Modal*/
  width: PropTypes.string,
  /** Custom Height for Modal, default is `auto`*/
  height: PropTypes.string,
  /** Padding for Modal content*/
  padding: PropTypes.string,
  /** A boolean property for portalize */
  portalize: PropTypes.bool,
  /** portal Target*/
  portalTarget: PropTypes.string,
  /** A Call back function for Close Icon*/
  onClose: PropTypes.func.isRequired,
  /** The render should be function that can be rendered as a React Component(use render prop mechanisms) */
  render: PropTypes.func.isRequired,
  /**Modal predefined size's*/
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**Footer with array of button*/
  footerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
      disabled: PropTypes.bool,
    })
  ),
  /**Header with array of button*/
  headerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
      disabled: PropTypes.bool,
    })
  ),
  /**Header Text*/
  headerText: PropTypes.string,
}

Modal.defaultProps = {
  showCloseButton: false,
  width: '444px',
  height: 'auto',
  padding: '20px',
  portalize: true,
  portalTarget: 'body',
  footerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerText: 'Header',
}
