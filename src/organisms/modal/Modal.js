import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Close } from '../../assets/images'
import { StyledModal } from './styles'
import { ModalFooter } from './ModalFooter'
import { ModalHeader } from './ModalHeader'

export default class Modal extends Component {
  componentDidMount() {
    document.body.style.overflow = 'hidden'
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto'
  }

  render() {
    return this.props.portalize
      ? ReactDOM.createPortal(
          this.renderContent(),
          document.querySelector(this.props.portalTarget)
        )
      : this.renderContent()
  }

  renderContent() {
    const {
      width,
      padding,
      onBackdropClick,
      showCloseButton,
      onClose,
      render,
      size,
      footerButtons = [],
      headerText,
      headerButtons = [],
      ...restProps
    } = this.props
    return (
      <StyledModal
        size={size}
        width={width}
        padding={padding}
        onClick={onBackdropClick}
        data-testid='modal'
        {...restProps}
      >
        <div className='modal_content' onClick={e => e.stopPropagation()}>
          {showCloseButton && (
            <div
              className='close-container'
              onClick={onClose}
              tabIndex='0'
              onKeyPress={e => e.key === 'Enter' && onClose()}
              data-testid='close-modal'
            >
              <Close />
            </div>
          )}
          <ModalHeader headerText={headerText} headerButtons={headerButtons} />
          {render && render()}
          {footerButtons.length > 0 && (
            <ModalFooter footerButtons={footerButtons} />
          )}
        </div>
      </StyledModal>
    )
  }
}

Modal.propTypes = {
  /**A  callback function for backdropClick event*/
  onBackdropClick: PropTypes.func,
  /** to set visibility of close Icon*/
  showCloseButton: PropTypes.bool,
  /** Custom width for Modal*/
  width: PropTypes.string,
  /** Custom Height for Modal, default is `auto`*/
  height: PropTypes.string,
  /** Padding for Modal content*/
  padding: PropTypes.string,
  /** A boolean property for portalize */
  portalize: PropTypes.bool,
  /** portal Target*/
  portalTarget: PropTypes.string,
  /** A Call back function for Close Icon*/
  onClose: PropTypes.func.isRequired,
  /** The render should be function that can be rendered as a React Component(use render prop mechanisms) */
  render: PropTypes.func.isRequired,
  /**Modal predefined size's*/
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**Footer with array of button*/
  footerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
      disabled: PropTypes.bool,
    })
  ),
  /**Header with array of button*/
  headerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
      disabled: PropTypes.bool,
    })
  ),
  /** Header Text */
  headerText: PropTypes.string,
}

Modal.defaultProps = {
  showCloseButton: false,
  width: '444px',
  height: 'auto',
  padding: '20px',
  portalize: true,
  portalTarget: 'body',
  footerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerText: 'Header',
}
