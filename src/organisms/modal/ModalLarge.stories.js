import React from 'react'
import { Modal } from './modal.example'
import ModalsDocs from '../../../docs/Modals.mdx'
import {
  MODAL_DESIGN,
  MODAL_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Modals/Large',
  component: Modal,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: ['large'],
      },
    },
    width: {
      control: {
        type: 'text',
      },
    },
    onClose: {
      control: {
        type: 'event',
      },
    },
  },
  parameters: {
    docs: {
      page: ModalsDocs,
    },
  },
}

const Template = args => <Modal {...args} />
export const ModalLarge = Template.bind({})
ModalLarge.args = {
  showCloseButton: true,
  size: 'large',
  portalize: true,
  portalTarget: 'body',
  padding: '20px 40px 20px 40px',
  headerButtons: [],
}
ModalLarge.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: MODAL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: MODAL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const ModalLargeWidthRender = Template.bind({})
ModalLargeWidthRender.args = {
  showCloseButton: false,
  size: 'large',
  portalize: true,
  portalTarget: 'body',
  padding: '20px 40px 20px 40px',
  footerButtons: [],
  headerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  render: () => <div>render function goes here as a renderProp</div>,
}
ModalLargeWidthRender.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: MODAL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: MODAL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
