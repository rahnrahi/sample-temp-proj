import React from 'react'

import { render, cleanup } from '@testing-library/react'
import { Modal } from './index'
import 'regenerator-runtime/runtime'

afterEach(cleanup)
const modalProps = {
  showCloseButton: true,
  width: '444px',
  height: 'auto',
  padding: '20px',
  portalize: true,
  portalTarget: 'body',
  footerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false },
    { onClick: () => null, text: 'Save', isPrimary: true },
  ],
  headerText: 'Header',
  onClose: () => null,
  render: () => null,
}
describe('<Modal/>', () => {
  it('render Modal component correctly', () => {
    const { container } = render(<Modal {...modalProps} />)
    expect(container).toMatchSnapshot()
  })

  it('render Modal component with the header if it has the header text only', () => {
    const testProps = { ...modalProps, headerButtons: [] }
    const { getByText } = render(<Modal {...testProps} />)
    const header = getByText('Header')
    expect(header).toBeInTheDocument()
  })

  it('render Modal component with the header if it has the header buttons only', () => {
    const testProps = {
      ...modalProps,
      headerText: '',
      headerButtons: [
        { onClick: () => null, text: 'Header Cancel', isPrimary: false },
        { onClick: () => null, text: 'Header Save ', isPrimary: true },
      ],
    }
    const { getByText } = render(<Modal {...testProps} />)
    const headerSaveButton = getByText('Header Save')
    expect(headerSaveButton).toBeInTheDocument()
    const headerCancelButton = getByText('Header Cancel')
    expect(headerCancelButton).toBeInTheDocument()
  })

  it('render Modal component without the header container if the header props are empty', () => {
    const testProps = {
      ...modalProps,
      headerText: '',
      headerButtons: [],
    }
    const { container } = render(<Modal {...testProps} />)
    const headerText = container.getElementsByClassName('header-text')
    const buttonSection = container.getElementsByClassName('button-section')
    expect(headerText.length).toBe(0)
    expect(buttonSection.length).toBe(0)
  })
})
