import React from 'react'
import { Modal } from './modal.example'
import ModalsDocs from '../../../docs/Modals.mdx'
import {
  MODAL_DESIGN,
  MODAL_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Modals/Custom',
  component: Modal,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: ['medium'],
      },
    },
    width: {
      control: {
        type: 'text',
      },
    },
    onClose: {
      control: {
        type: 'event',
      },
    },
  },
  parameters: {
    docs: {
      page: ModalsDocs,
    },
  },
}

const Template = args => <Modal {...args} />

export const ModalCustom = Template.bind({})
ModalCustom.args = {
  showCloseButton: true,
  size: '',
  portalize: true,
  portalTarget: 'body',
  padding: '20px 40px 20px 40px',
  headerButtons: [],
  width: '800px',
  height: '500px',
  footerButtons: [
    { onClick: () => null, text: 'Cancel', isPrimary: false, disabled: true },
    { onClick: () => null, text: 'Save', isPrimary: true, disabled: true },
  ],
  render: () => <div>render function goes here as a renderProp</div>,
}
ModalCustom.storyName = 'Modal WithCustom Width & Height'
ModalCustom.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: MODAL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: MODAL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
