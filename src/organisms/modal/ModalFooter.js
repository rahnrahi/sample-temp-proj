import React from 'react'
import { StyledModalFooter } from './styles'
import PropTypes from 'prop-types'
import { Button, ButtonWithIcon } from '../../atoms'
export const ModalFooter = ({ footerButtons, ...props }) => (
  <StyledModalFooter {...props}>
    {footerButtons.map(({ isPrimary, text, onClick, ...rest }, index) =>
      !isPrimary ? (
        <ButtonWithIcon
          emphasis='high'
          iconPosition='left'
          isPrimary={false}
          onClick={onClick}
          text={text}
          theme='light'
          key={index}
          showIcon
          {...rest}
        />
      ) : (
        <Button
          data-dismiss='modal'
          aria-label='Close'
          size='small'
          onClick={onClick}
          text={text}
          key={index}
          {...rest}
        />
      )
    )}
  </StyledModalFooter>
)
ModalFooter.propTypes = {
  footerButtons: PropTypes.arrayOf(
    PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      isPrimary: PropTypes.bool.isRequired,
    })
  ),
}
