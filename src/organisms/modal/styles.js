import styled, { css } from 'styled-components'
import theme from '../../shared/theme'

const smallCss = css`
  width: 444px;
`
const mediumCss = css`
  width: 779px;
`
const largeCss = css`
  width: 1219px;
`
const maxSizeMap = new Map()
maxSizeMap.set('small', smallCss)
maxSizeMap.set('medium', mediumCss)
maxSizeMap.set('large', largeCss)

export const ModalOverlay = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1040;
  width: 100vw;
  height: 100vh;
  background-color: #121213;
  opacity: 0.75;
`
export const StyledModalHeader = styled('div')`
  display: flex;
  justify-content: space-between;
  ${theme.typography.caption};
  margin-bottom: 2rem;
  button {
    margin-right: 5px;
    margin-left: 5px;
  }
  .header-text {
    ${theme.typography.body};
  }
  .buttons-section {
    display: flex;
  }
`
export const StyledModalFooter = styled('div')`
  display: flex;
  justify-content: flex-end;
  ${theme.typography.caption};
  margin-top: 3rem;
  button {
    margin-right: 5px;
    margin-left: 5px;
  }
`
export const ModalWrapper = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1050;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 0;
  font-family: ${theme.typography.caption.fontFamily};
  .content-wrapper {
  }
  .cross-btn {
    position: absolute;
    top: 23px;
    right: 23px;
    ${({ size }) => maxSizeMap.get(`${size}`)};
    display: flex;
    padding-right: 3.125px;
    justify-content: flex-end;
  }
  .modal {
    z-index: 100;
    background: white;
    position: relative;
    margin: 1.75rem auto;
    border-radius: 0.1875rem;
    ${({ size }) => maxSizeMap.get(`${size}`)};
  }

  .modal-header {
    display: flex;
    justify-content: flex-end;
  }

  .modal-close-button {
    display: inline-flex;
    align-items: center;
    margin-right: 8px;
    border: none;
  }

  button {
    display: block;
    padding: 0.5rem 1.25rem;
    font-size: 0.875rem;
  }
  .modal-titel {
    font-size: 1.125rem;
  }
  .modal-body {
    font-size: 0.8125rem;
    margin-top: 1.3125rem;
  }
`
export const StyledModal = styled.div`
  position: fixed;
  z-index: ${theme.zIndex.hoverOver};
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: rgba(18, 18, 19, 0.75);
  display: flex;
  justify-content: center;
  align-items: center;
  .modal_content {
    box-sizing: border-box;
    border-radius: 6px;
    position: relative;
    background: ${theme.palette.brand.primary.white};
    color: ${theme.palette.brand.primary.charcoal};
    padding: ${({ padding }) => `${padding}`};
    ${({ size, width, height }) =>
      size ? maxSizeMap.get(`${size}`) : `width: ${width}; height: ${height}`};
    min-height: 24px;
    z-index: ${theme.zIndex.modal};
    .close-container {
      cursor: pointer;
      position: absolute;
      top: 20px;
      right: 24px;
    }
  }
`
