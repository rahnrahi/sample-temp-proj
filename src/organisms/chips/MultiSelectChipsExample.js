import React, { Component } from 'react'
import { Chips } from './Chips'
import PropTypes from 'prop-types'

class MultiSelectChips extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chips: [],
    }
  }

  onChange = chips => {
    this.setState({ chips })
  }

  handleChipsClear = event => {
    event.stopPropagation()
    // Deselect all the selected chips
    this.onChange([])
  }

  render() {
    return (
      <Chips
        value={this.state.chips}
        onChange={this.onChange}
        dropdownOptions={this.props.dropdownOptions}
        dropdownWidth={this.props.dropdownWidth}
        chipsAlign={this.props.chipsAlign}
        showRemoveIcon={this.props.showRemoveIcon}
        width={this.props.width}
        label={this.props.label}
        showArrowIcon={this.props.showArrowIcon}
        dropdownPadding={this.props.dropdownPadding}
        showClearChipsButton={this.props.showClearChipsButton}
        chipsClearHandler={this.handleChipsClear}
        minimumChips={this.props.minimumChips}
        enableCheckboxes={this.props.enableCheckboxes}
        disabled={this.props.disabled}
        viewMoreModalHeaderText={this.props.viewMoreModalHeaderText}
      />
    )
  }
}

MultiSelectChips.propTypes = {
  /** chips contain width */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ),
  /** An array of data that represents the value of the chips */
  value: PropTypes.array.isRequired,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func.isRequired,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom Options usage. A function that passes the option of the dropdown as an argument, must return an element that will be rendered as each option. */
  renderDropdownOptions: PropTypes.func,
  /** A value for changing the direction chip's alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
  /** to enable Remove Icon*/
  showRemoveIcon: PropTypes.bool,
  /** A Field for Multi Select Label*/
  label: PropTypes.string,
  /** A Field to enable dropdown Icons*/
  showArrowIcon: PropTypes.bool,
  /**
   * For custom field for dropdown container padding.
   * For different padding of all sides, you can also use css style padding string as well like 14px 20px 10px 0px
   * */
  dropdownPadding: PropTypes.string,
  /** Show/Hide clear button to deselect all selected chips */
  showClearChipsButton: PropTypes.bool,
  /** Minimum number of chips to render before grouping them to 'view more' button */
  minimumChips: PropTypes.number,
  /** Enable/disable checkboxes in dropdown */
  enableCheckboxes: PropTypes.bool,
  /** Function to clear all the selected chips */
  chipsClearHandler: PropTypes.func,
  /** Enable/Disable component */
  disabled: PropTypes.bool,
  /** The Header to show on the modal that appears once we click show more*/
  viewMoreModalHeaderText: PropTypes.string,
}

MultiSelectChips.defaultProps = {
  chipsAlign: 'vertical',
  width: '100%',
  dropdownWidth: '80%',
  value: [{ label: 'label1', id: 'label1', value: 'label1' }],
  showRemoveIcon: true,
  label: 'Label',
  showArrowIcon: false,
  dropdownPadding: '14px 24px',
  disabled: false,
  viewMoreModalHeaderText: 'View More Modal Header Text',
}

export default MultiSelectChips
