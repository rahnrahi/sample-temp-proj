import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { Chips } from './Chips'
import 'jest-styled-components'
import { dropdownOptions, defaultLabel } from './constants'

describe('Chips', () => {
  it('should render MultiSelectChips correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(
      <Chips
        onChange={changeHandler}
        dropdownOptions={dropdownOptions}
        value={[]}
        showClearChipsButton={true}
        minimumChips={5}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render MultiSelectChips without checkboxes and fire onchange on option click', () => {
    const changeHandler = jest.fn()
    const { container } = render(
      <Chips
        onChange={changeHandler}
        dropdownOptions={dropdownOptions}
        value={[]}
        showClearChipsButton={true}
        minimumChips={5}
        enableCheckboxes={false}
      />
    )
    const label = screen.queryByText(defaultLabel)
    fireEvent.click(label)
    expect(container.getElementsByClassName('text-label').length).toBe(
      dropdownOptions.length
    )
    const option1 = screen.getByText(dropdownOptions[0].label)
    fireEvent.click(option1)
    expect(changeHandler).toHaveBeenCalledWith([dropdownOptions[0]])
  })

  it('should render MultiSelectChips with checkboxes', () => {
    const changeHandler = jest.fn()
    render(
      <Chips
        onChange={changeHandler}
        dropdownOptions={dropdownOptions}
        value={[]}
        showClearChipsButton={true}
        minimumChips={5}
        enableCheckboxes={true}
      />
    )
    const label = screen.queryByText(defaultLabel)
    fireEvent.click(label)
    expect(screen.getAllByRole('checkbox').length).toBe(dropdownOptions.length)
  })

  it('should renders component with custom render option', () => {
    const changeHandler = jest.fn()
    const randomTestId = `custom-option-id`
    const renderOption = option => (
      <div className='text-label' data-testid={randomTestId}>
        {option.value}
      </div>
    )
    render(
      <Chips
        onChange={changeHandler}
        dropdownOptions={dropdownOptions}
        value={[]}
        showClearChipsButton={true}
        minimumChips={5}
        renderDropdownOptions={renderOption}
      />
    )
    const label = screen.queryByText(defaultLabel)
    fireEvent.click(label)
    const options = screen.getAllByTestId(randomTestId)
    expect(options.length).toBe(dropdownOptions.length)
  })
})
