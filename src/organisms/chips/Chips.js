import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import {
  StyledMultiSelectDropdownContainer,
  StyledDropdownContainer,
  StyledChipsRenderContainer,
  StyledChipsLabelWrapper,
  StyledClearChipsIconWrapper,
  StyledViewMoreButton,
  StyledViewModalBody,
  StyledCustomDropdownRowDiv,
} from './styles'
import { Checkbox, Chip, Icon, IconButton } from '../../atoms'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import useClickOutside from '../../hooks/click-outside'
import { Modal } from '../modal'
import { v4 as uuidv4 } from 'uuid'

const Chips = ({
  value,
  onChange,
  renderChip,
  dropdownWidth,
  dropdownOptions,
  renderDropdownOptions,
  chipsAlign,
  showRemoveIcon,
  width,
  label,
  showArrowIcon,
  labelMargin,
  dropdownPadding,
  showClearChipsButton,
  chipsClearHandler,
  minimumChips,
  enableCheckboxes,
  disabled,
  viewMoreModalHeaderText,
}) => {
  const [isEnabled, setEnabled] = useState(false)
  const [selectedChips, setSelectedChips] = useState([])
  const [isModalVisible, setModalVisible] = useState(false)
  const [mDropdownOptions, setmDropdownOptions] = useState([])

  useEffect(() => {
    setSelectedChips(() => {
      return value
    })
  }, [value])

  useEffect(() => {
    setmDropdownOptions(() => {
      return dropdownOptions
    })
  }, [dropdownOptions])

  useEffect(() => {
    mDropdownOptions.length === 0 && setEnabled(false)
  }, [mDropdownOptions])

  // eslint-disable-next-line no-unused-vars
  const addChip = option => {
    if (!isEmpty(find(value, ['id', option.id]))) {
      return
    }
    let chips = [...value, option]
    onChange(chips)

    setmDropdownOptions(() => {
      return mDropdownOptions.filter(op => op.id !== option.id)
    })
  }

  const removeChip = ({ id, event }) => {
    event.stopPropagation()
    const val = value.filter(chip => chip.id === id)
    setmDropdownOptions(() => {
      return [...mDropdownOptions, val[0]]
    })
    const nextChips = value.filter(chip => chip.id !== id)
    onChange(nextChips)

    /*
			If remaining chips are less than minimum number of chips to show then
			hide modal as well because there is nothing to show in modal now
		*/
    nextChips.length <= minimumChips && setModalVisible(false)
  }

  const onChangeOption = ({ event, ...option }) => {
    event.stopPropagation()
    if (!option.optionChecked) {
      removeChip({ ...option, event })
      return
    }
    addChip(option)
  }

  const renderChips = () => {
    return selectedChips.map((chip, idx) => {
      return idx + 1 <= minimumChips
        ? React.cloneElement(renderChip(chip), {
            ...chip,
            onRemove: event => removeChip({ ...chip, idx, event }),
            index: idx,
            key: `chip${idx}`,
            ['data-testid']: `chip_${idx}`,
            tabIndex: idx,
            showRemoveIcon,
            disabled,
          })
        : idx + 1 === minimumChips + 1 && (
            <StyledViewMoreButton
              onClick={viewMoreClickHandler}
              key={`chip${idx}`}
            >{`+ View ${
              selectedChips.length - minimumChips
            } more`}</StyledViewMoreButton>
          )
    })
  }

  const renderOptions = () => {
    /*
  		1. For backward compatibility, users will get default implementation i.e
  				with checkboxes.
  		2. If a user says he/she doesn't want checkboxes then a 'enableCheckboxes'
  				flag prop can be used which will then provide user a implementation
  				without checkboxes.
  		3. If a user wants custom implementation, he/she can use
  				render props mechanism by passing a function using prop
  				'renderDropdownOptions' with his/her custom implementation
	 	*/

    return mDropdownOptions.map((option, idx) => {
      if (renderDropdownOptions) {
        // Provide custom user provided implementation
        return renderDropdownOptions(option)
      }
      if (!enableCheckboxes) {
        // Provide implementation without checkboxes
        return (
          <StyledCustomDropdownRowDiv
            {...option}
            onClick={() => addChip(option)}
          >
            <div className='text-label'>{option.value}</div>
          </StyledCustomDropdownRowDiv>
        )
      }
      return (
        // Default implementation (with checkboxes) for backward compatibility
        React.cloneElement(
          <Checkbox {...option} className='checkbox_background_hover' />,
          {
            ...option,
            checked: !isEmpty(find(value, ['id', option.id])),
            onChange: event =>
              onChangeOption({
                ...option,
                idx,
                optionChecked: event.target.checked,
                event,
              }),
            index: idx,
            key: uuidv4(),
            ['data-testid']: `option_${idx}`,
            tabIndex: idx,
          }
        )
      )
    })
  }

  const viewMoreClickHandler = event => {
    event.stopPropagation()
    setEnabled(false)
    setModalVisible(true)
  }

  const getExtraChips = () => {
    // This function will return all the remaining chips to show
    const extraChips = selectedChips.slice(minimumChips)
    const result = extraChips.map((chip, idx) => {
      return React.cloneElement(renderChip(chip), {
        ...chip,
        onRemove: event =>
          removeChip({ ...chip, idx: idx + minimumChips, event }),
        index: idx + minimumChips,
        key: `chip${idx + minimumChips}`,
        ['data-testid']: `chip_${idx + minimumChips}`,
        tabIndex: idx + minimumChips,
        showRemoveIcon,
      })
    })
    return <StyledViewModalBody>{result}</StyledViewModalBody>
  }

  const clickRef = useRef()
  useClickOutside(
    clickRef,
    () => {
      setEnabled(false)
    },
    isEnabled
  )

  const handleClearChips = event => {
    event.stopPropagation()
    setmDropdownOptions(dropdownOptions)
    chipsClearHandler && chipsClearHandler(event)
  }

  return (
    <>
      {isModalVisible && (
        <Modal
          headerText={viewMoreModalHeaderText}
          headerButtons={[]}
          footerButtons={[
            {
              text: 'Close',
              isPrimary: false,
              onClick: () => setModalVisible(false),
            },
          ]}
          onClose={() => setModalVisible(false)}
          onBackdropClick={() => setModalVisible(false)}
          render={() => getExtraChips()}
        />
      )}
      <StyledMultiSelectDropdownContainer
        ref={clickRef}
        chipsIn={value.length > 0}
        width={width}
        labelMargin={labelMargin}
        onClick={() => setEnabled(true)}
        disabled={disabled}
      >
        <StyledChipsLabelWrapper disabled={disabled}>
          <div className='floating-dropdown-container'>
            <div className='chips-label'>{label}</div>
          </div>
          {showClearChipsButton && value.length > 0 && (
            <StyledClearChipsIconWrapper className='chips-clear-button'>
              <IconButton
                icon='Close'
                isRounded
                buttonSize='24px'
                iconSize={14}
                onClick={handleClearChips}
              />
            </StyledClearChipsIconWrapper>
          )}
        </StyledChipsLabelWrapper>
        <span className='dropDown-trigger_icon'>
          {showArrowIcon && (
            <Icon
              iconName='DownArrow'
              size={16}
              onClick={() => setEnabled(!isEnabled)}
            />
          )}
        </span>
        {value.length > 0 && (
          <StyledChipsRenderContainer
            chipsAlign={chipsAlign}
            onClick={() => setEnabled(!isEnabled)}
          >
            {renderChips()}
          </StyledChipsRenderContainer>
        )}
        {isEnabled && mDropdownOptions.length > 0 && (
          <StyledDropdownContainer
            dropdownWidth={dropdownWidth}
            dropdownPadding={dropdownPadding}
          >
            {renderOptions()}
          </StyledDropdownContainer>
        )}
      </StyledMultiSelectDropdownContainer>
    </>
  )
}

Chips.propTypes = {
  /** chips contain width */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ).isRequired,
  /** An array of data that represents the value of the chips */
  value: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ).isRequired,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func.isRequired,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom Options usage. A function that passes the option of the dropdown as an argument, must return an element that will be rendered as each option. */
  renderDropdownOptions: PropTypes.func,
  /** For custom field for dropdown container padding. */
  dropdownPadding: PropTypes.string,
  /** A value for changing the direction chip's alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
  /** to enable Remove Icon*/
  showRemoveIcon: PropTypes.bool,
  /** A Field for Multi Select Label*/
  label: PropTypes.string,
  /** A Field to enable dropdown Icons*/
  showArrowIcon: PropTypes.bool,
  /** A Field for custom margin for label, default is 16px */
  labelMargin: PropTypes.string,
  /** Minimum number of chips to render before grouping them to 'view more' button */
  minimumChips: PropTypes.number,
  /** Show/Hide clear button to deselect all selected chips */
  showClearChipsButton: PropTypes.bool,
  /** Function to clear all the selected chips */
  chipsClearHandler: PropTypes.func,
  /** Enable/disable checkboxes in dropdown */
  enableCheckboxes: PropTypes.bool,
  /** Enable/Disable chips component */
  disabled: PropTypes.bool,
  /** The Header to show on the modal that appears once we click show more*/
  viewMoreModalHeaderText: PropTypes.string,
}

Chips.defaultProps = {
  width: '100%',
  dropdownWidth: '100%',
  dropdownOptions: [],
  value: [],
  labelMargin: '16px',
  onChange: () => {},
  // eslint-disable-next-line react/prop-types
  renderChip: ({ label, ...restProps }) => (
    <Chip {...restProps} className='chips-margin-bottom'>
      {label}
    </Chip>
  ),
  chipsAlign: 'vertical',
  showRemoveIcon: true,
  label: 'Label',
  showArrowIcon: false,
  dropdownPadding: '14px 24px',
  minimumChips: 5,
  enableCheckboxes: true,
  disabled: false,
  viewMoreModalHeaderText: '',
}

export { Chips }
