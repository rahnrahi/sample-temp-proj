import React, { Component } from 'react'
import { ChipsInput } from './index'
import PropTypes from 'prop-types'

class InputChips extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chips: [],
    }
  }

  onChange = chips => {
    this.setState({ chips })
  }

  handleChipsClear = event => {
    event.stopPropagation()
    // Deselect all chips
    this.onChange([])
  }

  render() {
    return (
      <div>
        <ChipsInput
          localeCode={this.props.localeCode}
          showClearChipsButton={this.props.showClearChipsButton}
          chipsClearHandler={this.handleChipsClear}
          minimumChips={this.props.minimumChips}
          value={this.state.chips}
          onChange={this.onChange}
          createChipKeys={[13]}
          viewMoreModalHeaderText={this.props.viewMoreModalHeaderText}
          errorMessage={this.props.errorMessage}
        />
      </div>
    )
  }
}
InputChips.propTypes = {
  /** A Field for Input Label*/
  label: PropTypes.string.isRequired,
  /** An array of data that represents the value of the chips */
  value: PropTypes.arrayOf(PropTypes.string).isRequired,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func,
  /** A filed for input Chips placeholder */
  placeholder: PropTypes.string,
  /** A filed for unique chips */
  uniqueChips: PropTypes.bool,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom chip key number in array */
  createChipKeys: PropTypes.array,
  /** Show/Hide clear button to deselect all selected chips */
  showClearChipsButton: PropTypes.bool,
  /** Function to clear all the selected chips */
  chipsClearHandler: PropTypes.func,
  /** Minimum number of chips to render before grouping them to 'view more' button */
  minimumChips: PropTypes.number,
  /** A field for view more modal header text */
  viewMoreModalHeaderText: PropTypes.string,
  /** A field for the error message */
  errorMessage: PropTypes.string,
  /** Locale Code to render the component in that language */
  localeCode: PropTypes.string,
}
export default InputChips
