import React from 'react'
import { render } from '@testing-library/react'
import { TextChip } from './TextChip'
import 'jest-styled-components'

describe('TextChip', () => {
  it('renders primary MultiSelectTextChips correctly with default props', () => {
    const { container } = render(<TextChip />)
    expect(container).toMatchSnapshot()
  })
})
