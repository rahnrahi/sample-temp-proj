import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import MultiSelectTextChips from './MultiSelectTextChips'
import { dropdownOptions } from './constants'
import 'jest-styled-components'

describe('MultiSelectTextChips', () => {
  it('should render primary MultiSelectTextChips correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(
      <MultiSelectTextChips onChange={changeHandler} />
    )
    expect(container).toMatchSnapshot()
  })

  it('should select all chips when the "All" checkbox is selected', () => {
    const { queryByTestId } = render(
      <MultiSelectTextChips dropdownOptions={dropdownOptions} />
    )
    let allCheckbox = queryByTestId('option_0')
    expect(allCheckbox).toBe(null)
    const dropdownTrigger = queryByTestId('dropdown-trigger-icon')
    fireEvent.click(dropdownTrigger)
    allCheckbox = queryByTestId('option_0')
    expect(allCheckbox).not.toBe(null)
    let chipsContainer = queryByTestId('chips-render-container')
    expect(chipsContainer).toBe(null)
    fireEvent.click(allCheckbox)
    chipsContainer = queryByTestId('chips-render-container')
    expect(chipsContainer.children.length).toBe(3)
    expect(chipsContainer.children[0].textContent).toBe('label1,')
    expect(chipsContainer.children[1].textContent).toBe('label2,')
    expect(chipsContainer.children[2].textContent).toBe('label3,')
  })

  it('should deselect all chips when the "All" checkbox is de-selected', () => {
    const { queryByTestId } = render(
      <MultiSelectTextChips dropdownOptions={dropdownOptions} />
    )
    const dropdownTrigger = queryByTestId('dropdown-trigger-icon')
    fireEvent.click(dropdownTrigger)
    let allCheckbox = queryByTestId('option_0')
    let chipsContainer = queryByTestId('chips-render-container')
    fireEvent.click(allCheckbox)
    chipsContainer = queryByTestId('chips-render-container')
    expect(chipsContainer.children.length).toBe(3)
    expect(chipsContainer.children[0].textContent).toBe('label1,')
    expect(chipsContainer.children[1].textContent).toBe('label2,')
    expect(chipsContainer.children[2].textContent).toBe('label3,')
    allCheckbox = queryByTestId('option_0')
    fireEvent.click(allCheckbox)
    expect(chipsContainer).not.toBeInTheDocument()
    fireEvent.click(document)
  })
  it('should fire the onBlur handler function when the component loses focus', () => {
    const blurHandler = jest.fn()
    render(
      <MultiSelectTextChips
        onBlur={blurHandler}
        dropdownOptions={dropdownOptions}
      />
    )
    const dropdownTrigger = screen.queryByTestId('dropdown-trigger-icon')
    fireEvent.click(dropdownTrigger)
    fireEvent.click(document)
    expect(blurHandler).toHaveBeenCalledTimes(1)
  })
})
