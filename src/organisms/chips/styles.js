import styled, { css, keyframes } from 'styled-components'
import { theme } from '../../shared'

export const StyledChip = styled.div`
  .chip-value {
    ${theme.typography.h6.css};
    padding-right: 10px;
  }
  svg {
    cursor: pointer;
  }
`
const floatingAnimation = keyframes`
 0% { margin-bottom: 0; }
 50% { margin-bottom: 12px; }
 100% { margin-bottom: 16px; }
`

const labelAnimation = () =>
  css`
    ${floatingAnimation} 5s infinite alternate;
  `
export const StyledInputChipsContainer = styled.div`
  .chips-values-container {
    display: flex;
    align-items: baseline;
  }

  :hover {
    .chips-clear-button {
      opacity: 1;
    }
  }

  .react-autosuggest__input {
    width: 100%;
    display: flex;
    border-right-width: 0;
    border-left-width: 0;
    border-top-width: 0;
    border-bottom-width: 1px;
    ${theme.typography.h5.css};
  }
  input:focus {
    outline-offset: 0;
    outline: none;
  }
  .chips-label {
    color: ${({ chipsIn }) =>
      chipsIn
        ? theme.palette.brand.primary.gray
        : theme.palette.brand.primary.charcoal};
    ${theme.typography.h6.css};
    margin-bottom: ${({ chipsIn }) => (chipsIn ? '16px' : '0')};
    ${({ chipsIn }) =>
      chipsIn &&
      `animation: ${labelAnimation};
		`}
  }
  cursor: text;

  .input-chip {
    margin-bottom: 15px;
  }
`
export const StyledChipsContainer = styled.div`
  .react-autosuggest__input {
    width: 100%;
    display: flex;
    border-right-width: 0;
    border-left-width: 0;
    border-top-width: 0;
    border-bottom-width: 1px;
    ${theme.typography.h5.css};
  }
  input:focus {
    outline-offset: 0;
    outline: none;
  }
  .chips-label {
    color: ${({ chipsIn }) =>
      chipsIn
        ? theme.palette.brand.primary.gray
        : theme.palette.brand.primary.charcoal};
  }
`

export const StyledMultiSelectDropdownContainer = styled.div`
  width: ${({ width }) => width};
  display: flex;
  flex-direction: column;
  border-right-width: 0;
  border-left-width: 0;
  border-top-width: 0;
  cursor: pointer;
  border-bottom-width: 1px;
  border-bottom-color: ${({ disabled }) =>
    disabled
      ? `${theme.palette.ui.neutral.grey7};`
      : `${theme.palette.brand.primary.charcoal};`};
  border-bottom-style: solid;
  ${({ disabled }) => disabled && `pointer-events: none;`};
  ${theme.typography.h6.css};
  padding-bottom: ${({ chipsIn }) => (chipsIn ? '7px' : '')};
  position: relative;
  .chips-label {
    color: ${({ chipsIn }) =>
      chipsIn
        ? theme.palette.brand.primary.gray
        : theme.palette.brand.primary.charcoal};
  }
  .chips_container {
    display: flex;
    flex-wrap: wrap;
    .chips-margin-bottom {
      margin-bottom: 8px;
    }
  }
  .dropDown-trigger_icon {
    position: absolute;
    top: ${({ chipsIn }) => (chipsIn ? '50%' : '0')};
    right: 8px;
  }
  .floating-dropdown-container {
    margin-bottom: ${({ chipsIn, labelMargin }) =>
      !chipsIn ? '16px' : labelMargin};
  }

  :hover {
    .chips-clear-button {
      opacity: 1;
    }
`
export const StyledChipsRenderContainer = styled.div`
  display: flex;
  flex-wrap: ${({ chipsAlign }) =>
    chipsAlign === 'vertical' ? 'wrap' : 'nowrap'};
  .chips-margin-bottom {
    margin-bottom: 8px;
  }
  overflow-x: ${({ chipsAlign }) =>
    chipsAlign === 'horizontal' ? 'scroll' : 'none'};
  overflow-y: ${({ chipsAlign }) =>
    chipsAlign === 'vertical' ? 'scroll' : 'none'};
  align-items: baseline;
  ::-webkit-scrollbar {
    width: 0;
    background: transparent;
  }
`
export const StyledDropdownContainer = styled.div`
  position: absolute;
  top: calc(100% + 4px);
  z-index: ${theme.zIndex.drawer};
  display: flex;
  flex-direction: column;
  width: ${({ dropdownWidth }) => dropdownWidth};
  max-height: 219px;
  background: ${theme.palette.brand.primary.white};
  border: 1px solid ${theme.palette.ui.neutral.grey2};
  ${theme.shadows.light.level2};
  border-radius: 4px;
  padding: ${({ dropdownPadding }) => dropdownPadding};
  overflow-y: scroll;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 4px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${theme.palette.brand.primary.gray};
    box-shadow: 0 0 0.0625rem rgba(255, 255, 255, 0.5);
  }
  .checkbox_background_hover {
    &:hover {
      background: ${theme.palette.brand.primary.white} !important;
    }
  }
`

export const StyledTextChip = styled.div`
  ${theme.typography.h6.css};
  padding-right: 5px;
  padding-bottom: 9px;
  color: ${theme.palette.brand.primary.charcoal};
`

export const StyledChipsLabelWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  .chips-label {
    ${({ disabled }) => disabled && `color: ${theme.palette.ui.neutral.grey7}`};
  }
`

export const StyledClearChipsIconWrapper = styled.div`
  position: relative;
  top: 4px;
  right: 4px;
  float: right;
  opacity: 0;
`

export const StyledViewMoreButton = styled.div`
  ${theme.typography.h6.css};
  color: ${theme.palette.ui.cta.blue};
  cursor: pointer;
`

export const StyledCustomDropdownRowDiv = styled.div`
  .text-label {
    padding: 9px 0px 9px 16px;
    ${theme.typography.h6.css};
    ${({ disabled }) => disabled && `color: ${theme.palette.ui.neutral.grey7}`};
  }
  ${({ disabled }) => disabled && `pointer-events: none;`};
`

export const StyledViewModalBody = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  overflow-y: scroll;
  align-items: baseline;
  align-content: flex-start;
  height: 300px;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */

  ::-webkit-scrollbar {
    display: none;
  }

  > button {
    margin-bottom: 5px;
  }
`
