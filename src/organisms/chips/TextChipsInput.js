import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  StyledMultiSelectDropdownContainer,
  StyledDropdownContainer,
  StyledChipsRenderContainer,
} from './styles'
import { Checkbox, Chip, Icon } from '../../atoms'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import useClickOutside from '../../hooks/click-outside'
import { v4 as uuidv4 } from 'uuid'

const TextChipsInput = ({
  value,
  onChange,
  onBlur,
  renderChip,
  dropdownWidth,
  dropdownOptions,
  renderDropdownOptions,
  chipsAlign,
  showRemoveIcon,
  width,
  label,
  showArrowIcon,
  labelMargin,
  dropdownPadding,
}) => {
  const [isEnabled, setEnabled] = useState(false)
  const [allChecked, setAllChecked] = useState(false)
  const mounted = useRef(false)

  useEffect(() => {
    if (mounted.current) {
      onChangeOptions({
        options: dropdownOptions,
        allChecked: allChecked,
      })
    }
    mounted.current = true
  }, [allChecked])

  // eslint-disable-next-line no-unused-vars
  const addChip = option => {
    if (!isEmpty(find(value, ['id', option.id]))) {
      return
    }
    let chips = [...value, option]
    onChange(chips)
  }

  const removeChip = ({ id, event }) => {
    event.stopPropagation()
    const nextChips = value.filter(chip => chip.id !== id)
    onChange(nextChips)
  }

  const onChangeOption = ({ event, ...option }) => {
    event.stopPropagation()
    if (!option.optionChecked) {
      removeChip({ ...option, event })
      return
    }
    addChip(option)
  }

  const onChangeOptions = ({ options, allChecked }) => {
    allChecked ? onChange(options) : onChange([])
  }

  const renderChips = () => {
    return value.map((chip, idx) => {
      return React.cloneElement(renderChip(chip), {
        ...chip,
        onRemove: event => removeChip({ ...chip, idx, event }),
        index: idx,
        key: `chip${idx}`,
        ['data-testid']: `chip_${idx}`,
        tabIndex: idx,
        showRemoveIcon,
      })
    })
  }

  const renderOptions = () => {
    const arr = dropdownOptions.map((option, idx) => {
      return React.cloneElement(renderDropdownOptions(option), {
        ...option,
        checked: !isEmpty(find(value, ['id', option.id])),
        onChange: event => {
          onChangeOption({
            ...option,
            idx: idx + 1,
            optionChecked: event.target.checked,
            event,
          })
        },
        index: idx + 1,
        key: uuidv4(),
        ['data-testid']: `option_${idx + 1}`,
        tabIndex: idx + 1,
      })
    })

    const allOption = {
      label: 'All',
      id: 'All',
      value: 'All',
    }

    const allCheckbox = React.cloneElement(renderDropdownOptions(allOption), {
      ...allOption,
      checked: allChecked,
      onChange: event => {
        event.stopPropagation()
        setAllChecked(!allChecked)
      },
      index: 0,
      key: uuidv4(),
      ['data-testid']: `option_${0}`,
      tabIndex: 0,
    })
    return [allCheckbox, ...arr]
  }

  const clickRef = useRef()
  useClickOutside(
    clickRef,
    () => {
      setEnabled(false)
      onBlur?.(clickRef.current)
    },
    isEnabled
  )

  const toggle = e => {
    e.stopPropagation()
    setEnabled(!isEnabled)
  }

  return (
    <StyledMultiSelectDropdownContainer
      ref={clickRef}
      chipsIn={value.length > 0}
      width={width}
      labelMargin={labelMargin}
    >
      <div onClick={toggle} className='floating-dropdown-container'>
        <div className='chips-label'>{label}</div>
        <span className='dropDown-trigger_icon'>
          {showArrowIcon && (
            <Icon
              iconName={'DownArrow'}
              size={16}
              data-testid='dropdown-trigger-icon'
            />
          )}
        </span>
      </div>
      {value.length > 0 && (
        <StyledChipsRenderContainer
          chipsAlign={chipsAlign}
          onClick={() => setEnabled(!isEnabled)}
          data-testid='chips-render-container'
        >
          {renderChips()}
        </StyledChipsRenderContainer>
      )}
      {dropdownOptions?.length > 0 && isEnabled && (
        <StyledDropdownContainer
          dropdownWidth={dropdownWidth}
          dropdownPadding={dropdownPadding}
        >
          {renderOptions()}
        </StyledDropdownContainer>
      )}
    </StyledMultiSelectDropdownContainer>
  )
}

TextChipsInput.propTypes = {
  /** chips contain width */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ).isRequired,
  /** An array of data that represents the value of the chips */
  value: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ).isRequired,
  /** The handler function to execute when focus is lost */
  onBlur: PropTypes.func,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func.isRequired,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom Options usage. A function that passes the option of the dropdown as an argument, must return an element that will be rendered as each option. */
  renderDropdownOptions: PropTypes.func,
  /** For custom field for dropdown container padding. */
  dropdownPadding: PropTypes.string,
  /** A value for changing the direction chip's alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
  /** to enable Remove Icon*/
  showRemoveIcon: PropTypes.bool,
  /** A Field for Multi Select Label*/
  label: PropTypes.string,
  /** A Field to enable dropdown Icons*/
  showArrowIcon: PropTypes.bool,
  /** A Field for custom margin for label, default is 16px */
  labelMargin: PropTypes.string,
}

TextChipsInput.defaultProps = {
  width: '100%',
  dropdownWidth: '100%',
  dropdownOptions: [],
  value: [],
  labelMargin: '16px',
  onChange: () => {},
  renderDropdownOptions: props => (
    <Checkbox {...props} className='checkbox_background_hover' />
  ),
  // eslint-disable-next-line react/prop-types
  renderChip: ({ label, ...restProps }) => (
    <Chip {...restProps} className='chips-margin-bottom'>
      {label}
    </Chip>
  ),
  chipsAlign: 'vertical',
  showRemoveIcon: true,
  label: 'Label',
  showArrowIcon: false,
  dropdownPadding: '14px 24px',
}

export { TextChipsInput }
