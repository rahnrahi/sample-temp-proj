import React from 'react'
import { render } from '@testing-library/react'
import { TextChips } from './TextChips'
import 'jest-styled-components'

describe('TextChips', () => {
  it('renders primary MultiSelectTextChips correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(<TextChips onChange={changeHandler} />)
    expect(container).toMatchSnapshot()
  })
})
