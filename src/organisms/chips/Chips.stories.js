import React from 'react'
import Chips from './ChipsExample'
import {
  INPUT_CHIP_DESIGN,
  INPUT_CHIP_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

export default {
  title: 'Components/Input Fields/Input Chips',
  component: Chips,
  decorators: [
    Story => <div style={{ height: '300px', width: '100%' }}>{Story()}</div>,
  ],
  argTypes: {
    label: { control: 'text' },
    placeholder: { control: 'text' },
  },
}

const Template = args => <Chips {...args} />

export const ChipsDefault = Template.bind({})
ChipsDefault.args = {
  minimumChips: 2,
  showClearChipsButton: true,
  showClearButton: true,
}
ChipsDefault.storyName = 'Input Chips'
ChipsDefault.parameters = getDesignSpecifications(
  INPUT_CHIP_DESIGN,
  INPUT_CHIP_PRESENTATION
)
