import React from 'react'
import { TextChipsInput } from './TextChipsInput'
import PropTypes from 'prop-types'

export const TextChips = ({
  value,
  onChange,
  onBlur,
  dropdownOptions,
  dropdownWidth,
  width,
  label,
  renderChip,
  dropdownPadding,
  showArrowIcon,
  chipsAlign,
}) => {
  return (
    <TextChipsInput
      value={value}
      onBlur={onBlur}
      onChange={onChange}
      dropdownOptions={dropdownOptions}
      dropdownWidth={dropdownWidth}
      chipsAlign={chipsAlign}
      showRemoveIcon={false}
      width={width}
      label={label}
      showArrowIcon={showArrowIcon}
      renderChip={renderChip}
      labelMargin='8px'
      dropdownPadding={dropdownPadding}
    />
  )
}

TextChips.propTypes = {
  /** chips contain width */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ),
  /** An array of data that represents the value of the chips */
  value: PropTypes.array.isRequired,
  /** A function called when clicked outside. */
  onBlur: PropTypes.func,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func.isRequired,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom Options usage. A function that passes the option of the dropdown as an argument, must return an element that will be rendered as each option. */
  renderDropdownOptions: PropTypes.func,
  /** A Field for Multi Select Label*/
  label: PropTypes.string,
  /** For custom field for dropdown container padding. */
  dropdownPadding: PropTypes.string,
  /** A Field to enable dropdown Icons*/
  showArrowIcon: PropTypes.bool,
  /** A value for changing the direction chip's alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
}

TextChips.defaultProps = {
  chipsAlign: 'horizontal',
  width: '100%',
  dropdownWidth: '80%',
  value: [{ label: 'label1', id: 'label1', value: 'label1' }],
  showRemoveIcon: true,
  showArrowIcon: true,
  label: 'Label',
  dropdownPadding: '14px 24px',
}
