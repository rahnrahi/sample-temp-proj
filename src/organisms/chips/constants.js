export const dropdownOptions = [
  {
    id: 'label1',
    label: 'label1',
    value: 'label1',
  },
  {
    id: 'label2',
    label: 'label2',
    value: 'label2',
  },
  {
    id: 'label3',
    label: 'label3',
    value: 'label3',
  },
]

export const defaultLabel = 'Label'
