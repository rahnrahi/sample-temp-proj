import React from 'react'
import { fireEvent, render } from '@testing-library/react'
import { Chips } from './Chips'
import 'jest-styled-components'

import { theme } from '../../shared/index'
describe('Chips', () => {
  it('renders MultiSelectChips correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(<Chips onChange={changeHandler} />)
    expect(container).toMatchSnapshot()
  })
  it('renders MultiSelectChips with arrow icon', () => {
    const changeHandler = jest.fn()
    const { container } = render(
      <Chips onChange={changeHandler} showArrowIcon={true} />
    )
    expect(container).toMatchSnapshot()
  })

  it('renders component with custom props', () => {
    const changeHandler = jest.fn()
    const { getByText } = render(
      <Chips
        label='Add new chips'
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
  })

  it('add render chips inside component', () => {
    const changeHandler = jest.fn()
    const { getByText } = render(
      <Chips
        label='Add new chips'
        value={[
          {
            id: 1,
            label: 'HELLO',
            value: 'HELLO',
          },
        ]}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
    const chipTxt = getByText('HELLO')
    expect(chipTxt).toBeInTheDocument()
  })

  it('should show disabled dropdown fields chips ', () => {
    const changeHandler = jest.fn()
    const { getByText } = render(
      <Chips
        label='Add new chips'
        dropdownOptions={[
          {
            id: 1,
            label: 'HELLO',
            value: 'HELLO',
          },
          {
            id: 2,
            label: 'WORLD',
            value: 'WORLD',
            disabled: true,
          },
        ]}
        minimumChips={3}
        enableCheckboxes={false}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
    fireEvent.click(label)
    const chipTxt = getByText('WORLD')
    expect(chipTxt).toBeInTheDocument()
    expect(chipTxt).toHaveStyle(`color: ${theme.palette.ui.neutral.grey7};`)
  })

  it('add render chips inside component', () => {
    const changeHandler = jest.fn()
    const { getByText } = render(
      <Chips
        label='Add new chips'
        value={[
          {
            id: 1,
            label: 'HELLO',
            value: 'HELLO',
          },
        ]}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
    const chipTxt = getByText('HELLO')
    expect(chipTxt).toBeInTheDocument()
  })

  it('displays viewMoreModalHeaderText prop in modal header', () => {
    const changeHandler = jest.fn()
    const { getByText, getByTestId } = render(
      <Chips
        label='Add new chips'
        value={[
          {
            id: 1,
            label: 'Chip1',
            value: 'Chip1',
          },
          {
            id: 2,
            label: 'Chip2',
            value: 'Chip2',
          },
          {
            id: 3,
            label: 'Chip3',
            value: 'Chip3',
          },
        ]}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
        viewMoreModalHeaderText='Test Header'
      />
    )
    const viewMoreButton = getByText('+ View 1 more')
    expect(viewMoreButton).toBeInTheDocument()
    fireEvent(
      viewMoreButton,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      })
    )
    const modal = getByTestId('modal')
    expect(modal).toBeInTheDocument()
    expect(modal).toBeVisible()
    const modalContent = modal.querySelector('.modal_content')
    expect(modalContent).toBeInTheDocument()
    const headerText = getByText('Test Header')
    expect(headerText).toBeInTheDocument()
  })
})
