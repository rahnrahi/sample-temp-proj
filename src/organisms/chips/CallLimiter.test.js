import { act } from '@testing-library/react'
import CallLimitier from './CallLimiter'

describe('CallLimitier', () => {
  beforeEach(() => {
    jest.clearAllTimers()
    jest.clearAllTimers()
  })

  it('should create a default object', () => {
    const limiter = new CallLimitier()
    expect(limiter).toBeTruthy()
  })

  it('should register a function', () => {
    const limiter = new CallLimitier()
    const testFn = jest.fn()
    limiter.register(testFn)
    expect(limiter.fn).toEqual(testFn)
  })

  it('should set and get the tm attribute', () => {
    const limiter = new CallLimitier()
    limiter.tm = 12
    expect(limiter.tm).toEqual(12)
  })

  it('should register a function', async () => {
    jest.useFakeTimers()
    const testFn = jest.fn()
    const limiter = new CallLimitier(testFn, 0)
    limiter.tm = 12
    await limiter.invoke()
    act(() => {
      jest.runAllTimers()
    })
    expect(testFn).toHaveBeenCalledTimes(1)
  })
})
