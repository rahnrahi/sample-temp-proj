import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import {
  StyledInputChipsContainer,
  StyledViewMoreButton,
  StyledViewModalBody,
  StyledChipsLabelWrapper,
  StyledClearChipsIconWrapper,
} from './styles'
import { Chip, IconButton, Input } from '../../atoms'
import { Modal } from '../index'

const ChipsInput = props => {
  let lastEvent = null
  const [value, setValue] = useState('')
  const [, setChips] = useState([])
  const [chipSelected, setChipSelected] = useState(false)
  const [isModalVisible, setModalVisible] = useState(false)

  const chipsContainerRef = useRef()
  const inputRef = useRef()

  const onClick = () => {
    inputRef.current.focus()
  }

  const onBlur = () => {
    inputRef.current.blur()
  }

  const onFocus = () => {
    inputRef.current.focus()
  }

  const handleKeyDown = e => {
    if (e.keyCode === 13 && lastEvent === e) {
      lastEvent = null
      return
    }
    if (
      props.createChipKeys.includes(e.keyCode) ||
      props.createChipKeys.includes(e.key)
    ) {
      e.preventDefault()
      if (value.trim()) addChip(value)
    }
    if (e.keyCode === 8) {
      onBackspace()
    } else if (chipSelected) {
      setChipSelected(false)
    }
  }

  const onBackspace = () => {
    if (value === '' && props.value.length > 0) {
      // eslint-disable-next-line no-constant-condition
      if (chipSelected) {
        const nextChips = props.value.slice(0, -1)
        setChipSelected(false)
        setChips(nextChips)
        props.onChange(nextChips)
      } else {
        setChipSelected(true)
      }
    }
  }

  const addChip = value => {
    if (props.uniqueChips && props.value.indexOf(value) !== -1) {
      setValue(value)
      return
    }
    let chips = [...props.value, value]
    props.onChange(chips)
    setValue('')
  }

  const removeChip = idx => () => {
    let left = props.value.slice(0, idx)
    let right = props.value.slice(idx + 1)
    const nextChips = [...left, ...right]
    props.onChange(nextChips)
    /*
			If remaining chips are less than minimum number of chips to show then
			hide modal as well because there is nothing to show in modal now
		*/
    nextChips.length <= props.minimumChips && setModalVisible(false)
  }

  const viewMoreClickHandler = () => setModalVisible(true)

  const renderChips = () => {
    return props.value.map((chip, idx) => {
      // Show chips below threshold i.e, minimum num of chips to show, as they are.
      return idx + 1 <= props.minimumChips
        ? React.cloneElement(props.renderChip(chip), {
            onRemove: removeChip(idx),
            // eslint-disable-next-line react/prop-types
            value: props.children,
            index: idx,
            key: `chip${idx}`,
          })
        : // Show first extra chips as "view more" button and ignore remaining ones
          idx + 1 === props.minimumChips + 1 && (
            <StyledViewMoreButton
              onClick={viewMoreClickHandler}
              key={`chip${idx}`}
            >{`+ View ${
              props.value.length - props.minimumChips
            } more`}</StyledViewMoreButton>
          )
    })
  }

  const getExtraChips = () => {
    // This function will return all the remaining chips to show
    const extraChips = props.value.slice(props.minimumChips)
    const result = extraChips.map((chip, idx) => {
      return React.cloneElement(props.renderChip(chip), {
        onRemove: removeChip(idx + props.minimumChips),
        // eslint-disable-next-line react/prop-types
        value: props.children,
        index: idx + props.minimumChips,
        key: `chip${idx + props.minimumChips}`,
      })
    })
    return <StyledViewModalBody>{result}</StyledViewModalBody>
  }

  const handleClearChips = event => {
    event.stopPropagation()
    props.chipsClearHandler && props.chipsClearHandler(event)
  }

  const onChange = e => {
    if (
      e.target.value.indexOf(',') !== -1 &&
      props.createChipKeys.includes(9)
    ) {
      let chips = e.target.value
        .split(',')
        .map(val => val.trim())
        .filter(val => val !== '')
      chips.forEach(chip => {
        addChip(chip)
      })
    } else {
      setValue(e.target.value)
    }
  }

  const { placeholder, label } = props

  const inputProps = {
    placeholder,
    value,
    onChange,
    onKeyDown: handleKeyDown,
    onBlur,
    onFocus,
  }

  return (
    <>
      {isModalVisible && (
        <Modal
          headerText={props.viewMoreModalHeaderText}
          headerButtons={[]}
          footerButtons={[
            {
              text: 'Close',
              isPrimary: false,
              onClick: () => setModalVisible(false),
            },
          ]}
          onClose={() => setModalVisible(false)}
          onBackdropClick={() => setModalVisible(false)}
          render={() => getExtraChips()}
        />
      )}
      <StyledInputChipsContainer
        ref={chipsContainerRef}
        className='chipsContainer'
        onClick={() => {
          onClick()
        }}
      >
        <StyledChipsLabelWrapper>
          <div className='chips-label'>{label}</div>
          {props.showClearChipsButton && props.value.length > 0 && (
            <StyledClearChipsIconWrapper className='chips-clear-button'>
              <IconButton
                icon='Close'
                isRounded
                buttonSize='24px'
                iconSize={14}
                onClick={handleClearChips}
              />
            </StyledClearChipsIconWrapper>
          )}
        </StyledChipsLabelWrapper>
        <div className='chips-values-container'>{renderChips()}</div>
        <Input
          localeCode={props.localeCode}
          error={props.errorMessage}
          errorMessage={props.errorMessage}
          ref={inputRef}
          {...inputProps}
        />
      </StyledInputChipsContainer>
    </>
  )
}

ChipsInput.propTypes = {
  /** A Field for Input Label*/
  label: PropTypes.string.isRequired,
  /** An array of data that represents the value of the chips */
  value: PropTypes.arrayOf(PropTypes.string).isRequired,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func,
  /** A filed for input Chips placeholder */
  placeholder: PropTypes.string,
  /** A filed for unique chips */
  uniqueChips: PropTypes.bool,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom chip key number in array */
  createChipKeys: PropTypes.array,
  /** Minimum number of chips to render before grouping them to 'view more' button */
  minimumChips: PropTypes.number,
  /** Show/Hide clear button to deselect all selected chips */
  showClearChipsButton: PropTypes.bool,
  /** Function to clear all the selected chips */
  chipsClearHandler: PropTypes.func,
  /** A field for view more modal header text */
  viewMoreModalHeaderText: PropTypes.string,
  /** A field for the error message */
  errorMessage: PropTypes.string,
  /** Locale Code to render the component in that language */
  localeCode: PropTypes.string,
}

ChipsInput.defaultProps = {
  label: 'Input Chips',
  placeholder: '',
  createChipKeys: [9],
  uniqueChips: true,
  value: [],
  onChange: () => {},
  renderChip: (value, customTheme) => (
    <Chip theme={customTheme} className='input-chip'>
      {value}
    </Chip>
  ),
  getChipValue: item => item,
  minimumChips: 5,
  localeCode: '',
}

export { ChipsInput }
