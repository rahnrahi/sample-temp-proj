import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { ChipsInput } from './ChipsInput'
import 'jest-styled-components'

describe('ChipsInput', () => {
  it('renders primary ChipsInput correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(<ChipsInput onChange={changeHandler} />)
    expect(container).toMatchSnapshot()
  })

  it('should allow to add more chips using the text input component', () => {
    const changeHandler = jest.fn()
    const { getByText } = render(
      <ChipsInput
        label='Add new chips'
        value={['HELLO']}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
    const chipTxt = getByText('HELLO')
    expect(chipTxt).toBeInTheDocument()
  })

  it('should allow to add more chips using the text input component', () => {
    const changeHandler = jest.fn()
    const { getByText, getByDisplayValue, container } = render(
      <ChipsInput
        label='Add new chips'
        value={['HELLO']}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const label = getByText('Add new chips')
    expect(label).toBeInTheDocument()
    const chipTxt = getByText('HELLO')
    expect(chipTxt).toBeInTheDocument()
    const textInput = container.querySelector('input')
    fireEvent.change(textInput, { target: { value: 'BYE' } })
    fireEvent.keyPress(textInput, { key: 'Enter', code: 13, charCode: 13 })
    const chipTxtBye = getByDisplayValue('BYE')
    expect(chipTxtBye).toBeInTheDocument()
  })

  it('should display error message', () => {
    const changeHandler = jest.fn()
    const value = []
    const { getByText } = render(
      <ChipsInput
        label='Add new chips'
        value={value}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
        errorMessage='No Chips Are Present'
      />
    )
    const errorMessage = getByText('No Chips Are Present')
    expect(errorMessage).toBeInTheDocument()
  })

  it('should not display error message', () => {
    const changeHandler = jest.fn()
    const value = ['Chip1']
    const { queryByText } = render(
      <ChipsInput
        label='Add new chips'
        value={value}
        minimumChips={2}
        showClearChipsButton={true}
        showClearButton={true}
        onChange={changeHandler}
      />
    )
    const errorMessage = queryByText('No Chips Are Present')
    expect(errorMessage).toBe(null)
  })
})
