import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TextChip } from './TextChip'
import { TextChips } from './TextChips'

class MultiSelectTextChips extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chips: [],
    }
  }

  onChange = chips => {
    this.setState({ chips })
  }

  renderChip = ({ value }) => {
    return <TextChip>{value}</TextChip>
  }

  render() {
    return (
      <TextChips
        value={this.state.chips}
        onChange={this.onChange}
        onBlur={this.props.onBlur}
        dropdownOptions={this.props.dropdownOptions}
        dropdownWidth={this.props.dropdownWidth}
        chipsAlign={'horizontal'}
        showRemoveIcon={this.props.showRemoveIcon}
        width={this.props.width}
        label={this.props.label}
        showArrowIcon={this.props.showArrowIcon}
        renderChip={this.renderChip}
        dropdownPadding={this.props.dropdownPadding}
      />
    )
  }
}

MultiSelectTextChips.propTypes = {
  /** chips contain width */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ),
  /** An array of data that represents the value of the chips */
  value: PropTypes.array.isRequired,
  /** A function called when clicked outside, passes parent element of dropdown as argument. */
  onBlur: PropTypes.func,
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func.isRequired,
  /** For custom chip usage. A function that passes the value of the chip as an argument, must return an element that will be rendered as each chip. */
  renderChip: PropTypes.func,
  /** For custom Options usage. A function that passes the option of the dropdown as an argument, must return an element that will be rendered as each option. */
  renderDropdownOptions: PropTypes.func,
  /** A value for changing the direction chip's alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
  /** to enable Remove Icon*/
  showRemoveIcon: PropTypes.bool,
  /** A Field for Multi Select Label*/
  label: PropTypes.string,
  /** For custom field for dropdown container padding. */
  dropdownPadding: PropTypes.string,
  /** A Field to enable dropdown Icons*/
  showArrowIcon: PropTypes.bool,
}

MultiSelectTextChips.defaultProps = {
  chipsAlign: 'vertical',
  width: '100%',
  dropdownWidth: '80%',
  value: [{ label: 'label1', id: 'label1', value: 'label1' }],
  showRemoveIcon: true,
  label: 'Label',
  dropdownPadding: '14px 24px',
  showArrowIcon: true,
}

export default MultiSelectTextChips
