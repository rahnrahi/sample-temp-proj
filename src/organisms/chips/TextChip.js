import React from 'react'
import PropTypes from 'prop-types'
import { StyledTextChip } from './styles'

export const TextChip = ({ value }) => (
  <StyledTextChip>{value ? `${value},` : ''}</StyledTextChip>
)

TextChip.propTypes = {
  value: PropTypes.oneOf([PropTypes.string, PropTypes.number, PropTypes.bool]),
}
