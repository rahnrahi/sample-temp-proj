import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { TextChipsInput } from './TextChipsInput'
import { dropdownOptions, defaultLabel } from './constants'
import 'jest-styled-components'

describe('TextChipsInput', () => {
  it('renders primary TextChipsInput correctly with default props', () => {
    const changeHandler = jest.fn()
    const { container } = render(<TextChipsInput onChange={changeHandler} />)
    expect(container).toMatchSnapshot()
  })

  it('should fire the onchange handler when option is clicked', () => {
    const changeHandler = jest.fn()
    render(
      <TextChipsInput
        onChange={changeHandler}
        dropdownOptions={dropdownOptions}
      />
    )
    const label = screen.queryByText(defaultLabel)
    fireEvent.click(label)
    const option1 = screen.getByText(dropdownOptions[0].label)
    fireEvent.click(option1)
    expect(changeHandler).toHaveBeenCalledTimes(1)
  })
})
