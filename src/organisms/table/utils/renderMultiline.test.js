import { renderMultiline } from './renderMultiline'
import React from 'react'
import PropTypes from 'prop-types'
import { render, screen } from '@testing-library/react'

const rowData = { id: 1, arrayCol: ['firstRow', 'secondRow'] }
const TestCellItemComponent = ({ children }) => (
  <div data-testid={'test-cell-item'}>{children}</div>
)
TestCellItemComponent.propTypes = {
  children: PropTypes.node,
}
const TestCellItemComponent2 = ({ children }) => (
  <div data-testid={'test-cell-item-2'}>{children}</div>
)
TestCellItemComponent2.propTypes = {
  children: PropTypes.node,
}

describe('renderMultiline', () => {
  it('should allow access by lodash like path', () => {
    const renderFunction = renderMultiline(
      ['arrayCol'],
      [TestCellItemComponent]
    )
    render(renderFunction(rowData))
    expect(screen.getByTestId('test-cell-item')).toBeInTheDocument()
    expect(screen.getByText('firstRow')).toBeInTheDocument()
    expect(screen.getByText('secondRow')).toBeInTheDocument()
  })
  it('should allow access by custom accessor', () => {
    const renderFunction = renderMultiline(rowData => rowData.arrayCol, [
      TestCellItemComponent,
    ])
    render(renderFunction(rowData))
    expect(screen.getByTestId('test-cell-item')).toBeInTheDocument()
    expect(screen.getByText('firstRow')).toBeInTheDocument()
    expect(screen.getByText('secondRow')).toBeInTheDocument()
  })
  describe('Given wrong accessor type', () => {
    it('should render empty', () => {
      const renderFunction = renderMultiline(123, [TestCellItemComponent])
      const { container } = render(renderFunction(rowData))
      expect(container).toBeEmptyDOMElement()
    })
  })
  describe('Given non array data', () => {
    it('should render cell using first given component', () => {
      const renderFunction = renderMultiline('id', [TestCellItemComponent])
      render(renderFunction(rowData))
      expect(screen.getByTestId('test-cell-item')).toBeInTheDocument()
      expect(screen.getByText('1')).toBeInTheDocument()
    })
  })
  describe('Components array is shorter than cell data array', () => {
    it('should use Fragment for out of the bound items', () => {
      const renderFunction = renderMultiline('arrayCol', [])
      render(renderFunction(rowData))
      expect(screen.queryByTestId('test-cell-item')).not.toBeInTheDocument()
      expect(screen.getByText('firstRowsecondRow')).toBeInTheDocument()
    })
  })
  it('it should allow usage of element in components array', () => {
    const renderFunction = renderMultiline(
      ['arrayCol'],
      [<TestCellItemComponent key={0} />]
    )
    render(renderFunction(rowData))
    expect(screen.getByTestId('test-cell-item')).toBeInTheDocument()
    expect(screen.getByText('firstRow')).toBeInTheDocument()
    expect(screen.getByText('secondRow')).toBeInTheDocument()
  })
})
