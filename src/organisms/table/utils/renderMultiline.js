import get from 'lodash/get'
import React, { cloneElement, Fragment, isValidElement } from 'react'

const pathDataAccessor = path => rowData => get(rowData, path)

export const renderMultiline = (dataAccessorOrPath, componentsArr) => {
  if (
    typeof dataAccessorOrPath !== 'string' &&
    !Array.isArray(dataAccessorOrPath) &&
    typeof dataAccessorOrPath !== 'function'
  ) {
    return () => null
  }
  const accessor =
    typeof dataAccessorOrPath === 'function'
      ? dataAccessorOrPath
      : pathDataAccessor(dataAccessorOrPath)
  return rowData => {
    const cellData = accessor(rowData)
    const normalizedCellData = Array.isArray(cellData) ? cellData : [cellData]
    return normalizedCellData.map((item, idx) => {
      const Component = componentsArr[idx]
      if (isValidElement(Component)) {
        return cloneElement(Component, { key: idx, children: item })
      }
      if (typeof Component === 'function') {
        return <Component key={idx}>{item}</Component>
      }
      return <Fragment key={idx}>{item}</Fragment>
    })
  }
}
