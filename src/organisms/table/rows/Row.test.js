import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import { Row } from './Row'

afterEach(cleanup)

describe('<Row/>', () => {
  const rowTestId = 'row-0'
  const rowClickHandler = jest.fn()
  const renameHandler = jest.fn()
  const rowCollapseHandler = jest.fn()
  const mouseEnterHandler = jest.fn()
  const mouseLeaveHandler = jest.fn()
  const rowSelectHandler = jest.fn()
  const checkColumnHandler = jest.fn()

  let columns
  let data

  beforeEach(() => {
    columns = [
      {
        title: 'Name',
        accessor: 'name',
        isSortable: true,
      },
      {
        title: 'Updated',
        accessor: 'updated',
        isSortable: false,
      },
      {
        title: 'Status',
        accessor: 'status',
      },
    ]

    data = {
      name: 'Oldest item',
      updated: '2023-01-01',
      status: 'ACTIVE',
      _isCheck: true,
      _isDisable: true,
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  const renderRow = ({ isClickable }) => {
    return render(
      <Row
        columns={columns}
        data={data}
        collapse={[true, false, false]}
        index={0}
        cellClassName='cell'
        isSelectable={true}
        rowClassName='row'
        collapseOnRowClick={false}
        showIndex={true}
        handleRowClick={rowClickHandler}
        handleRename={renameHandler}
        handleRowCollapse={rowCollapseHandler}
        handleRowMouseEnter={mouseEnterHandler}
        handleRowMouseLeave={mouseLeaveHandler}
        handleRowSelection={rowSelectHandler}
        checkColumnHasChild={checkColumnHandler}
        isClickable={isClickable}
      />
    )
  }

  it('should render the Row with custom props', () => {
    const { getByText } = renderRow({ isClickable: true })

    const nameCell = getByText('Oldest item')
    expect(nameCell).toBeInTheDocument()
    const updatedCell = getByText('2023-01-01')
    expect(updatedCell).toBeInTheDocument()
    const statusCell = getByText('ACTIVE')
    expect(statusCell).toBeInTheDocument()
  })

  it('should be able to have a disabled and checked column', () => {
    const { getByTestId } = renderRow({ isClickable: false })

    const checkbox = getByTestId('select-checkbox-0')
    expect(checkbox).toBeInTheDocument()
    fireEvent.click(checkbox)
    expect(checkColumnHandler).toHaveBeenCalledTimes(1)
  })

  it('should show a pointer if the row is clickable', () => {
    const { getByTestId } = renderRow({ isClickable: true })

    const row = getByTestId(rowTestId)

    fireEvent.mouseOver(row)

    expect(row).toHaveStyle('cursor: pointer;')
  })

  it('should not show a pointer if the row is not clickable', () => {
    const { getByTestId } = renderRow({ isClickable: false })

    const row = getByTestId(rowTestId)

    fireEvent.mouseOver(row)

    expect(row).not.toHaveStyle('cursor: pointer;')
  })
})
