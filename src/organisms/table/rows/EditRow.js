import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Input, ButtonWithIcon } from '../../../atoms'
import { StyledRow, StyledCell, StyledEditBox } from '../Table.style'

export const EditRow = ({
  data,
  columns,
  showIndex,
  editText,
  setEditText,
  handleEditSuccess,
  handleEditCancel,
  isEditRow,
}) => {
  useEffect(() => {
    setEditText(data[columns[0].accessor])
  }, [data])
  return (
    <StyledRow isEditRow={isEditRow}>
      <StyledCell colSpan={showIndex ? columns.length + 2 : columns.length + 1}>
        <StyledEditBox>
          <Input
            data-testid='edit-input'
            className='input-wrapper'
            inputProps={{
              value: editText,
              className: 'edit-input',
              boxed: true,
            }}
            onChange={e => setEditText(e.target.value)}
          />
          <span>
            <ButtonWithIcon
              data-testid='edit-success-btn'
              className='success-btn'
              icon='Checkmark'
              isRoundIcon
              showIcon
              onClick={e => handleEditSuccess(e, data, editText)}
            />
          </span>
          <span>
            <ButtonWithIcon
              data-testid='edit-cancel-btn'
              className='cancel-btn'
              icon='Close'
              isRoundIcon
              isPrimary={false}
              showIcon
              onClick={e => handleEditCancel(e, data, editText)}
            />
          </span>
        </StyledEditBox>
      </StyledCell>
    </StyledRow>
  )
}
EditRow.propTypes = {
  data: PropTypes.object,
  columns: PropTypes.array,
  showIndex: PropTypes.bool,
  editText: PropTypes.string,
  setEditText: PropTypes.func,
  handleEditSuccess: PropTypes.func,
  handleEditCancel: PropTypes.func,
  isEditRow: PropTypes.bool,
}
