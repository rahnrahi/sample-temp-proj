import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { v4 as uuidv4 } from 'uuid'
import { Checkbox } from '../../../atoms'

import { StyledRow, StyledCell, CheckboxWrapper } from '../Table.style'
import { DownArrow } from '../../../assets/images'

export const Row = ({
  columns,
  showIndex,
  isClickable,
  isSelectable,
  collapse,
  collapseOnRowClick,
  data,
  index,
  checkColumnHasChild,
  handleRowClick,
  handleRowCollapse,
  handleRowMouseEnter,
  handleRowMouseLeave,
  handleRowSelection,
  handleRename,
  rowClassName,
  cellClassName,
}) => {
  const handleOnRowClick = useCallback(
    e => {
      handleRowClick(e, data)
      collapseOnRowClick && handleRowCollapse(index)
    },
    [data, collapseOnRowClick, handleRowClick, handleRowCollapse]
  )

  const handleOnRowCollapse = useCallback(
    e => {
      if (!collapseOnRowClick) {
        e.stopPropagation()
        handleRowCollapse(index)
      }
    },
    [collapseOnRowClick, handleRowCollapse]
  )

  return (
    <StyledRow
      data-testid={`row-${index}`}
      className={rowClassName}
      showOutline={collapse[index] === true}
      onClick={handleOnRowClick}
      onMouseEnter={e => handleRowMouseEnter(e, index)}
      onMouseLeave={e => handleRowMouseLeave(e, index)}
      isClickable={isClickable}
    >
      {showIndex && (
        <StyledCell
          isSelectable={isSelectable}
          showIndex={showIndex}
          className={cellClassName}
          onClick={e => e.stopPropagation()}
        >
          {isSelectable ? (
            <CheckboxWrapper>
              <Checkbox
                name={uuidv4()}
                checked={data._isCheck}
                value={'' + index}
                onChange={e => handleRowSelection(e, index)}
                data-testid={`select-checkbox-${index}`}
                disabled={data._isDisable}
                isPartial={data._isPartial ? data._isPartial : false}
              />
            </CheckboxWrapper>
          ) : (
            <span>{index + 1}</span>
          )}
        </StyledCell>
      )}
      {collapse.length !== 0 && (
        <StyledCell
          data-testid={`expansion-arrow-${index + 1}`}
          className={`expansion-cell ${cellClassName} ${
            collapse[index] && 'rotate-caret'
          }`}
        >
          {checkColumnHasChild(index) && (
            <DownArrow
              data-testid={`collapse-chevron-${index + 1}`}
              onClick={handleOnRowCollapse}
            />
          )}
        </StyledCell>
      )}

      {columns.map(({ accessor, render }, j) => {
        return render !== undefined ? (
          <StyledCell
            key={j}
            className={cellClassName}
            isCollapse={collapse.length !== 0}
          >
            {render(data, handleRename)}
          </StyledCell>
        ) : (
          <StyledCell
            key={j}
            className={cellClassName}
            isCollapse={collapse.length !== 0}
          >
            {data[accessor]}
          </StyledCell>
        )
      })}
    </StyledRow>
  )
}

Row.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object),
  showIndex: PropTypes.bool,
  isSelectable: PropTypes.bool,
  collapse: PropTypes.array,
  collapseOnRowClick: PropTypes.bool,
  data: PropTypes.object,
  index: PropTypes.number,
  checkColumnHasChild: PropTypes.func,
  handleRowClick: PropTypes.func,
  handleRowCollapse: PropTypes.func,
  handleRowMouseEnter: PropTypes.func,
  handleRowMouseLeave: PropTypes.func,
  handleRowSelection: PropTypes.func,
  handleRename: PropTypes.func,
  rowClassName: PropTypes.string,
  cellClassName: PropTypes.string,
  isClickable: PropTypes.bool,
}
