import React from 'react'
import { Checkbox } from '../../../atoms'
import { StyledRow, StyledCell, CheckboxWrapper } from '../Table.style'

export const ChildRow = ({
  columns,
  data,
  isCollapse,
  showIndex,
  isClickable,
  isSelectable,
  isParentSelected,
  parentIndex,
  handleChildRowSelection,
  rowClassName,
  cellClassName,
  handleRowClick,
  onRowMouseEnter,
  onRowMouseLeave,
}) => {
  const isChild = true
  const handleRowMouseEnter = (e, obj) => {
    typeof onRowMouseEnter === 'function' && onRowMouseEnter(e, obj, isChild)
  }

  const handleRowMouseLeave = (e, obj) => {
    typeof onRowMouseLeave === 'function' && onRowMouseLeave(e, obj, isChild)
  }

  return data.children.map((obj1, childIndex) => (
    <StyledRow
      key={childIndex}
      className={`child-row ${rowClassName}`}
      isClickable={isClickable}
      isLastChild={childIndex === data.children.length - 1}
      onClick={e => handleRowClick(e, obj1)}
      onMouseEnter={e => handleRowMouseEnter(e, obj1)}
      onMouseLeave={e => handleRowMouseLeave(e, obj1)}
    >
      {showIndex && !isSelectable && <StyledCell></StyledCell>}
      {isSelectable ? (
        <StyledCell className={cellClassName}>
          <CheckboxWrapper onClick={e => e.stopPropagation()}>
            <Checkbox
              value={'' + childIndex}
              checked={!obj1._isDisable && (obj1._isCheck || isParentSelected)}
              onChange={e => {
                handleChildRowSelection(
                  e,
                  { ...obj1, _isCheck: e.target.checked },
                  parentIndex,
                  childIndex
                )
              }}
              disabled={obj1._isDisable}
            />
          </CheckboxWrapper>
        </StyledCell>
      ) : (
        ''
      )}
      {isCollapse && (
        <StyledCell className={`expansion-cell ${cellClassName}`}></StyledCell>
      )}
      {columns.map(obj2 => {
        return (
          obj2.children &&
          obj2.children.map(({ accessor, render }, m) => {
            return render !== undefined ? (
              <StyledCell key={m} className={cellClassName}>
                {render(obj1)}
              </StyledCell>
            ) : (
              <StyledCell key={m} className={cellClassName}>
                {obj1[accessor]}
              </StyledCell>
            )
          })
        )
      })}
    </StyledRow>
  ))
}
