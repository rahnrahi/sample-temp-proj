import React from 'react'
import styled from 'styled-components'
import { v4 as uuidv4 } from 'uuid'

import { Tooltip } from '../../atoms'

const StyledTitle = styled.div`
  width: 250px;
`

const StyledType = styled.div`
  width: 200px;
`

const StyledStatus = styled.div`
  width: 300px;
  span {
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .tooltip {
    right: 12px;
  }
`

export const pimColumns = [
  {
    title: 'Attribute title',
    accessor: 'title',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: ({ title }) => <StyledTitle>{title}</StyledTitle>,
  },
  {
    title: 'Type',
    accessor: 'type',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: ({ type }) => <StyledType>{type}</StyledType>,
  },
  {
    title: 'Status',
    accessor: 'status',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: ({ status, _isDisable, show }) => {
      console.log('show', show)
      return (
        <StyledStatus>
          <span>{status}</span>
          {_isDisable && (
            <Tooltip
              className='tooltip'
              showTooltip={show}
              isExternalEvent={true}
              text='This is a long status.This is a long status.This is a long status.This is a long status.This is a long status.'
            />
          )}
        </StyledStatus>
      )
    },
  },
]

export const pimData = [
  {
    uid: uuidv4(),
    title: 'vendor',
    type: 'Boolean',
    status: '',
  },
  {
    uid: uuidv4(),
    title: 'Vendor Part ID',
    type: 'Dynamic list of attributes',
    status: '',
  },
  {
    uid: uuidv4(),
    title: 'Vendor GLN',
    type: 'Date',
    status: 'This is a long status This is a long status This is a long status',
    _isDisable: true,
  },
  {
    uid: uuidv4(),
    title: 'Product Short Description',
    type: 'List of values',
    status: '',
  },
  {
    uid: uuidv4(),
    title: 'Manufacture type',
    type: 'Text',
    status: '',
  },
  {
    uid: uuidv4(),
    title: 'Country of Origin',
    type: 'Boolean',
    status: 'Used by a different group',
  },
  {
    uid: uuidv4(),
    title: 'UPC Code',
    type: 'List of values',
    status: '',
  },
]
