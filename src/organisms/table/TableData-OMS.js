import { v4 as uuidv4 } from 'uuid'

export const omsColumns = [
  {
    title: 'SKU ID',
    accessor: 'sku',
    isSortable: true,
  },

  {
    title: 'Product Name',
    accessor: 'product_name',
    isSortable: false,
  },
  {
    title: '$/unit',
    accessor: 'unit_price',
    isSortable: true,
  },
  {
    title: 'Qty',
    accessor: 'quantity',
    isSortable: true,
  },
  {
    title: 'Total Price',
    accessor: 'total_price',
    isSortable: true,
  },
]

export const omsData = [
  {
    uid: uuidv4(),
    sku: '1538910',
    product_name: 'a DNA Luminous Cushion Lagoan',
    unit_price: 71.79,
    quantity: 9,
    total_price: 646.11,
  },
  {
    uid: uuidv4(),
    sku: '1538911',
    product_name: 'b DNA Luminous Cushion Lagoan',
    unit_price: 72.79,
    quantity: 12,
    total_price: 873.48,
  },
  {
    uid: uuidv4(),
    sku: '1538912',
    product_name: 'c DNA Luminous Cushion Lagoan',
    unit_price: 60.79,
    quantity: 6,
    total_price: 364.74,
  },
  {
    uid: uuidv4(),
    sku: '',
    product_name: 'Total',
    unit_price: '',
    quantity: 27,
    total_price: 1884.33 + ' USD',
  },
]
