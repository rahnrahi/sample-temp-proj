import React, { useState, useMemo, useEffect } from 'react'
import PropTypes from 'prop-types'

import { Link } from '../../../atoms'
import {
  StyledPaginationNav,
  StyledPagination,
  StyledPages,
  StyledPageNumber,
  StyledNavText,
} from './Pagination.style'
import { LeftArrow, RightArrow } from '../../../assets/images'

const renderPages = (currentIndex, arr) => {
  let length = arr.length
  if (arr.length >= 8) {
    if (currentIndex < 4) {
      return [1, 2, 3, 4, '...', arr[length - 1]]
    }
    if (currentIndex >= 4 && currentIndex <= arr.length - 3) {
      return [
        1,
        '...',
        currentIndex - 1,
        currentIndex,
        currentIndex + 1,
        '...',
        arr[length - 1],
      ]
    }
    return [
      1,
      '...',
      arr[length - 4],
      arr[length - 3],
      arr[length - 2],
      arr[length - 1],
    ]
  }
  return arr
}

export const Pagination = ({
  totalRecords,
  perPage,
  handlePagination,
  setCaption,
  activePageNumber,
  ...props
}) => {
  const [currentIndex, setCurrentIndex] = useState(activePageNumber)

  useEffect(() => {
    setCurrentIndex(activePageNumber)
  }, [activePageNumber])

  const indexArray = useMemo(() => {
    if (perPage > 0 && totalRecords > 0) {
      const totalPages = Math.ceil(totalRecords / perPage)
      return Array.from({ length: totalPages }).map((v, i) => i + 1)
    } else {
      return []
    }
  }, [totalRecords, perPage])
  const [showArray, setShowArray] = useState([])

  const handlePageClick = val => {
    setCurrentIndex(val)
    handlePagination(val)
  }

  useEffect(() => {
    const pages = renderPages(currentIndex, indexArray)
    setShowArray(pages)
    setCaption(
      `Showing ${perPage * (currentIndex - 1) + 1}-${
        perPage * currentIndex > totalRecords
          ? totalRecords
          : perPage * currentIndex
      } of ${totalRecords}`
    )
    return () => {}
  }, [perPage, currentIndex, totalRecords, indexArray, setCaption])

  const handleNextClick = () => {
    if (currentIndex + 1 <= indexArray[indexArray.length - 1]) {
      setCurrentIndex(i => i + 1)
      handlePagination(currentIndex + 1)
    }
  }
  const handlePreviousClick = () => {
    if (currentIndex - 1 >= indexArray[0]) {
      setCurrentIndex(i => i - 1)
      handlePagination(currentIndex - 1)
    }
  }

  if (indexArray.length === 0) {
    return null
  }

  return (
    <StyledPagination {...props}>
      <StyledPaginationNav
        type='button'
        data-testid='previous-nav'
        onClick={handlePreviousClick}
        hide={currentIndex !== indexArray[0]}
      >
        <LeftArrow />
        <StyledNavText> Previous</StyledNavText>
      </StyledPaginationNav>

      <StyledPages>
        {showArray.map((v, i) => {
          return (
            <StyledPageNumber key={i} active={currentIndex === v}>
              {typeof v === 'number' ? (
                <Link
                  tabIndex='0'
                  data-testid={`pageNumber-${v}`}
                  type='tertiary'
                  text={String(v)}
                  onClick={() => handlePageClick(v)}
                />
              ) : (
                v
              )}
            </StyledPageNumber>
          )
        })}
      </StyledPages>

      <StyledPaginationNav
        type='button'
        data-testid='next-nav'
        onClick={handleNextClick}
        hide={currentIndex !== indexArray[indexArray.length - 1]}
      >
        <StyledNavText>Next</StyledNavText>
        <RightArrow />
      </StyledPaginationNav>
    </StyledPagination>
  )
}

Pagination.defaultProps = {
  handlePagination: () => {},
  setCaption: () => {},
  activePageNumber: 1,
}

Pagination.propTypes = {
  totalRecords: PropTypes.number.isRequired,
  perPage: PropTypes.number.isRequired,
  handlePagination: PropTypes.func.isRequired,
  setCaption: PropTypes.func,
  activePageNumber: PropTypes.number,
}
