import React from 'react'
import { Pagination as CustomPagination } from './Pagination'
import PaginationDocs from '../../../../docs/Pagination.mdx'
import {
  PAGINATION_DESIGN,
  PAGINATION_PRESENTATION,
} from '../../../hooks/constants'
import { getDesignSpecifications } from '../../../hooks/utils'

export default {
  title: 'Modules/Pagination',
  component: CustomPagination,
  argTypes: {},
  parameters: {
    docs: {
      page: PaginationDocs,
    },
  },
}

const PaginationTemplate = args => (
  <div
    style={{
      width: '100%',
      height: '100vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <CustomPagination {...args} />
  </div>
)

export const Pagination = PaginationTemplate.bind({})

Pagination.args = {
  totalRecords: 140,
  perPage: 5,
  handlePagination: id => {
    console.log('index', id)
  },
  setCaption: () => {},
}
Pagination.storyName = 'Pagination'
Pagination.parameters = getDesignSpecifications(
  PAGINATION_DESIGN,
  PAGINATION_PRESENTATION
)
