import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import { Pagination } from './Pagination'
import theme from '../../../shared/theme'

afterEach(cleanup)

const fn = jest.fn()

describe('<Pagination/>', () => {
  it('should render Pagination component with totalRecords:100 and perPage:10', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
      />
    )
    Array.from({ length: 10 }).forEach(() => {
      fireEvent.click(getByTestId('next-nav'))
    })
    expect(container).toMatchSnapshot()
  })

  it('should not render Pagination component with totalRecords:0 and perPage:0 ', () => {
    const { container } = render(
      <Pagination
        totalRecords={0}
        perPage={0}
        handlePagination={fn}
        setCaption={fn}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render Pagination component with current index on next navigation click', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
      />
    )
    fireEvent.click(getByTestId('next-nav'))
    expect(container).toMatchSnapshot()
  })

  it('should render Pagination component with current index on prev navigation click', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
      />
    )
    fireEvent.click(getByTestId('next-nav'))
    fireEvent.click(getByTestId('previous-nav'))
    expect(container).toMatchSnapshot()
  })

  it('should render Pagination with activePageNumber', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
        activePageNumber={5}
      />
    )
    expect(getByTestId('pageNumber-4')).toHaveStyle(
      `color: ${theme.palette.brand.primary.gray}`
    )
    expect(container).toMatchSnapshot()
  })
  it('should render links to pages 3 and 5 when the active page is 4', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
        activePageNumber={4}
      />
    )
    expect(getByTestId('pageNumber-3')).toBeVisible()
    expect(getByTestId('pageNumber-5')).toBeVisible()
    expect(container).toMatchSnapshot()
  })
  it('should render links to the previous and next page when the active page is 4th from last', () => {
    const { container, getByTestId } = render(
      <Pagination
        totalRecords={100}
        perPage={10}
        handlePagination={fn}
        setCaption={fn}
        activePageNumber={7}
      />
    )
    expect(getByTestId('pageNumber-6')).toBeVisible()
    expect(getByTestId('pageNumber-8')).toBeVisible()
    expect(container).toMatchSnapshot()
  })
})
