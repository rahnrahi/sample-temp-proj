import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledPaginationNav = styled.button`
  display: inline-flex;
  align-items: center;
  box-sizing: border-box;
  padding: 8px 14px;
  position: relative;
  background: ${theme.palette.brand.primary.white};
  border: 1px solid ${theme.palette.brand.primary.charcoal};
  color: ${theme.palette.brand.primary.charcoal};
  border-radius: 16px;
  cursor: default;
  height: 32px;

  :hover {
    color: ${theme.palette.brand.primary.white};
    background: ${theme.palette.brand.primary.gray};
    border-color: ${theme.palette.brand.primary.gray};
    svg > path {
      fill: ${theme.palette.brand.primary.white};
    }
  }
  :focus {
    outline: none;
    color: ${theme.palette.brand.primary.charcoal};
    border: 1px solid ${theme.palette.ui.cta.yellow};
    background: ${theme.palette.brand.primary.white};
    svg > path {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }

  :active {
    border: ${theme.palette.ui.neutral.grey1};
    background: ${theme.palette.ui.neutral.grey1};
    border: 1px solid transparent;
    color: ${theme.palette.brand.primary.white};
    svg > path {
      fill: ${theme.palette.brand.primary.white};
    }
  }

  :first-child {
    margin-right: 24px;
  }
  :last-child {
    margin-left: 24px;
  }
  ${({ hide }) =>
    !hide &&
    `
		visibility:hidden;
		opacity:0;
	`}
`

export const StyledPagination = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${theme.typography.h6.fontSize};
  line-height: ${theme.typography.h6.lineHeight};
  letter-spacing: ${theme.typography.h6.letterSpacing};
  user-select: none;
`

export const StyledPageNumber = styled.div`
  color: ${theme.palette.brand.primary.gray};
  padding: 0 4px;
  a {
    ${({ active }) =>
      active && `color:${theme.palette.brand.primary.charcoal} !important`};
  }
`

export const StyledNavText = styled.span`
  :first-child {
    margin-right: 5px;
  }
  :last-child {
    margin-left: 5px;
  }
`

export const StyledPages = styled.div`
  display: flex;
`
