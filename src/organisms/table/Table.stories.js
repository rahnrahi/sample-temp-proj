import React, { useEffect, useState } from 'react'
import isEmpty from 'lodash/isEmpty'
import styled from 'styled-components'
import { Button, Link } from '../../atoms'
import { Table } from './Table'
import { columns, data } from './TableData-XM'
import { omsColumns, omsData } from './TableData-OMS'
import { orderColumns, orderData } from './TableData-customCaption'
import { pimColumns, pimData } from './TableData-PIM'
import { delay, isFunction } from '../../shared/utils'
import { theme } from '../../shared'
import { multilineColumns, multilineData } from './TableData-MultiLine'
import TablesDocs from '../../../docs/Tables.mdx'
import {
  TABLE_EXPANDABLE_DESIGN,
  TABLE_EXPANDABLE_PRESENTATION,
  TABLE_SIMPLE_DESIGN,
  TABLE_SIMPLE_PRESENTATION,
  TABLE_CHECKBOX_DESIGN,
  TABLE_CHECKBOX_PRESENTATION,
  TABLE_EMPTY_DESIGN,
  TABLE_EMPTY_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Tables/Table',
  component: Table,
  argTypes: {},
  parameters: {
    docs: {
      page: TablesDocs,
    },
  },
}

/* eslint-disable */

const Container = styled.div`
  padding: 40px;
  background: #ccc;
  width: 900px;
  .custom-col {
    padding-left: 20px;
  }
`

// eslint-disable-next-line react/prop-types
const Template1 = ({ perPage, data, ...args }) => {
  const [tableData, setTableData] = useState(data.slice(0, 4))
  const [loading, setLoading] = useState(false)

  const handlePagination = async id => {
    // API call with page number (ie. id)
    // eslint-disable-next-line react/prop-types
    setLoading(true)
    await delay(1000).then(() => {
      const d = data.slice(perPage * (id - 1), perPage * id)
      setTableData(d)
    })
    setLoading(false)
  }

  const handleRename = val => {
    setTableData(arr => {
      return arr.map(obj => {
        if (obj.uid === val.uid) {
          return { ...obj, _rename: 1 }
        }
        return obj
      })
    })
  }

  const handleEditSuccess = (e, obj, text) => {
    setLoading(true)
    delay(10000).then(() => {
      setTableData(arr => {
        return arr.map(item => {
          if (obj.uid === item.uid) {
            delete item['_rename']
            return { ...item, name: text }
          }
          return item
        })
      })
      setLoading(false)
    })
  }

  const handleEditCancel = (e, obj) => {
    setTableData(arr => {
      return arr.map(item => {
        if (obj.uid === item.uid) {
          delete item['_rename']
          return item
        }
        return item
      })
    })
  }

  const handleMouseEnter = (e, val, isChild, setTableData) => {
    if (isChild) {
      isFunction(setTableData) &&
        setTableData(arr =>
          arr.map(obj => {
            if (Array.isArray(obj.children) && !_.isEmpty(obj.children)) {
              obj.children = obj.children.map(child => {
                if (child.uid === val.uid) {
                  return { ...child, _isHover: true }
                }
                return child
              })
              return obj
            }
            return obj
          })
        )
    } else {
      isFunction(setTableData) &&
        setTableData(arr => {
          return arr.map(obj => {
            if (obj.uid === val.uid) {
              return { ...obj, _isHover: true }
            }
            return obj
          })
        })
    }
  }

  const handleMouseLeave = (e, val, isChild, setTableData) => {
    if (isChild) {
      isFunction(setTableData) &&
        setTableData(arr =>
          arr.map(obj => {
            if (Array.isArray(obj.children) && !_.isEmpty(obj.children)) {
              obj.children = obj.children.map(child => {
                if (child.uid === val.uid) {
                  return { ...child, _isHover: false }
                }
                return child
              })
              return obj
            }
            return obj
          })
        )
    } else {
      isFunction(setTableData) &&
        setTableData(arr => {
          return arr.map(obj => {
            if (obj.uid === val.uid) {
              return { ...obj, _isHover: false }
            }
            return obj
          })
        })
    }
  }

  const handleAscSort = async (key, data, callback) => {
    setLoading(true)
    await delay(1000)
    const result = [...data].sort((a, b) => (a[key] > b[key] ? 1 : -1))
    callback(result)
    setLoading(false)
  }

  const handleDscSort = async (key, data, callback) => {
    setLoading(true)
    await delay(1000)
    const result = [...data].sort((a, b) => (a[key] < b[key] ? 1 : -1))
    callback(result)
    setLoading(false)
  }

  const handleResetSort = async (key, data, callback) => {
    setLoading(true)
    await delay(1000)
    callback(data)
    setLoading(false)
  }

  // Remove default onRowClick action handler from global args
  delete args.onRowClick

  return (
    <Container>
      <Table
        data={tableData}
        {...args}
        perPage={perPage}
        handlePagination={handlePagination}
        handleRename={handleRename}
        handleEditSuccess={handleEditSuccess}
        handleEditCancel={handleEditCancel}
        loading={loading}
        onRowMouseEnter={handleMouseEnter}
        onRowMouseLeave={handleMouseLeave}
        rowSelectHandler={(e, rowData) =>
          console.log('selected row data', rowData, e.target.checked)
        }
        onAllRowSelect={(e, isSelected) =>
          console.log('selected all rows', isSelected)
        }
        handleAscendingSort={handleAscSort}
        handleDescendingSort={handleDscSort}
        handleResetSort={handleResetSort}
      />
    </Container>
  )
}

export const Table_XM = Template1.bind({})
Table_XM.args = {
  columns: columns,
  data: data,
  loading: false,
  showIndex: false,
  isSelectable: false,
  enableSort: true,
  showPagination: true,
  totalRecords: 16,
  perPage: 4,
  cellClassName: 'table-cell',
}
Table_XM.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_EXPANDABLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_EXPANDABLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

const StyledOmsTable = styled.div`
  background: #ccc;
  padding: 40px;
  width: 900px;
  .oms-table-row {
    :last-child {
      :hover {
        background: #ffffff !important;
      }
    }
  }
`

const Template2 = args => (
  <StyledOmsTable>
    <Table
      {...args}
      handleAscendingSort={(accessor, data, callback) => {
        const tableData = data.slice(0, data.length - 1)
        const lastRowData = data[data.length - 1]
        const result = tableData
          .sort((a, b) => (a[accessor] > b[accessor] ? 1 : -1))
          .concat(lastRowData)
        callback(result)
      }}
      handleDescendingSort={(accessor, data, callback) => {
        const tableData = data.slice(0, data.length - 1)
        const lastRowData = data[data.length - 1]
        const result = tableData
          .sort((a, b) => (a[accessor] < b[accessor] ? 1 : -1))
          .concat(lastRowData)
        callback(result)
      }}
    />
  </StyledOmsTable>
)

export const Table_OMS = Template2.bind({})
Table_OMS.args = {
  data: omsData,
  columns: omsColumns,
  borderRadius: '0',
  loading: false,
  showIndex: false,
  isSelectable: false,
  enableSort: true,
  showPagination: false,
  rowClassName: 'oms-table-row',
}
Table_OMS.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_SIMPLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_SIMPLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

const Styledwrapper = styled.div`
  text-align: center;
  width: 500px;
  height: 500px;
  margin: 66px auto 0 auto;
`
const StyledTitle = styled.div`
  margin-bottom: 32px;
  ${theme.typography.kicker.css};
  color: ${theme.palette.brand.primary.gray};
`
const StyledMessage = styled.p`
  margin: 0 auto;
  ${theme.typography.subtitle2.css};
  color: ${theme.palette.brand.primary.charcoal};
`

const StyledLink = styled.div`
  margin-top: 46px;
`
const Template3 = args => {
  return (
    <Container>
      <Table
        {...args}
        render={({ data }) => {
          return isEmpty(data) ? (
            <tbody>
              <tr>
                <td colSpan={columns.length + 1}>
                  <Styledwrapper>
                    <StyledTitle>NO RESULTS FOUND</StyledTitle>
                    <StyledMessage>
                      Try searching for something more general,
                    </StyledMessage>
                    <StyledMessage>change filters or check for</StyledMessage>
                    <StyledMessage>spelling mistakes</StyledMessage>
                    <StyledLink>
                      <Link text='Clear search' type='primary' />
                    </StyledLink>
                  </Styledwrapper>
                </td>
              </tr>
            </tbody>
          ) : null
        }}
      />
    </Container>
  )
}

export const TableWithNoSearchResult = Template3.bind({})
TableWithNoSearchResult.args = {
  data: [],
  borderRadius: '0',
  columns: [
    {
      title: 'Item Name',
      accessor: 'item_name',
      isSortable: false,
    },
    {
      title: 'Product ID',
      accessor: 'product_id',
      isSortable: false,
    },
    {
      title: 'SKU ID',
      accessor: 'sku_id',
      isSortable: false,
    },
    {
      title: 'Available stock',
      accessor: 'available_stock',
      isSortable: false,
    },
    {
      title: 'In-transit',
      accessor: 'in_transit',
      isSortable: false,
    },
  ],
  loading: false,
  showIndex: false,
  isSelectable: false,
  enableSort: false,
  showPagination: false,
}
TableWithNoSearchResult.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_EMPTY_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_EMPTY_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}


const StyledCell = styled.th`
  padding: ${({ index }) => (index === 0 ? '25px 0 25px 30px' : '25px 0px')};
  font-size: 13px;
  color: ${({ status }) =>
    status === -1 ? '#737f8f' : status === 0 ? '#121213' : '#121213'};
  cursor: pointer;
`

const Template4 = args => (
  <StyledOmsTable>
    <Table
      {...args}
      customHeader={({ columns, sortArray, handleSort }) => {
        return (
          <thead
            style={{ boxShadow: 'inset 0px -1px 0px rgba(115, 127, 143, 0.2)' }}
          >
            <tr>
              {columns.length &&
                columns.map(({ title, accessor }, i) => (
                  <StyledCell
                    key={i}
                    index={i}
                    status={sortArray[i].status}
                    onClick={() => {
                      handleSort(accessor, i)
                    }}
                  >
                    <span>{title}</span>
                  </StyledCell>
                ))}
            </tr>
          </thead>
        )
      }}
      handleAscendingSort={(accessor, data, callback) => {
        const tableData = data.slice(0, data.length - 1)
        const lastRowData = data[data.length - 1]
        const result = tableData
          .sort((a, b) => (a[accessor] > b[accessor] ? 1 : -1))
          .concat(lastRowData)
        callback(result)
      }}
      handleDescendingSort={(accessor, data, callback) => {
        const tableData = data.slice(0, data.length - 1)
        const lastRowData = data[data.length - 1]
        const result = tableData
          .sort((a, b) => (a[accessor] < b[accessor] ? 1 : -1))
          .concat(lastRowData)
        callback(result)
      }}
    />
  </StyledOmsTable>
)

export const TableWithCustomHeader = Template4.bind({})

TableWithCustomHeader.args = {
  data: omsData,
  columns: omsColumns,
  borderRadius: '0',
  loading: false,
  showIndex: false,
  isSelectable: false,
  enableSort: true,
  showPagination: false,
  rowClassName: 'oms-table-row',
}
TableWithCustomHeader.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_CHECKBOX_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_CHECKBOX_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}


// Custom caption for order table

const StyledCaption = styled.div`
  display: flex;
  align-items: center;
  font-size: 14px;
  margin-bottom: 15px;
  .btn {
    margin-left: 14px;
    border: none;
  }
`

const Template5 = ({ perPage, data, ...args }) => {
  const [orders, setOrders] = useState([])
  const [count, setCount] = useState(0)
  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    const count = orders.reduce((acc, currentValue) => {
      if (currentValue._isCheck) {
        acc = acc + 1
      } else {
        acc = acc - 1
      }
      return acc
    }, 0)
    setCount(count)
  }, [orders])

  return (
    <Container>
      <StyledCaption>
        <div>
          Selected {count} orders.
          <Link type='primary' text='select all orders' />
        </div>
        <div>
          <Button isPrimary={false} text='Bulk Action' className='btn' />
        </div>
      </StyledCaption>
      <Table
        data={data}
        {...args}
        rowSelectHandler={(e, rowData) => {
          console.log('selected row data', rowData, e.target.checked)
          setOrders(prevData => [...prevData, rowData])
        }}
        onAllRowSelect={(e, isSelected) => {
          console.log('selected all rows', isSelected)
          isSelected ? setCount(data.length) : setCount(0)
        }}
        onRowClick={(e, rowData) => console.log('row clicked', rowData)}
      />
    </Container>
  )
}

export const TableWithCustomCaption = Template5.bind({})
TableWithCustomCaption.args = {
  columns: orderColumns,
  data: orderData,
  isSelectable: true,
  enableSort: true,
  hideCaption: true,
}
TableWithCustomCaption.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_CHECKBOX_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_CHECKBOX_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

// Disabled row

const Template6 = args => {
  const handleMouseEnter = (event, data, isChild, setTableData) => {
    if (data._isDisable) {
      setTableData(prevState =>
        prevState.map(obj => {
          if (obj._isDisable) {
            return { ...obj, ...data, show: true }
          }
          return obj
        })
      )
    }
  }

  const handleMouseLeave = (event, data, isChild, setTableData) => {
    if (data._isDisable) {
      setTableData(prevState =>
        prevState.map(obj => {
          if (obj._isDisable) {
            return { ...obj, show: false }
          }
          return obj
        })
      )
    }
  }

  return (
    <Container>
      <Table
        {...args}
        onRowMouseEnter={handleMouseEnter}
        onRowMouseLeave={handleMouseLeave}
      />
    </Container>
  )
}

export const TableWithDisabledRow = Template6.bind({})
TableWithDisabledRow.args = {
  columns: pimColumns,
  data: pimData,
  isSelectable: true,
  hideCaption: true,
}
TableWithDisabledRow.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_SIMPLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_SIMPLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}


export const TableWithMultiLineCellRender = Template6.bind({})
TableWithMultiLineCellRender.args = {
  columns: multilineColumns,
  data: multilineData,
  isSelectable: true,
  hideCaption: true,
}
TableWithMultiLineCellRender.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_SIMPLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_SIMPLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
TableWithMultiLineCellRender.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_SIMPLE_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_SIMPLE_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

