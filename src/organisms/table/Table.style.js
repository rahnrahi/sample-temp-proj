import styled, { css } from 'styled-components'
import { theme } from '../../shared'

export const StyledTable = styled.table`
  position: relative;
  table-layout: auto;
  width: 100%;
  background: ${theme.palette.brand.primary.white};
  text-align: left;
  box-sizing: border-box;
  border-collapse: collapse;
  ${({ borderRadius }) => borderRadius && `border-radius:${borderRadius}`};

  .child-row {
    border-left: 1px solid ${theme.palette.ui.states.active};
    border-right: 1px solid ${theme.palette.ui.states.active};
  }
  .child-cell {
    :first-child {
      padding-right: 14px;
    }
  }
  .has-child {
    padding: 0;
  }
  .expansion-cell {
    position: relative;
    width: 60px !important;
    svg {
      position: absolute;
      left: 50%;
      top: 50%;
      transform-origin: 50% 50%;
      transform: translate(-50%, -50%);
      transition: all 0.2s ease;
    }
  }
  .rotate-caret {
    transform: rotate(180deg);
    svg {
      path {
        fill: ${theme.palette.ui.states.active};
      }
    }
  }
  .hide {
    width: 0;
    opacity: 0;
  }
`

export const StyledPaginationInfo = styled.div`
  position: relative;

  > span {
    position: absolute;
    display: inline-flex;
    align-items: center;
    background: transparent;
    left: 2px;
    bottom: calc(100% + 16px);
    font-size: 14px;
    line-height: 16px;
    color: ${theme.palette.brand.primary.charcoal};
  }
`

export const StyledHead = styled.thead`
  border-radius: 4px 4px 0px 0px;
`
export const StyledTh = styled.th`
  position: relative;
  box-sizing: border-box;
  padding: 32px 0px;
  font-size: 13px;
  line-height: 16px;
  letter-spacing: 0.02em;
  color: ${theme.palette.brand.primary.gray};
  cursor: default;
  user-select: none;
  word-wrap: break-word;

  ${({ isSortable }) => isSortable && `span { cursor: pointer; } `}

  ${({ isSortable }) =>
    !isSortable &&
    `
		pointer-events:none;
	`};
  ${({ sort }) =>
    (sort === '0' || sort === '1') &&
    `
      color: ${theme.palette.brand.primary.charcoal} !important;
    `}
  &:last-child {
    padding-right: 24px;
  }
  ${({ showIndex, isCollapse }) =>
    showIndex &&
    `
	&:first-child {
    ${showIndex && `width:70px`};
    ${!isCollapse && showIndex ? `text-align:center` : `text-align:left`};
	`}

  ${({ showIndex }) =>
    !showIndex &&
    `
		&:first-child{
			padding-left:30px;
		}
	`}
`

export const StyledRow = styled.tr`
  ${({ isEditRow }) => isEditRow && `height:70px;`}
  box-shadow: inset 0px -1px 0px rgba(115, 127, 143, 0.2);
  ${({ showOutline }) =>
    showOutline &&
    `
		border:1px solid ${theme.palette.ui.states.active};
		border-bottom:none;
	`}

  ${({ isLastChild }) =>
    isLastChild &&
    `
		border-bottom:1px solid ${theme.palette.ui.states.active};
	`}

  :not(.table-main-row) {
    :hover {
      background: ${theme.palette.ui.neutral.grey5};
      box-shadow: inset 0px -1px 0px ${theme.palette.ui.neutral.grey4};
    }
  }

  ${({ isClickable }) => isClickable && 'cursor:pointer;'}
`

export const StyledCell = styled.td`
  box-sizing: border-box;
  position: relative;
  font-size: ${theme.typography.h6.fontSize};
  line-height: ${theme.typography.h6.lineHeight};
  letter-spacing: ${theme.typography.h6.letterSpacing};
  color: ${theme.palette.brand.primary.charcoal};
  padding: 25px 24px 25px 0px;
  &:first-child {
    text-align: ${({ isSelectable, showIndex, shouldCenter }) =>
      isSelectable || showIndex || shouldCenter ? 'center' : 'left'};
    width: ${({ showIndex }) => showIndex && '70px'};
    padding-left: ${({ isCollapse, showIndex, isfooter }) =>
      !isCollapse && !showIndex && !isfooter ? '30px' : '0px'};
  }
  word-wrap: break-word;
  transition: all 0.2s ease;
`

export const CheckboxWrapper = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  label {
    &:hover {
      background: none;
    }
  }
`

const AscendingSortStyle = css`
  path {
    fill: ${theme.palette.brand.primary.charcoal};
  }
`

const DescendingSortStyle = css`
  svg {
    transform: rotate(180deg);
    path {
      fill: ${theme.palette.brand.primary.charcoal};
    }
  }
`
const sortArrowStyle = new Map()

sortArrowStyle.set('0', AscendingSortStyle)
sortArrowStyle.set('1', DescendingSortStyle)

export const StyledSortArrow = styled.span`
  position: relative;
  padding: 0 8px;
  svg {
    position: absolute;
    transform-origin: 50% 50%;
    transition: all 0.2s ease;
    path {
      fill: ${theme.palette.brand.primary.gray};
    }
    ${({ sort }) => (sort === '0' || sort === '1') && sortArrowStyle.get(sort)}
  }
  ${({ sort }) => sort && sortArrowStyle.get(sort)}
`

export const StyledEditBox = styled.div`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  padding-left: 85px;
  display: inline-flex;
  align-items: center;
  .input-wrapper {
    display: inline-block;
  }
  .edit-input {
    display: inline-block;
    margin-right: 11px;
    width: 396px;
    height: 40px;
    border: 1px solid #0d62ff;
    border-radius: 4px;
    ${theme.typography.h6.css};
    color: ${theme.palette.brand.primary.charcoal};
  }
  .success-btn {
    width: 32px;
    height: 32px;
    position: relative;
    top: 2px;
    & > svg {
      position: absolute;
      transform: translateX(1px);
      width: 14px;
      height: 10px;
    }
  }
  .cancel-btn {
    width: 32px;
    height: 32px;
    margin-left: 9px;
    :hover {
      svg > path {
        fill: ${theme.palette.brand.primary.white};
      }
    }
  }
`
