import React, { useState, useRef, useMemo, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { v4 as uuidv4 } from 'uuid'
import { Flyout } from '../../molecules'
import { Dots } from '../../assets/images'
import useClickOutside from '../../hooks/click-outside'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 20px;
  height: 20px;
  border-radius: 20px;
`

const FlyoutOption = styled.div`
  width: 150px;
  padding: 10px 15px;
  cursor: pointer;
  :hover {
    background: #f1f2f4;
  }
`

const Menu = ({ data, handleRename }) => {
  const [show, setShow] = useState(false)
  const id = useMemo(() => uuidv4(), [])
  const ref = useRef(null)

  const handleOptionClick = (e, val) => {
    e.stopPropagation()
    if (val === 'Rename') {
      handleRename(data)
    }
  }
  useEffect(() => {
    !data._isHover && show && setShow(false)
  }, [data])

  useClickOutside(
    ref,
    () => {
      setShow(false)
    },
    show
  )

  return (
    <>
      <Wrapper
        id={id}
        ref={ref}
        onClick={e => {
          setShow(s => !s)
          e.stopPropagation()
        }}
      >
        {data._isHover && <Dots />}
      </Wrapper>
      <Flyout
        show={show}
        className='dot-menu'
        id={id}
        offset={10}
        placement='leftTop'
        width='150px'
      >
        {data.menu.map((d, i) => (
          <FlyoutOption uid={i} key={i} onClick={e => handleOptionClick(e, d)}>
            {d}
          </FlyoutOption>
        ))}
      </Flyout>
    </>
  )
}
Menu.propTypes = {
  data: PropTypes.object,
  handleRename: PropTypes.func,
}

const StyledCell = styled.div`
  padding-left: 20px;
`

export const columns = [
  {
    title: 'Name',
    accessor: 'name',
    isSortable: true,
    className: 'custom-col',
    render: data => <StyledCell>{data.name}</StyledCell>,
    children: [
      {
        title: 'Name',
        accessor: 'name',
        render: data => <StyledCell>{data.name}</StyledCell>,
      },
      {
        title: 'Updated',
        accessor: 'updated',
        render: data => <StyledCell>{data.updated}</StyledCell>,
      },
      {
        title: 'Status',
        accessor: 'status',
        render: data => <StyledCell>{data.status}</StyledCell>,
      },
      {
        title: '\u00a0',
        accessor: 'menu',
        render: (data, handleRename) => (
          <Menu data={data} handleRename={handleRename} />
        ),
      },
    ],
  },

  {
    title: 'Updated',
    accessor: 'updated',
    isSortable: false,
    className: 'custom-col',
    render: data => <StyledCell>{data.updated}</StyledCell>,
  },
  {
    title: 'Status',
    accessor: 'status',
    isSortable: true,
    className: 'custom-col',
    render: data => <StyledCell>{data.status}</StyledCell>,
  },
  {
    title: '\u00a0',
    accessor: 'menu',
    render: (data, handleRename) => (
      <Menu data={data} handleRename={handleRename} />
    ),
  },
]

export const data = [
  {
    uid: uuidv4(),
    name: 'A Browse Menu version 939348 ',
    updated: '5 mins ago',
    status: 'published',
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
    children: [
      {
        uid: uuidv4(),
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'published',
        menu: ['Edit', 'Duplicate', 'Delete'],
      },
      {
        uid: uuidv4(),
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'Draft',
        menu: ['Edit', 'Duplicate', 'Delete'],
      },
    ],
  },
  {
    uid: uuidv4(),
    name: 'C Browse Menu version 939348',
    updated: '6 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'D Browse Menu version 939348',
    updated: '7 mins ago',
    status: 'Draft',
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
    children: [
      {
        uid: uuidv4(),
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'published',
        menu: ['Edit', 'Duplicate', 'Delete'],
      },
      {
        uid: uuidv4(),
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'Draft',
        menu: ['Edit', 'Duplicate', 'Delete'],
      },
    ],
  },
  {
    uid: uuidv4(),
    name: 'B Browse Menu version 939348',
    updated: '8 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'E Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'L Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'K Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'C Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'M Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'P Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'R Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'Q Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'L Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'T Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'X Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
  {
    uid: uuidv4(),
    name: 'Z Browse Menu version 939348',
    updated: '9 mins ago',
    status: 'Draft',
    children: [],
    menu: ['Edit', 'Duplicate', 'Rename', 'Delete'],
  },
]
