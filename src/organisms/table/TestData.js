import React from 'react'

export const columns = [
  {
    title: 'Name',
    accessor: 'name',
    isSortable: true,
    // eslint-disable-next-line react/prop-types
    render: ({ text }) => <a href='/'>{text}</a>,
    children: [
      {
        title: 'Name',
        accessor: 'name',
      },
      {
        title: 'Updated',
        accessor: 'updated',
      },
      {
        title: 'Status',
        accessor: 'status',
      },
      {
        title: '\u00a0',
        accessor: 'menu',
        render: () => <span>&nbsp;</span>,
      },
    ],
  },

  {
    title: 'Updated',
    accessor: 'updated',
    isSortable: false,
  },
  {
    title: 'Status',
    accessor: 'status',
    // eslint-disable-next-line react/prop-types
    render: ({ text }) => <a href='/'>{text}</a>,
  },
]

export const data = [
  {
    uid: 1,
    name: 'A Browse Menu version 939348 ',
    updated: '5 mins ago',
    status: 'published',
    children: [
      {
        uid: 2,
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'published',
        menu: [],
      },
      {
        uid: 3,
        name: 'Browse Menu version 939348',
        updated: '5 mins ago',
        status: 'Draft',
      },
    ],
  },
  {
    uid: 4,
    name: 'C Browse Menu version 939348',
    updated: '6 mins ago',
    status: 'Draft',
    children: [],
  },
  {
    uid: 5,
    name: 'D Browse Menu version 939348',
    updated: '7 mins ago',
    status: 'Draft',
    children: [],
    _isDisable: true,
  },
]
