import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import PropTypes from 'prop-types'
import { Checkbox } from '../../../atoms'
import {
  StyledHead,
  StyledRow,
  StyledTh,
  CheckboxWrapper,
  StyledSortArrow,
} from '../Table.style'

import { DownArrow } from '../../../assets/images'

export const Head = ({
  columns,
  isSelectable,
  showIndex,
  enableSort,
  selectAll,
  collapse,
  sort,
  handleSelectAll,
  handleSort,
  isPartialCheck,
}) => {
  return (
    <StyledHead>
      <StyledRow className='table-main-row'>
        {showIndex && (
          <StyledTh
            isSelectable={isSelectable}
            showIndex={showIndex}
            isSortable={true}
          >
            {isSelectable ? (
              <CheckboxWrapper>
                <Checkbox
                  name={uuidv4()}
                  onChange={e => handleSelectAll(e)}
                  checked={selectAll}
                  isPartial={isPartialCheck}
                  value={''}
                  data-testid='select-all'
                />
              </CheckboxWrapper>
            ) : (
              '#'
            )}
          </StyledTh>
        )}
        {collapse.length !== 0 && (
          <StyledTh className='expansion-cell'></StyledTh>
        )}
        {columns.length > 0 &&
          columns.map(
            ({ title, accessor, isSortable = true, className }, i) => {
              const sortStatus = '' + (sort[i] && sort[i].status)
              return (
                <StyledTh
                  key={i}
                  sort={sortStatus}
                  isCollapse={collapse.length !== 0}
                  isSortable={isSortable}
                  className={className || ''}
                  onClick={() => handleSort(accessor, i)}
                >
                  <span>
                    <span>{title}</span>
                    {title !== '\u00a0' && enableSort && isSortable && (
                      <StyledSortArrow sort={sortStatus}>
                        <DownArrow />
                      </StyledSortArrow>
                    )}
                  </span>
                </StyledTh>
              )
            }
          )}
      </StyledRow>
    </StyledHead>
  )
}

Head.propTypes = {
  columns: PropTypes.array,
  isSelectable: PropTypes.bool,
  showIndex: PropTypes.bool,
  enableSort: PropTypes.bool,
  selectAll: PropTypes.bool,
  collapse: PropTypes.array,
  sort: PropTypes.array,
  handleSelectAll: PropTypes.func,
  handleSort: PropTypes.func,
  isPartialCheck: PropTypes.bool,
}
