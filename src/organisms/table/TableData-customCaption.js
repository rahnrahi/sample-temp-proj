import { v4 as uuidv4 } from 'uuid'

export const orderColumns = [
  {
    title: 'Order ID',
    accessor: 'order_id',
    isSortable: false,
  },

  {
    title: 'Customer Name',
    accessor: 'customer_name',
    isSortable: false,
  },
  {
    title: 'Value',
    accessor: 'value',
    isSortable: false,
  },
  {
    title: 'Status',
    accessor: 'status',
    isSortable: false,
  },
  {
    title: 'Order Date',
    accessor: 'order_date',
    isSortable: false,
  },
]

export const orderData = [
  {
    uid: uuidv4(),
    order_id: '28394-23234-2348392',
    customer_name: 'Annebelle Sinh',
    value: '$640',
    status: 'Created',
    order_date: 'Jan. 20, 2020 4:00 PM',
  },
  {
    uid: uuidv4(),
    order_id: '38394-23234-2348392',
    customer_name: 'Annebelle Sinh',
    value: '$640',
    status: 'Created',
    order_date: 'Jan. 20, 2020 4:00 PM',
  },
  {
    uid: uuidv4(),
    order_id: '48394-23234-2348392',
    customer_name: 'Annebelle Sinh',
    value: '$640',
    status: 'Created',
    order_date: 'Jan. 20, 2020 4:00 PM',
  },
  {
    uid: uuidv4(),
    order_id: '58394-23234-2348392',
    customer_name: 'Annebelle Sinh',
    value: '$640',
    status: 'Created',
    order_date: 'Jan. 20, 2020 4:00 PM',
  },
]
