import React, { useState, useCallback, useEffect, useMemo } from 'react'
import has from 'lodash/has'
import isEmpty from 'lodash/isEmpty'
import PropTypes from 'prop-types'

import { TableLoader } from '../../molecules'
import Pagination from './pagination'
import { Row, ChildRow, EditRow } from './rows'
import { Head } from './head'
import { isFunction } from '../../shared/utils'

import {
  StyledCell,
  StyledPaginationInfo,
  StyledRow,
  StyledTable,
} from './Table.style'

export const Table = ({
  columns,
  data,
  loading,
  borderRadius,
  showIndex,
  isSelectable,
  enableSort,
  onRowMouseEnter,
  onRowMouseLeave,
  onRowClick,
  onAllRowSelect,
  rowSelectHandler,
  handleAscendingSort,
  handleDescendingSort,
  handleResetSort,
  showPagination,
  totalRecords,
  perPage,
  handlePagination,
  handleRename,
  handleEditSuccess,
  handleEditCancel,
  rowClassName,
  cellClassName,
  render,
  changedPageNumber,
  tableCaption,
  customHeader,
  activePageNumber,
  collapseOnRowClick,
  hideCaption,
  defaultRowSelection,
  allowDataCheckBoxes,
}) => {
  const addCheckToData = data => {
    if (isEmpty(data)) return data
    return data.map(obj => {
      if (checkHasChildren(obj)) {
        const children = obj.children.map(child =>
          !has(child, '_isCheck') ? { ...child, _isCheck: false } : child
        )
        return !has(obj, '_isCheck')
          ? { ...obj, children, _isCheck: false, _isPartial: false }
          : { ...obj, children }
      }
      return !has(obj, '_isCheck')
        ? { ...obj, _isCheck: false, _isPartial: false }
        : obj
    })
  }

  const getCollapseArray = useCallback(
    length =>
      Array.isArray(columns[0]?.children) ? Array(length).fill(false) : [],
    [columns]
  )

  const [tableData, setTableData] = useState(() => addCheckToData(data))
  const rowCount = data.length

  showIndex = isSelectable ? true : showIndex

  const [page, setPage] = useState(activePageNumber)
  const [caption, setCaption] = useState(tableCaption)

  const [sort, setSort] = useState(() =>
    columns.map(({ accessor }) => ({ accessor, status: -1 }))
  )
  const [selectAll, setSelectAll] = useState(false)
  const [isPartial, setIsPartial] = useState(false)
  const [collapse, setCollapse] = useState(getCollapseArray(rowCount))
  const [editText, setEditText] = useState('')

  const hasRowClickHandler = isFunction(onRowClick)

  useEffect(() => {
    !showPagination && setCaption(tableCaption)
  }, [tableCaption, showPagination])

  useEffect(() => {
    setCollapse(getCollapseArray(rowCount))
  }, [columns, rowCount])

  useEffect(() => {
    const transformedData = addCheckToData(data)
    setTableData(transformedData)
  }, [data])

  useEffect(() => {
    setPage(activePageNumber)
  }, [activePageNumber])

  useEffect(() => {
    setSort(columns.map(({ accessor }) => ({ accessor, status: -1 })))
  }, [columns, caption])

  useEffect(() => {
    resetCollapse(setCollapse)
    if (!allowDataCheckBoxes) {
      resetAllCheck(setTableData)
    }
  }, [changedPageNumber])

  useEffect(() => {
    setSelectAll(
      tableData.some(obj => {
        if (obj._isCheck) return true
        if (checkHasChildren(obj)) {
          return obj.children.some(child => child._isCheck === true)
        }
      })
    )
  }, [tableData])

  useEffect(() => {
    setIsPartial(
      selectAll &&
        tableData.some(obj => {
          if (!obj._isCheck) return true
          if (checkHasChildren(obj)) {
            return obj.children.some(child => child._isCheck === false)
          }
        })
    )
  }, [selectAll, tableData])

  const handleSort = (key, id) => {
    setTableData(data)
    resetCollapse(setCollapse)
    if (sort[id].status === -1) {
      setSort(arr =>
        arr.map(obj =>
          obj.accessor === key ? { ...obj, status: 0 } : { ...obj, status: -1 }
        )
      )
      handleAscendingSort(key, data, setTableData)
    } else if (sort[id].status === 0) {
      setSort(arr =>
        arr.map(obj =>
          obj.accessor === key ? { ...obj, status: 1 } : { ...obj, status: -1 }
        )
      )
      handleDescendingSort(key, data, setTableData)
    } else if (sort[id].status === 1) {
      setSort(arr =>
        arr.map(obj =>
          obj.accessor === key ? { ...obj, status: -1 } : { ...obj, status: -1 }
        )
      )
      handleResetSort(key, data, setTableData)
    }
  }

  const handleRowSelection = useCallback(
    (e, index) => {
      e.persist()
      setTableData(data =>
        data.map((obj, i) => {
          if (i === index) {
            if (checkHasChildren(obj)) {
              const children = [...obj.children].map(child => ({
                ...child,
                _isCheck: e.target.checked,
              }))
              return {
                ...obj,
                children,
                _isCheck: e.target.checked,
                _isPartial: false,
              }
            }
            return { ...obj, _isCheck: e.target.checked }
          } else {
            return obj
          }
        })
      )
      const data = tableData
        .filter((_, i) => i === index)
        .map(obj => {
          if (checkHasChildren(obj)) {
            const children = [...obj.children].map(child => ({
              ...child,
              _isCheck: e.target.checked,
            }))
            return { ...obj, children, _isCheck: e.target.checked }
          }
          return obj
        })[0]

      isFunction(rowSelectHandler) &&
        rowSelectHandler(e, { ...data, _isCheck: e.target.checked })
    },
    [setTableData, tableData]
  )

  const handleChildRowSelection = useCallback(
    (e, data, parentIndex, childIndex) => {
      e.persist()
      setTableData(data =>
        data.map((obj, i) => {
          if (i === parentIndex && checkHasChildren(obj)) {
            obj.children.map((child, j) => {
              if (j === childIndex) {
                child._isCheck = e.target.checked
              }
              return child
            })
            const isPartial = obj.children.some(
              child => child._isCheck === false
            )
            obj._isPartial = isPartial
            obj._isCheck = obj.children.some(child => child._isCheck === true)
          }
          return obj
        })
      )
      isFunction(rowSelectHandler) &&
        rowSelectHandler(e, { ...data, _isCheck: e.target.checked })
    },
    [setTableData, tableData]
  )

  const checkColumnHasChild = useCallback(
    id => {
      if (columns[0].children !== undefined) {
        if (tableData.length !== 0 && tableData[id].children !== undefined) {
          return Array.isArray(tableData[id].children) &&
            tableData[id].children.length !== 0
            ? true
            : false
        }
        return false
      }
      return false
    },
    [columns, tableData]
  )

  const handleSelectAll = e => {
    let isSelectedAll
    if (!selectAll) {
      setSelectAll(v => !v)
      setTableData(data =>
        data.map(obj => {
          if (checkHasChildren(obj)) {
            const children = obj.children.map(child => ({
              ...child,
              _isCheck: true,
            }))
            return { ...obj, children, _isCheck: true, _isPartial: false }
          }
          return { ...obj, _isCheck: true }
        })
      )
      isSelectedAll = true
    } else {
      setSelectAll(false)
      setTableData(data =>
        data.map(obj => {
          if (checkHasChildren(obj)) {
            const children = obj.children.map(child => ({
              ...child,
              _isCheck: false,
            }))
            return { ...obj, children, _isCheck: false, _isPartial: false }
          }
          return { ...obj, _isCheck: false }
        })
      )
      isSelectedAll = false
    }
    isFunction(onAllRowSelect) && onAllRowSelect(e, isSelectedAll)
  }

  const handleRowClick = (e, obj) => hasRowClickHandler && onRowClick(e, obj)

  const handleRowCollapse = id =>
    id !== undefined &&
    checkColumnHasChild(id) &&
    handleCollapse(id, setCollapse)

  const handleRowMouseEnter = (e, id) =>
    isFunction(onRowMouseEnter) &&
    onRowMouseEnter(e, tableData[id], false, setTableData)

  const handleRowMouseLeave = (e, id) =>
    isFunction(onRowMouseLeave) &&
    onRowMouseLeave(e, tableData[id], false, setTableData)

  const onPageChange = id => {
    setPage(id)
    isFunction(handlePagination) && handlePagination(id)
  }

  const loaderColumns = columns
    .filter(col => col.title !== '\u00a0')
    .map(col => col.title)

  const paginationInfoElement = useMemo(
    () =>
      caption &&
      !hideCaption && (
        <StyledPaginationInfo className='table-pagination-info'>
          <span>{caption}</span>
        </StyledPaginationInfo>
      ),
    [caption, hideCaption]
  )

  if (isFunction(render) && render({ data: tableData }) === null) {
    if (loading) {
      return (
        <TableLoader
          data-testid='table-loader'
          theme='light'
          columns={loaderColumns}
          rowCount={perPage}
        />
      )
    }
  }

  return (
    <>
      {paginationInfoElement}
      <StyledTable
        isSelectable={isSelectable}
        showIndex={showIndex}
        borderRadius={borderRadius}
      >
        {isFunction(customHeader) ? (
          customHeader({ columns, sortArray: sort, handleSort })
        ) : (
          <Head
            columns={columns}
            isSelectable={isSelectable}
            showIndex={showIndex}
            enableSort={enableSort}
            selectAll={selectAll}
            collapse={collapse}
            sort={sort}
            handleSelectAll={handleSelectAll}
            handleSort={handleSort}
            isPartialCheck={isPartial}
          />
        )}
        {isFunction(render) && render({ data: tableData }) !== null ? (
          render({ data: tableData })
        ) : (
          <tbody>
            {Array.isArray(tableData) &&
              tableData.length !== 0 &&
              tableData.map((obj, i) => {
                const hasChildren = checkHasChildren(obj)
                return (
                  <React.Fragment key={i}>
                    {!has(obj, '_rename') ? (
                      <Row
                        columns={columns}
                        showIndex={showIndex}
                        isSelectable={isSelectable}
                        collapse={collapse}
                        collapseOnRowClick={collapseOnRowClick}
                        data={obj}
                        index={i}
                        checkColumnHasChild={checkColumnHasChild}
                        handleRowClick={handleRowClick}
                        handleRowCollapse={handleRowCollapse}
                        handleRowMouseEnter={handleRowMouseEnter}
                        handleRowMouseLeave={handleRowMouseLeave}
                        handleRowSelection={handleRowSelection}
                        handleRename={handleRename}
                        rowClassName={rowClassName}
                        cellClassName={cellClassName}
                        isClickable={
                          hasRowClickHandler ||
                          (hasChildren && collapseOnRowClick)
                        }
                      />
                    ) : (
                      <EditRow
                        data-testid='edit-row'
                        data={obj}
                        columns={columns}
                        showIndex={showIndex}
                        editText={editText}
                        setEditText={setEditText}
                        handleEditSuccess={handleEditSuccess}
                        handleEditCancel={handleEditCancel}
                        isEditRow={true}
                      />
                    )}

                    {!has(obj, '_rename') && hasChildren && collapse[i] && (
                      <ChildRow
                        columns={columns}
                        data={obj}
                        isCollapse={collapse.length !== 0}
                        showIndex={showIndex}
                        isSelectable={isSelectable}
                        parentIndex={i}
                        handleChildRowSelection={handleChildRowSelection}
                        rowClassName={rowClassName}
                        cellClassName={cellClassName}
                        isParentSelected={
                          obj._isCheck &&
                          !obj._isPartial &&
                          !defaultRowSelection
                        }
                        handleRowClick={handleRowClick}
                        onRowMouseEnter={onRowMouseEnter}
                        onRowMouseLeave={onRowMouseLeave}
                        isClickable={hasRowClickHandler}
                      />
                    )}
                  </React.Fragment>
                )
              })}
          </tbody>
        )}
        <tfoot>
          {showPagination && (
            <StyledRow className='table-main-row'>
              <StyledCell
                colSpan={showIndex ? columns.length + 2 : columns.length + 1}
                isfooter={true}
              >
                <Pagination
                  totalRecords={tableData.length && totalRecords}
                  perPage={perPage}
                  activePageNumber={page}
                  handlePagination={onPageChange}
                  setCaption={setCaption}
                  onClick={() => {
                    resetCollapse(setCollapse)
                    if (!allowDataCheckBoxes) {
                      resetAllCheck(setTableData)
                    }
                  }}
                />
              </StyledCell>
            </StyledRow>
          )}
        </tfoot>
      </StyledTable>
    </>
  )
}

Table.defaultProps = {
  loading: false,
  borderRadius: '4px 4px 0 0',
  showIndex: false,
  isSelectable: false,
  enableSort: false,
  showPagination: false,
  tableCaption: '',
  collapseOnRowClick: true,
  handleAscendingSort: (accessor, data, callback) => {
    const result = [...data].sort((a, b) =>
      a[accessor] > b[accessor] ? 1 : -1
    )
    callback(result)
  },
  handleDescendingSort: (accessor, data, callback) => {
    const result = [...data].sort((a, b) =>
      a[accessor] < b[accessor] ? 1 : -1
    )
    callback(result)
  },
  handleResetSort: (accessor, data, callback) => {
    callback(data)
  },
  render: () => {
    return null
  },
  rowClassName: '',
  cellClassName: '',
  activePageNumber: 1,
  hideCaption: false,
  defaultRowSelection: false,
}

Table.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  /** show/hide table loader using loading (true/false) */
  loading: PropTypes.bool,
  /** set table border radius */
  borderRadius: PropTypes.string,
  /** show/hide row index column (true/false) */
  showIndex: PropTypes.bool,
  /** show/hide checkbox in row using isSelectable (true/false) */
  isSelectable: PropTypes.bool,
  /** enable/disable sorting for all columns */
  enableSort: PropTypes.bool,
  /** row handler for mouse enter, (event,rowdata,isChild)=>{} */
  onRowMouseEnter: PropTypes.func,
  /** row handler for mouse leave, (event,rowdata,isChild)=>{} */
  onRowMouseLeave: PropTypes.func,
  /** handler for selecting all rows, (event,isSelected)=>{} */
  onAllRowSelect: PropTypes.func,
  /** handler for row click (event,rowData)=>{ }*/
  onRowClick: PropTypes.func,
  /** handler for checkbox selection for each row (event,rowData)=>{ }*/
  rowSelectHandler: PropTypes.func,
  /** handler for ascending sort */
  handleAscendingSort: PropTypes.func,
  /** handler for descending sort */
  handleDescendingSort: PropTypes.func,
  /** handler for resetting sort state */
  handleResetSort: PropTypes.func,
  /** show/hide pagination */
  showPagination: PropTypes.bool,
  totalRecords: PropTypes.number,
  perPage: PropTypes.number,
  /** handler for page change, (pageIndex)=>{} */
  handlePagination: PropTypes.func,
  activePageNumber: PropTypes.number,
  rowClassName: PropTypes.string,
  cellClassName: PropTypes.string,
  /** handler for rename option in menu,
   * handler is available as parameter in render.
   * compare the parameter with the table data and add
   * _rename property to it to enter into edit mode.
   */
  handleRename: PropTypes.func,
  /** handler for edit success,
   * (event,rowData,editText)=>{},
   * delete _rename key from the object to exit the
   * edit mode.
   */
  handleEditSuccess: PropTypes.func,
  /** handler for canceling edit.
   * (event,rowData)=>{},
   * delete _rename key from the object to exit the
   * edit mode.
   */
  handleEditCancel: PropTypes.func,
  /** {({data})=>{
   * 	return custom component or null`
   * }
   * */
  customHeader: PropTypes.func,
  /** render prop for custom table body render={({data})=> return table body} */
  render: PropTypes.func,
  changedPageNumber: PropTypes.number,
  /** set table caption when using pagination as separate component */
  tableCaption: PropTypes.string,
  /** collapse child rows on parent row click,(true/false) */
  collapseOnRowClick: PropTypes.bool,
  /** hide table caption(true/false) */
  hideCaption: PropTypes.bool,
  /** set true for default row selection with _isCheck:true in row data object or else set false */
  defaultRowSelection: PropTypes.bool,
  /** isChecked checkbox status from data allowed */
  allowDataCheckBoxes: PropTypes.bool,
}

const handleCollapse = (id, callback) => {
  callback(arr => arr.map((v, i) => (id === i ? !v : false)))
}

const resetCollapse = callback => callback(arr => arr.map(() => false))

const resetAllCheck = callback =>
  callback(arr =>
    arr.map(obj => {
      obj._isCheck = false
      return obj
    })
  )

const checkHasChildren = obj => !isEmpty(obj.children)
