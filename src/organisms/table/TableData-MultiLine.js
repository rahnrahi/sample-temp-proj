import React from 'react'
import styled from 'styled-components'
import { v4 as uuidv4 } from 'uuid'

import { renderMultiline } from './utils/renderMultiline'

const StyledTitle = styled.div`
  width: 250px;
`

const StyledType = styled.div`
  width: 200px;
`

export const multilineColumns = [
  {
    title: 'Attribute title',
    accessor: 'title',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: ({ title }) => <StyledTitle>{title}</StyledTitle>,
  },
  {
    title: 'Type',
    accessor: 'type',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: ({ type }) => <StyledType>{type}</StyledType>,
  },
  {
    title: 'Statuses',
    accessor: 'statuses',
    isSortable: false,
    // eslint-disable-next-line react/prop-types
    render: renderMultiline('statuses', [<div key={0} />, <div key={1} />]),
  },
]

export const multilineData = [
  {
    uid: uuidv4(),
    title: 'vendor',
    type: 'Boolean',
    statuses: [''],
  },
  {
    uid: uuidv4(),
    title: 'Vendor Part ID',
    type: 'Dynamic list of attributes',
    statuses: [''],
  },
  {
    uid: uuidv4(),
    title: 'Vendor GLN',
    type: 'Date',
    statuses: ['Status 1', 'Another'],
    _isDisable: true,
  },
  {
    uid: uuidv4(),
    title: 'Product Short Description',
    type: 'List of values',
    status: [''],
  },
  {
    uid: uuidv4(),
    title: 'Manufacture type',
    type: 'Text',
    status: [''],
  },
  {
    uid: uuidv4(),
    title: 'Country of Origin',
    type: 'Boolean',
    status: ['Used by a different group'],
  },
  {
    uid: uuidv4(),
    title: 'UPC Code',
    type: 'List of values',
    status: [''],
  },
]
