import React from 'react'
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react'
import 'jest-styled-components'
import { Table } from './Table'
import { columns, data } from './TestData'

afterEach(cleanup)

const fn = jest.fn()

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => '1'),
  }
})

describe('<Table/>', () => {
  const getPageInfoCaption = queryByText =>
    queryByText(/Showing \d+-\d+ of \d+/i)

  it('should render Table component with pagination', () => {
    const { container, getByTestId } = render(
      <Table
        columns={columns}
        data={data}
        showPagination={true}
        totalRecords={3}
        perPage={2}
        handlePagination={fn}
      />
    )
    fireEvent.click(getByTestId('expansion-arrow-1'))
    expect(container).toMatchSnapshot()
  })

  it('should render Table component without pagination', () => {
    const { container } = render(<Table columns={columns} data={data} />)
    expect(container).toMatchSnapshot()
  })

  it('should render Table component with index column', () => {
    const { container } = render(
      <Table columns={columns} data={data} showIndex={true} />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render Table component with selectable column', () => {
    const { container, getByTestId } = render(
      <Table
        columns={columns}
        data={data}
        showIndex={true}
        isSelectable={true}
        rowSelectHandler={fn}
        onRowMouseEnter={fn}
        onRowMouseLeave={fn}
        onAllRowSelect={fn}
        onRowClick={fn}
      />
    )
    const row = getByTestId('row-0')
    fireEvent.mouseEnter(row)
    fireEvent.mouseLeave(row)
    fireEvent.click(row)
    fireEvent.click(getByTestId('select-all'))
    expect(container).toMatchSnapshot()
  })

  it('should render Table component without index column', () => {
    const { container } = render(<Table columns={columns} data={data} />)
    expect(container).toMatchSnapshot()
  })

  it('should render Table component with sorting', () => {
    const { container } = render(
      <Table
        columns={columns}
        data={data}
        enableSort={true}
        handleAscendingSort={fn}
        handleDescendingSort={fn}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render table with edit row', () => {
    const data1 = data.map((obj, i) => {
      if (i === 0) {
        return { ...obj, _rename: 1 }
      }
      return obj
    })
    const { container, getByTestId } = render(
      <Table
        columns={columns}
        data={data1}
        handleRename={fn}
        handleEditSuccess={fn}
        handleEditCancel={fn}
      />
    )
    expect(container).toMatchSnapshot()
    expect(getByTestId('edit-success-btn')).toBeInTheDocument()
    expect(getByTestId('edit-cancel-btn')).toBeInTheDocument()
    fireEvent.click(getByTestId('edit-success-btn'))
    expect(container).toMatchSnapshot()
  })

  it('should render table loader with loading prop', () => {
    const { container, getByTestId } = render(
      <Table columns={columns} data={data} loading={true} />
    )
    expect(getByTestId('table-loader')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('should render table with custom table body using render prop and data=[]', () => {
    const { getByText } = render(
      <Table
        columns={columns}
        data={[]}
        render={({ data }) => {
          return data.length === 0 ? (
            <tbody>
              <tr>
                <td>No Search Result</td>
              </tr>
            </tbody>
          ) : null
        }}
      />
    )
    expect(getByText('No Search Result')).toBeInTheDocument()
  })
  it('should render table with custom header', () => {
    const { container, getByTestId } = render(
      <Table
        columns={columns}
        data={data}
        customHeader={({ columns, sortArray, handleSort }) => {
          return (
            <thead>
              <tr>
                {columns.length &&
                  columns.map(({ title, accessor }, i) => (
                    <th
                      key={i}
                      data-testid={`${title}`}
                      onClick={() => {
                        handleSort(accessor, i)
                      }}
                    >
                      <span>{title}</span>
                      <span data-testid={`${title}-status`}>
                        {sortArray[i].status}
                      </span>
                    </th>
                  ))}
              </tr>
            </thead>
          )
        }}
      />
    )
    const column = getByTestId('Name')
    const status = getByTestId('Name-status')
    expect(container).toMatchSnapshot()
    expect(status).toHaveTextContent('-1')
    fireEvent.click(column)
    expect(status).toHaveTextContent('0')
    fireEvent.click(column)
    expect(status).toHaveTextContent('1')
    fireEvent.click(column)
    expect(status).toHaveTextContent('-1')
  })

  it('should render table with collapseOnRowClick=false ', () => {
    const { container, getByTestId } = render(
      <Table columns={columns} data={data} collapseOnRowClick={false} />
    )
    fireEvent.click(getByTestId('collapse-chevron-1'))
    expect(container).toMatchSnapshot()
  })

  it('should render table with partial state of checkbox in head  ', () => {
    const { container, getByTestId } = render(
      <Table columns={columns} data={data} isSelectable rowSelectHandler={fn} />
    )
    fireEvent.click(getByTestId('select-checkbox-0'))
    expect(container).toMatchSnapshot()
  })

  it('should update without crashing when columns prop changes', () => {
    const newColumns = [
      ...columns,
      {
        title: 'New column',
        accessor: 'new-column',
      },
    ]
    const { container, rerender } = render(
      <Table columns={columns} data={data} />
    )
    expect(container).toMatchSnapshot()
    rerender(<Table data={data} columns={newColumns} />)
    expect(container).toMatchSnapshot()
  })

  it('should render a table that shows a pointer on all rows when a row click handler is attached', () => {
    const { getByTestId } = render(
      <Table columns={columns} data={data} onRowClick={fn} />
    )

    const pointerStyle = 'cursor: pointer;'
    const expandableRow = getByTestId('row-0')

    expect(expandableRow).toHaveStyle(pointerStyle)

    fireEvent.click(expandableRow)

    const childRow = expandableRow.nextSibling

    expect(childRow).toHaveClass('child-row')

    // non-expandable rows
    expect(childRow).toHaveStyle(pointerStyle)
    expect(getByTestId('row-1')).toHaveStyle(pointerStyle)
    expect(getByTestId('row-2')).toHaveStyle(pointerStyle)
  })

  it('should render a table that shows a pointer on expandable rows when collapseOnRowClick is true', () => {
    const { getByTestId } = render(
      <Table columns={columns} data={data} collapseOnRowClick={true} />
    )

    const pointerStyle = 'cursor: pointer;'
    const expandableRow = getByTestId('row-0')

    expect(expandableRow).toHaveStyle(pointerStyle)

    fireEvent.click(expandableRow)

    const childRow = expandableRow.nextSibling

    expect(childRow).toHaveClass('child-row')

    // non-expandable rows
    expect(childRow).not.toHaveStyle(pointerStyle)
    expect(getByTestId('row-1')).not.toHaveStyle(pointerStyle)
    expect(getByTestId('row-2')).not.toHaveStyle(pointerStyle)
  })

  it('should render a table that does not show a pointer when rows are not clickable', () => {
    const { getByTestId } = render(
      <Table columns={columns} data={data} collapseOnRowClick={false} />
    )

    const pointerStyle = 'cursor: pointer;'
    const expandableRow = getByTestId('row-0')

    expect(expandableRow).not.toHaveStyle(pointerStyle)

    // Click on expansion arrow, not the row, since collapseOnRowClick is false
    fireEvent.click(getByTestId('collapse-chevron-1'))

    const childRow = expandableRow.nextSibling

    expect(childRow).toHaveClass('child-row')

    // non-expandable rows
    expect(childRow).not.toHaveStyle(pointerStyle)
    expect(getByTestId('row-1')).not.toHaveStyle(pointerStyle)
    expect(getByTestId('row-2')).not.toHaveStyle(pointerStyle)
  })

  it('should render a table that shows the page information', () => {
    const { queryByText } = render(
      <Table
        columns={columns}
        data={data}
        hideCaption={false}
        showPagination={true}
      />
    )

    waitFor(() => {
      expect(getPageInfoCaption(queryByText)).toBeInTheDocument()
    })
  })

  it('should render a table without the page information if hideCaption is true', () => {
    const { queryByText } = render(
      <Table
        columns={columns}
        data={data}
        hideCaption={true}
        showPagination={true}
      />
    )

    waitFor(() => {
      expect(getPageInfoCaption(queryByText)).not.toBeInTheDocument()
    })
  })

  it('should render a table without the page information if showPagination is false and tableCaption is not set', () => {
    const { queryByText } = render(
      <Table
        columns={columns}
        data={data}
        hideCaption={false}
        showPagination={false}
      />
    )

    waitFor(() => {
      expect(getPageInfoCaption(queryByText)).not.toBeInTheDocument()
    })
  })

  it('should update the custom pagination caption on the table', async () => {
    const initialPaginationCaption = 'My Custom Caption'
    const updatedPaginationCaption = 'Updated Caption'

    const { findByText, rerender } = render(
      <Table
        columns={columns}
        data={data}
        hideCaption={false}
        showPagination={false}
        tableCaption={initialPaginationCaption}
      />
    )

    const paginationCaption = await findByText(initialPaginationCaption)

    expect(paginationCaption).toBeInTheDocument()
    expect(paginationCaption).toHaveTextContent(initialPaginationCaption)

    rerender(
      <Table
        columns={columns}
        data={data}
        hideCaption={false}
        showPagination={false}
        tableCaption={updatedPaginationCaption}
      />
    )

    await waitFor(async () => {
      expect(paginationCaption).toHaveTextContent(updatedPaginationCaption)
    })
  })
})
