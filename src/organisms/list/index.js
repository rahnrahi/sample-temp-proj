import React from 'react'
import PropTypes from 'prop-types'
import { ListRoot } from './styles'
import { ListItem, ListSubtitle, ListTitle } from '../../atoms'
import ListCollapsibleItem from '../../molecules/list-collapsible-item'

const List = ({ children, ...otherProps }) => {
  return <ListRoot {...otherProps}>{children}</ListRoot>
}
List.Title = ListTitle
List.Subtitle = ListSubtitle
List.Item = ListItem
List.CollapsibleItem = ListCollapsibleItem
List.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

export default List
