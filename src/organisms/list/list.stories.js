import React, { useRef } from 'react'
import { default as List } from './index'
import Collapse from '../../atoms/collapse'
import { theme } from '../../shared'
import styled from 'styled-components'
import {
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION,
} from '../../hooks/constants'
import { getDesignSpecifications } from '../../hooks/utils'

const StoryContainer = styled.div`
  width: 222px;
`
const OrdersSummaryListRow = styled(List.Subtitle)`
  display: flex;
  justify-content: space-between;
`
const OrderSummaryDetailPanelRow = styled.div`
  display: flex;
  padding: 0.5rem 0;
  justify-content: space-between;
  ${theme.typography.h6}
  line-height: 16px;
  color: ${theme.palette.brand.primary.gray};
`
const OrderSummaryDetailPanel = styled.div`
  margin-left: 0.5rem;
  margin-top: -0.5rem;
`
const listSections = [
  'Created',
  'Backorder',
  'Pre-order',
  'Ready to allocate',
  'Allocated',
  'Partially allocated',
  'Shipped',
  'Partially shipped',
  'Delivered',
  'Partially delivered',
  'Returned',
  'Partially returned',
  'Cancelled',
  'Partially cancelled',
  'Hold',
  'Fraud',
]
const listCollapses = ['Order Status', 'Shipment', 'Delivery', 'Payment']
const Template = () => {
  return (
    <StoryContainer>
      <List>
        <Collapse defaultExpandedSections={['0']}>
          <List.Title>Orders information</List.Title>
          <OrdersSummaryListRow>
            <span>Total orders:</span> <span>999,999</span>
          </OrdersSummaryListRow>
          {listCollapses.map((collapseName, collapseIdx) => (
            <List.CollapsibleItem
              key={collapseIdx}
              title={collapseName}
              id={collapseIdx.toString()}
            >
              <OrderSummaryDetailPanel>
                {listSections.map((name, idx) => (
                  <OrderSummaryDetailPanelRow key={idx}>
                    <span>{name}</span>
                    <span>999</span>
                  </OrderSummaryDetailPanelRow>
                ))}
              </OrderSummaryDetailPanel>
            </List.CollapsibleItem>
          ))}
          <List.Item>Item 01</List.Item>
          <List.Item>Item 02</List.Item>
        </Collapse>
      </List>
    </StoryContainer>
  )
}
const TemplateEvents = () => {
  const handleOnCollapse = (item, currentState) => {
    alert(
      `Section ${item} triggered collapse, current expansion state: ${JSON.stringify(
        currentState
      )}`
    )
  }
  const handleOnExpand = (item, currentState) => {
    alert(
      `Section ${item} triggered expand, current expansion state: ${JSON.stringify(
        currentState
      )}`
    )
  }
  const eventOutlet = useRef(null)
  const handleMouseOver = e => {
    if (eventOutlet.current) {
      eventOutlet.current.innerHTML = JSON.stringify({
        event: e.type,
        pageX: e.pageX,
        pageY: e.pageY,
      })
    }
  }
  return (
    <StoryContainer>
      <div ref={eventOutlet} />
      <List>
        <Collapse
          defaultExpandedSections={['0']}
          onCollapse={handleOnCollapse}
          onExpand={handleOnExpand}
        >
          <List.Title>Orders information</List.Title>
          <OrdersSummaryListRow>
            <span>Total orders:</span> <span>999,999</span>
          </OrdersSummaryListRow>
          {listCollapses.map((collapseName, collapseIdx) => (
            <List.CollapsibleItem
              key={collapseIdx}
              onMouseOver={handleMouseOver}
              title={collapseName}
              id={collapseIdx.toString()}
            >
              <OrderSummaryDetailPanel>
                {listSections.map((name, idx) => (
                  <OrderSummaryDetailPanelRow key={idx}>
                    <span>{name}</span>
                    <span>999</span>
                  </OrderSummaryDetailPanelRow>
                ))}
              </OrderSummaryDetailPanel>
            </List.CollapsibleItem>
          ))}
          <List.Item>Item 01</List.Item>
          <List.Item>Item 02</List.Item>
        </Collapse>
      </List>
    </StoryContainer>
  )
}

export const BasicList = Template.bind({})
BasicList.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)
export const EventHandlers = TemplateEvents.bind({})
EventHandlers.parameters = getDesignSpecifications(
  EXPANDABLE_CARD_DESIGN,
  EXPANDABLE_CARD_PRESENTATION
)

export default {
  title: 'Modules/Cards/List',
  component: List,
}
