import { theme } from '../../shared'
import styled from 'styled-components'

export const ListRoot = styled.div`
  background: ${theme.palette.brand.primary.white};
  color: ${theme.palette.brand.primary.charcoal};
  border-radius: 0.25rem;
  padding: 1rem 0;
`
