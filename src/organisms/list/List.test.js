import React from 'react'
import { render } from '@testing-library/react'
import List from './'

describe('<List/>', () => {
  it('should render correctly', () => {
    const { container } = render(
      <List>
        <List.Item>List item content</List.Item>
      </List>
    )
    expect(container).toMatchSnapshot()
  })
})
