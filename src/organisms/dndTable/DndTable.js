import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useTable } from 'react-table'
import { StyledDndTable } from './DndTable.style'
import TableRow from './DndTableRow'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

export const DndTable = ({
  columns,
  data,
  isDraggable,
  canRemoveRow,
  onRemoveRow,
  onRowDragEnd,
}) => {
  const [records, setRecords] = useState([])

  const { getTableProps, headerGroups, rows, prepareRow } = useTable({
    data: records,
    columns,
  })

  const reorderData = (startIndex, endIndex) => {
    const newData = [...records]
    const [movedRow] = newData.splice(startIndex, 1)
    newData.splice(endIndex, 0, movedRow)
    setRecords(newData)
    onRowDragEnd && onRowDragEnd(startIndex, endIndex)
  }

  const removeRow = (row, index) => {
    onRemoveRow && onRemoveRow(row, index)
  }

  const onDragEnd = React.useCallback(
    ({ source, destination }) => {
      if (!destination) return
      reorderData(source.index, destination.index)
    },
    [records]
  )

  React.useEffect(() => {
    setRecords(data)
  }, [data])

  return (
    <StyledDndTable isDraggable={isDraggable} canRemoveRow={canRemoveRow}>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup, headerGroupindex) => (
            <tr key={headerGroupindex} {...headerGroup.getHeaderGroupProps()}>
              {isDraggable && <th></th>}
              {headerGroup.headers.map((column, headerIndex) => (
                <th key={headerIndex} {...column.getHeaderProps()}>
                  {column.render('Header')}
                </th>
              ))}
              {canRemoveRow && <th></th>}
            </tr>
          ))}
        </thead>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId='table-body'>
            {provided => (
              <tbody ref={provided.innerRef} {...provided.droppableProps}>
                {rows.map(row => {
                  prepareRow(row)
                  return (
                    <Draggable
                      draggableId={row.original.uid}
                      key={row.original.uid}
                      index={row.index}
                    >
                      {(drgProvider, snapshot) => {
                        return (
                          <TableRow
                            row={row}
                            isDraggable={isDraggable}
                            canRemoveRow={canRemoveRow}
                            index={row.index}
                            provided={drgProvider}
                            snapshot={snapshot}
                            removeRow={() => removeRow(row.original, row.index)}
                          />
                        )
                      }}
                    </Draggable>
                  )
                })}
                {provided.placeholder}
              </tbody>
            )}
          </Droppable>
        </DragDropContext>
      </table>
    </StyledDndTable>
  )
}

DndTable.defaultProps = {
  canRemoveRow: true,
  isDraggable: true,
}

DndTable.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  /** enable/disable table row drag and drop feature (true/false) */
  isDraggable: PropTypes.bool,
  /** enable/disable table row remove feature (true/false) */
  canRemoveRow: PropTypes.bool,
  /** handler for row remove event (rowData, index)=>{ }*/
  onRemoveRow: PropTypes.func,
  /** handler for row drag end event (sourceIndex, destinationIndex)=>{ }*/
  onRowDragEnd: PropTypes.func,
}
