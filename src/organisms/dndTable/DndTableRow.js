/* eslint-disable react/prop-types */
import React from 'react'
import { TrWrapper, MoveIcon, DeleteIcon } from './DndTable.style'
import { Icon } from '../../atoms/icon/Icon'

const UpDownArrow = props => (
  <span
    {...props.dragHandleProps}
    className={props.className}
    aria-label='move'
    role='img'
    data-testid={`move-${props.index}`}
  >
    <MoveIcon {...props}>
      <Icon iconName='DragAndDrop' size={16} />
    </MoveIcon>
  </span>
)

const Row = ({
  row,
  provided,
  snapshot,
  isDraggable,
  canRemoveRow,
  removeRow,
}) => {
  return (
    <TrWrapper
      {...row.getRowProps()}
      {...provided.draggableProps}
      ref={provided.innerRef}
      isDragging={snapshot.isDragging}
      isDraggable={isDraggable}
      canRemoveRow={canRemoveRow}
    >
      {isDraggable && (
        <td>
          <UpDownArrow
            index={row.index}
            isDragging={snapshot.isDragging}
            dragHandleProps={provided.dragHandleProps}
            isSomethingDragging={snapshot.isDraggingOver}
          />
        </td>
      )}

      {row.cells.map((cell, clindex) => (
        <td key={clindex} {...cell.getCellProps()}>
          {cell.render('Cell')}
        </td>
      ))}
      {canRemoveRow && (
        <td>
          <DeleteIcon
            data-testid={`delete-${row.index}`}
            aria-label='delete'
            role='button'
            onClick={removeRow}
          >
            <Icon iconName='Delete' size={24} />
          </DeleteIcon>
        </td>
      )}
    </TrWrapper>
  )
}

export default Row
