import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { DndTable } from './DndTable'
import { Columns, mockData } from './DndTableData-mock'

describe('<DndTable/>', () => {
  it('should render drag and drop Table component with mock data', () => {
    render(<DndTable columns={Columns} data={mockData} />)
    const tableCell = screen.getByText('Jack Harris')
    expect(tableCell).toBeInTheDocument()
    expect(screen.getByText('Name', { selector: 'th' })).toBeInTheDocument()
  })

  it('should check drag and drop Table component hover and delete action', () => {
    const onRowdeleteFn = jest.fn()
    render(
      <DndTable columns={Columns} data={mockData} onRemoveRow={onRowdeleteFn} />
    )
    const tableCell = screen.getByText('Jack Harris')
    fireEvent.mouseOver(tableCell)
    const deleteIcon = screen.getByTestId('delete-1', { hidden: false })
    fireEvent.click(deleteIcon)
    expect(onRowdeleteFn).toBeCalledTimes(1)
  })

  it('should check drag and drop action of the table row', () => {
    const onRowDragEnd = jest.fn()
    const mouse = [
      { clientX: 10, clientY: 20 },
      { clientX: 15, clientY: 30 },
    ]
    render(
      <DndTable columns={Columns} data={mockData} onRowDragEnd={onRowDragEnd} />
    )
    const tableCell = screen.getByText('Jack Harris')
    fireEvent.mouseOver(tableCell)
    const moveIcon = screen.getByTestId('move-1', { hidden: false })
    fireEvent.mouseDown(moveIcon, mouse[0])
    fireEvent.mouseMove(moveIcon, mouse[1])
    fireEvent.mouseUp(moveIcon)
    expect(onRowDragEnd).toBeCalledTimes(1)
  })
})
