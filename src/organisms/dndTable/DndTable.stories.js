import React from 'react'
import { DndTable } from './DndTable'
import { Columns, mockData } from './DndTableData-mock'
import styled from 'styled-components'
import {
  TABLE_DND_DESIGN,
  TABLE_DND_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Tables/Drag and Drop',
  component: DndTable,
  argTypes: {},
}

const Container = styled.div`
  padding: 40px;
  background: #ccc;
  width: 900px;
  .custom-col {
    padding-left: 20px;
  }
`

const TemplateDefault = args => {
  return (
    <Container>
      <DndTable {...args} />
    </Container>
  )
}

export const Default = TemplateDefault.bind({})
Default.args = {
  columns: Columns,
  data: mockData,
  isDraggable: true,
  canRemoveRow: true,
}
Default.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_DND_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_DND_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
