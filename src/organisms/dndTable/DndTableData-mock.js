import { v4 as uuidv4 } from 'uuid'

export const Columns = [
  {
    Header: 'ID',
    accessor: 'uid',
  },

  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Birth Date',
    accessor: 'birth_date',
  },
]

export const mockData = [
  {
    uid: uuidv4(),
    name: 'Annebelle Sinh',
    birth_date: 'Jan. 20, 2020 4:00 PM',
  },
  {
    uid: uuidv4(),
    name: 'Jack Harris',
    birth_date: 'Jan. 20, 2000 4:00 PM',
  },
  {
    uid: uuidv4(),
    name: 'Tom Moody',
    birth_date: 'Jan. 20, 2001 4:00 PM',
  },
  {
    uid: uuidv4(),
    name: 'Raj Sharma',
    birth_date: 'Jan. 20, 1987 4:00 PM',
  },
]
