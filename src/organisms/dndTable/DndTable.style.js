import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledDndTable = styled.div`
  table {
    table-layout: fixed;
    width: 100%;
    background: white;
    box-sizing: border-box;
    border-collapse: collapse;
    thead {
      background: white;
      height: 15px;
      ${theme.typography.h6}
      color: ${theme.palette.brand.primary.gray};
      box-shadow: inset 0px -1px 0px ${theme.palette.ui.neutral.grey2};

      th {
        text-align: left;
        padding-bottom: 8px;
        padding-top: 1px;
        :first-child {
          width: ${({ isDraggable }) => (isDraggable ? '32px' : 'auto')};
          padding-left: ${({ isDraggable }) => (isDraggable ? '0px' : '32px')};
        }
        :last-child {
          width: ${({ canRemoveRow }) => (canRemoveRow ? '24px' : 'auto')};
        }
        :not(:first-child) {
          padding-right: 32px;
        }
      }
    }
    tbody {
      tr:not(:last-child) {
        box-shadow: inset 0px -1px 0px ${theme.palette.ui.neutral.grey4};
      }
    }
  }
`

export const TrWrapper = styled.tr`
  display: ${({ isDragging }) => (isDragging ? 'table' : '')};
  background: white;
  ${({ isDragging }) =>
    isDragging
      ? `
    border: 2px ${theme.palette.ui.states.active} solid;
    border-radius: 2px;
    `
      : ''};
  height: 32px;
  ${theme.typography.h6}
  color: ${theme.palette.brand.primary.charcoal};
  td:first-child {
    padding-left: ${({ isDraggable }) => (isDraggable ? '0px' : '32px')};
  }
  td:not(:first-child):not(:last-child):not(:nth-last-child(2)) {
    padding-right: 32px;
  }
  td:nth-last-child(2) {
    padding-right: ${({ canRemoveRow }) => (canRemoveRow ? '0px' : '32px')};
  }
  td:last-child {
    padding-right: ${({ canRemoveRow }) => (canRemoveRow ? '0px' : '32px')};
  }
  td {
    margin: 0px;
    padding-bottom: 12px;
    padding-top: 12px;
    text-align: left;
    height: 32px;
  }
`
export const MoveIcon = styled.span`
  padding-left: 8px;
  svg {
    visibility: ${({ isDragging }) => (isDragging ? 'visible' : 'hidden')};
  }
  tr:hover & {
    svg {
      visibility: visible;
    }
  }
`

export const DeleteIcon = styled.span`
  svg {
    visibility: hidden;
  }
  tr:hover & {
    svg {
      visibility: visible;
    }
  }
  cursor: pointer;
`
