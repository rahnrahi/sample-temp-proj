import React, { useState } from 'react'
import styled from 'styled-components'
import { TableWithScroll } from './TableWithScroll'
import { columns, data } from './data'
import TablesDocs from '../../../docs/Tables.mdx'
import {
  TABLE_SCROLL_DESIGN,
  TABLE_SCROLL_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Tables/TableWithScroll',
  component: TableWithScroll,
  argTypes: {},
  parameters: {
    docs: {
      page: TablesDocs,
    },
  },
}

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 1000px;
  padding: 70px;
  background: #ccc;
`

// eslint-disable-next-line react/prop-types
const Template = ({ tableProps, ...args }) => {
  // eslint-disable-next-line react/prop-types
  const { data, perPage } = tableProps
  const [input, setInput] = useState(() => {
    // eslint-disable-next-line react/prop-types
    return data.slice(0, 4)
  })

  const handlePagination = id => {
    // eslint-disable-next-line react/prop-types
    const d = data.slice(perPage * (id - 1), perPage * id)
    setInput(d)
  }

  return (
    <StyledContainer>
      <TableWithScroll
        {...args}
        tableProps={{
          ...tableProps,
          data: input,
          handlePagination: handlePagination,
        }}
      />
    </StyledContainer>
  )
}

export const WithScroll = Template.bind({})

WithScroll.args = {
  width: '800px',
  tableProps: {
    columns,
    data,
    borderRadius: '4px',
    totalRecords: 12,
    perPage: 4,
    showPagination: true,
  },
}
WithScroll.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: TABLE_SCROLL_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: TABLE_SCROLL_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
