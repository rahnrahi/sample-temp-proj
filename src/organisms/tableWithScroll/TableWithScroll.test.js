import React from 'react'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import 'jest-styled-components'
import { TableWithScroll } from './TableWithScroll'
import { columns, data } from './data'

afterEach(cleanup)

const fn = jest.fn()

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => '1'),
  }
})

describe('<TableWithScroll/>', () => {
  it('should render TableWithScroll component with pagination', () => {
    const { container } = render(
      <TableWithScroll
        width='800px'
        tableProps={{
          columns,
          data,
          showPagination: true,
          totalRecords: 12,
          perPage: 4,
          handlePagination: fn,
        }}
      />
    )
    expect(container).toMatchSnapshot()
  })

  it('should render TableWithScroll component without pagination', () => {
    const { container } = render(
      <TableWithScroll
        width='800px'
        tableProps={{
          columns,
          data,
          showPagination: false,
        }}
      />
    )
    expect(container).toMatchSnapshot()
  })

  describe('Given activePageNumber property', () => {
    it('should keep it in sync with internal pagination state', () => {
      const { rerender } = render(
        <TableWithScroll
          width='800px'
          tableProps={{
            columns,
            data,
            showPagination: true,
            totalRecords: 12,
            perPage: 4,
            handlePagination: jest.fn(),
          }}
        />
      )
      expect(screen.queryByTestId('previous-nav')).not.toBeVisible()
      expect(screen.queryByTestId('next-nav')).toBeVisible()
      fireEvent.click(screen.queryByTestId('pageNumber-2'))
      expect(screen.queryByTestId('next-nav')).toBeVisible()
      rerender(
        <TableWithScroll
          width='800px'
          tableProps={{
            activePageNumber: 3,
            columns,
            data,
            showPagination: true,
            totalRecords: 12,
            perPage: 4,
            handlePagination: jest.fn(),
          }}
        />
      )
      expect(screen.queryByTestId('next-nav')).not.toBeVisible()
    })
  })
})
