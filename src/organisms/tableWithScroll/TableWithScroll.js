import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Pagination, Table } from '../table'
import { StyledContainer, StyledWrapper, StyleFooter } from './Table.style'

export const TableWithScroll = ({ width, tableProps }) => {
  const {
    borderRadius,
    data,
    showPagination,
    totalRecords,
    perPage,
    handlePagination,
    activePageNumber = 1,
  } = tableProps
  const [page, setPageChange] = useState(activePageNumber)
  const [caption, setCaption] = useState('')

  const onPageChange = useCallback(
    id => {
      setPageChange(id)
      handlePagination(id)
    },
    [handlePagination]
  )

  useEffect(() => {
    Number.isInteger(activePageNumber) && setPageChange(activePageNumber)
  }, [activePageNumber])

  return (
    <StyledWrapper width={width}>
      <StyledContainer width={width} borderRadius={borderRadius}>
        <Table
          {...tableProps}
          changedPageNumber={page}
          borderRadius='0px'
          showPagination={false}
          activePageNumber={undefined}
          tableCaption={caption}
        />
      </StyledContainer>
      {showPagination && (
        <StyleFooter borderRadius={borderRadius}>
          <Pagination
            showPagination={showPagination}
            totalRecords={data && data.length && totalRecords}
            activePageNumber={page}
            perPage={perPage}
            setCaption={setCaption}
            handlePagination={onPageChange}
          />
        </StyleFooter>
      )}
    </StyledWrapper>
  )
}

TableWithScroll.defaultProps = {
  width: '100%',
}

TableWithScroll.propTypes = {
  width: PropTypes.string,
  tableProps: PropTypes.object,
}
