import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledWrapper = styled.div`
  position: relative;
  width: ${({ width }) => (width ? width : '100%')};
  .expansion-cell {
    box-sizing: border-box;
    padding-left: 50px;
    width: 96px !important;
    height: 69px !important;
  }
`

export const StyledContainer = styled.div`
  width: ${({ width }) => width};
  border-top-left-radius: ${({ borderRadius }) =>
    borderRadius ? borderRadius : '0px'};
  border-top-right-radius: ${({ borderRadius }) =>
    borderRadius ? borderRadius : '0px'};
  background: ${theme.palette.brand.primary.white};
  padding-bottom: ${({ width }) => (width === '100%' ? '0px' : '24px')};
  overflow-x: auto;
  scroll-behavior: smooth;
  ::-webkit-scrollbar {
    padding-left: 48px;
    height: 3px;
  }
  ::-webkit-scrollbar-track {
    background: ${theme.palette.ui.neutral.grey4};
    border-radius: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background: ${theme.palette.brand.primary.charcoal};
    border-radius: 4px;
  }

  ::-webkit-scrollbar-button {
    display: block;
    width: 24px;
  }

  ::-webkit-scrollbar-button:decrement:start {
    background-color: ${theme.palette.brand.primary.white};
  }

  ::-webkit-scrollbar-button:increment:start {
    background-color: ${theme.palette.brand.primary.white};
  }

  ::-webkit-scrollbar-button:decrement:end {
    background-color: ${theme.palette.brand.primary.white};
  }

  ::-webkit-scrollbar-button:increment:end {
    background-color: ${theme.palette.brand.primary.white};
  }

  > .table-pagination-info {
    position: static;
  }
`

export const StyleFooter = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 25px 24px 25px 0px;
  border-bottom-left-radius: ${({ borderRadius }) =>
    borderRadius ? borderRadius : '0px'};
  border-bottom-right-radius: ${({ borderRadius }) =>
    borderRadius ? borderRadius : '0px'};
  background: ${theme.palette.brand.primary.white};
`
