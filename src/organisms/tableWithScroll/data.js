import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import styled from 'styled-components'

const imageUrl =
  'https://rukminim1.flixcart.com/image/660/792/kk1h5e80/suitcase/i/b/z/csk-bruges-hard-sided-polypropylene-luggage-set-of-3-yellow-original-imafzhfgbyg9tvjm.jpeg?q=50'

const StyledCell = styled.div`
  width: 150px;
`
const StyledImage = styled.div`
  padding-left: 20px;
  width: 80px;
  img {
    width: 32px;
    height: 32px;
  }
`

// eslint-disable-next-line react/prop-types
const Image = ({ data }) => {
  return (
    <StyledImage>
      {/* eslint-disable-next-line react/prop-types */}
      <img src={data.image} alt='product-image' />
    </StyledImage>
  )
}
export const columns = [
  {
    title: '',
    accessor: 'image',
    isSortable: false,
    render: data => <Image data={data} />,
    children: [
      {
        title: '',
        accessor: 'image',
        isSortable: false,
        render: data => <Image data={data} />,
      },
      {
        title: 'SKU ID',
        isSortable: false,
        accessor: 'sku',
      },
      {
        title: 'Cost',
        isSortable: false,
        accessor: '',
        render: data => <StyledCell>{data.cost}</StyledCell>,
      },
      {
        title: 'Price',
        isSortable: false,
        accessor: '',
        render: data => <StyledCell>{data.price}</StyledCell>,
      },
      {
        title: 'Start date',
        isSortable: false,
        accessor: 'startDate',
        render: data => <StyledCell>{data.startDate}</StyledCell>,
      },
      {
        title: 'End date',
        isSortable: false,
        accessor: 'endDate',
        render: data => <StyledCell>{data.endDate}</StyledCell>,
      },
    ],
  },
  {
    title: 'SKU ID',
    isSortable: false,
    accessor: 'sku',
    render: data => <StyledCell>{data.sku}</StyledCell>,
  },
  {
    title: 'Cost',
    isSortable: false,
    accessor: '',
    render: data => <StyledCell>{data.cost}</StyledCell>,
  },
  {
    title: 'Price',
    isSortable: false,
    accessor: '',
    render: data => <StyledCell>{data.price}</StyledCell>,
  },
  {
    title: 'Start date',
    isSortable: false,
    accessor: 'startDate',
    render: data => <StyledCell>{data.startDate}</StyledCell>,
  },
  {
    title: 'End date',
    isSortable: false,
    accessor: 'endDate',
    render: data => <StyledCell>{data.endDate}</StyledCell>,
  },
]

export const data = [
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'A1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
    children: [
      {
        id: uuidv4(),
        startDate: '2021-03-09T17:19:07.485Z',
        endDate: '2099-12-31T00:00:00.000Z',
        sku: 'AB1234',
        title: 'Test Product',
        image: imageUrl,
        price: '800USD',
        cost: '700USD',
      },
    ],
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'B1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'C1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'D1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'E1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'F1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'G1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'H1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'I1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'J1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'K1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
  {
    id: uuidv4(),
    startDate: '2021-03-09T17:19:07.485Z',
    endDate: '2099-12-31T00:00:00.000Z',
    sku: 'L1234',
    title: 'Test Product',
    image: imageUrl,
    price: '800USD',
    cost: '700USD',
  },
]
