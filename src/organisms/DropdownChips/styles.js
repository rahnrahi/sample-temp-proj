import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledChipsDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${theme.typography.link.css};
  margin-bottom: 4px;
  width: ${({ width, showClearButton }) => {
    if (width) return width
    return showClearButton ? '140px' : '124px'
  }};
  min-width: ${({ showClearButton }) => (showClearButton ? '140px' : '124px')};
  height: 36px;
  border-radius: 40px;
  background-color: ${theme.palette.brand.primary.white};
  color: ${theme.palette.brand.primary.charcoal};
  overflow: hidden;
  outline: none;
  cursor: pointer;

  &:hover {
    background-color: ${theme.palette.ui.neutral.grey3};
    color: ${theme.palette.brand.primary.charcoal};
    .icon {
      div {
        svg {
          fill: ${theme.palette.brand.primary.charcoal};
        }
      }
    }
  }

  &:focus {
    background-color: ${theme.palette.ui.neutral.grey5};
    border: 1px solid ${theme.palette.ui.cta.yellow};
  }

  &:active {
    background-color: ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};

    .icon {
      div {
        svg {
          fill: ${theme.palette.brand.primary.white};
        }
      }
    }
  }

  ${({ showOptions }) =>
    showOptions &&
    `
    background-color: ${theme.palette.ui.neutral.grey1};
    color: ${theme.palette.brand.primary.white};
    .icon {
      div {
        svg {
          fill: ${theme.palette.brand.primary.white};
        }
      }
    }
    `};

  ${({ disabled }) =>
    disabled &&
    `
    color: ${theme.palette.ui.neutral.grey7};
    pointer-events: none;`}

  &.multiInput {
    padding: 0px 24px;
  }
`

export const StyledChipsTitleDiv = styled.div`
  min-width: 72px;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 10px 0px 10px 17px;
  display: inline;
  white-space: nowrap;
`

export const StyledChipsIconWrapperDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const StyledChipsIconDiv = styled.div`
  width: 26px;
  min-width: 26px;
  max-width: 26px;
`

export const StyledCheckboxDiv = styled.div`
  & > label {
    width: 100%;
  }
`

export const StyledFlyoutDiv = styled.div`
  ${({ multiInput }) => multiInput && `padding: 15px 24px 12px 24px;`};

  ${({ multiInput }) => multiInput && `& > div {padding-bottom: 20px}`};
`

export const StyledStorybookDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`
