import React from 'react'
import { DropdownChips } from './index'
import { Dropdown } from '../../molecules'
import { Input } from '../../atoms'
import { StyledStorybookDiv } from './styles'
import { getFlyoutPlacements } from '../../shared/utils'
import FlyoutDocs from '../../../docs/Flyouts.mdx'
import {
  DROPDOWN_FLYOUT_DESIGN,
  DROPDOWN_FLYOUT_PRESENTATION,
  DROPDOWN_WITHINPUT_DESIGN,
  DROPDOWN_WITHINPUT_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Dropdowns/Flyout',
  component: DropdownChips,
  argTypes: {
    placement: {
      control: {
        type: 'select',
        options: getFlyoutPlacements(),
      },
    },
  },
  parameters: {
    docs: {
      page: FlyoutDocs,
    },
  },
}

const Template = args => (
  <StyledStorybookDiv>
    <DropdownChips {...args} />
  </StyledStorybookDiv>
)

export const WithCheckbox = Template.bind({})
WithCheckbox.args = {
  chipsType: 'multi-select',
  autoClose: true,
  multiSelectOptions: [
    {
      id: '1',
      name: 'Pending',
      isChecked: true,
      tooltipProps: {},
    },
    {
      id: '2',
      name: 'Done',
      isChecked: false,
      tooltipProps: {},
    },
    {
      id: '3',
      name: 'Dispatched',
      isChecked: true,
      tooltipProps: {},
    },
    {
      id: '4',
      name: 'ThisIsATextOverflowShowingEllipsis',
      isChecked: true,
      tooltipProps: {},
    },
  ],
  chipsName: 'Status',
  onChange: selectedChips => console.log('On change called', selectedChips),
  width: '124px',
  flyoutWidth: '200px',
  localeCode: '',
}
WithCheckbox.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: DROPDOWN_FLYOUT_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: DROPDOWN_FLYOUT_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

const TemplateWithChildren = args => {
  return (
    <StyledStorybookDiv>
      <DropdownChips {...args}>
        <Input
          localeCode={args.localeCode}
          className='primary'
          inputProps={{
            disabled: false,
            onChange: () => {
              alert(`Please check console.`)
              console.log(
                `Here users will pass their own custom onChange handler and control value accordingly as control is in user's hands for this type i.e multi-input dropdown.`
              )
            },
            value: '',
          }}
          isFloatedLabel
          label='City'
        />

        <Dropdown
          width='100%'
          onSelect={e => console.log(e)}
          options={[
            {
              id: 1,
              name: 'State 1',
            },
            {
              id: 2,
              name: 'State 2',
            },
            {
              id: 3,
              name: 'State 3',
            },
            {
              id: 4,
              name: 'State 4',
            },
          ]}
          titleLabel='State'
          value={{
            id: 0,
            name: 'Select',
          }}
        />
      </DropdownChips>
    </StyledStorybookDiv>
  )
}

export const WithInput = TemplateWithChildren.bind({})
WithInput.args = {
  chipsType: 'multi-input',
  chipsName: 'Location',
  localeCode: '',
}
WithInput.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: DROPDOWN_WITHINPUT_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: DROPDOWN_WITHINPUT_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
