import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import {
  StyledCheckboxDiv,
  StyledChipsDiv,
  StyledChipsIconDiv,
  StyledChipsIconWrapperDiv,
  StyledChipsTitleDiv,
  StyledFlyoutDiv,
} from './styles'
import { Flyout } from '../../molecules'
import { v4 as uuidv4 } from 'uuid'
import { Checkbox } from '../../atoms'
import { Close, DownArrow } from '../../assets/images'
import { isFunction } from '../../shared/utils'
import useClickOutside from '../../hooks/click-outside'
import { ChipsType } from './DropdownChips.enum'

export const DropdownChips = ({
  chipsName,
  chipsType,
  multiSelectOptions,
  onChange,
  children,
  customTitle,
  width,
  flyoutWidth,
  placement,
  showClearButton,
  onClear,
  autoClose,
}) => {
  // Common state
  const [showOptions, setShowOptions] = useState(false)
  const [chipsTitle, setChipsTitle] = useState(chipsName)
  const flyoutId = useMemo(() => uuidv4(), [])

  // Multi select type dropdown state
  const [selectedChips, setSelectedChips] = useState([])

  const btnRef = useRef(null)
  const flyoutRef = useRef(null)
  useClickOutside(
    [btnRef, flyoutRef],
    () => {
      if (autoClose) {
        setShowOptions(false)
      }
    },
    autoClose && showOptions
  )

  useEffect(() => {
    if (chipsType.toUpperCase() === 'multi-select'.toUpperCase()) {
      setSelectedChips(
        multiSelectOptions
          .filter(option => option.isChecked)
          .map(option => option.id)
      )
    }
  }, [chipsType, multiSelectOptions])

  const chipsClickHandler = () => setShowOptions(show => !show)

  const multiSelectOptionClickHandler = useCallback(
    (e, id) => {
      const { checked } = e.target
      let options = []
      if (checked) {
        setSelectedChips(() => {
          return [...selectedChips, id]
        })

        options = multiSelectOptions.filter(item => {
          delete item.isChecked
          return [...selectedChips, id].includes(item.id) && item
        })
      } else {
        setSelectedChips(() => {
          return selectedChips.filter(index => index !== id)
        })
        options = multiSelectOptions.filter(item => {
          delete item.isChecked
          return (
            selectedChips.filter(index => index !== id).includes(item.id) &&
            item
          )
        })
      }

      isFunction(onChange) && onChange(options)
    },
    [selectedChips, onChange]
  )

  const clearOptions = event => {
    event.stopPropagation()
    if (chipsType && chipsType.toUpperCase() === 'multi-select'.toUpperCase()) {
      setSelectedChips([])
      setShowOptions(false)
      setChipsTitle(chipsName)
    } else if (
      chipsType &&
      chipsType.toUpperCase() === 'multi-input'.toUpperCase()
    ) {
      // Call user's passed on clear method
      onClear && onClear()
    }
  }

  const getChipsInitialTitle = () => {
    // Render custom title if provided else keep default behaviour
    return customTitle || chipsTitle
  }

  const getChipsName = () => {
    return selectedChips && selectedChips.length
      ? `${multiSelectOptions
          .filter(item => item && selectedChips.includes(item.id))
          .map(item => item.name)
          .join(',')}`
      : chipsName
  }

  useEffect(() => {
    /*
    	1. If there are selected options then update name of chips to joint comma
					separated name of selected options
			2. If all options are selected, show initials of chips name appended
					with 'All'
			3. Otherwise use provided default chips name
		*/
    selectedChips &&
    selectedChips.length &&
    selectedChips.length === multiSelectOptions.length
      ? setChipsTitle(`${chipsName}: All`)
      : setChipsTitle(getChipsName())
  }, [chipsName, multiSelectOptions, selectedChips])

  const MultiSelectComponent = () =>
    multiSelectOptions &&
    multiSelectOptions.map((option, idx) => (
      <StyledCheckboxDiv key={idx}>
        <Checkbox
          key={idx}
          label={option.name}
          onChange={e => multiSelectOptionClickHandler(e, option.id)}
          value={option.name}
          checked={selectedChips.includes(option.id)}
          tooltipProps={option.tooltipProps}
        />
      </StyledCheckboxDiv>
    ))

  const getChipsComponent = () => {
    if (!chipsType) return <></>
    switch (chipsType.toUpperCase()) {
      case ChipsType.MULTI_SELECT:
        return MultiSelectComponent()
      case ChipsType.MULTI_INPUT:
        return children
    }
    return <></>
  }

  return (
    <>
      <StyledChipsDiv
        id={flyoutId}
        onClick={chipsClickHandler}
        width={width}
        showOptions={showOptions}
        showClearButton={showClearButton}
        ref={btnRef}
      >
        <StyledChipsTitleDiv>{getChipsInitialTitle()}</StyledChipsTitleDiv>
        <StyledChipsIconWrapperDiv>
          <StyledChipsIconDiv className='icon'>
            <DownArrow />
          </StyledChipsIconDiv>
          {showClearButton && (
            <StyledChipsIconDiv className='icon' onClick={clearOptions}>
              <Close />
            </StyledChipsIconDiv>
          )}
        </StyledChipsIconWrapperDiv>
      </StyledChipsDiv>
      <Flyout
        show={showOptions}
        className='primary'
        id={flyoutId}
        offset={10}
        placement={placement}
        width={flyoutWidth ? flyoutWidth : '200px'}
        overflow='visible'
        ref={flyoutRef}
      >
        <StyledFlyoutDiv
          multiInput={
            chipsType && chipsType.toUpperCase() === 'multi-input'.toUpperCase()
          }
        >
          {getChipsComponent()}
        </StyledFlyoutDiv>
      </Flyout>
    </>
  )
}

DropdownChips.defaultProps = {
  chipsType: 'multi-select',
  multiSelectOptions: [],
  chipsName: 'Chips Name',
  placement: 'leftTop',
  autoClose: false,
  locale: '',
}

DropdownChips.propTypes = {
  /** Chips type i.e multi-select / multi-input */
  chipsType: PropTypes.oneOf(['multi-select', 'multi-input']).isRequired,
  /** Chips title */
  chipsName: PropTypes.string.isRequired,
  /**
   * 1. Multi select options array [Only for multi-select type dropdown]
   * 2. isChecked represent initial value for checkbox
   */
  multiSelectOptions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      isChecked: PropTypes.bool,
      tooltipProps: PropTypes.object,
    })
  ),
  /** On change handler for checkbox in multi-select dropdown (selectedItems)=>{}*/
  onChange: PropTypes.func,
  /** Child components [Only for multi-input type dropdown] */
  children: PropTypes.node,
  /**
   * Custom title for chips when there are selected values.
   * Otherwise, title will be computed automatically, using selected values.
   */
  customTitle: PropTypes.string,
  /** Width of chips */
  width: PropTypes.string,
  /** Width of options popup */
  flyoutWidth: PropTypes.string,
  /** Flyout position */
  placement: PropTypes.string,
  /** Show clear chips button */
  showClearButton: PropTypes.bool,
  /** Close flyout automatically on outside click */
  autoClose: PropTypes.bool,
  /** On clear chips handler [Only for multi-input type dropdown]*/
  onClear: PropTypes.func,
  /** Locale Language to render the component in */
  localeCode: PropTypes.string,
}
