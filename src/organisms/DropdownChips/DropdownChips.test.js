import React from 'react'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import 'jest-styled-components'
import { DropdownChips } from './index'

afterEach(cleanup)

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => '1'),
  }
})

const options = [
  { id: '1', name: 'Pending', isChecked: true },
  { id: '2', name: 'Done', isChecked: false },
  { id: '3', name: 'Dispatched', isChecked: true },
]

describe('<DropdownChips/>', () => {
  it('should render multi-select Dropdown chips component', () => {
    const { container } = render(
      <DropdownChips
        chipsType='multi-select'
        multiSelectOptions={options}
        chipsName='Status'
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render multi-select Dropdown chips component with clear button', () => {
    const { container } = render(
      <DropdownChips
        chipsType='multi-select'
        multiSelectOptions={options}
        chipsName='Status'
        showClearButton={true}
      />
    )
    expect(container).toMatchSnapshot()
  })
  it('should render multi-select Dropdown chips component of specific width', () => {
    const { container } = render(
      <DropdownChips
        chipsType='multi-select'
        multiSelectOptions={options}
        width={900}
        chipsName='Status'
      />
    )
    expect(container).toMatchSnapshot()
  })
  describe('Given autoClose property', () => {
    it('should close when clicking outside', () => {
      render(
        <>
          <div>Outside</div>
          <DropdownChips
            chipsType='multi-select'
            multiSelectOptions={options}
            chipsName='Status'
            autoClose
          />
        </>
      )
      expect(screen.queryByText('Done')).not.toBeInTheDocument()
      const dropdownBtn = screen.getByText('Pending,Dispatched')
      fireEvent.click(dropdownBtn)
      expect(screen.getByText('Done')).toBeVisible()
      fireEvent.click(screen.getByText('Outside'))
      expect(screen.queryByText('Done')).not.toBeInTheDocument()
    })
  })
  describe('Given onChange property', () => {
    it('should keep it in sync & call it when value changes', () => {
      const onChange = jest.fn()
      const onChange2 = jest.fn()
      const { rerender } = render(
        <DropdownChips
          chipsType='multi-select'
          multiSelectOptions={options}
          chipsName='Status'
          onChange={onChange}
        />
      )
      rerender(
        <DropdownChips
          chipsType='multi-select'
          multiSelectOptions={options}
          chipsName='Status'
          onChange={onChange2}
        />
      )
      const dropdownBtn = screen.getByText('Pending,Dispatched')
      fireEvent.click(dropdownBtn)
      fireEvent.click(screen.getByRole('checkbox', { name: 'Done' }))
      expect(onChange).toHaveBeenCalledTimes(0)
      expect(onChange2).toHaveBeenCalledTimes(1)
      expect(onChange2).toHaveBeenLastCalledWith([
        { id: '1', name: 'Pending' },
        { id: '2', name: 'Done' },
        { id: '3', name: 'Dispatched' },
      ])
    })
  })
})
