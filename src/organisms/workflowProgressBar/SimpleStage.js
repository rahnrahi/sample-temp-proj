import React from 'react'
import PropTypes from 'prop-types'

import { Icon } from '../../atoms'
import { CircleStage } from './'

export const SimpleStage = ({ statuses, stage }) => {
  const isDone = statuses[stage]?.done
  const isWarning = statuses[stage]?.warning
  const isCritical = statuses[stage]?.critical

  const getStatus = () => {
    if (isDone) return 'success'
    if (isWarning) return 'warning'
    if (isCritical) return 'critical'
  }

  return (
    <CircleStage status={getStatus()}>
      {isDone && <Icon iconName='Checkmark' size={16} fill='white' />}
      {!isDone && statuses[stage]?.amount}
    </CircleStage>
  )
}

SimpleStage.propTypes = {
  statuses: PropTypes.object,
  stage: PropTypes.string,
}
