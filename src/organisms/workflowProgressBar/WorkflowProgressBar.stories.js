import React from 'react'
import { WorkflowProgressBar, SimpleStage } from '.'
import { CircleStage, SquareStage } from './WorkflowProgressBar.styles'

import { testStages, testData } from './mocks'
import PropTypes from 'prop-types'

import {
  PROGRESS_STEPPER_DESIGN,
  PROGRESS_STEPPER_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

const OrderStage = ({ statuses, stage }) => {
  return <CircleStage> {statuses[stage]?.amount} </CircleStage>
}
OrderStage.propTypes = {
  statuses: PropTypes.object,
  stage: PropTypes.string,
}

const cellComponents = { CircleStage, SquareStage, SimpleStage, OrderStage }

export default {
  title: 'Modules/Progress Trackers/Progress Stepper',
  component: WorkflowProgressBar,
  args: {
    height: '60px',
    width: '600px',
  },
  argTypes: {
    CellComponent: {
      options: Object.keys(cellComponents),
      mapping: cellComponents,
      control: {
        type: 'select',
        labels: {
          CircleStage: 'Circle',
          SquareStage: 'Square',
          OrderStage: 'Order',
          SimpleStage: 'SimpleStage',
        },
      },
    },
  },
}

const Template = args => {
  return <WorkflowProgressBar {...args} />
}

const RowHeader = data => {
  return (
    <div>
      <div>{data.id}</div>
      <div>
        {data.name} ({data.amount})
      </div>
    </div>
  )
}

export const Standard = Template.bind({})
Standard.args = {
  CellComponent: 'CircleStage',
  RowHeaderComponent: RowHeader,
  firstColumnTitle: 'SKU ID',
  stagesGroups: testStages,
  data: testData,
}
Standard.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PROGRESS_STEPPER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PROGRESS_STEPPER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}

export const OrderStatus = Template.bind({})
OrderStatus.args = {
  CellComponent: 'OrderStage',
  RowHeaderComponent: RowHeader,
  firstColumnTitle: 'SKU ID',
  stagesGroups: testStages,
  data: testData,
}
OrderStatus.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: PROGRESS_STEPPER_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: PROGRESS_STEPPER_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
