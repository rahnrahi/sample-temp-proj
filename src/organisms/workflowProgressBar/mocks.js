export const testStages = [
  ['Created', 'Allocated', 'Shipped', 'Delivered'],
  ['Returned'],
  ['Cancelled'],
  ['Hold'],
]

export const testData = [
  {
    id: '01283881',
    name: 'Cotton Shirt',
    statuses: {
      Created: { amount: 2, done: true },
      Allocated: { amount: 2 },
      Shipped: { amount: 2 },
      Delivered: { amount: 1 },
      Hold: { amount: 1, critical: true },
    },
    amount: 3,
  },
  {
    id: '01283881',
    name: 'Boyfriend jeans',
    statuses: {
      Allocated: { amount: 1 },
      Returned: { amount: 1, warning: true },
    },
    amount: 2,
  },
]
