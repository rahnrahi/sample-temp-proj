import React from 'react'
import { Table } from '../../organisms/table'
import { CellRoot, LineAcross, LineLeft } from './WorkflowProgressBar.styles'
import PropTypes from 'prop-types'

export const WorkflowProgressBar = ({
  CellComponent,
  RowHeaderComponent,
  stagesGroups,
  firstColumnTitle,
  data,
  ...rest
}) => {
  const renderStageCell = (data, stage, isLastStage, isSingle) => {
    const lineElement = isLastStage ? (
      <LineLeft data-testid='line-left' />
    ) : (
      <LineAcross data-testid='line-across' />
    )

    return (
      <CellRoot>
        {isSingle ? null : lineElement}
        <CellComponent {...data} stage={stage} />
      </CellRoot>
    )
  }

  const getColumnsFromStages = stages => {
    return stages.map((stage, index) => ({
      title: stage,
      render: data =>
        renderStageCell(
          data,
          stage,
          index == stages.length - 1,
          stages.length === 1
        ),
    }))
  }

  const columns = [
    { title: firstColumnTitle, render: RowHeaderComponent },
    ...stagesGroups.flatMap(stages => getColumnsFromStages(stages)),
  ]

  return <Table columns={columns} data={data} {...rest} />
}

WorkflowProgressBar.defaultProps = {}

WorkflowProgressBar.propTypes = {
  CellComponent: PropTypes.elementType,
  RowHeaderComponent: PropTypes.elementType,
  stagesGroups: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  firstColumnTitle: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.object),
}
