import styled from 'styled-components'
import theme from '../../shared/theme'

export const BaseStage = styled.div`
  position: relative; // thanks to this Stage is displayed on top of a line across

  width: 2rem;
  height: 2rem;
  background-color: ${({ status }) => {
    switch (status) {
      case 'success':
        return theme.palette.ui.cta.green
      case 'warning':
        return theme.palette.ui.cta.yellow
      case 'critical':
        return theme.palette.ui.cta.red
      default:
        return 'white'
    }
  }};
  color: ${({ status }) =>
    ['warning', 'critical'].includes(status)
      ? theme.palette.brand.primary.white
      : 'black'};

  border: 1px grey solid;
`

export const CircleStage = styled(BaseStage)`
  display: flex;
  justify-content: center;
  align-items: center;

  border: 1px grey solid;
  border-radius: 50%;
`

export const SquareStage = styled(BaseStage)``

export const CellRoot = styled.div`
  position: 'relative';
`

export const LineAcross = styled.div`
  position: absolute;
  top: 50%;

  width: 100%;
  height: 2px;

  border-top: 1px grey solid;
`

export const LineLeft = styled.div`
  position: absolute;
  top: 50%;

  left: -25%;

  width: 50%;
  height: 2px;

  border-top: 1px grey solid;
`
