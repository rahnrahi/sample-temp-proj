import React from 'react'
import { cleanup, render } from '@testing-library/react'
import 'jest-styled-components'
import { WorkflowProgressBar } from './'
import { testStages, testData } from './mocks'
import { SimpleStage } from './SimpleStage'

afterEach(cleanup)

const RowHeader = data => {
  return (
    <div>
      <div>{data.id}</div>
      <div>
        {data.name} ({data.amount})
      </div>
    </div>
  )
}

const ui = overrideProps => {
  return (
    <WorkflowProgressBar
      CellComponent={SimpleStage}
      RowHeaderComponent={RowHeader}
      firstColumnTitle='SKU ID'
      stagesGroups={testStages}
      data={testData}
      {...overrideProps}
    />
  )
}

const setup = overrideProps => {
  return render(ui(overrideProps))
}

describe('<WorkflowProgressBar />', () => {
  it('should render storybook example', () => {
    const { container } = setup()
    expect(container).toMatchSnapshot()
  })

  it('can render single multi-stage group', () => {
    const { queryAllByTestId } = setup({
      stagesGroups: [testStages[0]],
    })

    expect(queryAllByTestId('line-across')).toHaveLength(6)
    expect(queryAllByTestId('line-left')).toHaveLength(2)
  })

  it('can render two multi-stage groups', () => {
    const { queryAllByTestId } = setup({
      stagesGroups: [testStages[0], testStages[0]],
    })

    expect(queryAllByTestId('line-across')).toHaveLength(12)
    expect(queryAllByTestId('line-left')).toHaveLength(4)
  })

  it('rendered single multi-stage group should have different amount of lines than two multi-stages, but with half amount of stages', () => {
    const { queryAllByTestId } = setup({
      stagesGroups: [[...testStages[0], ...testStages[0]]],
    })

    expect(queryAllByTestId('line-across')).toHaveLength(14)
    expect(queryAllByTestId('line-left')).toHaveLength(2)
  })

  it('can render many single-stage groups', () => {
    const { queryAllByTestId } = setup({
      stagesGroups: [['Returned'], ['Cancelled'], ['Hold']],
    })

    expect(queryAllByTestId('line-across')).toHaveLength(0)
    expect(queryAllByTestId('line-left')).toHaveLength(0)
  })

  it('can dynamically replace Cell component', () => {
    const { container, rerender } = setup()

    expect(container).toMatchSnapshot()

    const OtherCell = () => 'other cell'
    rerender(ui({ CellComponent: OtherCell }))

    expect(container).toMatchSnapshot()
  })
})
