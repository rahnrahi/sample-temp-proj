import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledMultiSelectDropdownContainer = styled.div`
  width: ${({ width }) => width};
	min-width: 150px;
  display: flex;
  cursor: pointer;
  flex-direction: column;
  border-bottom: 1px solid ${theme.palette.brand.primary.charcoal};
  ${theme.typography.h6.css};
  position: relative;
	${({ disabled }) => disabled && `pointer-events: none;`};
  .chips-label {
    color: ${({ chipsIn }) =>
      chipsIn
        ? theme.palette.brand.primary.gray
        : theme.palette.brand.primary.charcoal};
  }

  .chips_container {
    display: flex;
    flex-wrap: wrap;
    .chips-margin-bottom {
      margin-bottom: 8px;
    }
  }

  .dropDown-trigger_icon {
    position: absolute;
    top: 0;
    right: 8px;
  }

	.floating-dropdown-container {
			margin-bottom: 8px;
		}
	}

  :hover {
    .chips-clear-button {
      opacity: 1;
    }
  }

	.dropdown-arrow {
		padding-right: 16px;
	}
`

export const StyledValuesRenderContainer = styled.div`
  display: flex;
  flex-wrap: ${({ chipsAlign }) =>
    chipsAlign === 'vertical' ? 'wrap' : 'nowrap'};
  .chips-margin-bottom {
    margin-bottom: 8px;
  }
  overflow-x: ${({ chipsAlign }) =>
    chipsAlign === 'horizontal' ? 'scroll' : 'none'};
  align-items: center;
  justify-content: space-between;
  padding-bottom: ${({ chipsIn }) => (chipsIn ? '16px' : '')};

  .values-inner-container {
    width: calc(100% - 40px);
    overflow: hidden;
    text-overflow: ellipsis;
  }
`

export const StyledDropdownContainer = styled.div`
  position: absolute;
  top: calc(100% + 4px);
  z-index: ${theme.zIndex.drawer};
  display: flex;
  flex-direction: column;
  width: ${({ dropdownWidth }) => dropdownWidth};
  max-height: 219px;
  background: ${theme.palette.brand.primary.white};
  border: 1px solid ${theme.palette.ui.neutral.grey2};
  ${theme.shadows.light.level2};
  border-radius: 4px;
  padding: ${({ dropdownPadding }) => dropdownPadding};
  overflow-y: scroll;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 4px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${theme.palette.brand.primary.gray};
    box-shadow: 0 0 0.0625rem rgba(255, 255, 255, 0.5);
  }

  .search-container {
    margin-bottom: 9px;

    svg {
      width: 14px;
      height: 14px;
    }
  }

  .option-checkbox {
    padding-left: 0;
  }

  .checkbox_background_hover {
    &:hover {
      background: ${theme.palette.brand.primary.white} !important;
    }
  }
`
