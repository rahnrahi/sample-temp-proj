import React, { useEffect, useRef, useState } from 'react'
import {
  StyledDropdownContainer,
  StyledMultiSelectDropdownContainer,
  StyledValuesRenderContainer,
} from './styles'
import PropTypes from 'prop-types'
import { Checkbox, Icon, Input } from '../../atoms'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import useClickOutside from '../../hooks/click-outside'

export const MultiSelectDropdown = ({
  width,
  label,
  showArrowIcon,
  value,
  chipsAlign,
  onChange,
  dropdownOptions,
  dropdownWidth,
  dropdownPadding,
  searchPlaceholder,
  disabled,
  localeCode,
}) => {
  const sortOptions = options => {
    return options.sort((option1, option2) =>
      !isEmpty(find(value, ['id', option1.id])) >
      !isEmpty(find(value, ['id', option2.id]))
        ? -1
        : 1
    )
  }
  const [isEnabled, setEnabled] = useState(false)
  const [searchQuery, setSearchQuery] = useState('')
  const [options, setOptions] = useState(() => {
    return sortOptions(dropdownOptions)
  })

  useEffect(() => {
    if (!searchQuery) {
      setOptions(sortOptions(dropdownOptions))
      return
    }

    setOptions(() => {
      return sortOptions(
        dropdownOptions.filter(option =>
          option.value.toLowerCase().includes(searchQuery.toLowerCase())
        )
      )
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchQuery])

  const clickRef = useRef()
  useClickOutside(
    clickRef,
    () => {
      setEnabled(false)
    },
    isEnabled
  )

  const toggle = e => {
    e.stopPropagation()
    setEnabled(!isEnabled)
    setOptions(sortOptions([...options]))
  }

  const addChip = option => {
    if (!isEmpty(find(value, ['id', option.id]))) {
      return
    }
    let chips = [...value, option]
    onChange(chips)
  }

  const onChangeOption = ({ event, ...option }) => {
    event.stopPropagation()
    const { checked } = event.target
    if (!checked) {
      removeChip({ ...option, event })
    } else {
      addChip(option)
    }
  }

  const removeChip = ({ id, event }) => {
    event.stopPropagation()
    const nextChips = value.filter(chip => chip.id !== id)
    onChange && typeof onChange === 'function' && onChange(nextChips)
  }

  const getSelectedValues = () => value.map(v => v.value).join(',')

  const renderDropdownOptions = props => {
    return (
      <Checkbox
        {...props}
        className='option-checkbox checkbox_background_hover'
      />
    )
  }

  const allOptionClickHandler = event => {
    const { checked } = event.target

    if (!checked) {
      // Make all options unchecked
      onChange && typeof onChange === 'function' && onChange([])
      return
    }

    // Make all options checked
    onChange && typeof onChange === 'function' && onChange(options)
  }

  const renderOptions = () => {
    const allOption = React.cloneElement(
      renderDropdownOptions({ label: 'All', id: 'All', value: 'All' }),
      {
        onChange: e => allOptionClickHandler(e),
        checked: value.length === options.length,
      }
    )
    const otherOptions = options.map((option, idx) => {
      return React.cloneElement(renderDropdownOptions(option), {
        ...option,
        checked: !isEmpty(find(value, ['id', option.id])),
        onChange: event => {
          onChangeOption({
            ...option,
            idx,
            optionChecked: event.target.checked,
            event,
          })
        },
        index: idx,
        key: idx,
        ['data-testid']: `option_${idx}`,
        tabIndex: idx,
      })
    })
    return !searchQuery ? [allOption, ...otherOptions] : [...otherOptions]
  }

  const searchOptionHandler = event => {
    const { value } = event.target

    if (!value) {
      setSearchQuery('')
    }

    setSearchQuery(value)
  }

  return (
    <StyledMultiSelectDropdownContainer
      width={width}
      chipsIn={value.length > 0}
      ref={clickRef}
      disabled={disabled}
    >
      <div className='floating-dropdown-container'>
        <div className='chips-label' onClick={toggle}>
          {label}
        </div>
      </div>
      <span className='dropDown-trigger_icon dropdown-arrow'>
        {!value.length && showArrowIcon && (
          <Icon iconName='DownArrow' size={16} onClick={toggle} />
        )}
      </span>
      {value.length > 0 && (
        <StyledValuesRenderContainer
          className='values-container'
          width={width}
          chipsIn={value.length > 0}
          chipsAlign={chipsAlign}
          onClick={toggle}
        >
          <span className='values-inner-container'>{getSelectedValues()}</span>
          <span className='dropdown-arrow'>
            {value.length > 0 && showArrowIcon && (
              <Icon iconName='DownArrow' size={16} onClick={toggle} />
            )}
          </span>
        </StyledValuesRenderContainer>
      )}
      {isEnabled && (
        <StyledDropdownContainer
          dropdownWidth={dropdownWidth}
          dropdownPadding={dropdownPadding}
        >
          <div className='search-container'>
            <Input
              localeCode={localeCode}
              className='search-local'
              icon='Search'
              inputProps={{
                boxed: false,
                placeholder: searchPlaceholder,
                onChange: e => searchOptionHandler(e),
                value: searchQuery,
              }}
              kind='md'
              isFloatedLabel={false}
              label=''
              width='100%'
            />
          </div>
          {renderOptions()}
        </StyledDropdownContainer>
      )}
    </StyledMultiSelectDropdownContainer>
  )
}

MultiSelectDropdown.propTypes = {
  /** Width of component */
  width: PropTypes.string,
  /** A Dropdown options width */
  dropdownWidth: PropTypes.string,
  /** An array of options that represents the value for Options */
  dropdownOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ).isRequired,
  /** For custom field for dropdown container padding. */
  dropdownPadding: PropTypes.string,
  /** Label of component */
  label: PropTypes.string,
  /** Show arrow icon */
  showArrowIcon: PropTypes.bool,
  /** Array of selected values */
  value: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
      ]),
    })
  ),
  /** Value for changing the direction values alignment */
  chipsAlign: PropTypes.oneOf(['horizontal', 'vertical']),
  /** A function called when the value of chips changes, passes the chips value as an argument. */
  onChange: PropTypes.func,
  /** Options search placeholder*/
  searchPlaceholder: PropTypes.string,
  /** Enable/Disable dropdown */
  disabled: PropTypes.bool,
  /** Locale Language to render the component in */
  localeCode: PropTypes.string,
}

MultiSelectDropdown.defaultProps = {
  label: 'Label',
  width: '100%',
  dropdownWidth: '80%',
  dropdownOptions: [],
  onChange: () => {},
  dropdownPadding: '10px 32px 17px 40px',
  showArrowIcon: true,
  searchPlaceholder: 'Search',
  disabled: false,
  localeCode: '',
}
