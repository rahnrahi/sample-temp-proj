import React, { useState } from 'react'
import { MultiSelectDropdown as MultiSelectDropdownExample } from '../MultiSelectDropdown'
import DropdownDocs from '../../../docs/Dropdowns.mdx'
import {
  DROPDOWN_WITHINPUT_DESIGN,
  DROPDOWN_WITHINPUT_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

export default {
  title: 'Modules/Dropdowns/Multi Select',
  component: MultiSelectDropdownExample,
  decorators: [
    Story => <div style={{ height: '500px', width: '100%' }}>{Story()}</div>,
  ],
  argTypes: {
    chipsAlign: {
      control: { type: 'select', options: ['vertical', 'horizontal'] },
    },
    value: {
      table: {
        disable: true,
      },
    },
  },
  parameters: {
    docs: {
      page: DropdownDocs,
    },
  },
}

const MultiSelectDropdown = props => {
  const [value, setValue] = useState([])

  const onChangeHandler = values => {
    setValue(values)
  }

  return (
    <MultiSelectDropdownExample
      {...props}
      value={value}
      onChange={onChangeHandler}
    />
  )
}

const Template = args => <MultiSelectDropdown {...args} />

export const WithTextAndSearch = Template.bind({})
WithTextAndSearch.args = {
  dropdownOptions: [
    { label: 'label1', id: 'label1', value: 'label1' },
    { label: 'label2', id: 'label2', value: 'label2' },
    { label: 'label3', id: 'label3', value: 'label3' },
    { label: 'label4', id: 'label4', value: 'label4' },
    { label: 'label5', id: 'label5', value: 'label5' },
    { label: 'label6', id: 'label6', value: 'label6' },
    { label: 'label7', id: 'label7', value: 'label7' },
  ],
  value: [],
}
WithTextAndSearch.parameters = {
  design: [
    {
      name: DESIGN_TAB_MOCKUP,
      type: 'figma',
      url: DROPDOWN_WITHINPUT_DESIGN,
      allowFullscreen: true,
    },
    {
      name: DESIGN_TAB_PRESENTATION,
      type: 'figma',
      url: DROPDOWN_WITHINPUT_PRESENTATION,
      allowFullscreen: true,
    },
  ],
}
