import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import { MultiSelectDropdown } from './index'

afterEach(cleanup)

const props = {
  dropdownOptions: [
    { label: 'label1', id: 'label1', value: 'label1' },
    { label: 'label2', id: 'label2', value: 'label2' },
    { label: 'label3', id: 'label3', value: 'label3' },
    { label: 'label4', id: 'label4', value: 'label4' },
    { label: 'label5', id: 'label5', value: 'label5' },
    { label: 'label6', id: 'label6', value: 'label6' },
    { label: 'label7', id: 'label7', value: 'label7' },
  ],
  value: [],
}

describe('<MultiSelectDropdown />', () => {
  it('renders multiselect dropdown component', () => {
    const { container } = render(<MultiSelectDropdown {...props} />)
    expect(container).toMatchSnapshot()
  })
})

describe('MultiSelectDropdown Component', () => {
  it('should click the component, select and deselect the option', () => {
    const { getByText } = render(<MultiSelectDropdown {...props} />)
    /* Open the Component */
    const label = getByText('Label')
    fireEvent.click(label)
    /* Get and Select the First Option. Confirm that this option is in the document */
    const label1 = getByText('label1')
    fireEvent.click(label1)
    expect(label1).toBeInTheDocument()
    /* Deselect the option. If the test above passes, the second fireEvent just confirm the scenario and do a new click event to deselect. */
    fireEvent.click(label1)
  })
})
