import { AssetModal } from './AssetModal'
import { defaultProps, mockSelectedAsset } from './mock'
import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import {
  NO_ASSETS_MESSAGE,
  NO_ASSETS_WITH_FILTER_MESSAGE,
  NO_SELECTED_ASSET_MODAL_TITLE,
} from './constants'

const observe = jest.fn()
beforeEach(() => {
  window.IntersectionObserver = jest.fn(() => ({
    observe,
    disconnect: jest.fn(),
    unobserve: jest.fn(),
  }))
})

afterEach(() => {
  jest.resetAllMocks()
})

describe('AssetModal', () => {
  it('should render Modal component', () => {
    const { getByTestId } = render(<AssetModal {...defaultProps} />)

    const modal = getByTestId('modal')
    expect(modal).toBeInTheDocument()
  })

  it('should show empty modal title if no asset was previously selected', () => {
    const { getByText } = render(<AssetModal {...defaultProps} />)

    expect(getByText(NO_SELECTED_ASSET_MODAL_TITLE)).toBeInTheDocument()
  })

  it('should not have operation listed in sidebar if unsupported', () => {
    const clonedProps = JSON.parse(JSON.stringify(defaultProps))
    const unsupportedOperation = {
      title: 'Unsupported operation',
      operationType: 'UNSUPPORTED_OP',
      onSubmit: jest.fn(),
    }
    clonedProps.supportedOperations.push(unsupportedOperation)

    const { queryByText } = render(<AssetModal {...clonedProps} />)
    expect(queryByText(unsupportedOperation.title)).not.toBeInTheDocument()
  })

  describe('View Asset Detail', () => {
    it('should not render View Asset Detail menu if there are no selected asset', () => {
      const viewAssetTitle = defaultProps.supportedOperations[0].title
      const { queryByText } = render(<AssetModal {...defaultProps} />)

      const viewAssetMenu = queryByText(viewAssetTitle)
      expect(viewAssetMenu).not.toBeInTheDocument()
    })

    const propsWithViewAssetMenu = JSON.parse(JSON.stringify(defaultProps))
    propsWithViewAssetMenu.supportedOperations[0].selectedAsset = mockSelectedAsset

    it('should render View Asset Detail menu if there is a selected asset', () => {
      const viewAssetTitle = defaultProps.supportedOperations[0].title

      const { getByText } = render(<AssetModal {...propsWithViewAssetMenu} />)

      const viewAssetMenu = getByText(viewAssetTitle)
      expect(viewAssetMenu).toBeInTheDocument()
    })

    it('should render submit button', () => {
      const { queryByRole } = render(<AssetModal {...propsWithViewAssetMenu} />)

      const submitBtnText = propsWithViewAssetMenu.customTextLabels.saveText
      const submitBtn = queryByRole('button', { name: submitBtnText })

      expect(submitBtn).toBeInTheDocument()
    })

    it('should redirect to Upload from Brandfolder menu if "Remove Image" is clicked', () => {
      const viewAssetTitle = defaultProps.supportedOperations[0].title
      const uploadFromDamTitle = defaultProps.supportedOperations[1].title

      const { getByText, queryByText, queryByTestId } = render(
        <AssetModal {...propsWithViewAssetMenu} />
      )

      const assetPreview = queryByTestId('selected-asset-preview')
      expect(assetPreview).toBeInTheDocument()

      const removeImageBtn = getByText(/remove image/i)
      fireEvent.click(removeImageBtn)

      const viewAssetMenu = queryByText(viewAssetTitle)
      const uploadFromDamMenu = getByText(uploadFromDamTitle)

      expect(viewAssetMenu).not.toBeInTheDocument()
      expect(assetPreview).not.toBeInTheDocument()
      expect(uploadFromDamMenu).toHaveClass('active')
    })
  })

  it('should render list of sections and able to select a section', () => {
    const EXPECTED_NUM_SECTIONS = defaultProps.options.sectionOptions.length

    const { getAllByTestId } = render(<AssetModal {...defaultProps} />)

    const sectionChips = getAllByTestId('chip')
    const [sectionOne, sectionTwo] = sectionChips

    expect(sectionChips).toHaveLength(EXPECTED_NUM_SECTIONS)
    expect(sectionOne).toHaveTextContent(defaultProps.options.sectionOptions[0])
    expect(sectionTwo).toHaveTextContent(defaultProps.options.sectionOptions[1])
  })

  it('should call onSectionSelect if a section is selected', () => {
    const onSectionSelectProp =
      defaultProps.supportedOperations[1].onSectionSelect

    const { getAllByTestId } = render(<AssetModal {...defaultProps} />)

    const [sectionOne] = getAllByTestId('chip')

    fireEvent.click(sectionOne)
    expect(onSectionSelectProp).toHaveBeenCalledTimes(1)
    expect(onSectionSelectProp).toHaveBeenCalledWith({
      label: 'All',
      isSelected: true,
    })

    // Clicking on a selected section unselects it
    fireEvent.click(sectionOne)
    expect(onSectionSelectProp).toHaveBeenCalledTimes(2)
    expect(onSectionSelectProp).toHaveBeenCalledWith({
      label: 'All',
      isSelected: false,
    })
  })

  it('should render list of assets', () => {
    const EXPECTED_NUM_ASSETS = defaultProps.options.assetOptions.length

    const { getAllByRole } = render(<AssetModal {...defaultProps} />)

    const assetsList = getAllByRole('img')
    expect(assetsList).toHaveLength(EXPECTED_NUM_ASSETS)
  })

  it('should show no assets message if there are no assets', () => {
    const updatedProps = JSON.parse(JSON.stringify(defaultProps))
    updatedProps.options.assetOptions = []

    const { getByText } = render(<AssetModal {...updatedProps} />)

    const noAssetsMessage = getByText(NO_ASSETS_MESSAGE)
    expect(noAssetsMessage).toBeInTheDocument()
  })

  it('should show no assets with filter message if there are no assets if a section is selected', () => {
    const updatedProps = JSON.parse(JSON.stringify(defaultProps))
    updatedProps.options.assetOptions = []
    updatedProps.supportedOperations[1].onSectionSelect = jest.fn()

    const { getByText } = render(<AssetModal {...updatedProps} />)

    const sectionOne = getByText('All')
    fireEvent.click(sectionOne)

    const noAssetsMessage = getByText(NO_ASSETS_WITH_FILTER_MESSAGE)
    expect(noAssetsMessage).toBeInTheDocument()
  })

  it('should call onClose if cancel button is clicked', () => {
    const { getByRole } = render(<AssetModal {...defaultProps} />)

    const cancelBtn = getByRole('button', { name: /cancel/i })
    expect(cancelBtn).toBeInTheDocument()

    fireEvent.click(cancelBtn)
    expect(defaultProps.onClose).toHaveBeenCalledTimes(1)
  })

  it('should have "Save" button disabled if no asset is selected', () => {
    const { getByRole } = render(<AssetModal {...defaultProps} />)

    const submitBtnText = defaultProps.customTextLabels.saveText
    const submitBtn = getByRole('button', { name: submitBtnText })

    expect(submitBtn).toBeDisabled()
  })

  it('should be able to submit if an asset is selected', () => {
    const onSubmitProp = defaultProps.supportedOperations[1].onSubmit
    const selectedAsset = defaultProps.options.assetOptions[0]

    const { getByTestId, getByRole } = render(<AssetModal {...defaultProps} />)

    const submitBtnText = defaultProps.customTextLabels.saveText

    const submitBtn = getByRole('button', { name: submitBtnText })
    const assetOne = getByTestId('asset-0')

    fireEvent.click(assetOne)

    expect(submitBtn).toBeEnabled()
    expect(submitBtn).toHaveTextContent(defaultProps.customTextLabels.saveText)

    fireEvent.click(submitBtn)
    expect(onSubmitProp).toHaveBeenCalledTimes(1)
    expect(onSubmitProp).toHaveBeenCalledWith(selectedAsset)
  })

  it('should unselect an asset on click if the asset was selected', () => {
    const { getByTestId, getByRole } = render(<AssetModal {...defaultProps} />)

    const submitBtnText = defaultProps.customTextLabels.saveText
    const submitBtn = getByRole('button', { name: submitBtnText })
    const assetOne = getByTestId('asset-0')

    fireEvent.click(assetOne)
    expect(submitBtn).toBeEnabled()

    fireEvent.click(assetOne)
    expect(submitBtn).toBeDisabled()
  })

  it('should focus on View Image Detail when an asset is double clicked', () => {
    const viewAssetTitle = defaultProps.supportedOperations[0].title
    render(<AssetModal {...defaultProps} />)

    fireEvent.dblClick(screen.getByTestId('asset-0'))
    const viewMenuOption = screen.getByText(viewAssetTitle)
    const title = document.querySelector('.modal_content .modal-title')

    expect(viewMenuOption).toBeInTheDocument()
    expect(title).toHaveTextContent(defaultProps.options.assetOptions[0].name)
  })

  it('should have Save button disabled when user comes back to "Upload from Brandfolder Library"', () => {
    const uploadFromDamTitle = defaultProps.supportedOperations[1].title
    const submitBtnText = defaultProps.customTextLabels.saveText
    render(<AssetModal {...defaultProps} />)

    fireEvent.dblClick(screen.getByTestId('asset-0'))
    fireEvent.click(screen.getByText(uploadFromDamTitle))
    const submitBtn = screen.getByRole('button', { name: submitBtnText })

    expect(submitBtn).toBeDisabled()
  })

  it('should render asset title and thumbnail on left hand side of the modal when an asset is selected', () => {
    const { name, url } = defaultProps.options.assetOptions[0]
    render(<AssetModal {...defaultProps} />)

    fireEvent.click(screen.getByTestId('asset-0'))
    const title = document.querySelector('.modal_content .modal-title')
    const img = document.querySelector('.modal_content img')

    expect(title).toHaveTextContent(name)
    expect(img).toHaveAttribute('src', url)
  })

  it('should render asset details in View Image Detail', () => {
    const viewAssetTitle = defaultProps.supportedOperations[0].title
    const { name, description, altText } = defaultProps.options.assetOptions[0]
    render(<AssetModal {...defaultProps} />)

    fireEvent.click(screen.getByTestId('asset-0'))
    fireEvent.click(screen.getByText(viewAssetTitle))
    const modal = document.querySelector('.modal_content')
    const inputGroup = modal.querySelectorAll('input')
    const descriptionTextarea = modal.querySelector('textarea')

    expect(inputGroup[0]).toHaveValue(name)
    expect(inputGroup[1]).toHaveValue(altText)
    expect(descriptionTextarea).toHaveValue(description)
  })

  it('should strip all html elements from Description field', () => {
    const viewAssetTitle = defaultProps.supportedOperations[0].title
    render(<AssetModal {...defaultProps} />)

    fireEvent.click(screen.getByTestId('asset-1'))
    fireEvent.click(screen.getByText(viewAssetTitle))

    const modal = document.querySelector('.modal_content')
    const descriptionTextarea = modal.querySelector('textarea')
    expect(descriptionTextarea).toHaveValue(
      'My second description. My second line'
    )
  })

  it('should be able to select a label', () => {
    const onLabelSelect = defaultProps.supportedOperations[1].onLabelSelect
    const { getByText } = render(<AssetModal {...defaultProps} />)

    const labelsDropdown = getByText(/label/i)
    expect(labelsDropdown).toBeInTheDocument()

    fireEvent.click(labelsDropdown)

    defaultProps.options.labelOptions.forEach(labelName => {
      expect(getByText(labelName)).toBeInTheDocument()
    })

    const labelOne = getByText(defaultProps.options.labelOptions[0])
    fireEvent.click(labelOne)

    expect(onLabelSelect).toHaveBeenCalledTimes(1)
  })

  it('should not render label dropdown if none are provided', () => {
    const newProps = JSON.parse(JSON.stringify(defaultProps))
    newProps.options.labelOptions = []

    const { queryByText } = render(<AssetModal {...newProps} />)

    const labelsDropdown = queryByText(/label/i)
    expect(labelsDropdown).not.toBeInTheDocument()
  })

  it('should call fetchMoreAssets if it is intersection and there are more assets', () => {
    const mockedEntries = [
      {
        isIntersecting: true,
      },
    ]

    render(<AssetModal {...defaultProps} />)

    let entriesCallback = window.IntersectionObserver.mock.calls[0][0]
    entriesCallback(mockedEntries)

    expect(observe).toHaveBeenCalledTimes(1)
    expect(defaultProps.fetchMoreAssets).toHaveBeenCalledTimes(1)
  })

  it('should call not fetchMoreAssets if it is not intersection', () => {
    const mockedEntries = [
      {
        isIntersecting: false,
      },
    ]

    render(<AssetModal {...defaultProps} />)

    let entriesCallback = window.IntersectionObserver.mock.calls[0][0]
    entriesCallback(mockedEntries)

    expect(observe).toHaveBeenCalledTimes(1)
    expect(defaultProps.fetchMoreAssets).not.toHaveBeenCalled()
  })

  it('should show "View More" button if hasMoreSections is true', () => {
    const { getByText } = render(<AssetModal {...defaultProps} />)

    const viewMoreSectionsBtn = getByText('View More')
    expect(viewMoreSectionsBtn).toBeInTheDocument()

    fireEvent.click(viewMoreSectionsBtn)
    expect(defaultProps.fetchMoreSections).toHaveBeenCalledTimes(1)
  })

  it('should not show "View More" button if hasMoreSections is false', () => {
    const updatedProps = JSON.parse(JSON.stringify(defaultProps))
    updatedProps.hasMoreSections = false

    const { queryByText } = render(<AssetModal {...updatedProps} />)

    const viewMoreSectionsBtn = queryByText('View More')
    expect(viewMoreSectionsBtn).not.toBeInTheDocument()
  })
})
