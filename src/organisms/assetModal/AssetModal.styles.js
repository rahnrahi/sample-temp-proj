import styled from 'styled-components'
import { theme } from '../../shared'

export const StyledGrid = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 30% 1fr;
  height: 600px;
`

export const StyledMenuSection = styled.div`
  background: #737f8f22;

  .modal-title {
    ${theme.typography.subtitle2}
    padding: 2rem 0 2rem 2rem;
    margin: 0;
  }
`

export const StyledMenu = styled.div`
  margin: 1rem 0;
  padding: 1rem 3rem;
  font-size: 14px;
  cursor: pointer;
  border-left: 3px solid transparent;

  &.active {
    background: white;
    border-color: ${theme.palette.brand.primary.charcoal};
    font-weight: bold;
  }

  &:hover:not(.active) {
    background: ${theme.palette.ui.neutral.grey4};
  }
`

export const StyledCurrentAsset = styled.div`
  width: 100%;
  padding: 1rem 3rem;
  margin: 2rem 0;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    overflow: hidden;
    max-height: 200px;
    object-fit: scale-down;
    object-position: center;
  }
`

export const StyledContentSection = styled.div`
  display: grid;
  grid-template-rows: 1fr 60px;
  width: 100%;
  min-width: 740px;
`

export const StyledContent = styled.div`
  padding: 1rem 2rem 0 2rem;
  height: 540px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;

  .spinner {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
  }
`

export const StyledFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 2rem;
  box-shadow: 0px -1px 8px rgba(0, 0, 0, 0.1);
  flex: 0 0 auto;
  z-index: 3;

  .assetmodal-btn-cancel {
    border: none;
  }
`
