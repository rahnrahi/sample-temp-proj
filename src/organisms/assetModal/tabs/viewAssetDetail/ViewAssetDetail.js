import React from 'react'
import {
  StyledContainer,
  StyledRemoveAssetWrapper,
  StyledInputField,
} from './styles'
import PropTypes from 'prop-types'
import { Input, Textarea } from '../../../../atoms'
import { REMOVE_IMAGE_CTA } from '../../constants'
import { extractTextContent } from '../../utils'

export const ViewAssetDetail = ({ currentAsset, onRemoveAsset }) => {
  return (
    <StyledContainer>
      <StyledRemoveAssetWrapper>
        <div className='cta-remove-asset' onClick={onRemoveAsset}>
          {REMOVE_IMAGE_CTA}
        </div>
      </StyledRemoveAssetWrapper>
      <StyledInputField>
        <Input
          label='Asset Name'
          isFloatedLabel
          inputProps={{
            disabled: true,
            value: currentAsset?.name,
          }}
        />
      </StyledInputField>
      <StyledInputField>
        <Textarea
          label='Description'
          className='textarea-description'
          disabled
          textareaProps={{
            value: extractTextContent(currentAsset?.description),
            disabled: true,
          }}
        />
      </StyledInputField>
      <StyledInputField>
        <Input
          label='Alt-Text'
          isFloatedLabel
          inputProps={{
            disabled: true,
            value: currentAsset?.altText,
          }}
        />
      </StyledInputField>
    </StyledContainer>
  )
}

ViewAssetDetail.propTypes = {
  currentAsset: PropTypes.shape({
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    description: PropTypes.string,
    altText: PropTypes.string,
    tags: PropTypes.array,
  }),
  onRemoveAsset: PropTypes.func,
}
