import styled from 'styled-components'
import { theme } from '../../../../shared'

export const StyledContainer = styled.div`
  padding-top: 1rem;
  padding-right: 3rem;
`

export const StyledRemoveAssetWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  .cta-remove-asset {
    ${theme.typography.link}
    cursor: pointer;
    color: ${theme.palette.ui.cta.red};

    &:hover {
      opacity: 0.7;
    }
  }
`

export const StyledInputField = styled.div`
  margin: 2rem 0;

  .textarea-description textarea,
  input {
    font-size: ${theme.typography.link.fontSize};
    background-color: transparent;
  }

  .textarea-description > div.disabled span,
  .textarea-description > div.disabled textarea {
    color: ${theme.palette.ui.neutral.grey7};
    opacity: 1;
  }
`
