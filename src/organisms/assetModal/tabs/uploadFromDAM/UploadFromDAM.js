import React, { useCallback, useRef, useState } from 'react'
import { Chip, Loading } from '../../../../atoms'
import { Dropdown } from '../../../../molecules'
import TransparentBackground from '../../transparentBackground'

import {
  StyledAssetsContainer,
  StyledAsset,
  StyledAssetBody,
  StyledSection,
  StyledOptionsWrapper,
} from './styles'
import PropTypes from 'prop-types'
import { hasOptions } from './utils'
import {
  DEFAULT_EMPTY_ASSET,
  NO_ASSETS_MESSAGE,
  NO_ASSETS_WITH_FILTER_MESSAGE,
} from '../../constants'
import noop from 'lodash/noop'

const DEFAULT_LABEL = {
  id: -1,
  name: 'Label',
}

export const UploadFromDAM = ({
  options,
  isAssetsLoading,
  setSelectedAsset,
  onSectionSelect,
  onLabelSelect,
  hasMoreAssets,
  fetchMoreAssets,
  hasMoreSections,
  fetchMoreSections,
  setCurrentAsset,
  viewAssetOperation,
  setActiveMenu,
  setModalState,
}) => {
  const [selectedIndex, setSelectedIndex] = useState(null)
  const [selectedSection, setSelectedSection] = useState(null)
  const [selectedLabel, setSelectedLabel] = useState(DEFAULT_LABEL)

  const observer = useRef()
  const labelsDropdownRef = useRef()

  const lastAssetRef = useCallback(
    node => {
      if (observer.current) {
        observer.current.disconnect()
      }

      observer.current = new IntersectionObserver(
        entries => {
          if (entries[0].isIntersecting && hasMoreAssets) {
            fetchMoreAssets()
          }
        },
        { rootMargin: '100px' }
      )

      if (node) {
        observer.current.observe(node)
      }
    },

    [hasMoreAssets, fetchMoreAssets]
  )

  const handleOnAssetClick = (asset, assetIdx) => {
    if (selectedIndex === assetIdx) {
      setModalState(state => ({ ...state, selected: false }))
      setSelectedAsset(DEFAULT_EMPTY_ASSET)
      setSelectedIndex(null)
      setCurrentAsset(null)
      return
    }

    setModalState(state => ({
      ...state,
      selected: true,
    }))
    setSelectedAsset(asset)
    setCurrentAsset(asset)
    setSelectedIndex(assetIdx)
  }

  const handleOnAssetDoubleClick = asset => {
    setModalState(state => ({ ...state, doubleClicked: true }))
    setActiveMenu(viewAssetOperation)
    setSelectedAsset(asset)
    setCurrentAsset(asset)
  }

  const handleSectionClick = selected => {
    setSelectedSection(
      selected.label === selectedSection ? null : selected.label
    )
    onSectionSelect(selected)
  }

  const handleLabelChange = _label => {
    labelsDropdownRef.current.blur()
    onLabelSelect(_label.name)
    setSelectedLabel({
      ..._label,
      name: `Label: ${_label.name}`,
    })
  }

  const renderLabels = () => {
    if (hasOptions(options?.labelOptions)) {
      const constructedOptions = options.labelOptions.map(
        (label, labelIdx) => ({
          id: labelIdx,
          name: label,
        })
      )

      return (
        <Dropdown
          type='small'
          ref={labelsDropdownRef}
          value={selectedLabel}
          options={constructedOptions}
          onSelect={handleLabelChange}
        />
      )
    }

    return null
  }

  const renderAssets = () => {
    if (isAssetsLoading) {
      return <Loading className='spinner' />
    }

    if (!hasOptions(options?.assetOptions)) {
      const isFilterEnabled = selectedLabel.id !== -1 || selectedSection

      return (
        <p>
          {isFilterEnabled ? NO_ASSETS_WITH_FILTER_MESSAGE : NO_ASSETS_MESSAGE}
        </p>
      )
    }

    return (
      <StyledAssetsContainer data-testid='grid-asset'>
        {options?.assetOptions.map((asset, assetIdx) => (
          <StyledAsset
            key={asset.name}
            ref={
              options?.assetOptions.length === assetIdx + 1
                ? lastAssetRef
                : null
            }
            data-testid={`asset-${assetIdx}`}
            selected={assetIdx === selectedIndex}
            onClick={() => handleOnAssetClick(asset, assetIdx)}
            onDoubleClick={() => handleOnAssetDoubleClick(asset)}
          >
            <TransparentBackground height={195}>
              <img loading='lazy' src={asset.url} alt={asset.altText} />
            </TransparentBackground>
            <StyledAssetBody>
              <span>{asset.name}</span>
            </StyledAssetBody>
          </StyledAsset>
        ))}
      </StyledAssetsContainer>
    )
  }

  return (
    <>
      <StyledOptionsWrapper>{renderLabels()}</StyledOptionsWrapper>
      {hasOptions(options?.sectionOptions) && (
        <StyledSection>
          <span>Sections: </span>
          {options.sectionOptions.map(sectionLabel => (
            <Chip
              key={sectionLabel}
              showRemoveIcon={false}
              isSelectable
              isSelected={sectionLabel === selectedSection}
              disabled={false}
              onClick={handleSectionClick}
            >
              {sectionLabel}
            </Chip>
          ))}
          {hasMoreSections && (
            <span onClick={fetchMoreSections} className='sections-view-more'>
              View More
            </span>
          )}
        </StyledSection>
      )}
      {renderAssets()}
    </>
  )
}

UploadFromDAM.defaultProps = {
  options: {
    assetOptions: [
      {
        name: '',
      },
    ],
  },
  setSelectedAsset: noop,
  onSectionSelect: noop,
  onLabelSelect: noop,
  isAssetsLoading: false,
  hasMoreAssets: false,
  fetchMoreAssets: noop,
  hasMoreSections: false,
  fetchMoreSections: noop,
  setCurrentAsset: noop,
  viewAssetOperation: {},
  setActiveMenu: noop,
  setModalState: noop,
}

UploadFromDAM.propTypes = {
  options: PropTypes.shape({
    assetOptions: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
        altText: PropTypes.string,
        tags: PropTypes.array,
      })
    ).isRequired,
    collectionOptions: PropTypes.array,
    tagOptions: PropTypes.array,
    labelOptions: PropTypes.array,
    sectionOptions: PropTypes.array,
  }).isRequired,
  setSelectedAsset: PropTypes.func.isRequired,
  onSectionSelect: PropTypes.func.isRequired,
  onLabelSelect: PropTypes.func.isRequired,
  isAssetsLoading: PropTypes.bool.isRequired,
  hasMoreAssets: PropTypes.bool.isRequired,
  fetchMoreAssets: PropTypes.func.isRequired,
  hasMoreSections: PropTypes.bool.isRequired,
  fetchMoreSections: PropTypes.func.isRequired,
  setCurrentAsset: PropTypes.func.isRequired,
  viewAssetOperation: PropTypes.shape({
    title: PropTypes.string,
    operationType: PropTypes.string,
    selectedAsset: PropTypes.oneOf([{}, null]),
  }).isRequired,
  setActiveMenu: PropTypes.func.isRequired,
  setModalState: PropTypes.func.isRequired,
}
