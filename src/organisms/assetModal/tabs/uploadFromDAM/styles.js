import styled, { css } from 'styled-components'
import { theme } from '../../../../shared'

export const StyledSection = styled.div`
  ${theme.typography.caption}

  span {
    margin-right: 0.5rem;
  }

  button {
    margin-bottom: 0.5rem;
  }

  .sections-view-more {
    ${theme.typography.caption};
    color: ${theme.palette.brand.primary.charcoal};
    user-select: none;

    &:hover {
      cursor: pointer;
      color: ${theme.palette.brand.primary.gray};
    }
  }
`

export const StyledOptionsWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 0.5rem;

  & > div:focus .dropdown-header {
    box-shadow: inset 0 0 1px 1px ${theme.palette.ui.cta.yellow};
  }

  .dropdown-header {
    padding: 8px 16px;
    background: #ffffff;
    border-radius: 40px;
    ${theme.shadows.light.level1};

    label {
      ${theme.typography.link};
    }
  }
`

export const StyledAssetsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 2rem;
  align-items: flex-start;
  border: none;
  margin-top: 1rem;
  overflow-y: auto;
  padding-bottom: 1rem;
  flex: 0 1 auto;
`

export const StyledAsset = styled.div`
  background: rgba(255, 255, 255, 0.75);
  box-shadow: 0px 5px 16px rgba(115, 127, 143, 0.1);
  border-radius: 4px;
  overflow: auto;
  height: max-content;
  box-sizing: border-box;
  border: 1px solid transparent;
  cursor: pointer;

  &:hover {
    border: 1px solid ${theme.palette.ui.states.hover};
  }

  ${({ selected }) =>
    selected &&
    css`
      border: 2px solid ${theme.palette.ui.states.active};

      &:hover {
        border: 2px solid ${theme.palette.ui.states.active};
      }
    `}
`

export const StyledAssetBody = styled.div`
  ${theme.typography.caption};
  padding: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .asset-extension {
    font-size: 10px;
    line-height: 12px;
    padding: 3px 5px;
    width: 35px;
    height: 18px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: rgba(243, 243, 245, 0.82);
    border-radius: 8px;
  }
`
