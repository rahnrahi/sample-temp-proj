export const hasOptions = option => {
  return option && Array.isArray(option) && option.length > 0
}
