import { OPERATION_TYPES } from './constants'

const { UPLOAD_FROM_DAM_LIBRARY, VIEW_ASSET_DETAIL } = OPERATION_TYPES

/**
 * Returns an object that contains properties for Asset details and Upload library
 * @param {Object[]} supportedOperations
 * @returns {Object} set of properties for each Asset details and Upload library
 */
export const extractOperations = supportedOperations => {
  return supportedOperations.reduce((acc, cur) => {
    switch (cur.operationType) {
      case VIEW_ASSET_DETAIL:
        acc.viewAssetOperation = cur
        break
      case UPLOAD_FROM_DAM_LIBRARY:
        acc.uploadFromLibraryOperation = cur
        break
      default:
        break
    }

    return acc
  }, {})
}

/**
 * Returns boolean that controls disabled/enabled state of the submit button
 * @param {Object} { state: modal state, operation: type of operation }
 * @returns {boolean}
 */
export const isSubmitButtonDisabled = ({ state, operation }) => {
  const { selected, doubleClicked } = state

  switch (operation) {
    case VIEW_ASSET_DETAIL:
      return !selected && !doubleClicked
    case UPLOAD_FROM_DAM_LIBRARY:
      return !selected
    default:
      break
  }
}

/**
 * Strips rich text or HTML tags from string then returns only text content
 * First operation is removing all tags and replace with a space, then removes any multiple whitespaces and replaced
 * with a single one
 * @param {string}
 * @returns {string}
 */
export const extractTextContent = richTextContent => {
  return richTextContent
    ?.replace(/<[^>]*>?/g, ' ')
    .replace(/\s\s+/g, ' ')
    .trim()
}
