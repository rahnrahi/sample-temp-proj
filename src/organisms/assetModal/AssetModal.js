import Modal from '../modal/Modal'
import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import {
  DEFAULT_EMPTY_ASSET,
  NO_SELECTED_ASSET_MODAL_TITLE,
  OPERATION_TYPES,
} from './constants'
import {
  StyledGrid,
  StyledMenuSection,
  StyledContentSection,
  StyledMenu,
  StyledFooter,
  StyledContent,
  StyledCurrentAsset,
} from './AssetModal.styles'
import classnames from 'classnames'
import noop from 'lodash/noop'
import { Button } from '../../atoms'
import { UploadFromDAM } from './tabs/uploadFromDAM'
import { ViewAssetDetail } from './tabs/viewAssetDetail'
import { extractOperations, isSubmitButtonDisabled } from './utils'

const { UPLOAD_FROM_DAM_LIBRARY, VIEW_ASSET_DETAIL } = OPERATION_TYPES

export const AssetModal = ({
  supportedOperations,
  customTextLabels,
  options,
  onClose,
  onBackdropClick,
  isAssetsLoading,
  hasMoreAssets,
  fetchMoreAssets,
  hasMoreSections,
  fetchMoreSections,
}) => {
  const { cancelText, saveText } = customTextLabels
  const { viewAssetOperation, uploadFromLibraryOperation } = extractOperations(
    supportedOperations
  )
  const [currentAsset, setCurrentAsset] = useState(
    viewAssetOperation?.selectedAsset
  )
  const [activeMenu, setActiveMenu] = useState(supportedOperations[0])
  const [selectedAsset, setSelectedAsset] = useState(DEFAULT_EMPTY_ASSET)
  const [modalState, setModalState] = useState({
    selected: false,
    doubleClicked: false,
  })

  useEffect(() => {
    if (!viewAssetOperation?.selectedAsset) {
      setActiveMenu(uploadFromLibraryOperation)
    }
  }, [viewAssetOperation, uploadFromLibraryOperation])

  const handleOnSubmit = () => {
    activeMenu.onSubmit(selectedAsset)
  }

  const handleRemoveAsset = () => {
    setCurrentAsset(null)
    setSelectedAsset(DEFAULT_EMPTY_ASSET)
    setActiveMenu(uploadFromLibraryOperation)
    setModalState(state => ({
      ...state,
      selected: true,
    }))
  }

  const modalSections = {
    [UPLOAD_FROM_DAM_LIBRARY]: {
      render: () => {
        return (
          <UploadFromDAM
            setCurrentAsset={setCurrentAsset}
            viewAssetOperation={viewAssetOperation}
            setActiveMenu={setActiveMenu}
            options={options}
            isAssetsLoading={isAssetsLoading}
            setSelectedAsset={setSelectedAsset}
            onSectionSelect={selectedSection =>
              activeMenu.onSectionSelect(selectedSection)
            }
            onLabelSelect={selectedLabel =>
              activeMenu.onLabelSelect(selectedLabel)
            }
            hasMoreAssets={hasMoreAssets}
            fetchMoreAssets={fetchMoreAssets}
            hasMoreSections={hasMoreSections}
            fetchMoreSections={fetchMoreSections}
            setModalState={setModalState}
          />
        )
      },
    },
    [VIEW_ASSET_DETAIL]: {
      render: () => (
        <ViewAssetDetail
          currentAsset={currentAsset}
          onRemoveAsset={handleRemoveAsset}
        />
      ),
    },
  }

  const renderModalContent = () => {
    return (
      <StyledGrid>
        <StyledMenuSection>
          <p className='modal-title'>
            {currentAsset?.name ?? NO_SELECTED_ASSET_MODAL_TITLE}
          </p>
          {supportedOperations.map(operation => {
            const { title, operationType } = operation

            // filters unsupported operations
            if (!OPERATION_TYPES[operationType]) {
              return null
            }

            // checks if an asset is provided for viewing
            if (operationType === VIEW_ASSET_DETAIL && !currentAsset) {
              return null
            }

            return (
              <StyledMenu
                key={operationType}
                className={classnames({
                  active: activeMenu.operationType === operationType,
                })}
                onClick={() => {
                  if (operationType === VIEW_ASSET_DETAIL) {
                    setSelectedAsset(currentAsset)
                  } else {
                    setModalState(state => ({ ...state, selected: false }))
                    setSelectedAsset(DEFAULT_EMPTY_ASSET)
                  }
                  setActiveMenu(operation)
                }}
              >
                {title}
              </StyledMenu>
            )
          })}
          {currentAsset && (
            <StyledCurrentAsset data-testid='selected-asset-preview'>
              <img src={currentAsset?.url} alt={currentAsset?.altText} />
            </StyledCurrentAsset>
          )}
        </StyledMenuSection>
        <StyledContentSection>
          <StyledContent>
            {modalSections[activeMenu.operationType].render()}
          </StyledContent>
          <StyledFooter>
            <Button
              className='assetmodal-btn-cancel'
              data-testid='cancel-button'
              isPrimary={false}
              text={cancelText}
              size='small'
              onClick={onClose}
            />
            <Button
              data-testid='save-asset-button'
              text={saveText}
              size='small'
              onClick={handleOnSubmit}
              disabled={isSubmitButtonDisabled({
                state: modalState,
                operation: activeMenu.operationType,
              })}
            />
          </StyledFooter>
        </StyledContentSection>
      </StyledGrid>
    )
  }

  return (
    <Modal
      footerButtons={[]}
      headerButtons={[]}
      onBackdropClick={onBackdropClick}
      onClose={onClose}
      render={renderModalContent}
      size='large'
      padding='0px'
      showCloseButton={false}
      headerText={null}
    />
  )
}

AssetModal.defaultProps = {
  supportedOperations: [
    {
      title: '',
      operationType: VIEW_ASSET_DETAIL,
      selectedAsset: {
        name: '',
        url: '',
      },
    },
  ],
  customTextLabels: {
    cancelText: 'Cancel',
    saveText: 'Save',
  },
  options: {
    assetOptions: [
      {
        name: '',
        url: '',
      },
    ],
  },
  onClose: noop,
  isAssetsLoading: false,
  hasMoreAssets: false,
}

AssetModal.propTypes = {
  supportedOperations: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      operationType: PropTypes.oneOf(Object.keys(OPERATION_TYPES)).isRequired,
      selectedAsset: PropTypes.shape({
        name: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        description: PropTypes.string,
        altText: PropTypes.string,
        tags: PropTypes.array,
      }),
      onSubmit: PropTypes.func,
      onSectionSelect: PropTypes.func,
      onLabelSelect: PropTypes.func,
    })
  ).isRequired,
  customTextLabels: PropTypes.shape({
    cancelText: PropTypes.string,
    saveText: PropTypes.string,
  }),
  options: PropTypes.shape({
    assetOptions: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        description: PropTypes.string,
        altText: PropTypes.string,
        tags: PropTypes.array,
      })
    ).isRequired,
    collectionOptions: PropTypes.array,
    tagOptions: PropTypes.array,
    labelOptions: PropTypes.array,
    sectionOptions: PropTypes.array,
  }).isRequired,
  onClose: PropTypes.func.isRequired,
  onBackdropClick: PropTypes.func,
  isAssetsLoading: PropTypes.bool,
  hasMoreAssets: PropTypes.bool,
  hasMoreSections: PropTypes.bool,
  fetchMoreAssets: PropTypes.func,
  fetchMoreSections: PropTypes.func,
}
