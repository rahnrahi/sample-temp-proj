export const OPERATION_TYPES = {
  VIEW_ASSET_DETAIL: 'VIEW_ASSET_DETAIL',
  UPLOAD_FROM_DAM_LIBRARY: 'UPLOAD_FROM_DAM_LIBRARY',
}

export const NO_SELECTED_ASSET_MODAL_TITLE = 'No Image'
export const REMOVE_IMAGE_CTA = 'Remove Image'

export const ASSETS_PER_OFFSET = 20

export const DEFAULT_EMPTY_ASSET = {
  name: '',
  url: '',
  description: '',
  altText: '',
  tags: [],
}

export const NO_ASSETS_MESSAGE = 'No assets found'
export const NO_ASSETS_WITH_FILTER_MESSAGE = `No results. Refine your filters and try again`
