import styled from 'styled-components'
import { theme } from '../../../shared'

export const StyledTransparentBackground = styled.div`
  background: linear-gradient(
      45deg,
      ${theme.palette.ui.neutral.grey4} 25%,
      transparent 25%,
      transparent 75%,
      ${theme.palette.ui.neutral.grey4} 75%
    ),
    linear-gradient(
      45deg,
      ${theme.palette.ui.neutral.grey4} 25%,
      transparent 25%,
      transparent 75%,
      ${theme.palette.ui.neutral.grey4} 75%
    );
  background-color: ${theme.palette.ui.neutral.grey3};
  background-size: 20px 20px;
  background-position: 0 0, 10px 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: ${props => props.height}px;

  img {
    display: block;
    width: 100%;
    height: 180px;
    object-fit: scale-down;
    object-position: center;
  }
`
