import React from 'react'
import PropTypes from 'prop-types'
import { StyledTransparentBackground } from './styles'

export const TransparentBackground = ({ children, height }) => {
  return (
    <StyledTransparentBackground height={height}>
      {children}
    </StyledTransparentBackground>
  )
}

TransparentBackground.defaultProps = {
  height: 200,
}

TransparentBackground.propTypes = {
  height: PropTypes.number.isRequired,
  children: PropTypes.node,
}
