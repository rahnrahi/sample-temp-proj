import { OPERATION_TYPES } from './constants'

export const mockSelectedAsset = Object.freeze({
  name: `IMG-101`,
  description: 'Image description',
  altText: 'Alt Text',
  tags: [],
  url: 'https://example1.com/image.jpg',
})

export const defaultProps = {
  supportedOperations: [
    {
      title: 'View Image Detail',
      operationType: OPERATION_TYPES.VIEW_ASSET_DETAIL,
      selectedAsset: null,
      onSubmit: jest.fn(),
    },
    {
      title: 'Upload from DAM library',
      operationType: OPERATION_TYPES.UPLOAD_FROM_DAM_LIBRARY,
      onSubmit: jest.fn(),
      onSectionSelect: jest.fn(),
      onLabelSelect: jest.fn(),
    },
  ],
  customTextLabels: {
    cancelText: 'Cancel',
    saveText: 'Save',
  },
  options: {
    assetOptions: [
      {
        name: `IMG-1292`,
        description: 'My description',
        altText: 'Alt Text',
        tags: [],
        url: 'https://example1.com/ex1.jpg',
      },
      {
        name: `IMG-2048`,
        description: '<p>My second description.</p><p>My second line</p>',
        altText: 'Alt Text',
        tags: [],
        url: 'https://example2.com/ex2.jpg',
      },
    ],
    sectionOptions: ['All', 'Media'],
    labelOptions: ['Digital dreams', 'Amazon forest'],
  },
  onClose: jest.fn(),
  onBackdropClick: jest.fn(),
  fetchMoreAssets: jest.fn(),
  fetchMoreSections: jest.fn(),
  hasMoreAssets: true,
  hasMoreSections: true,
  isAssetsLoading: false,
}
