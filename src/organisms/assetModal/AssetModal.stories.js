import { AssetModal } from './AssetModal'
import React from 'react'
import { OPERATION_TYPES } from './constants'
import DAMModalDocs from '../../../docs/DAMModal.mdx'
import {
  DAM_MODEL_DESIGN,
  DAM_MODEL_PRESENTATION,
  DESIGN_TAB_PRESENTATION,
  DESIGN_TAB_MOCKUP,
} from '../../hooks/constants'

const DEFAULT_NUM_ASSETS = 3

const design = [
  {
    name: DESIGN_TAB_MOCKUP,
    type: 'figma',
    url: DAM_MODEL_DESIGN,
    allowFullscreen: true,
  },
  {
    name: DESIGN_TAB_PRESENTATION,
    type: 'figma',
    url: DAM_MODEL_PRESENTATION,
    allowFullscreen: true,
  },
]

const constructAssetOptions = (numOptions = DEFAULT_NUM_ASSETS) => {
  return new Array(numOptions).fill().map((_, idx) => ({
    name: `IMG-${idx}`,
    description: 'Image description',
    altText: 'Alt Text',
    tags: [],
    url: 'https://mma.prnewswire.com/media/1315829/Fabric_Logo.jpg',
  }))
}

const defaultArgs = {
  supportedOperations: [
    {
      title: 'View Image Detail',
      operationType: OPERATION_TYPES.VIEW_ASSET_DETAIL,
      selectedAsset: null,
    },
    {
      title: 'Upload from Brandfolder Library',
      operationType: OPERATION_TYPES.UPLOAD_FROM_DAM_LIBRARY,
      onSubmit: asset => console.log(asset),
      onSectionSelect: section => console.log(section),
      onLabelSelect: label => console.log(label),
    },
  ],
  customTextLabels: {
    cancelText: 'Cancel',
    saveText: 'Save',
  },
  options: {
    assetOptions: constructAssetOptions(),
  },
  isAssetsLoading: false,
}

export default {
  title: 'Templates/DAM Modal',
  component: AssetModal,
  argTypes: {
    onClose: { action: 'onClose' },
    onBackdropClick: { action: 'onBackdropClick' },
  },
  parameters: {
    docs: {
      page: DAMModalDocs,
    },
  },
}

const Template = args => <AssetModal {...args} />

export const AssetModalDefault = Template.bind({})
AssetModalDefault.args = defaultArgs
AssetModalDefault.storyName = 'Upload from DAM Default'
AssetModalDefault.parameters = { design }

export const AssetModalWithOptions = Template.bind({})
AssetModalWithOptions.args = {
  ...defaultArgs,
  options: {
    ...defaultArgs.options,
    sectionOptions: ['All', 'Leisure', 'Business'],
    labelOptions: ['Seasonal Ads', 'Winter Wonderland', 'Forest Trees'],
    assetOptions: constructAssetOptions(10),
  },
  hasMoreAssets: true,
  hasMoreSections: true,
  fetchMoreAssets: () => console.log('Fetching More Assets'),
  fetchMoreSections: () => console.log('Fetching more sections'),
}
AssetModalWithOptions.storyName = 'Upload from DAM with options'
AssetModalWithOptions.parameters = { design }

export const AssetModalWithViewMenu = Template.bind({})
AssetModalWithViewMenu.args = {
  ...defaultArgs,
  supportedOperations: [
    {
      title: 'View Image Detail',
      operationType: OPERATION_TYPES.VIEW_ASSET_DETAIL,
      selectedAsset: {
        name: `IMG-101`,
        description: `<p>Lorem ipsum dolor sit amet <strong>consectetur</strong> adipisicing elit.</p><p>Quos perferendis temporibus quod? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, magnam! Lorem, ipsum.</p>`,
        altText: 'Alt Text',
        tags: [],
        url: `https://resources.fabric.inc/hubfs/Blog%20Header%20-%20Fabric%20Logo.png`,
      },
      onSubmit: asset => console.log(asset),
    },
    {
      title: 'Upload from Brandfolder Library',
      operationType: OPERATION_TYPES.UPLOAD_FROM_DAM_LIBRARY,
      onSubmit: asset => console.log(asset),
      onSectionSelect: section => console.log(section),
    },
  ],
}
AssetModalWithViewMenu.storyName = 'View Asset Detail Default'
AssetModalWithViewMenu.parameters = { design }
